#include <functional>
#include <queue>
#include <stack>
#include <utils/context_utils.hpp>
#include <utils/log.hpp>

void print_tree(com::ContextItem const *root, std::ostream &out)
{
	ASSUMPTION(root);
	static const std::string item_name_prefix  = "|-- ";
	static const std::string skip_level_prefix = "    ";
	static const std::string same_level_prefix = "|   ";

	std::stack<std::string> prefixes({""});
	std::string name_prefix = "";

	auto fn = [&out, &prefixes, &name_prefix](com::ContextItem const *item)
	{
		std::string top_prefix = prefixes.top();
		bool is_folder = item->is_folder();

		out << top_prefix << name_prefix << item->name() << (is_folder ? "/" : "") << std::endl;

		com::Folder *parent = item->folder();
		
		if (parent == nullptr)
		{
			name_prefix = item_name_prefix;
			return false;
		}

		bool is_last_child = parent->items().back() == item;

		if (is_last_child)
		{
			prefixes.pop();
		}
		
		if (is_folder)
		{
			com::Folder const * folder = dynamic_cast<com::Folder const *>(item);
			if (folder->items().empty() == false)
			{
				prefixes.push(top_prefix + (is_last_child ? skip_level_prefix : same_level_prefix));
			}
		}

		return false;
	};

	foreach_const_tree::dfs(root, fn);
}

com::ContextPath find_relative_path(com::ContextItem const *from, com::ContextItem const *to)
{
	com::ContextPath link_path = from->path();
	com::ContextPath target_path = to->path();

	com::ContextPath relative_path = { "." };

	size_t index = 0; //  Index of first different item
	const size_t size = link_path.size()  < target_path.size() ? link_path.size() : target_path.size();

	while(index < size - 1)
	{
		if (link_path[index] != target_path[index])
		{
			break;
		}

		index++;
	}

	for (size_t i = index; i < link_path.size() - 1; i++)
	{
		relative_path.push_back("..");
	}

	for (size_t i = index; i < target_path.size(); i++)
	{
		relative_path.push_back(target_path[i]);
	}

	return relative_path;
}