#include <utils/random.hpp>
#include <utils/assumptions.hpp>

namespace utils {
    scalar random(scalar const min, scalar const max) {
        ASSUMPTION(min < max);
        thread_local std::mt19937 engine{ std::random_device{}() };
        std::uniform_real_distribution dist(min, max);
        return dist(engine);
    }

    int random_int(int const min, int const max) {
        ASSUMPTION(min < max);
        thread_local std::mt19937 engine{ std::random_device{}() };
        std::uniform_int_distribution dist(min, max);
        return dist(engine);
    }
}
