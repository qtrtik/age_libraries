#ifndef STD_PAIR_HASH_HPP_INCLUDED
#   define STD_PAIR_HASH_HPP_INCLUDED

#   include <functional>
#   include <boost/container_hash/hash.hpp>
#   include <math/math.hpp>

// std::hash overrides
namespace std {
    template<typename S, typename T>
    struct hash<pair<S, T>> {
        inline size_t operator()(const pair<S, T>& v) const {
            size_t seed = 0;
            boost::hash_combine(seed, boost::hash_value(v.first));
            boost::hash_combine(seed, boost::hash_value(v.second));
            return seed;
        }
    };
}

// boost::hash_value overrides
namespace boost {
    inline std::size_t hash_value(const vec2& vec) {
        std::size_t seed = 0;
        hash_combine(seed, hash_value(vec.x));
        hash_combine(seed, hash_value(vec.y));
        return seed;
    }

    inline std::size_t hash_value(const vec3& vec) {
        std::size_t seed = 0;
        hash_combine(seed, hash_value(vec.x));
        hash_combine(seed, hash_value(vec.y));
        hash_combine(seed, hash_value(vec.z));
        return seed;
    }

    inline std::size_t hash_value(const vec4& vec) {
        std::size_t seed = 0;
        hash_combine(seed, hash_value(vec.x));
        hash_combine(seed, hash_value(vec.y));
        hash_combine(seed, hash_value(vec.z));
        hash_combine(seed, hash_value(vec.w));
        return seed;
    }
}

#   endif
