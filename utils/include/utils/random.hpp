#ifndef UTILS_RANDOM_HPP_INCLUDED
#define UTILS_RANDOM_HPP_INCLUDED

#include <random>

#include "math/math_scalar_type.hpp"

namespace utils {
    scalar random(scalar min, scalar max);
    int random_int(int min, int max);
}

#endif // UTILS_RANDOM_HPP_INCLUDED
