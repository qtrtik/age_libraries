#ifndef UTILS_CONTEXT_UTILS_HPP
#define UTILS_CONTEXT_UTILS_HPP
#include <com/context.hpp>
#include <deque>
#include <iosfwd>
// /**
//  * \brief Formatted print of the disk tree structure.
//  * \param item root of the output tree
//  * \param out output stream to print the tree to
//  */
// void print_tree(const com::ContextItem &item, std::ostream &out);

void print_tree(com::ContextItem const *root, std::ostream &out);
com::ContextPath find_relative_path(com::ContextItem const *from, com::ContextItem const *to);

template <class T, class F, class L> class ContextUtilsForeach
{
      private:
	static void traverse(T *root, std::function<bool(T *item)> const &fun, bool follow_links, bool is_dfs)
	{
		ASSUMPTION(root);
		std::deque<T *> deq;
		std::unordered_set<T *> visited_items;
		deq.push_back(root);

		while (!deq.empty())
		{
			T *item = is_dfs ? deq.back() : deq.front();
			visited_items.insert(item);

			if (is_dfs)
			{
				deq.pop_back();
			}
			else
			{
				deq.pop_front();
			}

			if (fun(item))
			{
				break;
			}

			if (item->is_folder())
			{
				F *folder = dynamic_cast<F *>(item);
				auto push_back_fn = [&visited_items, &deq](T* item)
				{
					if (visited_items.contains(item) == false)
					{
						deq.push_back(item);
					}
				};

				if (is_dfs)
				{
					for (auto it = folder->items().rbegin(); it != folder->items().rend(); ++it)
					{
						push_back_fn(*it);
					}
				}
				else
				{
					for (auto it = folder->items().begin(); it != folder->items().end(); ++it)
					{
						push_back_fn(*it);
					}
				}
			}

			if (follow_links && item->is_link())
			{
				L *link = dynamic_cast<L *>(item);
				T *target = link->target();

				if (!target)
				{
					LOG(logging_severity_level::LSL_WARNING,
					    "Link " + link->name() +
						" has no target, ignoring link");
					continue;
				}

				if (visited_items.contains(target) == false)
				{
					deq.push_back(target);
				}
			}
		}
	}

      public:
	static void dfs(T *root, std::function<bool(T *item)> const &fun, bool follow_links = false)
	{
		traverse(root, fun, follow_links, true);
	}

	static void bfs(T *root, std::function<bool(T *item)> const &fun, bool follow_links = false)
	{
		traverse(root, fun, follow_links, false);
	}
};

typedef ContextUtilsForeach<com::ContextItem, com::Folder, com::Link> foreach_tree;
typedef ContextUtilsForeach<const com::ContextItem, const com::Folder, const com::Link>
    foreach_const_tree;

#endif
