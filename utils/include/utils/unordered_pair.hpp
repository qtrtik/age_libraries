#ifndef UTILITY_UNORDERED_PAIR_HPP_INCLUDED
#    define UTILITY_UNORDERED_PAIR_HPP_INCLUDED

#include <functional>
#include <boost/functional/hash.hpp>
#include <type_traits>

namespace utils {
    template <typename T>
    requires std::equality_comparable<T> && requires(T a) { std::hash<T>{}(a); }
    struct unordered_pair {
        unordered_pair() = delete;
        unordered_pair(const T& a, const T& b) : m_first(a), m_second(b) {}

        [[nodiscard]] T& first() { return m_first; }
        [[nodiscard]] const T& first() const { return m_first; }

        [[nodiscard]] T& second() { return m_second; }
        [[nodiscard]] const T& second() const { return m_second; }

        [[nodiscard]] bool operator==(const unordered_pair& other) const {
            return (m_first == other.m_first && m_second == other.m_second) ||
                   (m_first == other.m_second && m_second == other.m_first);
        }

        [[nodiscard]] bool operator!=(const unordered_pair& other) const {
            return !(*this == other);
        }

        [[nodiscard]] bool contains(const T& value) const {
            return m_first == value || m_second == value;
        }

    private:
        T m_first;
        T m_second;

    public:
        struct hash {
            std::size_t operator()(const unordered_pair& pair) const {
                auto h1 = std::hash<T>{}(pair.first());
                auto h2 = std::hash<T>{}(pair.second());
                if (h1 < h2) {
                    boost::hash_combine(h1, h2);
                    return h1;
                } else {
                    boost::hash_combine(h2, h1);
                    return h2;
                }
            }
        };
    };
}

namespace std {
    template <typename T>
    struct hash<utils::unordered_pair<T>> {
        std::size_t operator()(const utils::unordered_pair<T>& pair) const {
            return utils::unordered_pair<T>::hash(pair);
        }
    };
}

#    endif //UTILITY_UNORDERED_PAIR_HPP_INCLUDED
