set(THIS_TARGET_NAME osi)

file(GLOB "${THIS_TARGET_NAME}_HPP" "./include/${THIS_TARGET_NAME}/*.hpp")
file(GLOB "${THIS_TARGET_NAME}_CPP" "./src/*.cpp")

add_library(${THIS_TARGET_NAME}
    "${${THIS_TARGET_NAME}_HPP}"
    "${${THIS_TARGET_NAME}_CPP}"
    )
