#ifndef OSI_WINDOW_HPP_INCLUDED
#   define OSI_WINDOW_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>

namespace osi {

struct Config;
void run(Config const&);

struct Window final : public com::File
{
    static inline std::string self_name() { return "window"; }

    Window(vec2u const& size_)
        : com::File{ self_name() }
        , m_size { size_ }
        , m_is_resized { false }
        , m_is_minimized { false }
        , m_has_keyboard_focus { true }
        , m_is_mouse_in_window { true }
        , m_pixel_size{ 0.345 / 1920.0, 0.195 / 1080.0 }
    {}
    ~Window() override {}

    vec2u const& size() const { return m_size; }
    bool is_resized() const { return m_is_resized; }
    bool is_minimized() const { return m_is_minimized; }
    bool has_keyboard_focus() const { return m_has_keyboard_focus; }
    bool is_mouse_in_window() const { return m_is_mouse_in_window; }

    // According to my investigation SDL2 does not provide this information.
    // Therefore, this method returns the same constant size for all displays and resolutions.
    vec2 const& pixel_size_in_meters() const { return m_pixel_size; }

    vec2 to_absolute_pixels(vec2 const& relative_coords_in_range_01) const
    { return vec2{ size() } * relative_coords_in_range_01; }

    vec2 to_absolute_meters(vec2 const& relative_coords_in_range_01) const
    { return to_absolute_pixels(relative_coords_in_range_01) * pixel_size_in_meters(); }

    static void set_title(const std::string &title);

    void set_pixel_size(vec2 new_pixel_size) { m_pixel_size = new_pixel_size; }

private:
    friend void ::osi::run(::osi::Config const&);
    vec2u m_size;
    bool m_is_resized;
    bool m_is_minimized;
    bool m_has_keyboard_focus;
    bool m_is_mouse_in_window;
    vec2 m_pixel_size;
};

}

#endif
