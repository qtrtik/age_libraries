#ifndef OSI_INDEX_HPP_INCLUDED
#   define OSI_INDEX_HPP_INCLUDED

#   include <osi/window.hpp>
#   include <osi/keyboard.hpp>
#   include <osi/mouse.hpp>
#   include <osi/timer.hpp>
#   include <osi/termination.hpp>
#   include <osi/data.hpp>
#   include <com/context.hpp>

namespace osi {

struct Index final
{
    static Index const& instance();

    com::Folder* runners() const { return m_runners; }
    com::Folder* updaters() const { return m_updaters; }
    com::Folder* presenters() const { return m_presenters; }
    Window const* window() const { return m_window; }
    Keyboard const* keyboard() const { return m_keyboard; }
    Mouse const* mouse() const { return m_mouse; }
    Timer const* timer() const { return m_timer; }
    Termination* termination() const { return m_termination; }
    Data const* data() const { return m_data; }

private:
    Index();
    Index(Index const&) = delete;
    Index(Index&&) = delete;
    Index& operator=(Index const&) const = delete;
    Index& operator=(Index&&) const = delete;

    com::Folder* m_runners;
    com::Folder* m_updaters;
    com::Folder* m_presenters;
    Window* m_window;
    Keyboard* m_keyboard;
    Mouse* m_mouse;
    Timer* m_timer;
    Termination* m_termination;
    Data* m_data;
};

inline Index const& index() { return Index::instance(); }
inline com::Folder* runners() { return index().runners(); }
inline com::Folder* updaters() { return index().updaters(); }
inline com::Folder* presenters() { return index().presenters(); }
inline Window const* window() { return index().window(); }
inline Keyboard const* keyboard() { return index().keyboard(); }
inline Mouse const* mouse() { return index().mouse(); }
inline Timer const* timer() { return index().timer(); }
inline Termination* termination() { return index().termination(); }
inline Data const* data() { return index().data(); }

}

#endif
