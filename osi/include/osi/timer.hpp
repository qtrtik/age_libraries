#ifndef OSI_TIMER_HPP_INCLUDED
#   define OSI_TIMER_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>

namespace osi {

struct Config;
void run(Config const&);

struct Timer final : public com::File
{
    static inline std::string self_name() { return "timer"; }

    Timer()
        : com::File{ self_name() }
        , m_passed_rounds { 0ULL }
        , m_passed_seconds { 0.0 }
        , m_time_step { 0.0f }
        , m_paused { false }
    {}
    ~Timer() override {}

    std::uint64_t passed_rounds() const { return m_passed_rounds; }
    double passed_seconds() const { return m_passed_seconds; }
    scalar dt() const { return m_time_step; }
    bool paused() const { return m_paused; }

    void set_paused(bool const state) { m_paused = state; }

private:
    friend void ::osi::run(::osi::Config const&);
    std::uint64_t m_passed_rounds;
    double m_passed_seconds;
    scalar m_time_step;
    bool m_paused;
};

}

#endif
