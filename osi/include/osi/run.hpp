#ifndef OSI_RUN_HPP_INCLUDED
#   define OSI_RUN_HPP_INCLUDED

#   include <osi/module.hpp>
#   include <com/module.hpp>
#   include <gfx/module.hpp>
#   include <lfs/module.hpp>
#   include <script/module.hpp>
#   include <com/context.hpp>
#   include <math/math.hpp>
#   include <string>
#   include <vector>
#   include <functional>
#   include <filesystem>

namespace osi {

struct Config final
{
    std::filesystem::path data_root_dir { "./data" };

    std::string window_title { "age" };
    vec2u window_pos { 0U, 0U };
    vec2u window_size { 1024U, 768U };
    bool resizable { true }; // Used only if 'windowed' is true.
    bool windowed { true }; // When set to false, then 'window_size' is ignored and the desktop size is used instead.
    bool v_sync { false };

    com::ContextPath icon_relative_path { "age", "icon", "age_icon.png" };

    scalar max_dt { 1.0f / 10.0f };
    scalar min_dt { 1.0f / 100.0f };
    std::uint32_t cpu_yield_ms { 1U };
    std::uint8_t num_warm_up_iterations { 10U };

    struct BootAndShutdown
    {
        std::function<void(com::Folder* ctx_root)> boot;
        std::function<void(com::Folder* ctx_root)> shutdown;
    };

    std::vector<BootAndShutdown> core_modules{
        { [this](com::Folder* const ctx_root) { osi::boot(ctx_root, *this); }, osi::shutdown },
        { lfs::boot, lfs::shutdown },
        { com::boot, com::shutdown },
        { gfx::boot, gfx::shutdown },
        { scr::boot, scr::shutdown },
        };
    std::vector<BootAndShutdown> user_modules{};
};

void run(Config const&);

}

#endif
