#ifndef OSI_DATA_HPP_INCLUDED
#   define OSI_DATA_HPP_INCLUDED

#   include <com/context.hpp>
#   include <filesystem>

namespace osi {

struct Data final : public com::File
{
    static inline std::string self_name() { return "data"; }

    Data(std::filesystem::path const& dir_)
        : com::File{ self_name() }
        , m_dir { dir_ }
    {}
    ~Data() override {}

    std::filesystem::path const& dir() const { return m_dir; }

private:
    std::filesystem::path m_dir;
};

}

#endif
