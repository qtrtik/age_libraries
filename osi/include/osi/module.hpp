#ifndef OSI_MODULE_HPP_INCLUDED
#   define OSI_MODULE_HPP_INCLUDED

namespace com { struct Folder; }

namespace osi {

struct Config;

void boot(com::Folder* ctx_root, Config const& cfg);
void shutdown(com::Folder* ctx_root);

}

#endif
