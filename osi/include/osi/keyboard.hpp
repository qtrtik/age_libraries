#ifndef OSI_KEYBOARD_HPP_INCLUDED
#   define OSI_KEYBOARD_HPP_INCLUDED

#   include <com/context.hpp>
#   include <string>
#   include <unordered_set>

namespace osi {

struct Config;
void run(Config const&);

struct Keyboard final : public com::File
{
    static inline std::string self_name() { return "keyboard"; }

    Keyboard()
        : com::File{ self_name() }
        , m_just_pressed {}
        , m_just_released {}
        , m_down {}
        , m_text {}
    {}
    ~Keyboard() override {}

    std::unordered_set<std::string> const& just_pressed() const { return m_just_pressed; }
    std::unordered_set<std::string> const& just_released() const { return m_just_released; }
    std::unordered_set<std::string> const& down() const { return m_down; }
    std::string const& text() const { return m_text; }

private:
    friend void ::osi::run(::osi::Config const&);
    std::unordered_set<std::string> m_just_pressed;
    std::unordered_set<std::string> m_just_released;
    std::unordered_set<std::string> m_down;
    std::string m_text;
};

}

#endif
