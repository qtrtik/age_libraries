#ifndef OSI_TERMINATION_HPP_INCLUDED
#   define OSI_TERMINATION_HPP_INCLUDED

#   include <com/context.hpp>
#   include <string>

namespace osi {

struct Termination final : public com::File
{
    static inline std::string self_name() { return "termination"; }

    Termination()
        : com::File{ self_name() }
        , m_quit_flag { false }
    {}
    ~Termination() override {}

    bool quit_flag() const { return m_quit_flag; }
    void set_quit_flag() { m_quit_flag = true; }

private:
    bool m_quit_flag;
};

}

#endif
