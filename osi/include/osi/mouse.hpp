#ifndef OSI_MOUSE_HPP_INCLUDED
#   define OSI_MOUSE_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>
#   include <string>
#   include <unordered_set>

namespace osi {

struct Config;
void run(Config const&);

struct Mouse final : public com::File
{
    static inline std::string self_name() { return "mouse"; }

    Mouse()
        : com::File{ self_name() }
        , m_pos { 0, 0 }
        , m_pos_delta { 0, 0 }
        , m_just_pressed {}
        , m_just_released {}
        , m_down {}
    {}
    ~Mouse() override {}

    vec2i const& pos() const { return m_pos; }
    vec2i const& pos_delta() const { return m_pos_delta; }
    std::unordered_set<std::string> const& just_pressed() const { return m_just_pressed; }
    std::unordered_set<std::string> const& just_released() const { return m_just_released; }
    std::unordered_set<std::string> const& down() const { return m_down; }

private:
    friend void ::osi::run(::osi::Config const&);
    vec2i m_pos;
    vec2i m_pos_delta;
    std::unordered_set<std::string> m_just_pressed;
    std::unordered_set<std::string> m_just_released;
    std::unordered_set<std::string> m_down;
};

}

#endif
