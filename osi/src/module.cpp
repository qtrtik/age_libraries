#include <osi/module.hpp>
#include <osi/index.hpp>
#include <osi/window.hpp>
#include <osi/keyboard.hpp>
#include <osi/mouse.hpp>
#include <osi/timer.hpp>
#include <osi/termination.hpp>
#include <osi/data.hpp>
#include <osi/run.hpp>
#include <com/context.hpp>

namespace osi {

void boot(com::Folder* const ctx_root, Config const& cfg)
{
    com::Folder* const root_osi = ctx_root->push_back<com::Folder>("osi");
    root_osi->push_back<Termination>();
    root_osi->push_back<Window>(cfg.window_size);
    root_osi->push_back<Keyboard>();
    root_osi->push_back<Mouse>();
    root_osi->push_back<Timer>();
    auto const* data_ptr = root_osi->push_back<Data>(std::filesystem::absolute(cfg.data_root_dir));
    if (!std::filesystem::is_directory(data_ptr->dir()))
        throw std::logic_error("Error: The data root directory is not valid: " + data_ptr->dir().string());
    com::Folder* const runners = root_osi->push_back<com::Folder>("runners");
    runners->push_back<com::Folder>("updaters");
    runners->push_back<com::Folder>("presenters");
    osi::index();
}

void shutdown(com::Folder* const ctx_root)
{
    com::Folder* const root_osi = ctx_root->find<com::Folder>("osi");
    root_osi->erase(root_osi->find<com::Folder>("runners"));
    root_osi->erase(Data::self_name());
    root_osi->erase(Timer::self_name());
    root_osi->erase(Mouse::self_name());
    root_osi->erase(Keyboard::self_name());
    root_osi->erase(Window::self_name());
    root_osi->erase(Termination::self_name());
    ctx_root->erase(root_osi);
}

}
