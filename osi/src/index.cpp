#include <osi/index.hpp>

namespace osi {

Index const& Index::instance()
{
    static Index const idx;
    return idx;
}

Index::Index()
    : m_runners{ nullptr }
    , m_updaters{ nullptr }
    , m_presenters{ nullptr }
    , m_window{ nullptr }
    , m_keyboard{ nullptr }
    , m_mouse{ nullptr }
    , m_timer{ nullptr }
    , m_termination{ nullptr }
    , m_data{ nullptr }
{
    com::Folder* const root_osi = com::Folder::root()->find<com::Folder>("osi");
    m_runners = root_osi->find<com::Folder>("runners");
    m_updaters = m_runners->find<com::Folder>("updaters");
    m_presenters = m_runners->find<com::Folder>("presenters");
    m_window = root_osi->find<Window>(Window::self_name());
    m_keyboard = root_osi->find<Keyboard>(Keyboard::self_name());
    m_mouse = root_osi->find<Mouse>(Mouse::self_name());
    m_timer = root_osi->find<Timer>(Timer::self_name());
    m_termination = root_osi->find<Termination>(Termination::self_name());
    m_data = root_osi->find<Data>(Data::self_name());
}

}
