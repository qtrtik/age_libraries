#include <osi/run.hpp>
#include <osi/index.hpp>
#include <lfs/index.hpp>
#include <gfx/image_utils.hpp>
#include <com/runner.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>
#include <SDL2/SDL.h>
#include <glad/glad.h>
#include <chrono>
#include <algorithm>
#include <memory>
#include <stdexcept>
#include <utils/timeprof.hpp>
#include <cctype>

namespace osi {

static std::vector<Config::BootAndShutdown> modules {};
static std::unique_ptr<com::Folder> root_folder_ptr = nullptr;
static SDL_Window* window_ptr = nullptr;
static SDL_GLContext gl_context_ptr = nullptr;

namespace detail {

com::Folder* get_root_folder() { return root_folder_ptr.get(); }

bool safe_isspace(char c) { return std::isspace(static_cast<unsigned char>(c)); }

}

void Window::set_title(const std::string &title)
{
    SDL_SetWindowTitle(window_ptr, title.c_str());
}

static std::string to_key_name(SDL_Keycode const code)
{
    std::string name { SDL_GetKeyName(code) };
    name.erase(std::remove_if(name.begin(), name.end(), detail::safe_isspace), name.end());
    return name;
}

static std::string to_button_name(std::uint8_t const button_index)
{
    switch (button_index)
    {
        case SDL_BUTTON_LEFT: return "MouseLeft";
        case SDL_BUTTON_RIGHT: return "MouseRight";
        case SDL_BUTTON_MIDDLE: return "MouseMiddle";
        case SDL_BUTTON_X1: return "MouseX1";
        case SDL_BUTTON_X2: return "MouseX2";
        default: return "MouseUNKNOWN";
    }
}

static SDL_Surface* png_bytes_to_sdl_surface(std::vector<std::uint8_t> const& bytes)
{
    int width, height, nr_channels;
    auto pixels = gfx::decode_png_image_from_bytes(bytes.data(), bytes.size(), width, height, nr_channels);
    if (pixels == nullptr)
        throw std::runtime_error("osi::run: Decoding png image from bytes has failed.");
    INVARIANT(nr_channels == 4);
    SDL_Surface* const surface = SDL_CreateRGBSurfaceWithFormat(0, width, height, 4*8, SDL_PIXELFORMAT_RGBA32);
    if (surface == nullptr)
        throw std::runtime_error("Allocation of SDL surface has failed.");
    SDL_LockSurface(surface);
    std::uint8_t* surface_pixel_ptr = static_cast<std::uint8_t*>(surface->pixels);
    std::memcpy(surface_pixel_ptr, pixels, width * height * 4);
    SDL_UnlockSurface(surface);
    gfx::free_memory_of_decoded_png_image(pixels);
    return surface;
}

static void start(Config const& cfg)
{
    if (cfg.min_dt > cfg.max_dt)
        throw std::runtime_error("Error: In Simulator::Config: min_dt > max_dt.");
    if ((scalar)cfg.cpu_yield_ms < 0.0f)
        throw std::runtime_error("Error: In Simulator::Config: cpu_yield_ms < 0.0f");

    if (SDL_Init(SDL_INIT_EVERYTHING))
        throw std::runtime_error("Error: Initialisation of the SDL2 library has FAILED.");

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    window_ptr = SDL_CreateWindow(
        cfg.window_title.c_str(),
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        (int)cfg.window_size.x,
        (int)cfg.window_size.y,
        SDL_WINDOW_OPENGL
            | SDL_WINDOW_SHOWN
            | (cfg.windowed ? (cfg.resizable ? SDL_WINDOW_RESIZABLE : SDL_WINDOW_BORDERLESS) :
                               SDL_WINDOW_FULLSCREEN_DESKTOP)
        );
    if (window_ptr == nullptr)
        throw std::runtime_error("Error: The call 'SDL_CreateWindow' function has FAILED.");

    gl_context_ptr = SDL_GL_CreateContext(window_ptr);
    if (gl_context_ptr == nullptr)
        throw std::runtime_error("Error: The call 'SDL_GL_CreateContext' function has FAILED.");

    if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
        throw std::runtime_error("Error: The call to 'gladLoadGLLoader' has FAILED.");

    // char const* dbg_version_string=(char const*)glGetString(GL_VERSION);

    SDL_GL_SetSwapInterval(cfg.v_sync ? 1 : 0);

    root_folder_ptr = std::make_unique<com::Folder>("root");
    if (root_folder_ptr == nullptr)
        throw std::runtime_error("Error: The construction of the root context folder has FAILED.");

    for (Config::BootAndShutdown const& module : cfg.core_modules)
    {
        module.boot(root_folder_ptr.get());
        modules.push_back(module);
    }
    for (Config::BootAndShutdown const& module : cfg.user_modules)
    {
        module.boot(root_folder_ptr.get());
        modules.push_back(module);
    }

    if (!cfg.icon_relative_path.empty() && lfs::loader() != nullptr)
        SDL_SetWindowIcon(window_ptr, png_bytes_to_sdl_surface(lfs::loader()->load_now(cfg.icon_relative_path)->data()));
}

static void finish()
{
    while (!modules.empty())
    {
        INVARIANT(root_folder_ptr != nullptr);
        try { modules.back().shutdown(root_folder_ptr.get()); } catch (...) {}
        modules.pop_back();
    }

    try { root_folder_ptr = nullptr; } catch (...) {}

    if (gl_context_ptr != nullptr)
    {
        SDL_GL_DeleteContext(gl_context_ptr);
        gl_context_ptr = nullptr;
    }

    if (window_ptr != nullptr)
    {
        SDL_DestroyWindow(window_ptr);
        window_ptr = nullptr;
    }

    SDL_Quit();
}

static void process_modules_dfs(com::Folder* const folder, bool (* const guard)(com::Runner const*))
{
    for (com::ContextItem* item : folder->items())
        if (com::Folder* const subfolder = dynamic_cast<com::Folder*>(item))
            process_modules_dfs(subfolder, guard);
        else
        {
            com::Runner* runner_ptr = nullptr;
            if (com::Link* const link = dynamic_cast<com::Link*>(item))
                runner_ptr = dynamic_cast<com::Runner*>(link->target());
            else
                runner_ptr = dynamic_cast<com::Runner*>(item);
            if (runner_ptr != nullptr && guard(runner_ptr))
                runner_ptr->next_round();
        }
}

void run(Config const& cfg)
{
    if (root_folder_ptr != nullptr)
        throw std::logic_error("Error: Function 'osi::run' was called recursively.");

    try
    {
        start(cfg);
    }
    catch (...)
    {
        finish();
        throw;
    }

    com::Folder* const updaters = osi::updaters();
    com::Folder* const presenters = osi::presenters();
    Window* const window = const_cast<Window*>(osi::window());
    Keyboard* const keyboard = const_cast<Keyboard*>(osi::keyboard());
    Mouse* const mouse = const_cast<Mouse*>(osi::mouse());
    Timer* const timer = const_cast<Timer*>(osi::timer());
    Termination* const termination = const_cast<Termination*>(osi::termination());


    float ddpi, hdpi, vdpi;
    if (!SDL_GetDisplayDPI(0, &ddpi, &hdpi, &vdpi) != 0) {
        window->set_pixel_size(vec2{ 0.0254f / hdpi, 0.0254f / vdpi });
    }

    struct guard
    {
        static inline bool always(com::Runner const*) { return true; }
        static inline bool ignore_paused(com::Runner const* const runner_ptr) { return runner_ptr->ignore_paused(); }
    };

    std::chrono::time_point<std::chrono::steady_clock> last_round_start_time = std::chrono::steady_clock::now();
    std::uint8_t num_warm_up_iterations = cfg.num_warm_up_iterations;

    while (true)
    {
#ifdef MEASURING_FPS
        if (osi::timer()->passed_seconds() >= 30)
            break;
#endif
#ifdef MEASURING_COMPILE
        if (osi::timer()->passed_rounds() >= 5000)
            break;
#endif

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                termination->set_quit_flag();
                break;
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                    case SDL_WINDOWEVENT_RESIZED:
                    case SDL_WINDOWEVENT_SIZE_CHANGED:
                        {
                            window->m_size.x = event.window.data1;
                            window->m_size.y = event.window.data2;
                            window->m_is_resized = true;
                        }
                        break;
                    case SDL_WINDOWEVENT_MINIMIZED:
                        window->m_is_minimized = true;
                        break;
                    case SDL_WINDOWEVENT_MAXIMIZED:
                    case SDL_WINDOWEVENT_RESTORED:
                        window->m_is_minimized = false;
                        break;
                    case SDL_WINDOWEVENT_ENTER:
                        window->m_is_mouse_in_window = true;
                        break;
                    case SDL_WINDOWEVENT_LEAVE:
                        window->m_is_mouse_in_window = false;
                        break;
                    case SDL_WINDOWEVENT_FOCUS_GAINED:
                        window->m_has_keyboard_focus = true;
                        break;
                    case SDL_WINDOWEVENT_FOCUS_LOST:
                        window->m_has_keyboard_focus = false;
                        break;
                    default: break;
                }
                break;
            case SDL_KEYDOWN:
                {
                    std::string const name = to_key_name(event.key.keysym.sym);
                    keyboard->m_just_pressed.insert(name);
                    keyboard->m_down.insert(name);
                }
                break;
            case SDL_KEYUP:
                {
                    std::string const name = to_key_name(event.key.keysym.sym);
                    keyboard->m_just_released.insert(name);
                    keyboard->m_down.erase(name);
                }
                break;
            case SDL_TEXTINPUT:
                keyboard->m_text += event.text.text;
                break;
            case SDL_MOUSEMOTION:
                mouse->m_pos.x = event.motion.x;
                mouse->m_pos.y = event.motion.y;
                mouse->m_pos_delta.x = event.motion.xrel;
                mouse->m_pos_delta.y = event.motion.yrel;
                break;
            case SDL_MOUSEBUTTONDOWN:
                {
                    std::string const name = to_button_name(event.button.button);
                    mouse->m_just_pressed.insert(name);
                    mouse->m_down.insert(name);
                }
                break;
            case SDL_MOUSEBUTTONUP:
                {
                    std::string const name = to_button_name(event.button.button);
                    mouse->m_just_released.insert(name);
                    mouse->m_down.erase(name);
                }
                break;
            case SDL_MOUSEWHEEL:
                break;
            default: break;
            }
        }
        if (termination->quit_flag())
            break;

        if (num_warm_up_iterations != 0U)
            --num_warm_up_iterations;
        else
        {
            std::chrono::time_point<std::chrono::steady_clock> const current_time = std::chrono::steady_clock::now();
            std::chrono::duration<double> const duration = current_time - last_round_start_time;

            last_round_start_time = current_time;
            timer->m_time_step = std::min(timer->m_time_step + static_cast<scalar>(duration.count()), cfg.max_dt);

            if (timer->m_time_step >= cfg.min_dt)
            {
                try
                {
                    process_modules_dfs(updaters, timer->paused() ? &guard::ignore_paused : &guard::always);
                    process_modules_dfs(presenters, &guard::always);
                }
                catch (...)
                {
                    finish();
                    throw;
                }

                keyboard->m_just_pressed.clear();
                keyboard->m_just_released.clear();
                keyboard->m_text.clear();

                mouse->m_just_pressed.clear();
                mouse->m_just_released.clear();
                mouse->m_pos_delta.x = 0;
                mouse->m_pos_delta.y = 0;

                window->m_is_resized = false;

                ++timer->m_passed_rounds;
                timer->m_passed_seconds += (double)timer->m_time_step;
                timer->m_time_step = 0.0f;

                SDL_GL_SwapWindow(window_ptr);
            }
        }

        SDL_Delay(cfg.cpu_yield_ms);
    }

    finish();
}

}
