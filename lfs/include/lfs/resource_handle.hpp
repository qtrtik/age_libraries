#ifndef LFS_RESOURCE_HANDLE_HPP_INCLUDED
#   define LFS_RESOURCE_HANDLE_HPP_INCLUDED

#   include <com/context.hpp>
#   include <cstdint>

namespace lfs {

struct Updater;
struct Loader;

struct ResourceHandle final : public com::File
{
    enum STATE
    {
        LOADING,
        LOADED,
        DISCARDED,
        ERROR
    };

    ResourceHandle(std::string const& name);
    ~ResourceHandle() override;

    STATE state() const { return m_state; }
    std::vector<std::uint8_t> const& data() const { return m_data; }
    std::string const& error_message() const { return m_error_message; }

    bool discardable() const { return m_discardable; }
    void set_discardable(bool const state) { m_discardable = state; }

protected:

    void initialize() override;
    void release() override;

private:
  
    friend struct lfs::Updater;
    friend struct lfs::Loader;

    STATE m_state;
    std::vector<std::uint8_t> m_data;
    std::string m_error_message;
    bool m_discardable;
};

}

#endif
