#ifndef LFS_UPDATER_HPP_INCLUDED
#   define LFS_UPDATER_HPP_INCLUDED

#   include <com/runner.hpp>

namespace com { struct Frame; }

namespace lfs {

struct Updater : public com::Runner
{
    static inline std::string self_name() { return "updater.run"; }

    Updater();
    ~Updater() override;

    void next_round() override;

protected:

    void initialize() override;
    void release() override;
};

}

#endif
