#ifndef LFS_INDEX_HPP_INCLUDED
#   define LFS_INDEX_HPP_INCLUDED

#   include <lfs/loader.hpp>
#   include <com/context.hpp>

namespace lfs {

struct Index final
{
    static Index const& instance();

    com::Folder* data_root() const { return m_data_root; }
    Loader* loader() const { return m_loader; }

private:
    Index();
    Index(Index const&) = delete;
    Index(Index&&) = delete;
    Index& operator=(Index const&) const = delete;
    Index& operator=(Index&&) const = delete;

    com::Folder* m_data_root;
    Loader* m_loader;
};

inline Index const& index() { return Index::instance(); }
inline com::Folder* data_root() { return index().data_root(); }
inline Loader* loader() { return index().loader(); }

}

#endif
