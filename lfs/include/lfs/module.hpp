#ifndef LFS_MODULE_HPP_INCLUDED
#   define LFS_MODULE_HPP_INCLUDED

namespace com { struct Folder; }

namespace lfs {

void boot(com::Folder* ctx_root);
void shutdown(com::Folder* ctx_root);

}

#endif
