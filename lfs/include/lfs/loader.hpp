#ifndef LFS_LOADER_HPP_INCLUDED
#   define LFS_LOADER_HPP_INCLUDED

#   include <lfs/resource_handle.hpp>
#   include <com/library.hpp>
#   include <cstdint>
#   include <filesystem>
#   include <deque>
#   include <vector>
#   include <thread>
#   include <mutex>

namespace lfs {

struct Updater;

struct Loader final : public com::Library
{
    static inline std::string self_name() { return "loader.lib"; }

    Loader();
    ~Loader() override;

    ResourceHandle* load(com::ContextPath const& path_name, bool top_priority = false);
    ResourceHandle* load(std::string const& name, com::ContextPath const& path, bool top_priority = false);

    ResourceHandle* load_now(com::ContextPath const& path_name);
    ResourceHandle* load_now(std::string const& name, com::ContextPath const& path);

protected:

    void initialize() override;
    void release() override;

private:

    friend struct lfs::Updater;

    struct Request
    {
        Request() = default;
        Request(com::Folder* folder, std::string const& name);
        std::filesystem::path disk_path;
        com::ContextPath ctx_path;
    };

    struct Response
    {
        std::filesystem::path disk_path;
        com::ContextPath ctx_path;
        std::vector<std::uint8_t> loaded_bytes;
        std::string error_message;
    };

    std::vector<ResourceHandle*> deliver_loaded_data();
    void deliver_loaded_data(Response& response, ResourceHandle* handle);
    void wait_for_data(ResourceHandle* handle);

    void worker_thread_procedure();

    std::deque<Request> m_requests;
    std::vector<Response> m_responses;
    std::thread m_worker;
    std::mutex m_mutex;
    bool m_worker_stop_flag;
};

}

#endif
