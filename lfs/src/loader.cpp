#include <lfs/loader.hpp>
#include <lfs/index.hpp>
#include <osi/index.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>
#include <chrono>
#include <fstream>
#include <iterator>

namespace lfs {

Loader::Request::Request(com::Folder* const folder, std::string const& name)
    : disk_path{ osi::data()->dir() }
    , ctx_path{ folder->path(data_root()) }
{
    ctx_path.push_back(name);
    for (std::string const& path_elem : ctx_path)
        disk_path /= path_elem;
    ASSUMPTION(std::filesystem::is_regular_file(disk_path));
}

Loader::Loader()
    : com::Library{ self_name() }
    , m_requests{}
    , m_responses{}
    , m_worker{}
    , m_mutex{}
    , m_worker_stop_flag{ false }
{}

Loader::~Loader()
{}

void Loader::initialize()
{
    m_worker = std::thread(&Loader::worker_thread_procedure, this);
}

void Loader::release()
{
    {
        std::lock_guard<std::mutex> const lock(m_mutex);
        m_worker_stop_flag = true;
        m_requests.clear();
        m_responses.clear();
    }
    if (m_worker.joinable())
        m_worker.join();
}

ResourceHandle* Loader::load(com::ContextPath const& path_name, bool const top_priority)
{
    com::ContextPath path{ path_name.begin(),  std::prev(path_name.end()) };
    return load(path_name.back(), path, top_priority);
}

ResourceHandle* Loader::load(std::string const& name, com::ContextPath const& path, bool const top_priority)
{
    com::Folder* const f = data_root()->push_back_folders(path);
    ASSUMPTION(f->is_under(data_root()));
    com::ContextItem* const item = f->find(name);
    if (item == nullptr)
    {
        {
            std::lock_guard<std::mutex> const lock(m_mutex);
            if (top_priority)
                m_requests.push_front({ f, name });
            else
                m_requests.push_back({ f, name });
        }
        return f->push_back<ResourceHandle>(name);
    }
    ResourceHandle* const handle = dynamic_cast<ResourceHandle*>(item);
    ASSUMPTION(handle != nullptr);
    if (handle->state() == ResourceHandle::DISCARDED)
    {
        {
            std::lock_guard<std::mutex> const lock(m_mutex);
            if (top_priority)
                m_requests.push_front({ f, name });
            else
                m_requests.push_back({ f, name });
        }
        handle->m_state = ResourceHandle::LOADING;
        handle->m_data.clear();
        handle->m_error_message.clear();
    }
    return handle;
}

ResourceHandle* Loader::load_now(com::ContextPath const& path_name)
{
    ResourceHandle* const handle = load(path_name, true);
    wait_for_data(handle);
    return handle;
}

ResourceHandle* Loader::load_now(std::string const& name, com::ContextPath const& path)
{
    ResourceHandle* const handle = load(name, path, true);
    wait_for_data(handle);
    return handle;
}

std::vector<ResourceHandle*> Loader::deliver_loaded_data()
{
    std::vector<Response> responses;
    {
        std::lock_guard<std::mutex> const lock(m_mutex);
        while (!m_responses.empty())
        {
            responses.push_back({});
            responses.back().disk_path.swap(m_responses.back().disk_path);
            responses.back().ctx_path.swap(m_responses.back().ctx_path);
            responses.back().loaded_bytes.swap(m_responses.back().loaded_bytes);
            responses.back().error_message.swap(m_responses.back().error_message);
            m_responses.pop_back();
        }
    }

    std::vector<ResourceHandle*> handles;
    while (!responses.empty())
    {
        if (ResourceHandle* const handle = data_root()->locate<ResourceHandle>(responses.back().ctx_path))
        {
            deliver_loaded_data(responses.back(), handle);
            handles.push_back(handle);
        }
        responses.pop_back();
    }

    return handles;
}

void Loader::deliver_loaded_data(Response& response, ResourceHandle* const handle)
{
    handle->m_error_message.swap(response.error_message);
    if (handle->error_message().empty())
    {
        handle->m_state = ResourceHandle::LOADED;
        handle->m_data.swap(response.loaded_bytes);
    }
    else
    {
        handle->m_state = ResourceHandle::ERROR;
        handle->m_data.clear();
    }
}

void Loader::wait_for_data(ResourceHandle* const handle)
{
    Response response;
    Response* response_ptr = nullptr;
    std::size_t idx{ 0 };
    while (response_ptr == nullptr && m_worker.joinable())
    {
        std::this_thread::yield();
        std::lock_guard<std::mutex> const lock(m_mutex);
        while (m_responses.size() > idx)
        {
            auto const it = std::next(m_responses.begin(), idx);
            INVARIANT(&*it == &m_responses.at(idx));
            if (data_root()->locate<ResourceHandle>(it->ctx_path) == handle)
            {
                response.disk_path.swap(it->disk_path);
                response.ctx_path.swap(it->ctx_path);
                response.loaded_bytes.swap(it->loaded_bytes);
                response.error_message.swap(it->error_message);

                m_responses.erase(it);

                response_ptr = &response;
                break;
            }
            ++idx;
        }
    }
    if (response_ptr == nullptr)
    {
        handle->m_state = ResourceHandle::ERROR;
        handle->m_error_message = "Failed to receive data from the worker thread.";
        handle->m_data.clear();
    }
    else
        deliver_loaded_data(*response_ptr, handle);
}

void Loader::worker_thread_procedure()
{
    Response response;
    while (true)
    {
        Response* response_ptr = nullptr;
        {
            std::lock_guard<std::mutex> const lock(m_mutex);
            if (m_worker_stop_flag)
                break;
            if (!m_requests.empty())
            {
                response.disk_path.swap(m_requests.front().disk_path);
                response.ctx_path.swap(m_requests.front().ctx_path);
                m_requests.pop_front();
                response_ptr = &response;
            }
        }
        if (response_ptr == nullptr)
        {
            //std::this_thread::yield();
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(10ms);
            continue;
        }

        response_ptr->loaded_bytes.clear();
        response_ptr->error_message.clear();

        try
        {
            if (std::filesystem::is_regular_file(response_ptr->disk_path))
            {
                std::ifstream istr{ response_ptr->disk_path.c_str(), std::ios_base::binary };
                if (istr.is_open())
                {
                    istr.unsetf(std::ios::skipws);
                    std::streampos size;
                    {
                        istr.seekg(0, std::ios::end);
                        size = istr.tellg();
                        istr.seekg(0, std::ios::beg);
                    }
                    response_ptr->loaded_bytes.resize(static_cast<unsigned int>(size), 0);
                    istr.read((char*)response_ptr->loaded_bytes.data(), response_ptr->loaded_bytes.size());
                }
                else
                    response_ptr->error_message = "Cannot open file referenced by the passed disk path.";
            }
            else
                response_ptr->error_message = "The disk path does not reference a regular file.";
        }
        catch(const std::exception& e)
        {
            response_ptr->error_message = "Reading bytes from the file on the passed disk path has failed. ";
            response_ptr->error_message += e.what();
        }

        {
            std::lock_guard<std::mutex> const lock(m_mutex);
            m_responses.push_back({});
            m_responses.back().disk_path.swap(response_ptr->disk_path);
            m_responses.back().ctx_path.swap(response_ptr->ctx_path);
            m_responses.back().loaded_bytes.swap(response_ptr->loaded_bytes);
            m_responses.back().error_message.swap(response_ptr->error_message);
        }
    }
}

}
