#include <lfs/resource_handle.hpp>

namespace lfs {

ResourceHandle::ResourceHandle(std::string const& name)
    : com::File{ name }
    , m_state{ LOADING }
    , m_data{}
    , m_error_message{}
    , m_discardable{ true }
{}

ResourceHandle::~ResourceHandle()
{}

void ResourceHandle::initialize()
{
}

void ResourceHandle::release()
{
}

}
