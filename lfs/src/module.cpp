#include <lfs/module.hpp>
#include <lfs/loader.hpp>
#include <lfs/updater.hpp>
#include <com/context.hpp>

namespace lfs {

void boot(com::Folder* const ctx_root)
{
    com::Folder* const root_lfs = ctx_root->push_back<com::Folder>("lfs");
    root_lfs->push_back<com::Folder>("data");
    root_lfs->push_back<Loader>();
    root_lfs->push_back<Updater>();
}

void shutdown(com::Folder* const ctx_root)
{
    com::Folder* const root_lfs = ctx_root->find<com::Folder>("lfs");
    root_lfs->erase(Updater::self_name());
    root_lfs->erase(Loader::self_name());
    root_lfs->erase("data");
    ctx_root->erase(root_lfs);
}

}
