#include <lfs/updater.hpp>
#include <lfs/index.hpp>
#include <osi/index.hpp>

namespace lfs {

Updater::Updater()
    : com::Runner{ self_name() }
{}

Updater::~Updater()
{}

void Updater::initialize()
{
    osi::updaters()->push_front<com::Link>("lfs_" + self_name() + ".link", this);
}

void Updater::release()
{
    osi::updaters()->erase("lfs_" + self_name() + ".link");
}

void Updater::next_round()
{
    std::vector<ResourceHandle*> const handles = loader()->deliver_loaded_data();
    for (ResourceHandle* const handle : handles)
        handle->notify_content_changed();
}

}
