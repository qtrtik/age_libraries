#include <lfs/index.hpp>

namespace lfs {

Index const& Index::instance()
{
    static Index const idx;
    return idx;
}

Index::Index()
    : m_data_root{ nullptr }
    , m_loader{ nullptr }
{
    com::Folder* root_lfs = com::Folder::root()->find<com::Folder>("lfs");
    m_data_root = root_lfs->find<com::Folder>("data");
    m_loader = root_lfs->find<Loader>(Loader::self_name());
}

}
