#include <script/console.hpp>
#include <osi/index.hpp>
#include <gfx/index.hpp>
#include <sstream>

namespace scr {

Console::Console()
    : com::Runner{ self_name() }
    , m_active{ false }
    , m_viewport{ nullptr }
{}

Console::~Console()
{}

void Console::initialize()
{
    osi::updaters()->push_back<com::Link>(self_name() + ".link", this);
    m_viewport = gfx::viewport_system()->insert_viewport("console", {});
    m_viewport->set_bottom_left({ 1, 0 });
}

void Console::release()
{
    osi::updaters()->erase(self_name() + ".link");
    gfx::viewport_system()->erase_viewport(m_viewport);
    m_viewport = nullptr;
}

void Console::next_round()
{
    if (osi::keyboard()->just_released().contains("`"))
    {
        if (m_active)
        {
            gfx::viewport_system()->viewport_default()->set_top_right({ 1, 1 });
            m_viewport->set_bottom_left({ 1, 0 });
        }
        else
        {
            gfx::viewport_system()->viewport_default()->set_top_right({ 0.5f, 1 });
            m_viewport->set_bottom_left({ 0.5f, 0 });
        }
        m_active = !m_active;
    }
}

}
