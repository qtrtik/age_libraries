#include <script/index.hpp>

namespace scr {

Index const& Index::instance()
{
    static Index const idx;
    return idx;
}

Index::Index()
    : m_root { com::Folder::root()->find<com::Folder>("scr") }
    , m_interpreter{ m_root->find<Interpreter>(Interpreter::self_name()) }
{}

}
