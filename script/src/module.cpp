#include <script/module.hpp>
#include <script/console.hpp>
#include <script/interpreter.hpp>
#include <com/context.hpp>

namespace scr {

void boot(com::Folder* const ctx_root)
{
    com::Folder* const root_scr = ctx_root->push_back<com::Folder>("scr");
    root_scr->push_back<Interpreter>(); 
    root_scr->push_back<Console>(); 
}

void shutdown(com::Folder* const ctx_root)
{
    com::Folder* const root_scr = ctx_root->find<com::Folder>("scr");
    root_scr->erase(root_scr->find<Console>(Console::self_name()));
    root_scr->erase(root_scr->find<Interpreter>(Interpreter::self_name()));
    ctx_root->erase(root_scr);
}

}
