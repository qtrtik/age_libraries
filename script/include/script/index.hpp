#ifndef SCRIPT_INDEX_HPP_INCLUDED
#   define SCRIPT_INDEX_HPP_INCLUDED

#   include <script/interpreter.hpp>
#   include <com/context.hpp>

namespace scr {

struct Index final
{
    static Index const& instance();

    com::Folder* root() const { return m_root; }
    Interpreter* interpreter() const { return m_interpreter; }

private:
    Index();
    Index(Index const&) = delete;
    Index(Index&&) = delete;
    Index& operator=(Index const&) const = delete;
    Index& operator=(Index&&) const = delete;

    com::Folder* m_root;
    Interpreter* m_interpreter;
};

inline Index const& index() { return Index::instance(); }
com::Folder* root() { return index().root(); }
Interpreter* interpreter() { return index().interpreter(); }

}

#endif
