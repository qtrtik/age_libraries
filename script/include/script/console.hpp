#ifndef SCRIPT_CONSOLE_HPP_INCLUDED
#   define SCRIPT_CONSOLE_HPP_INCLUDED

#   include <com/runner.hpp>
#   include <string>

namespace gfx { struct Viewport; }

namespace scr {

struct  Console : public com::Runner
{
    static inline std::string self_name() { return "console.run"; }

    Console();
    ~Console() override;

    void next_round() override;

protected:

    void initialize() override;
    void release() override;

private:
    bool m_active;
    gfx::Viewport* m_viewport;
};

}

#endif
