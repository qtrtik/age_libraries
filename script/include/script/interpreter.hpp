#ifndef SCRIPT_INTERPRETER_HPP_INCLUDED
#   define SCRIPT_INTERPRETER_HPP_INCLUDED

#   include <com/library.hpp>

namespace scr {

struct Interpreter : public com::Library
{
    static inline std::string self_name() { return "interpreter.lib"; }

    Interpreter();
    ~Interpreter() override;

protected:

    void initialize() override;
    void release() override;
};

}

#endif
