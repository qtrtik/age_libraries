#ifndef SCRIPT_MODULE_HPP_INCLUDED
#   define SCRIPT_MODULE_HPP_INCLUDED

namespace com { struct Folder; }

namespace scr {

void boot(com::Folder* ctx_root);
void shutdown(com::Folder* ctx_root);

}

#endif
