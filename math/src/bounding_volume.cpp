#include <math/bounding_volume.hpp>
#include <utils/assumptions.hpp>

#include "math/geometry/ray.hpp"

bool AABB::operator==(AABB const& other) const {
    return lo == other.lo && hi == other.hi;
}

bool AABB::intersects(AABB const& other) const {
    return get<0>(lo) <= get<0>(other.hi) && get<0>(hi) >= get<0>(other.lo) &&
           get<1>(lo) <= get<1>(other.hi) && get<1>(hi) >= get<1>(other.lo) &&
           get<2>(lo) <= get<2>(other.hi) && get<2>(hi) >= get<2>(other.lo);
}

bool AABB::contains(vec3 const& point) const {
    return get<0>(point) >= get<0>(lo) && get<0>(point) <= get<0>(hi) &&
           get<1>(point) >= get<1>(lo) && get<1>(point) <= get<1>(hi) &&
           get<2>(point) >= get<2>(lo) && get<2>(point) <= get<2>(hi);
}

bool AABB::contains(AABB const& other) const {
    return get<0>(lo) <= get<0>(other.lo) && get<0>(hi) >= get<0>(other.hi) &&
           get<1>(lo) <= get<1>(other.lo) && get<1>(hi) >= get<1>(other.hi) &&
           get<2>(lo) <= get<2>(other.lo) && get<2>(hi) >= get<2>(other.hi);
}

scalar AABB::volume() const {
    const vec3 extent = size();
    return get<0>(extent) * get<1>(extent) * get<2>(extent);
}

scalar AABB::surface_area() const {
    const vec3 extent = size();
    return 2.0f * (
        get<0>(extent) * get<1>(extent) +
        get<0>(extent) * get<2>(extent) +
        get<1>(extent) * get<2>(extent)
    );
}

vec3 AABB::center() const {
    return (lo + hi) / static_cast<scalar>(2.0);
}

vec3 AABB::size() const {
    return hi - lo;
}

vec3 AABB::half_size() const {
    return static_cast<scalar>(0.5) * size();
}

bool AABB::projection_intersects_xy(vec2 const& point) const {
    return point.x >= get<0>(lo) && point.y >= get<1>(lo)
        && point.x <= get<0>(hi) && point.y <= get<1>(hi);
}

bool AABB::projection_intersects_xz(vec2 const& point) const {
    return point.x >= get<0>(lo) && point.y >= get<2>(lo)
        && point.x <= get<0>(hi) && point.y <= get<2>(hi);
}

bool AABB::projection_intersects_yz(vec2 const& point) const {
    return point.x >= get<1>(lo) && point.y >= get<2>(lo)
        && point.x <= get<1>(hi) && point.y <= get<2>(hi);
}

void AABB::merge(AABB const& other) {
    lo = min(lo, other.lo);
    hi = max(hi, other.hi);
}

void AABB::merge(vec3 const& point) {
    lo = min(lo, point);
    hi = max(hi, point);
}

void AABB::intersection(AABB const& other) {
    lo = max(lo, other.lo);
    hi = min(hi, other.hi);

    if (get<0>(lo) > get<0>(hi) ||
        get<1>(lo) > get<1>(hi) ||
        get<2>(lo) > get<2>(hi))
    {
        lo = {0.0f, 0.0f, 0.0f};
        hi = {0.0f, 0.0f, 0.0f};
    }
}

void AABB::fatten(vec3 const& margin) {
    lo -= margin;
    hi += margin;
}

void AABB::shrink(vec3 const& margin) {
    lo += margin;
    hi -= margin;
}

void AABB::displace(vec3 const& displacement) {
    (*this) = displace(*this, displacement);
}

void AABB::to_points(vec3 points[8]) const {
    points[0] = { get<0>(lo), get<1>(lo), get<2>(lo) };
    points[1] = { get<0>(hi), get<1>(lo), get<2>(lo) };
    points[2] = { get<0>(hi), get<1>(hi), get<2>(lo) };
    points[3] = { get<0>(lo), get<1>(hi), get<2>(lo) };
    points[4] = { get<0>(lo), get<1>(lo), get<2>(hi) };
    points[5] = { get<0>(hi), get<1>(lo), get<2>(hi) };
    points[6] = { get<0>(hi), get<1>(hi), get<2>(hi) };
    points[7] = { get<0>(lo), get<1>(hi), get<2>(hi) };
}

AABB AABB::from_points(vec3 const* begin, vec3 const* end) {
    ASSUMPTION(begin != nullptr && begin != end);

    AABB aabb { *begin, *begin };
    for (++begin; begin != end; ++begin)
    {
        set<0>(aabb.lo, std::min(get<0>(aabb.lo), get<0>(*begin)) );
        set<1>(aabb.lo, std::min(get<1>(aabb.lo), get<1>(*begin)) );
        set<2>(aabb.lo, std::min(get<2>(aabb.lo), get<2>(*begin)) );
        set<0>(aabb.hi, std::max(get<0>(aabb.hi), get<0>(*begin)) );
        set<1>(aabb.hi, std::max(get<1>(aabb.hi), get<1>(*begin)) );
        set<2>(aabb.hi, std::max(get<2>(aabb.hi), get<2>(*begin)) );
    }
    return aabb;
}

bool AABB::intersects(AABB const& left, AABB const& right) {
    return left.intersects(right);
}

bool AABB::contains(AABB const& left, AABB const& right) {
    return left.contains(right);
}

bool AABB::contains(AABB const& aabb, vec3 const& point) {
    return aabb.contains(point);
}

AABB operator*(mat4x4 const& m, AABB const& aabb) {
    vec3 points[8];
    aabb.to_points(points);
    for (vec3& p : points)
        p = m * expand(p, 1);
    return AABB::from_points(points, points + 8);
}

bool AABB::projection_intersects_xy(AABB const& aabb, vec2 const& point) {
    return aabb.projection_intersects_xy(point);
}

bool AABB::projection_intersects_xz(AABB const& aabb, vec2 const& point) {
    return aabb.projection_intersects_xz(point);
}

bool AABB::projection_intersects_yz(AABB const& aabb, vec2 const& point) {
    return aabb.projection_intersects_yz(point);
}

AABB AABB::merge(AABB const& left, AABB const& right) {
    return {
        min(left.lo, right.lo),
        max(left.hi, right.hi)
    };
}

AABB AABB::merge(AABB const& aabb, vec3 const& point) {
    return {
        min(aabb.lo, point),
        max(aabb.hi, point)
    };
}

AABB AABB::intersection(AABB const& left, AABB const& right)
{
    AABB result = {
        max(left.lo, right.lo),
        min(left.hi, right.hi)
    };

    if (get<0>(result.lo) > get<0>(result.hi) ||
        get<1>(result.lo) > get<1>(result.hi) ||
        get<2>(result.lo) > get<2>(result.hi))
    {
        result = {{0, 0, 0}, {0, 0, 0}};
    }

    return result;
}

AABB AABB::fatten(AABB const& aabb, vec3 const& margin) {
    return {
        aabb.lo - margin,
        aabb.hi + margin
    };
}

AABB AABB::shrink(AABB const& aabb, vec3 const& margin) {
    return {
        aabb.lo + margin,
        aabb.hi - margin
    };
}

AABB AABB::displace(AABB const& aabb, vec3 const& displacement) {
    vec3 new_lo = aabb.lo;
    vec3 new_hi = aabb.hi;

    if (displacement.x > 0.0f) {
        new_hi.x += displacement.x;
    } else {
        new_lo.x += displacement.x;
    }

    if (displacement.y > 0.0f) {
        new_hi.y += displacement.y;
    } else {
        new_lo.y += displacement.y;
    }

    if (displacement.z > 0.0f) {
        new_hi.z += displacement.z;
    } else {
        new_lo.z += displacement.z;
    }

    return {new_lo, new_hi};
}

std::set<geometry::RayIntersection> AABB::ray_cast(geometry::Ray const& ray) const {
    vec3 dir = ray.direction_unscaled();
    vec3 invD = static_cast<scalar>(1.0) / ray.direction_unscaled();
    vec3 origin = ray.origin();

    scalar t_min = 0.0f;
    scalar t_max = 1.0f;

    auto t_min_axis = -1;
    auto t_max_axis = -1;

    for (int i = 0; i < 3; ++i) {
        scalar t1 = (lo[i] - origin[i]) * invD[i];
        scalar t2 = (hi[i] - origin[i]) * invD[i];

        if (invD[i] < 0.0f) std::swap(t1, t2);

        if (t1 > t_min) {
            t_min = t1;
            t_min_axis = i;
        }

        if (t2 < t_max) {
            t_max = t2;
            t_max_axis = i;
        }

        if (t_max < t_min) return {};
    }

    if (t_min_axis == -1 || t_max_axis == -1) {
        return {}; // No intersection
    }

    std::set<geometry::RayIntersection> result;

    auto t_min_normal = glm::vec3(0.0f);
    auto t_max_normal = glm::vec3(0.0f);

    t_min_normal[t_min_axis] = dir[t_min_axis] < 0.0f ? 1.0f : -1.0f;
    t_max_normal[t_max_axis] = dir[t_max_axis] < 0.0f ? 1.0f : -1.0f;

    result.insert({geometry::ParametrisedPoint{ray.origin() + dir * t_min, t_min}, t_min_normal});
    result.insert({geometry::ParametrisedPoint{ray.origin() + dir * t_max, t_max}, t_max_normal});

    return result;
}
