#include "math/geometry/halfedge.hpp"

#include "math/geometry/utils.hpp"

namespace geometry {
    DistanceQueryResult HalfEdge::distance_to(Feature* other) const
    {
        ASSUMPTION(other != nullptr && !m_start.expired() && !m_end.expired());
        if (const auto vertex = dynamic_cast<Vertex*>(other)) {
            return distance_point_line(vertex->position_ws(), m_start.lock()->position_ws(), m_end.lock()->position_ws());
        }
        if (const auto edge = dynamic_cast<HalfEdge*>(other)) {
            return distance_line_line(m_start.lock()->position_ws(), m_end.lock()->position_ws(), edge->m_start.lock()->position_ws(), edge->m_end.lock()->position_ws());
        }
        if (const auto face = dynamic_cast<Face*>(other)) {
            return distance_line_face(m_start.lock()->position_ws(), m_end.lock()->position_ws(), face->vertex_positions());
        }
        return DistanceQueryResult::max();
    }

    std::vector<Vertex*> HalfEdge::vertex_neighbours() const {
        ASSUMPTION(!m_start.expired() && !m_end.expired());
        return {m_start.lock().get(), m_end.lock().get()};
    }

    std::vector<Face*> HalfEdge::face_neighbours() const {
        ASSUMPTION(!m_face.expired() && !m_twin.expired() && !m_twin.lock()->m_face.expired());
        return {m_face.lock().get(), m_twin.lock()->m_face.lock().get()};
    }

    std::string HalfEdge::to_string() const {
        ASSUMPTION(!m_start.expired() && !m_end.expired());
        const auto start_position = m_start.lock()->position_ws();
        const auto end_position = m_end.lock()->position_ws();
        return "HalfEdge(vertices: [(" + std::to_string(start_position.x) + ", " + std::to_string(start_position.y) + ", " + std::to_string(start_position.z) + "), (" + std::to_string(end_position.x) + ", " + std::to_string(end_position.y) + ", " + std::to_string(end_position.z) + ")])";
    }
}
