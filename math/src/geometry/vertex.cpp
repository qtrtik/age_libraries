#include <math/math.hpp>
#include <math/geometry/vertex.hpp>
#include <math/geometry/halfedge.hpp>
#include <math/geometry/face.hpp>
#include <math/geometry/utils.hpp>
#include <math/geometry/mesh.hpp>

namespace geometry {
    vec3 Vertex::position_ws() const {
        return mesh()->get_vertex_position_ws(id());
    }

    DistanceQueryResult Vertex::distance_to(Feature* other) const {
        ASSUMPTION(other != nullptr);
        if (const auto vertex = dynamic_cast<Vertex*>(other)) {
            const auto this_pos = position_ws();
            const auto other_pos = vertex->position_ws();
            return {distance(this_pos, other_pos), this_pos, other_pos};
        }
        if (const auto edge = dynamic_cast<HalfEdge*>(other)) {
            return distance_point_line(position_ws(), edge->start()->position_ws(), edge->end()->position_ws());
        }
        if (const auto face = dynamic_cast<Face*>(other)) {
            return distance_point_face(position_ws(), face->vertex_positions());
        }
        return DistanceQueryResult::max();
    }

    std::string Vertex::to_string() const {
        return "Vertex(position: (" + std::to_string(position_ws().x) + ", " + std::to_string(position_ws().y) + ", " + std::to_string(position_ws().z) + "))";
    }

    std::vector<HalfEdge*> Vertex::halfedge_neighbours() const {
        std::vector<HalfEdge*> neighbours;
        neighbours.reserve(m_edges.size());
        for (const auto& edge : m_edges) {
            ASSUMPTION(!edge.expired());
            neighbours.push_back(edge.lock().get());
        }
        return neighbours;
    }

    std::vector<Face*> Vertex::incident_faces() const {
        // Collect all faces from the incident edges
        std::vector<Face*> faces;
        for (const auto& edge : m_edges) {
            ASSUMPTION(!edge.expired());
            const auto face = edge.lock()->face();
            if (face != nullptr) {
                faces.push_back(face);
            }
            const auto twin = edge.lock()->twin();
            if (twin != nullptr) {
                faces.push_back(twin->face());
            }
        }
        return faces;
    }
}
