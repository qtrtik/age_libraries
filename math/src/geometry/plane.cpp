#include <math/geometry/plane.hpp>

namespace geometry {
    scalar Plane::get_intersection_parameter(const vec3& point_a, const vec3& point_b) const {
        const scalar distance_a = dot(m_normal, point_a) + m_d;
        const scalar distance_b = dot(m_normal, point_b) + m_d;

        return distance_a / (distance_a - distance_b);
    }
}