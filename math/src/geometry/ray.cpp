#include <math/geometry/ray.hpp>

namespace geometry {
    Ray::Ray(const vec3& origin, const vec3& direction, const scalar length)
        : m_origin(origin)
        , m_direction(normalize(direction))
        , m_length(length)
    {}

    Ray Ray::clip(const scalar t_min, const scalar t_max) const {
        const auto new_origin = m_origin + m_direction * t_min * m_length;
        const auto new_end = m_origin + m_direction * t_max * m_length;
        return from_two_points(new_origin, new_end);
    }

    Ray Ray::from_two_points(const vec3& from, const vec3& to) {
        const auto new_direction = to - from;
        const auto magnitude = len(new_direction);
        return {from, new_direction, magnitude};
    }

    Ray Ray::transform(const mat4x4& transform) const {
        return {transform * vec4(m_origin, 1.0f), transform * vec4(m_direction, 0.0f), m_length};
    }

    scalar Ray::parameter_at(const vec3& point) const {
        return len(point - from()) / length();
    }
}
