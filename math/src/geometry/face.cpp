#include "math/geometry/face.hpp"

#include <ranges>

#include "math/geometry/utils.hpp"
#include "math/geometry/vertex.hpp"
#include "math/geometry/halfedge.hpp"

namespace geometry {
    Plane Face::plane() const {
        return m_plane * mesh()->transform();
    }

    std::vector<Vertex*> Face::vertices() const {
        const auto vertex_view = std::ranges::transform_view(m_edges, [](const std::weak_ptr<HalfEdge>& edge) { return edge.lock()->start(); });
        return std::vector<Vertex*>{vertex_view.begin(), vertex_view.end()};
    }

    std::vector<HalfEdge*> Face::edges() const {
        const auto edge_view = std::ranges::transform_view(m_edges, [](const std::weak_ptr<HalfEdge>& edge) { return edge.lock().get(); });
        return std::vector<HalfEdge*>{edge_view.begin(), edge_view.end()};
    }

    std::vector<vec3> Face::vertex_positions() const {
        const auto vertex_positions_view = std::ranges::transform_view(m_edges, [](const std::weak_ptr<HalfEdge>& edge) { return edge.lock()->start()->position_ws(); });
        return std::vector<vec3>{vertex_positions_view.begin(), vertex_positions_view.end()};
    }

    DistanceQueryResult Face::distance_to(Feature* other) const {
        ASSUMPTION(other != nullptr);
        if (const auto vertex = dynamic_cast<Vertex*>(other)) {
            return distance_point_face(vertex->position_ws(), vertex_positions());
        }
        if (const auto edge = dynamic_cast<HalfEdge*>(other)) {
            return distance_line_face(edge->start()->position_ws(), edge->end()->position_ws(), vertex_positions());
        }
        if (const auto face = dynamic_cast<Face*>(other)) {
            // Very expensive operation but V-clip never uses this
            return distance_face_face(vertex_positions(), face->vertex_positions());
        }
        return DistanceQueryResult::max();
    }

    std::string Face::to_string() const {
        const auto plane_ws = plane();

        auto vertices_string = std::string{"["};
        for (const auto& vertex : vertices()) {
            const auto vertex_position = vertex->position_ws();
            vertices_string += "(" + std::to_string(vertex_position.x) + ", " + std::to_string(vertex_position.y) + ", " + std::to_string(vertex_position.z) + "), ";
        }

        vertices_string.pop_back();
        vertices_string.pop_back();
        vertices_string += "]";

        return "Face(plane: " + plane_ws.to_string() + ", vertices: " + vertices_string + ")";
    }

    std::vector<HalfEdge*> Face::halfedge_neighbours() const {
        std::vector<HalfEdge*> neighbours;
        neighbours.reserve(m_edges.size());
        for (const auto& edge : m_edges) {
            ASSUMPTION(!edge.expired());
            neighbours.push_back(edge.lock().get());
        }
        return neighbours;
    }
}
