#include "math/geometry/mesh.hpp"

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <array>
#include <format>
#include <ranges>
#include <utils/hash.hpp>

#include "math/geometry/vertex.hpp"
#include "math/geometry/halfedge.hpp"
#include "math/geometry/face.hpp"

namespace geometry {
    void Mesh::build_dcel(const std::vector<vec3>& vertex_positions, const std::vector<std::array<size_t, 3>>& triangle_indices) {
        ASSUMPTION(vertex_positions.size() >= 3);
        ASSUMPTION(!triangle_indices.empty());

        const auto edge_count = vertex_positions.size() + triangle_indices.size() - 2; // Euler characteristic: V - E + F = 2 => E = V + F - 2

        // Clear the mesh and reserve space for the vertices, faces, and edges
        clear();
        m_vertices.reserve(vertex_positions.size());
        m_faces.reserve(triangle_indices.size());
        m_edges.reserve(edge_count * 2); // Each edge has a twin

        // Twin edge map
        std::unordered_map<std::pair<size_t, size_t>, std::shared_ptr<HalfEdge>> edge_map;

        // Create vertices
        for (const auto& vertex_pos : vertex_positions) {
            add_vertex(std::make_unique<Vertex>(vertex_pos));
        }

        // Create faces and edges
        for (const auto& face_indices : triangle_indices) {
            std::vector<std::weak_ptr<HalfEdge>> face_edges;
            face_edges.reserve(face_indices.size());

            // Create edges
            for (size_t i = 0; i < face_indices.size(); i++) {
                const auto i1 = face_indices[i];
                const auto i2 = face_indices[(i + 1) % face_indices.size()];

                // mesh->hedges.emplace_back(&mesh->vertices[i1], &mesh->vertices[i2]);
                add_half_edge(std::make_unique<HalfEdge>(vertex(i1), vertex(i2)));
                const auto edge = m_edges.back();
                edge_map[{i1, i2}] = edge;
                face_edges.emplace_back(edge);

                // Add the edge to the start vertex
                m_vertices[i1]->m_edges.emplace_back(edge);
            }

            // Create face
            const auto normal = Plane::get_plane_normal(vertex_positions[face_indices[0]], vertex_positions[face_indices[1]], vertex_positions[face_indices[2]]);
            add_face(std::make_unique<Face>(normal, dot(normal, vertex_positions[face_indices[0]])));
            const auto face = m_faces.back();
            face->m_edges = face_edges;

            // Set the face of the edges and the next and prev pointers
            for (size_t i = 0; i < face_edges.size(); i++) {
                const auto edge = face_edges[i];
                edge.lock()->m_face = face;
                edge.lock()->m_next = face_edges[(i + 1) % face_edges.size()];
                edge.lock()->m_prev = face_edges[(i + face_edges.size() - 1) % face_edges.size()];
            }
        }

        // Set the twin pointers
        for (const auto& [edge_pair, edge] : edge_map) {
            const auto& [i1, i2] = edge_pair;
            const auto twin_edge = edge_map[{i2, i1}];
            if (!twin_edge) {
                std::cerr << "Twin edge not found" << std::endl;
                continue;
            }
            edge->m_twin = twin_edge;
            twin_edge->m_twin = edge;
        }
    }

    Mesh::Mesh(const std::vector<vec3>& vertex_positions, const std::vector<std::array<size_t, 3>>& triangle_indices) {
        build_dcel(vertex_positions, triangle_indices);
    }

    Mesh::Mesh(const std::vector<std::shared_ptr<Vertex>>& vertices,
               const std::vector<std::shared_ptr<HalfEdge>>& edges,
               const std::vector<std::shared_ptr<Face>>& faces) {
        m_vertices = vertices;
        m_edges = edges;
        m_faces = faces;

        // Adjust pointers in features
        for (const auto& v : m_vertices) {
            v->m_mesh = this;
        }
        for (const auto& e : m_edges) {
            e->m_mesh = this;
        }
        for (const auto& f : m_faces) {
            f->m_mesh = this;
        }
    }

    void Mesh::add_vertex(std::shared_ptr<Vertex>&& vertex) {
        vertex->m_mesh = this;
        vertex->m_id = m_vertices.size();
        m_vertices.push_back(std::move(vertex));
    }

    void Mesh::add_face(std::shared_ptr<Face> &&face) {
        face->m_mesh = this;
        face->m_id = m_faces.size();
        m_faces.push_back(std::move(face));
    }

    void Mesh::add_half_edge(std::shared_ptr<HalfEdge>&& hedge) {
        hedge->m_mesh = this;
        hedge->m_id = m_edges.size();
        m_edges.push_back(std::move(hedge));
    }

    void Mesh::clear() {
        m_vertices.clear();
        m_faces.clear();
        m_edges.clear();
    }

    // Copying and moving
    Mesh::Mesh(const Mesh& other) {
        copy_from(other);
    }

    Mesh::Mesh(Mesh&& other) noexcept {
        move_from(std::move(other));
    }

    Mesh& Mesh::operator=(const Mesh& other) {
        if (this != &other) {
            clear();
            copy_from(other);
        }
        return *this;
    }

    Mesh& Mesh::operator=(Mesh&& other) noexcept {
        if (this != &other) {
            clear();
            move_from(std::move(other));
        }
        return *this;
    }

    void Mesh::copy_from(const Mesh& other) {
        // Maps to keep track of old to new pointers
        std::unordered_map<std::shared_ptr<Vertex>, std::shared_ptr<Vertex>> vertex_map;
        std::unordered_map<std::shared_ptr<Face>, std::shared_ptr<Face>> face_map;
        std::unordered_map<std::shared_ptr<HalfEdge>, std::shared_ptr<HalfEdge>> edge_map;

        // Copy vertices
        m_vertices.reserve(other.m_vertices.size());
        for (const auto& v : other.m_vertices) {
            auto new_vertex = std::make_shared<Vertex>(*v);
            vertex_map[v] = new_vertex;
            m_vertices.push_back(std::move(new_vertex));
        }

        // Copy faces
        m_faces.reserve(other.m_faces.size());
        for (const auto& f : other.m_faces) {
            auto new_face = std::make_shared<Face>(*f);
            face_map[f] = new_face;
            m_faces.push_back(std::move(new_face));
        }

        // Copy half-edges
        m_edges.reserve(other.m_edges.size());
        for (const auto& he : other.m_edges) {
            auto new_he = std::make_shared<HalfEdge>(*he);
            edge_map[he] = new_he;
            m_edges.push_back(std::move(new_he));
        }

        // Adjust pointers in vertices
        for (const auto& v : m_vertices) {
            v->m_mesh = this;
            v->m_edges.clear();
            for (const auto& old_edge : other.vertex(v->id()).lock()->m_edges) {
                v->m_edges.push_back(edge_map[old_edge.lock()]);
            }
        }

        // Adjust pointers in faces
        for (const auto& f : m_faces) {
            f->m_mesh = this;
            f->m_edges.clear();
            for (const auto& old_edge : other.face(f->id()).lock()->m_edges) {
                f->m_edges.push_back(edge_map[old_edge.lock()]);
            }
        }

        // Adjust pointers in half-edges
        for (const auto& he : m_edges) {
            he->m_mesh = this;
            const auto old_he = other.half_edge(he->id()).lock();
            he->m_start = vertex_map[old_he->m_start.lock()];
            he->m_end = vertex_map[old_he->m_end.lock()];
            he->m_twin = edge_map[old_he->m_twin.lock()];
            he->m_next = edge_map[old_he->m_next.lock()];
            he->m_prev = edge_map[old_he->m_prev.lock()];
            he->m_face = face_map[old_he->m_face.lock()];
        }
    }

    void Mesh::move_from(Mesh&& other) {
        m_vertices = std::move(other.m_vertices);
        m_faces = std::move(other.m_faces);
        m_edges = std::move(other.m_edges);
    }

    std::weak_ptr<Vertex> Mesh::vertex(const size_t index) const {
        return m_vertices[index];
    }

    std::weak_ptr<Face> Mesh::face(const size_t index) const {
        return m_faces[index];
    }

    std::weak_ptr<HalfEdge> Mesh::half_edge(const size_t index) const {
        return m_edges[index];
    }

    std::optional<std::weak_ptr<HalfEdge>> Mesh::half_edge_from_vertices(const std::shared_ptr<Vertex>& start, const std::shared_ptr<Vertex>& end) const {
        if (!start || !end || start->mesh() != this || end->mesh() != this) {
            return std::nullopt;
        }

        for (const auto& edge : start->edges()) {
            if (edge.lock()->end()->id() == end->id()) {
                return edge;
            }
        }
        return std::nullopt;
    }

    std::optional<std::weak_ptr<Face>> Mesh::face_from_vertices(const std::shared_ptr<Vertex>& v1, const std::shared_ptr<Vertex>& v2, const std::shared_ptr<Vertex>& v3) const {
        if (!v1 || !v2 || !v3 || v1->mesh() != this || v2->mesh() != this || v3->mesh() != this) {
            return std::nullopt;
        }

        const auto edge1 = half_edge_from_vertices(v1, v2);
        if (!edge1) {
            return std::nullopt;
        }
        const auto edge2 = half_edge_from_vertices(v1, v3);
        if (!edge2) {
            return std::nullopt;
        }

        const auto edge1_face = edge1.value().lock()->face();
        const auto edge1_twin_face = edge1.value().lock()->twin()->face();
        const auto edge2_face = edge2.value().lock()->face();
        const auto edge2_twin_face = edge2.value().lock()->twin()->face();

        if (edge1_face == edge2_face || edge1_face == edge2_twin_face) {
            return face(edge1_face->id());
        }
        if (edge1_twin_face == edge2_face || edge1_twin_face == edge2_twin_face) {
            return face(edge1_twin_face->id());
        }

        return std::nullopt;
    }

    bool operator==(const Mesh& lhs, const Mesh& rhs) {
        // First compare sizes
        if (lhs.m_vertices.size() != rhs.m_vertices.size() ||
            lhs.m_faces.size() != rhs.m_faces.size() ||
            lhs.m_edges.size() != rhs.m_edges.size()) {
            return false;
        }

        // Compare vertices
        for (size_t i = 0; i < lhs.m_vertices.size(); ++i) {
            // Assuming Vertex has its own operator== for meaningful comparison
            if (*lhs.m_vertices[i] != *rhs.m_vertices[i]) {
                return false;
            }
        }

        // Compare faces
        for (size_t i = 0; i < lhs.m_faces.size(); ++i) {
            if (*lhs.m_faces[i] != *rhs.m_faces[i]) {
                return false;
            }
        }

        // Compare half-edges
        for (size_t i = 0; i < lhs.m_edges.size(); ++i) {
            if (*lhs.m_edges[i] != *rhs.m_edges[i]) {
                return false;
            }
        }

        return true;
    }

    bool operator!=(const Mesh& lhs, const Mesh& rhs) {
        return !(lhs == rhs);
    }

    void Mesh::set_transform(const mat4x4& transform) {
        m_transform = transform;
        clear_cache();
    }

    vec3 Mesh::get_vertex_position_ws(size_t vertex_id) const {
        {
            std::shared_lock lock(m_pos_cache_mutex); // Shared lock for reading
            if (const auto it = m_pos_cache.find(vertex_id); it != m_pos_cache.end()) {
                return it->second;
            }
        }
        const vec4 globalPos = transform() * vec4(vertex(vertex_id).lock()->position(), 1.0f);
        {
            std::unique_lock lock(m_pos_cache_mutex); // Unique lock for writing
            // Double-checked locking to prevent race conditions
            if (auto it = m_pos_cache.find(vertex_id); it == m_pos_cache.end()) {
                m_pos_cache[vertex_id] = vec3(globalPos);
            } else {
                return it->second;
            }
        }
        return globalPos;
    }


    void Mesh::clear_cache() const {
        std::unique_lock lock(m_pos_cache_mutex);
        m_pos_cache.clear();
    }

    std::string Mesh::get_debug_data(std::string name) const {
        // Initialize the string for vertices
        std::string vertices_str = "Vertices: " + name + " = [";
        for (const auto& vertex_weak : vertices()) {
            if (auto vertex = vertex_weak.lock()) { // Safely lock the weak_ptr
                vertices_str += "("
                             + std::to_string(vertex->position_ws().x) + ", "
                             + std::to_string(vertex->position_ws().y) + ", "
                             + std::to_string(vertex->position_ws().z) + "), ";
            } else {
                vertices_str += "(expired), ";
            }
        }

        // Print out all edges in format "[(P[i] - P[j]) * t + P[i], (P[k] - P[l]) * t + P[k], ...]" where i, j, k, ... are indices of vertices indexed from 1 (i.e. shifted by one)
        std::string edges_str = "Edges: [";
        for (const auto& edge_weak : half_edges()) {
            if (const auto edge = edge_weak.lock()) { // Safely lock the weak_ptr
                // Get the start and end Vertex* pointers
                auto start_vertex_id = edge->start()->id();
                auto end_vertex = edge->end()->id();
                edges_str += std::format("({}[{}] - {}[{}]) * t + {}[{}], ", name, end_vertex + 1, name, start_vertex_id + 1, name, start_vertex_id + 1);
            } else {
                edges_str += "(expired), ";
            }
        }

        // Clean up the trailing comma and space, then close the brackets
        if (vertices_str.size() > 10) { // "Vertices: [".size() == 10
            vertices_str.pop_back();
            vertices_str.pop_back();
        }
        vertices_str += "]";

        if (edges_str.size() > 7) { // "Edges: [".size() == 7
            edges_str.pop_back();
            edges_str.pop_back();
        }
        edges_str += "]";

        // Combine and return the debug data
        return vertices_str + "\n" + edges_str;
    }

}
