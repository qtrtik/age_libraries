#include <array>
#include <execution>
#include <math/geometry/shape.hpp>

#include <math/geometry/mesh.hpp>

#include "math/geometry/ray.hpp"
#include "phx/index.hpp"
#include "phx/collisions/collision_system.hpp"
#include "utils/development.hpp"

namespace geometry {
    void Shape::generate_mesh() const {
        const auto vertices = generate_vertices();
        const auto indices = generate_indices();
        m_mesh = std::make_unique<Mesh>(vertices, indices);
    }

    Mesh* Shape::mesh() const {
        if (!m_mesh) {
            generate_mesh();
        }
        return m_mesh.get();
    }

    Mesh* Shape::try_get_mesh() const {
        if (!m_mesh) {
            return nullptr;
        }
        return m_mesh.get();
    }

    // Sphere
    vec3 Sphere::support(vec3 direction) const {
        return m_radius * normalize(direction);
    }

    std::vector<vec3> Sphere::generate_vertices() const {
        ASSUMPTION(DETAIL >= 3 && VERTICAL_DETAIL >= 1);
        ASSUMPTION(m_radius > 0.0f);
        std::vector<vec3> vertices;
        vertices.reserve(VERTEX_COUNT);

        // Top vertex
        vertices.emplace_back(0.0f, 0.0f, m_radius);

        // Generate vertices for each stack and slice
        for (size_t stack = 0; stack < VERTICAL_DETAIL; stack++) {
            const auto phi = Num::pi * static_cast<scalar>(stack + 1) / (VERTICAL_DETAIL + 1);
            for (size_t slice = 0; slice < DETAIL; slice++) {
                const auto theta = Num::two_pi * static_cast<scalar>(slice) / DETAIL;

                // Spherical to Cartesian conversion
                const auto x = sin(phi) * cos(theta) * m_radius;
                const auto y = sin(phi) * sin(theta) * m_radius;
                const auto z = cos(phi) * m_radius;

                vertices.emplace_back(x, y, z);
            }
        }

        // Bottom vertex
        vertices.emplace_back(0.0f, 0.0f, -m_radius);

        ASSUMPTION(vertices.size() == VERTEX_COUNT);

        return vertices;
    }

    std::vector<std::array<size_t, 3>> Sphere::generate_indices() const {
        ASSUMPTION(DETAIL >= 3 && VERTICAL_DETAIL >= 1);
        std::vector<std::array<size_t, 3>> triangles;
        triangles.reserve(INDEX_COUNT / 3);

        // Generate triangles for top
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const size_t current = slice + 1;
            const size_t next = (slice + 1) % DETAIL + 1;
            triangles.emplace_back(std::array{TOP_VERTEX_INDEX, current, next});
        }

        // Generate triangles for stacks
        for (unsigned int stack = 0; stack < VERTICAL_DETAIL - 1; stack++) {
            const size_t top = stack * DETAIL + 1;
            const size_t bottom = (stack + 1) * DETAIL + 1;
            for (unsigned int slice = 0; slice < DETAIL; slice++) {
                const size_t current = top + slice;
                const size_t next = top + (slice + 1) % DETAIL;
                const size_t below = bottom + slice;
                const size_t below_next = bottom + (slice + 1) % DETAIL;

                triangles.emplace_back(std::array{current, below, next});
                triangles.emplace_back(std::array{below, below_next, next});
            }
        }

        // Generate triangles for bottom
        for (size_t slice = 0; slice < DETAIL; slice++) {
            size_t current = BOTTOM_VERTEX_INDEX - DETAIL + slice;
            size_t next = (slice + 1) % DETAIL + BOTTOM_VERTEX_INDEX - DETAIL;
            triangles.emplace_back(std::array{BOTTOM_VERTEX_INDEX, next, current});
        }

        ASSUMPTION(triangles.size() == INDEX_COUNT / 3);

        return triangles;
    }

    AABB Sphere::calculate_aabb(mat4x4 transform) const {
        const auto scale = vec3{m_radius};
        const auto min = -scale;
        const auto max = scale;

        const auto origin = vec3(column(transform, 3));

        return AABB{origin + min, origin + max};
    }

    std::set<RayIntersection> Sphere::ray_cast(Ray ray, mat4x4 transform) const {
        // Transform the center of the sphere
        const auto center = vec3(column(transform, 3));
        const auto origin = ray.origin() - center;

        const auto b = dot(origin, ray.direction());
        const auto c = dot(origin, origin) - m_radius * m_radius;

        // Check if ray's origin is outside sphere and is pointing away
        if (c > 0.0f && b > 0.0f) {
            return {};
        }

        // Negative discriminant means ray misses sphere
        const auto discriminant = b * b - c;
        if (discriminant < 0.0f) {
            return {};
        }

        // Intersection case
        const auto t1 = (-b - sqrt(discriminant)) / ray.length();
        const auto t2 = (-b + sqrt(discriminant)) / ray.length();

        std::set<RayIntersection> result;
        if (t1 >= 0.0f && t1 <= 1.0f) {
            const auto point = ray.at(t1);
            auto normal = normalize(point - center);

            if (dot(normal, ray.direction()) > 0.0f) {
                normal = -normal;
            }

            result.emplace(ParametrisedPoint{point, t1}, normal);
        }
        if (t2 >= 0.0f && t2 <= 1.0f) {
            const auto point = ray.at(t2);
            auto normal = normalize(point - center);

            if (dot(normal, ray.direction()) > 0.0f) {
                normal = -normal;
            }

            result.emplace(ParametrisedPoint{point, t2}, normal);
        }

        return result;
    }

    // Box
    vec3 Box::support(vec3 direction) const {
        const auto half_extents = m_half_extents;
        const auto sign_no_zero = [](const auto& value) {
            return value < 0.0f ? -1.0f : 1.0f;
        };
        return vec3{half_extents.x * sign_no_zero(direction.x),
                    half_extents.y * sign_no_zero(direction.y),
                    half_extents.z * sign_no_zero(direction.z)};
    }

    AABB Box::calculate_aabb(mat4x4 transform) const {
        return calculate_aabb_from_obb(m_half_extents, transform);
    }

    std::vector<vec3> Box::generate_vertices() const {
        std::vector<vec3> vertices;
        vertices.reserve(VERTEX_COUNT);

        // Generate vertices
        for (size_t i = 0; i < 8; i++) {
            const auto x = (i & 1) ? m_half_extents.x : -m_half_extents.x;
            const auto y = (i & 2) ? m_half_extents.y : -m_half_extents.y;
            const auto z = (i & 4) ? m_half_extents.z : -m_half_extents.z;
            vertices.emplace_back(x, y, z);
        }

        ASSUMPTION(vertices.size() == VERTEX_COUNT);

        return vertices;
    }

    std::vector<std::array<size_t, 3>> Box::generate_indices() const {
        std::vector<std::array<size_t, 3>> triangles;
        triangles.reserve(INDEX_COUNT / 3);

        // Front
        triangles.emplace_back(std::array<size_t, 3>{0, 2, 1});
        triangles.emplace_back(std::array<size_t, 3>{1, 2, 3});

        // Right
        triangles.emplace_back(std::array<size_t, 3>{1, 3, 5});
        triangles.emplace_back(std::array<size_t, 3>{5, 3, 7});

        // Back
        triangles.emplace_back(std::array<size_t, 3>{5, 7, 4});
        triangles.emplace_back(std::array<size_t, 3>{4, 7, 6});

        // Left
        triangles.emplace_back(std::array<size_t, 3>{4, 6, 0});
        triangles.emplace_back(std::array<size_t, 3>{0, 6, 2});

        // Top
        triangles.emplace_back(std::array<size_t, 3>{2, 6, 3});
        triangles.emplace_back(std::array<size_t, 3>{3, 6, 7});

        // Bottom
        triangles.emplace_back(std::array<size_t, 3>{4, 0, 5});
        triangles.emplace_back(std::array<size_t, 3>{5, 0, 1});

        ASSUMPTION(triangles.size() == INDEX_COUNT / 3);

        return triangles;
    }

    std::set<RayIntersection> Box::ray_cast(Ray ray, mat4x4 transform) const {
        const auto inverse_transform = inverse(transform);
        const auto local_ray = ray.transform(inverse_transform);
        const auto inverse_direction = 1.0f / local_ray.length();

        const auto& dir = local_ray.direction_unscaled();
        const auto& origin = local_ray.origin();

        auto t_min = 0.0f;
        auto t_max = 1.0f;

        auto t_min_axis = -1;
        auto t_max_axis = -1;

        for (int i = 0; i < 3; ++i) {
            if (dir[i] == 0.0f) {
                // Ray is parallel to slab. No hit if origin not within slab
                if (origin[i] < lo()[i] || origin[i] > hi()[i]) {
                    return {}; // No intersection
                }
                // Else, the ray is parallel and inside the slab; continue
            } else {
                const auto invD = 1.0f / dir[i];
                auto t1 = (lo()[i] - origin[i]) * invD;
                auto t2 = (hi()[i] - origin[i]) * invD;

                if (invD < 0.0f) std::swap(t1, t2);

                if (t1 >= t_min - Num::epsilon) {
                    t_min = t1;
                    t_min_axis = i;
                }

                if (t2 <= t_max + Num::epsilon) {
                    t_max = t2;
                    t_max_axis = i;
                }

                if (t_max < t_min) return {}; // No intersection
            }
        }

        if (t_min_axis == -1 || t_max_axis == -1) {
            return {}; // No intersection
        }

        // Intersection case
        std::set<RayIntersection> result;

        // Rotate the normal back to world space
        auto t_min_normal = glm::vec3(0.0f);
        auto t_max_normal = glm::vec3(0.0f);

        t_min_normal[t_min_axis] = dir[t_min_axis] < 0.0f ? 1.0f : -1.0f;
        t_max_normal[t_max_axis] = dir[t_max_axis] < 0.0f ? 1.0f : -1.0f;

        // Transform the normals back to world space
        t_min_normal = normalize(vec3(transform * vec4{t_min_normal, 0.0f}));
        t_max_normal = normalize(vec3(transform * vec4{t_max_normal, 0.0f}));

        if (t_min >= 0.0f - Num::epsilon && t_min <= 1.0f + Num::epsilon) {
            result.emplace(ParametrisedPoint{ray.at(t_min), t_min}, t_min_normal);
        }
        if (t_max >= 0.0f - Num::epsilon && t_max <= 1.0f + Num::epsilon && t_max != t_min) {
            result.emplace(ParametrisedPoint{ray.at(t_max), t_max}, t_max_normal);
        }

        return result;
    }

    // Cylinder
    vec3 Cylinder::support(vec3 direction) const {
        const auto z = m_height / 2.0f * (direction.z < 0.0f ? -1.0f : 1.0f);
        const auto xy_dir = vec3{direction.x, direction.y, 0.0f};
        if (len(xy_dir) < Num::epsilon) {
            return vec3{0.0f, 0.0f, z};
        }
        const auto xy = m_radius * normalize(xy_dir);

        return vec3{xy.x, xy.y, z};
    }

    AABB Cylinder::calculate_aabb(mat4x4 transform) const {
        const auto a = vec3(transform * vec4{0.0f, 0.0f, 0.5f * m_height, 1.0f});
        const auto b = vec3(transform * vec4{0.0f, 0.0f, -0.5f * m_height, 1.0f});

        const auto v = b - a;
        const auto lengthSq = v.x * v.x + v.y * v.y + v.z * v.z;

        // Handle degenerate case
        if (lengthSq < Num::epsilon_sq) {
            return AABB{
                {a.x - m_radius, a.y - m_radius, a.z - m_radius},
                {a.x + m_radius, a.y + m_radius, a.z + m_radius}
            };
        }

        const auto invLength = 1.0f / sqrt(lengthSq);
        const auto u = v * invLength;
        const vec3 d{
            m_radius * sqrt(1.0f - u.x * u.x),
            m_radius * sqrt(1.0f - u.y * u.y),
            m_radius * sqrt(1.0f - u.z * u.z)
        };

        return AABB{
        {min(a.x, b.x) - d.x, min(a.y, b.y) - d.y, min(a.z, b.z) - d.z},
        {max(a.x, b.x) + d.x, max(a.y, b.y) + d.y, max(a.z, b.z) + d.z}
        };
    }

    std::vector<vec3> Cylinder::generate_vertices() const {
        ASSUMPTION(DETAIL >= 3);
        ASSUMPTION(m_radius > 0.0f);
        std::vector<vec3> vertices;
        vertices.reserve(VERTEX_COUNT);

        // Top vertex
        vertices.emplace_back(0.0f, 0.0f, m_height / 2.0f);

        // Top stack
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const auto theta = Num::two_pi * static_cast<scalar>(slice) / DETAIL;

            const auto x = cos(theta) * m_radius;
            const auto y = sin(theta) * m_radius;
            const auto z = m_height / 2.0f;

            vertices.emplace_back(x, y, z);
        }

        // Bottom stack
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const auto theta = Num::two_pi * static_cast<scalar>(slice) / DETAIL;

            const auto x = cos(theta) * m_radius;
            const auto y = sin(theta) * m_radius;
            const auto z = -m_height / 2.0f;

            vertices.emplace_back(x, y, z);
        }

        // Bottom vertex
        vertices.emplace_back(0.0f, 0.0f, -m_height / 2.0f);

        ASSUMPTION(vertices.size() == VERTEX_COUNT);

        return vertices;
    }

    std::vector<std::array<size_t, 3>> Cylinder::generate_indices() const {
        std::vector<std::array<size_t, 3>> triangles;
        triangles.reserve(INDEX_COUNT / 3);

        // Generate triangles for top face
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const size_t current = slice + 1;
            const size_t next = (slice + 1) % DETAIL + 1;
            triangles.emplace_back(std::array{TOP_VERTEX_INDEX, current, next});
        }

        // Generate triangles for the sides
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const size_t top = slice + 1;
            const size_t next = (slice + 1) % DETAIL + 1;
            const size_t bottom = slice + DETAIL + 1;
            const size_t next_bottom = (slice + 1) % DETAIL + DETAIL + 1;

            triangles.emplace_back(std::array{top, bottom, next});
            triangles.emplace_back(std::array{bottom, next_bottom, next});
        }

        // Generate triangles for the bottom face
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const size_t current = slice + DETAIL + 1;
            const size_t next = (slice + 1) % DETAIL + DETAIL + 1;
            triangles.emplace_back(std::array{BOTTOM_VERTEX_INDEX, next, current});
        }

        ASSUMPTION(triangles.size() == INDEX_COUNT / 3);

        return triangles;
    }

    std::set<RayIntersection> Cylinder::ray_cast(Ray ray, mat4x4 transform) const {
        const auto p_h = transform * vec4{0.0f, 0.0f, m_height / 2.0f, 1.0f}; // Top of the cylinder
        const auto q_h = transform * vec4{0.0f, 0.0f, -m_height / 2.0f, 1.0f}; // Bottom of the cylinder

        // Cap points of cylinder
        const auto p = vec3(p_h) / p_h.w;
        const auto q = vec3(q_h) / q_h.w;

        const auto d = q - p;
        const auto d_normalized = normalize(d);
        const auto m = ray.from() - p;
        const auto n = ray.direction_unscaled();

        const auto md = dot(m, d);
        const auto nd = dot(n, d);
        const auto dd = dot(d, d);

        // Check if ray is fully outside either endcap of the cylinder
        if ((md < 0.0f && md + nd < 0.0f) || (md > dd && md + nd > dd)) {
            return {}; // No intersection
        }

        const auto nn = dot(n, n);
        const auto mn = dot(m, n);

        const auto a = dd * nn - nd * nd;
        const auto k = dot(m, m) - (m_radius * m_radius);
        const auto c = dd * k - md * md;

        std::set<RayIntersection> intersections;

        if (abs(a) < Num::epsilon) {
            // Ray runs parallel to the cylinder axis 'd'
            if (c > 0.0f) {
                return {}; // No intersection, ray is outside the cylinder
            }

            // Ray starts inside the cylinder
            scalar t;
            vec3 normal;
            if (md < 0.0f) {
                // Intersects the cylinder at the top
                t = -mn / nn;
                normal = normalize(d);
            } else if (md > dd) {
                // Intersects the cylinder at the bottom
                t = (nd - mn) / nn;
                normal = normalize(-d);
            } else {
                // Doesn't intersect the caps
                return {}; // No intersection
            }

            return {{{ray.at(t), t}, normal}};
        }

        const scalar b = dd * mn - nd * md;

        if (const scalar discriminant = b * b - a * c; discriminant >= 0.0f) {
            const scalar sqrt_discriminant = sqrt(discriminant);
            auto t0 = (-b - sqrt_discriminant) / a;
            auto t1 = (-b + sqrt_discriminant) / a;

            // Order the roots
            if (t0 > t1) std::swap(t0, t1);

            // Check first root
            if (t0 >= 0.0f && t0 <= 1.0f) {
                scalar z = md + t0 * nd;
                if (z >= 0.0f && z <= dd) {
                    vec3 point = ray.at(t0);

                    // Calculate the normal
                    vec3 p_point = point - p;
                    vec3 proj_pq = dot(p_point, d_normalized) * d_normalized;
                    vec3 normal = normalize(p_point - proj_pq);

                    // Flip the normal if it's pointing in the wrong direction
                    if (dot(normal, n) > 0.0f) {
                        normal = -normal;
                    }

                    intersections.insert({{point, t0}, normal});
                }
            }

            // Check second root
            if (t1 >= 0.0f && t1 <= 1.0f) {
                scalar z = md + t1 * nd;
                if (z >= 0.0f && z <= dd) {
                    vec3 point = ray.at(t1);

                    // Calculate the normal
                    vec3 p_point = point - p;
                    vec3 proj_pq = dot(p_point, d_normalized) * d_normalized;
                    vec3 normal = normalize(p_point - proj_pq);

                    // Flip the normal if it's pointing in the wrong direction
                    if (dot(normal, n) > 0.0f) {
                        normal = -normal;
                    }

                    intersections.insert({{point, t1}, normal});
                }
            }
        }

        // Intersection with Caps

        // Bottom Cap (q)
        scalar denominator = dot(n, d_normalized);
        if (abs(denominator) > Num::epsilon) { // Avoid division by zero
            auto t_cap_bottom = dot(q - ray.from(), d_normalized) / denominator;
            if (t_cap_bottom >= 0.0f - Num::epsilon && t_cap_bottom <= 1.0f + Num::epsilon) {
                auto point = ray.at(t_cap_bottom);
                if (length(point - q) <= m_radius) {
                    auto normal = normalize(-d);

                    // Flip the normal if it's pointing in the wrong direction
                    if (dot(normal, n) > 0.0f) {
                        normal = -normal;
                    }

                    intersections.insert({{point, t_cap_bottom}, normal});
                }
            }
        }

        // Top Cap (p)
        denominator = dot(n, d_normalized);
        if (abs(denominator) > Num::epsilon) { // Avoid division by zero
            auto t_cap_top = dot(p - ray.from(), d_normalized) / denominator;
            if (t_cap_top >= 0.0f - Num::epsilon && t_cap_top <= 1.0f + Num::epsilon) {
                auto point = ray.at(t_cap_top);
                if (length(point - p) <= m_radius) {
                    auto normal = normalize(d);

                    // Flip the normal if it's pointing in the wrong direction
                    if (dot(normal, n) > 0.0f) {
                        normal = -normal;
                    }

                    intersections.insert({{point, t_cap_top}, normal});
                }
            }
        }

        return intersections;
    }

    // Cone
    vec3 Cone::support(vec3 direction) const {
        // The threshold is the sine of the angle between the cone's axis and the cone's side
        const auto sin_theta = m_radius / sqrt(m_radius * m_radius + m_height * m_height);

        if (direction.z > sin_theta) {
            return vec3{0.0f, 0.0f, m_height / 2.0f};
        }

        const auto xy_dir = vec3{direction.x, direction.y, 0.0f};
        if (len(xy_dir) < Num::epsilon) {
            return vec3{0.0f, 0.0f, -m_height / 2.0f};
        }

        const auto xy = m_radius * normalize(xy_dir);

        return vec3{xy.x, xy.y, -m_height / 2.0f};
    }

    AABB Cone::calculate_aabb(mat4x4 transform) const {
        const auto a = vec3(transform * vec4{0.0f, 0.0f, 0.5f * m_height, 1.0f});  // top point
        const auto b = vec3(transform * vec4{0.0f, 0.0f, -0.5f * m_height, 1.0f}); // bottom point

        const auto v = b - a;
        const auto lengthSq = v.x * v.x + v.y * v.y + v.z * v.z;

        // Handle degenerate case
        if (lengthSq < Num::epsilon_sq) {
            return AABB{
                {a.x - m_radius, a.y - m_radius, a.z - m_radius},
                {a.x + m_radius, a.y + m_radius, a.z + m_radius}
            };
        }

        const auto invLength = 1.0f / sqrt(lengthSq);
        const auto u = v * invLength;

        const vec3 d_bottom{
            m_radius * sqrt(1.0f - u.x * u.x),
            m_radius * sqrt(1.0f - u.y * u.y),
            m_radius * sqrt(1.0f - u.z * u.z)
        };

        // For each axis, we need to consider both radii when calculating the bounds
        return AABB{
            {
                min(a.x, b.x - d_bottom.x),
                min(a.y, b.y - d_bottom.y),
                min(a.z, b.z - d_bottom.z)
            },
            {
                max(a.x, b.x + d_bottom.x),
                max(a.y, b.y + d_bottom.y),
                max(a.z, b.z + d_bottom.z)
            }
        };
    }

    std::set<RayIntersection> Cone::ray_cast(Ray ray, mat4x4 transform) const {
        const auto p_h = transform * vec4{0.0f, 0.0f, m_height / 2.0f, 1.0f}; // Apex of the cone
        const auto q_h = transform * vec4{0.0f, 0.0f, -m_height / 2.0f, 1.0f}; // Base of the cone

        // Cap points of cone
        const auto p = vec3(p_h) / p_h.w;
        const auto q = vec3(q_h) / q_h.w;

        const auto d = q - p;
        const auto d_norm = normalize(d);
        const auto m = ray.from() - p;
        const auto n = ray.direction_unscaled();

        // Calculate the square cosine of the half angle
        const auto cos_sq_theta = (m_height * m_height) / (m_radius * m_radius + m_height * m_height);

        const auto md = dot(m, d);
        const auto nd = dot(n, d);
        const auto dd = dot(d, d);

        // Check if ray is fully outside either endcap of the cone
        if ((md < 0.0f && md + nd < 0.0f) || (md > dd && md + nd > dd)) {
            return {}; // No intersection
        }

        const auto nn = dot(n, n);
        const auto mn = dot(m, n);
        const auto mm = dot(m, m);
        const auto nd_norm = dot(n, d_norm);
        const auto md_norm = dot(m, d_norm);

        const auto a = nd_norm * nd_norm - nn * cos_sq_theta;
        const auto b = 2.0f * (nd_norm * md_norm - mn * cos_sq_theta);
        const auto c = md_norm * md_norm - mm * cos_sq_theta;

        std::set<RayIntersection> intersections;

        if (abs(a) < Num::epsilon) {
            if (abs(b) < Num::epsilon) {
                return {};
            }
            // Single intersection point
            scalar t = -c / b;
            if (t >= 0.0f && t <= 1.0f) {
                scalar z = md_norm + t * nd_norm;
                if (z >= 0.0f && z <= m_height) {
                    vec3 point = ray.at(t);

                    // Calculate normal
                    vec3 p_point = point - p;
                    vec3 proj_pq = dot(p_point, d_norm) * d_norm;
                    vec3 point_direction = normalize(p_point - proj_pq);
                    vec3 base_point = q + point_direction * m_radius;
                    vec3 apex_base_point = base_point - p;
                    vec3 base_perp = normalize(cross(d_norm, point_direction));

                    vec3 normal = normalize(cross(apex_base_point, base_perp));
                    normal = dot(normal, n) < 0.0f ? normal : -normal;

                    if (dot(normal, n) > 0.0f) {
                        normal = -normal;
                    }

                    intersections.insert({{point, t}, normal});
                }
            }
        } else {
            const scalar discriminant = b * b - 4.0f * a * c;
            if (discriminant >= 0.0f) {
                const scalar sqrt_discriminant = sqrt(discriminant);
                scalar t0 = (-b - sqrt_discriminant) / (2.0f * a);
                scalar t1 = (-b + sqrt_discriminant) / (2.0f * a);

                // Order the roots
                if (t0 > t1) std::swap(t0, t1);

                // Check first root
                if (t0 >= 0.0f && t0 <= 1.0f) {
                    scalar z = md_norm + t0 * nd_norm;
                    if (z >= 0.0f && z <= m_height) {
                        vec3 point = ray.at(t0);

                        // Calculate normal
                        vec3 p_point = point - p;
                        vec3 proj_pq = dot(p_point, d_norm) * d_norm;
                        vec3 point_direction = normalize(p_point - proj_pq);
                        vec3 base_point = q + point_direction * m_radius;
                        vec3 apex_base_point = base_point - p;
                        vec3 base_perp = normalize(cross(d_norm, point_direction));

                        vec3 normal = normalize(cross(apex_base_point, base_perp));
                        normal = dot(normal, n) < 0.0f ? normal : -normal;

                        if (dot(normal, n) > 0.0f) {
                            normal = -normal;
                        }

                        intersections.insert({{point, t0}, normal});
                    }
                }

                // Check second root
                if (t1 >= 0.0f && t1 <= 1.0f) {
                    scalar z = md_norm + t1 * nd_norm;
                    if (z >= 0.0f && z <= m_height) {
                        vec3 point = ray.at(t1);

                        // Calculate normal
                        vec3 p_point = point - p;
                        vec3 proj_pq = dot(p_point, d_norm) * d_norm;
                        vec3 point_direction = normalize(p_point - proj_pq);
                        vec3 base_point = q + point_direction * m_radius;
                        vec3 apex_base_point = base_point - p;
                        vec3 base_perp = normalize(cross(d_norm, point_direction));

                        vec3 normal = normalize(cross(apex_base_point, base_perp));
                        normal = dot(normal, n) < 0.0f ? normal : -normal;

                        if (dot(normal, n) > 0.0f) {
                            normal = -normal;
                        }

                        intersections.insert({{point, t1}, normal});
                    }
                }
            }
        }

        // Intersection with Base Cap
        // Plane of the base cap is perpendicular to v and passes through q
        if (abs(nd_norm) > Num::epsilon) {
            scalar t_cap = dot(q - ray.from(), d_norm) / nd_norm;
            if (t_cap >= 0.0f - Num::epsilon && t_cap <= 1.0f + Num::epsilon) {
                vec3 point = ray.at(t_cap);
                vec3 v_to_point = point - q;
                scalar dist_sq = dot(v_to_point, v_to_point);
                vec3 normal = normalize(d);

                if (dot(normal, n) > 0.0f) {
                    normal = -normal;
                }

                if (dist_sq <= m_radius * m_radius) {
                    intersections.insert({{point, t_cap}, normal});
                }
            }
        }

        return intersections;
    }

    std::vector<vec3> Cone::generate_vertices() const {
        ASSUMPTION(DETAIL >= 3);
        ASSUMPTION(m_radius > 0.0f);
        std::vector<vec3> vertices;
        vertices.reserve(VERTEX_COUNT);

        // Top vertex
        vertices.emplace_back(0.0f, 0.0f, m_height / 2.0f);

        // Bottom stack
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const auto theta = Num::two_pi * static_cast<scalar>(slice) / DETAIL;

            const auto x = cos(theta) * m_radius;
            const auto y = sin(theta) * m_radius;
            const auto z = -m_height / 2.0f;

            vertices.emplace_back(x, y, z);
        }

        // Bottom vertex
        vertices.emplace_back(0.0f, 0.0f, -m_height / 2.0f);

        ASSUMPTION(vertices.size() == VERTEX_COUNT);

        return vertices;
    }

    std::vector<std::array<size_t, 3>> Cone::generate_indices() const {
        std::vector<std::array<size_t, 3>> triangles;
        triangles.reserve(INDEX_COUNT / 3);

        // Generate triangles for top face
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const size_t current = slice + 1;
            const size_t next = (slice + 1) % DETAIL + 1;
            triangles.emplace_back(std::array{TOP_VERTEX_INDEX, current, next});
        }

        // Generate triangles for the bottom face
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const size_t current = slice + 1;
            const size_t next = (slice + 1) % DETAIL + 1;
            triangles.emplace_back(std::array{BOTTOM_VERTEX_INDEX, next, current});
        }

        ASSUMPTION(triangles.size() == INDEX_COUNT / 3);

        return triangles;
    }

    vec3 Capsule::support(vec3 direction) const {
        const auto height_without_caps = m_height - 2.0f * m_radius;
        const auto direction_z_sign = direction.z < 0.0f ? -1.0f : 1.0f;
        return m_radius * normalize(direction) + vec3{0.0f, 0.0f, height_without_caps / 2.0f * direction_z_sign};
    }

    AABB Capsule::calculate_aabb(mat4x4 transform) const {
        const auto height_without_caps = m_height - 2.0f * m_radius;

        // Calculate the AABB of the two spheres
        const auto a = vec3(transform * vec4{0.0f, 0.0f, 0.5f * height_without_caps, 1.0f});  // top point
        const auto b = vec3(transform * vec4{0.0f, 0.0f, -0.5f * height_without_caps, 1.0f}); // bottom point

        const auto aabb_top_sphere = AABB{
            {a.x - m_radius, a.y - m_radius, a.z - m_radius},
            {a.x + m_radius, a.y + m_radius, a.z + m_radius}
        };

        const auto aabb_bottom_sphere = AABB{
            {b.x - m_radius, b.y - m_radius, b.z - m_radius},
            {b.x + m_radius, b.y + m_radius, b.z + m_radius}
        };

        // Combine the AABBs
        return AABB::merge(aabb_top_sphere, aabb_bottom_sphere);
    }

    std::set<RayIntersection> Capsule::ray_cast(Ray ray, mat4x4 transform) const {
        const auto p = transform * vec4{0.0f, 0.0f, (m_height) / 2.0f - m_radius, 1.0f}; // Center of top sphere
        const auto q = transform * vec4{0.0f, 0.0f, (-m_height) / 2.0f + m_radius, 1.0f}; // Center of bottom sphere

        // Create translate matrix from point
        const auto translate = [](const vec4& point) {
            return mat4x4{1.0f, 0.0f, 0.0f, 0.0f,
                          0.0f, 1.0f, 0.0f, 0.0f,
                          0.0f, 0.0f, 1.0f, 0.0f,
                          point.x, point.y, point.z, point.w};
        };

        auto translate_p = translate(p);
        auto translate_q = translate(q);

        std::set<RayIntersection> intersections;

        const auto height_without_caps = m_height - 2.0f * m_radius;

        // Create the two spheres
        const auto top_sphere = Sphere{m_radius};
        const auto bottom_sphere = Sphere{m_radius};

        // Ray-Sphere intersection with top sphere
        auto top_sphere_intersections = top_sphere.ray_cast(ray, translate_p);
        for (const auto& intersection : top_sphere_intersections) {
            intersections.insert(intersection);
        }

        auto bottom_sphere_intersections = bottom_sphere.ray_cast(ray, translate_q);
        for (const auto& intersection : bottom_sphere_intersections) {
            intersections.insert(intersection);
        }

        // Create the cylinder
        const auto cylinder = Cylinder{m_radius, height_without_caps};

        // Ray-Cylinder intersection
        auto cylinder_intersections = cylinder.ray_cast(ray, transform);
        for (const auto& intersection : cylinder_intersections) {
            intersections.insert(intersection);
        }

        // Pick the closest and furthest intersection points
        if (intersections.empty()) {
            return {};
        }
        if (intersections.size() == 1) {
            return intersections;
        }

        auto it = intersections.begin();
        auto rit = intersections.rbegin();

        return {*it, *rit};
    }

    std::vector<vec3> Capsule::generate_vertices() const {
        ASSUMPTION(DETAIL >= 3 && VERTICAL_DETAIL >= 1);
        ASSUMPTION(m_radius > 0.0f);
        std::vector<vec3> vertices;
        vertices.reserve(VERTEX_COUNT);

        const auto height_without_caps = m_height - 2.0f * m_radius;

        // Top vertex
        vertices.emplace_back(0.0f, 0.0f, m_height / 2.0f);

        // Generate vertices for each stack and slice
        for (size_t stack = 0; stack < 2 * VERTICAL_DETAIL; stack++) {
            const auto phi = Num::pi * static_cast<scalar>(stack + 1) / (2 * VERTICAL_DETAIL + 1);
            for (size_t slice = 0; slice < DETAIL; slice++) {
                const auto theta = Num::two_pi * static_cast<scalar>(slice) / DETAIL;

                const auto height_offset = height_without_caps / 2.0f * (stack < VERTICAL_DETAIL ? 1.0f : -1.0f);

                // Spherical to Cartesian conversion
                const auto x = sin(phi) * cos(theta) * m_radius;
                const auto y = sin(phi) * sin(theta) * m_radius;
                const auto z = cos(phi) * m_radius + height_offset;

                vertices.emplace_back(x, y, z);
            }
        }

        // Bottom vertex
        vertices.emplace_back(0.0f, 0.0f, -m_height / 2.0f);

        ASSUMPTION(vertices.size() == VERTEX_COUNT);

        return vertices;
    }

    std::vector<std::array<size_t, 3>> Capsule::generate_indices() const {
        ASSUMPTION(DETAIL >= 3 && VERTICAL_DETAIL >= 1);
        std::vector<std::array<size_t, 3>> triangles;
        triangles.reserve(INDEX_COUNT / 3);

        // Generate triangles for top
        for (size_t slice = 0; slice < DETAIL; slice++) {
            const size_t current = slice + 1;
            const size_t next = (slice + 1) % DETAIL + 1;
            triangles.emplace_back(std::array{TOP_VERTEX_INDEX, current, next});
        }

        // Generate triangles for stacks
        for (unsigned int stack = 0; stack < 2 * VERTICAL_DETAIL - 1; stack++) {
            const size_t top = stack * DETAIL + 1;
            const size_t bottom = (stack + 1) * DETAIL + 1;
            for (unsigned int slice = 0; slice < DETAIL; slice++) {
                const size_t current = top + slice;
                const size_t next = top + (slice + 1) % DETAIL;
                const size_t below = bottom + slice;
                const size_t below_next = bottom + (slice + 1) % DETAIL;

                triangles.emplace_back(std::array{current, below, next});
                triangles.emplace_back(std::array{below, below_next, next});
            }
        }

        // Generate triangles for bottom
        for (size_t slice = 0; slice < DETAIL; slice++) {
            size_t current = BOTTOM_VERTEX_INDEX - DETAIL + slice;
            size_t next = (slice + 1) % DETAIL + BOTTOM_VERTEX_INDEX - DETAIL;
            triangles.emplace_back(std::array{BOTTOM_VERTEX_INDEX, next, current});
        }

        ASSUMPTION(triangles.size() == INDEX_COUNT / 3);

        return triangles;
    }

    // Convex Hull
    ConvexHull::ConvexHull(const std::vector<vec3>& points, const std::vector<std::array<size_t, 3>>& faces)
        : m_points(points), m_faces(faces) {
        ASSUMPTION(convex_test(points, faces));
    }

    ConvexHull::ConvexHull(const std::vector<vec3>& points, const std::vector<uint32_t>& indices) : m_points(points) {
        const auto num_faces = indices.size() / 3;
        m_faces.reserve(num_faces);
        for (size_t i = 0; i < num_faces; i++) {
            const auto a = indices[i * 3];
            const auto b = indices[i * 3 + 1];
            const auto c = indices[i * 3 + 2];
            m_faces.push_back({a, b, c});
        }
        ASSUMPTION(convex_test(points, m_faces));
    }


    vec3 ConvexHull::support(vec3 direction) const {
        if (m_points.empty()) {
            return vec3{}; // Return zero vector for empty hull
        }

        return *std::ranges::max_element(m_points, [&](vec3 a, vec3 b) {
            return dot(a, direction) < dot(b, direction);
        });
    }

    AABB ConvexHull::calculate_aabb(mat4x4 transform) const {
        auto min_x = Num::max, max_x = Num::min;
        auto max_y = Num::min, min_y = Num::max;
        auto max_z = Num::min, min_z = Num::max;

        for (const auto& point : m_points) {
            const auto transformed = vec3(transform * vec4{point, 1.0f});
            min_x = min(min_x, transformed.x);
            max_x = max(max_x, transformed.x);
            min_y = min(min_y, transformed.y);
            max_y = max(max_y, transformed.y);
            min_z = min(min_z, transformed.z);
            max_z = max(max_z, transformed.z);
        }

        return AABB{{min_x, min_y, min_z}, {max_x, max_y, max_z}};
    }

    std::set<RayIntersection> ConvexHull::ray_cast(Ray ray, mat4x4 transform) const {
        // Use phx module's narrow phase algorithm ray-casting.
        NOT_SUPPORTED();
        return {};
    }

    // Line segment - for narrow phase raycasts
    vec3 LineSegment::support(vec3 support_vector) const {
        if (dot(support_vector, direction()) > 0) {
            return to();
        }
        return from();
    }

    AABB LineSegment::calculate_aabb(mat4x4 transform) const {
        const auto from_transformed = vec3(transform * vec4{from(), 1.0f});
        const auto to_transformed = vec3(transform * vec4{to(), 1.0f});

        const auto min_x = min(from_transformed.x, to_transformed.x);
        const auto max_x = max(from_transformed.x, to_transformed.x);
        const auto min_y = min(from_transformed.y, to_transformed.y);
        const auto max_y = max(from_transformed.y, to_transformed.y);
        const auto min_z = min(from_transformed.z, to_transformed.z);
        const auto max_z = max(from_transformed.z, to_transformed.z);

        return AABB{{min_x, min_y, min_z}, {max_x, max_y, max_z}};
    }

    void LineSegment::generate_mesh() const {
        const auto vertex_from = std::make_shared<Vertex>(from());
        const auto vertex_to = std::make_shared<Vertex>(to());
        const auto edge_from_to = std::make_shared<HalfEdge>(vertex_from, vertex_to);
        const auto edge_to_from = std::make_shared<HalfEdge>(vertex_to, vertex_from);

        vertex_from->m_edges.push_back(edge_from_to);
        vertex_from->m_id = 0;
        vertex_to->m_edges.push_back(edge_to_from);
        vertex_to->m_id = 1;
        edge_from_to->m_twin = edge_to_from;
        edge_from_to->m_id = 0;
        edge_to_from->m_twin = edge_from_to;
        edge_to_from->m_id = 1;

        auto vertices = std::vector{vertex_from, vertex_to};
        auto edges = std::vector{edge_from_to, edge_to_from};

        m_mesh = std::make_unique<Mesh>(vertices, edges, std::vector<std::shared_ptr<Face>>{});
    }

    std::set<RayIntersection> LineSegment::ray_cast(Ray ray, mat4x4 transform) const {
        NOT_SUPPORTED();
        return {};
    }

    // Point - for narrow phase raycasts
    void Point::generate_mesh() const {
        const auto vertex = std::make_shared<Vertex>(position());
        auto vertices = std::vector{vertex};

        m_mesh = std::make_unique<Mesh>(vertices, std::vector<std::shared_ptr<HalfEdge>>{}, std::vector<std::shared_ptr<Face>>{});
    }

    std::set<RayIntersection> Point::ray_cast(Ray ray, mat4x4 transform) const {
        NOT_SUPPORTED();
        return {};
    }
}
