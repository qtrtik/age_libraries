#ifndef AGE_MATH_FEATURE_HPP_INCLUDED
#   define AGE_MATH_FEATURE_HPP_INCLUDED

#   include <span>
#   include <string>
#   include <vector>

#   include "utils.hpp"

namespace geometry {
    // Forward declarations
    struct Vertex;
    struct HalfEdge;
    struct Face;
    struct Mesh;

    struct Feature {
        virtual ~Feature() = default;

        // Template function to get neighbours of type T
        template <typename T = Feature>
        requires (std::is_same_v<T, Feature> || std::is_same_v<T, Vertex> ||
                  std::is_same_v<T, HalfEdge> || std::is_same_v<T, Face>)
        [[nodiscard]] std::vector<T*> get_neighbours() const {
            if constexpr (std::is_same_v<T, Feature>) {
                std::vector<T*> neighbours;
                size_t total_size = vertex_neighbours().size() +
                                    halfedge_neighbours().size() +
                                    face_neighbours().size();
                neighbours.reserve(total_size);
                for (auto vertex : vertex_neighbours()) {
                    neighbours.push_back(vertex);
                }
                for (auto halfedge : halfedge_neighbours()) {
                    neighbours.push_back(halfedge);
                }
                for (auto face : face_neighbours()) {
                    neighbours.push_back(face);
                }
                return neighbours;
            }
            else if constexpr (std::is_same_v<T, Vertex>) {
                return vertex_neighbours();
            }
            else if constexpr (std::is_same_v<T, HalfEdge>) {
                return halfedge_neighbours();
            }
            else if constexpr (std::is_same_v<T, Face>) {
                return face_neighbours();
            }
            return {};
        }

        friend bool operator==(const Feature& lhs, const Feature& rhs) {
            return lhs.m_mesh == rhs.m_mesh && lhs.m_id == rhs.m_id;
        }

        friend bool operator!=(const Feature& lhs, const Feature& rhs) {
            return !(lhs == rhs);
        }

        [[nodiscard]] Mesh* mesh() const { return m_mesh; }
        [[nodiscard]] size_t id() const { return m_id; }

        [[nodiscard]] virtual DistanceQueryResult distance_to(Feature* other) const = 0;
        virtual std::string to_string() const = 0;
    protected:
        Mesh* m_mesh{};
        size_t m_id{};

        [[nodiscard]] virtual std::vector<Vertex*> vertex_neighbours() const { return {}; }
        [[nodiscard]] virtual std::vector<HalfEdge*> halfedge_neighbours() const { return {}; }
        [[nodiscard]] virtual std::vector<Face*> face_neighbours() const { return {}; }
    };

    // Concepts
    template <typename T>
    concept IsFeature = std::is_base_of_v<Feature, T> || std::is_base_of_v<Feature, std::remove_pointer_t<T>>;

    // Global functions
    [[nodiscard]] static DistanceQueryResult feature_distance(Feature* feature_1, Feature* feature_2) {
        return feature_1->distance_to(feature_2);
    }
}

#   endif //AGE_MATH_FEATURE_HPP_INCLUDED
