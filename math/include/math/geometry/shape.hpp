#ifndef AGE_MATH_SHAPE_HPP_INCLUDED
#   define AGE_MATH_SHAPE_HPP_INCLUDED

#   include <vector>
#   include "math/bounding_volume.hpp"

#if USE_BULLET_PHX
    #include <bullet/btBulletDynamicsCommon.h>
#endif

namespace geometry {
    struct RayIntersection;
    struct ParametrisedPoint;
    struct Mesh;

    struct Shape {
        // Cosntants
        static constexpr std::string name() { return "shape"; }
        virtual bool is_convex() const { return false; }
        virtual bool is_implicit() const { return false; }

        constexpr static size_t VERTEX_COUNT = 0;
        constexpr static size_t INDEX_COUNT = 0;

        // Constructors
        Shape() = default;
        virtual ~Shape() = default;

        // Getter
        [[nodiscard]] Mesh* mesh() const;
        [[nodiscard]] Mesh* try_get_mesh() const;

        // Methods
        [[nodiscard]] virtual vec3 support(vec3 direction) const = 0;
        [[nodiscard]] virtual AABB calculate_aabb(mat4x4 transform) const = 0;
        [[nodiscard]] virtual std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform) const = 0;
#if USE_BULLET_PHX
        [[nodiscard]] virtual btCollisionShape* to_bullet_type() const = 0;
#endif

    protected:
        mutable std::unique_ptr<Mesh> m_mesh = nullptr; // Cache for mesh

        [[nodiscard]] virtual std::vector<vec3> generate_vertices() const = 0;
        [[nodiscard]] virtual std::vector<std::array<size_t, 3>> generate_indices() const = 0;

    private:
        virtual void generate_mesh() const;
    };

    struct Sphere final : Shape {
        // Constants
        static constexpr std::string name() { return "sphere"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return true; }

        constexpr static size_t DETAIL = 8;
        constexpr static size_t VERTICAL_DETAIL = 6;
        constexpr static size_t VERTEX_COUNT = DETAIL * VERTICAL_DETAIL + 2;
        constexpr static size_t INDEX_COUNT = 6 * DETAIL * VERTICAL_DETAIL;
        constexpr static size_t TOP_VERTEX_INDEX = 0;
        constexpr static size_t BOTTOM_VERTEX_INDEX = VERTEX_COUNT - 1;

        // Constructors
        explicit Sphere(const scalar radius) : m_radius(radius) {}
        ~Sphere() override = default;

        Sphere(const Sphere& other) : m_radius(other.m_radius) {}

        // Getters
        [[nodiscard]] scalar radius() const {return m_radius; }

        // Setters
        void set_radius(const scalar radius) { m_radius = radius; }

        // Methods
        [[nodiscard]] vec3 support(vec3 direction) const override;
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override;
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;

#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override { return new btSphereShape(m_radius); }
#endif

    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override;
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override;

    private:
        scalar m_radius{};
    };

    struct Box final : Shape {
        // Constants
        static constexpr std::string name() { return "box"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return true; }

        constexpr static size_t VERTEX_COUNT = 8;
        constexpr static size_t INDEX_COUNT = 36;

        // Constructors
        explicit Box(vec3 half_extents) : m_half_extents(half_extents) {}
        ~Box() override = default;

        Box(const Box& other) : m_half_extents(other.m_half_extents) {}

        // Getters
        [[nodiscard]] vec3 half_extents() const { return m_half_extents; }
        [[nodiscard]] vec3 lo() const { return -m_half_extents; }
        [[nodiscard]] vec3 hi() const { return m_half_extents; }

        // Setters
        void set_half_extents(vec3 half_extents) { m_half_extents = half_extents; }

        // Methods
        [[nodiscard]] vec3 support(vec3 direction) const override;
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override;
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;


#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override { return new btBoxShape(btVector3(m_half_extents.x, m_half_extents.y, m_half_extents.z)); }
#endif

    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override;
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override;

    private:
        vec3 m_half_extents{};
    };

    struct Cylinder final : Shape {
        // Constants
        static constexpr std::string name() { return "cylinder"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return true; }

        constexpr static size_t DETAIL = 8;
        constexpr static size_t VERTEX_COUNT = DETAIL * 2 + 2;
        constexpr static size_t INDEX_COUNT = DETAIL * 12;
        constexpr static size_t TOP_VERTEX_INDEX = 0;
        constexpr static size_t BOTTOM_VERTEX_INDEX = VERTEX_COUNT - 1;

        // Constructors
        Cylinder(const scalar radius, const scalar height) : m_radius(radius), m_height(height) {}
        ~Cylinder() override = default;

        Cylinder(const Cylinder& other) : m_radius(other.m_radius), m_height(other.m_height) {}

        // Getters
        [[nodiscard]] scalar radius() const { return m_radius; }
        [[nodiscard]] scalar height() const { return m_height; }

        // Setters
        void set_radius(const scalar radius) { m_radius = radius; }

        // Methods
        [[nodiscard]] vec3 support(vec3 direction) const override;
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override;
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;


#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override { return new btCylinderShape(btVector3(m_radius, m_height / 2, m_radius)); }
#endif

    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override;
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override;

    private:
        scalar m_radius{};
        scalar m_height{};
    };

    struct Cone final : Shape {
        // Constants
        static constexpr std::string name() { return "cone"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return true; }

        constexpr static size_t DETAIL = 8;
        constexpr static size_t VERTEX_COUNT = DETAIL + 2;
        constexpr static size_t INDEX_COUNT = DETAIL * 6;
        constexpr static size_t TOP_VERTEX_INDEX = 0;
        constexpr static size_t BOTTOM_VERTEX_INDEX = VERTEX_COUNT - 1;

        // Constructors
        Cone(const scalar radius, const scalar height) : m_radius(radius), m_height(height) {}
        ~Cone() override = default;

        Cone(const Cone& other) : m_radius(other.m_radius), m_height(other.m_height) {}

        // Getters
        [[nodiscard]] scalar radius() const { return m_radius; }
        [[nodiscard]] scalar height() const { return m_height; }

        // Setters
        void set_radius(const scalar radius) { m_radius = radius; }

        // Methods
        [[nodiscard]] vec3 support(vec3 direction) const override;
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override;
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;


#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override { return new btConeShape(m_radius, m_height); }
#endif

    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override;
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override;

    private:
        scalar m_radius{};
        scalar m_height{};
    };

    struct Capsule final : Shape {
        // Constants
        static constexpr std::string name() { return "capsule"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return true; }

        constexpr static size_t DETAIL = 8;
        constexpr static size_t VERTICAL_DETAIL = 3;
        constexpr static size_t VERTEX_COUNT = DETAIL * 2 * VERTICAL_DETAIL + 2;
        constexpr static size_t INDEX_COUNT = 6 * DETAIL * 2 * VERTICAL_DETAIL;
        constexpr static size_t TOP_VERTEX_INDEX = 0;
        constexpr static size_t BOTTOM_VERTEX_INDEX = VERTEX_COUNT - 1;

        // Constructors
        Capsule(const scalar radius, const scalar height) : m_radius(radius), m_height(height) {}
        ~Capsule() override = default;

        Capsule(const Capsule& other) : m_radius(other.m_radius), m_height(other.m_height) {}

        // Getters
        [[nodiscard]] scalar radius() const { return m_radius; }
        [[nodiscard]] scalar height() const { return m_height; }

        // Setters
        void set_radius(const scalar radius) { m_radius = radius; }

        // Methods
        [[nodiscard]] vec3 support(vec3 direction) const override;
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override;
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;

#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override { return new btCapsuleShape(m_radius, m_height); }
#endif

    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override;
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override;

    private:
        scalar m_radius{};
        scalar m_height{};
    };

    struct ConvexHull final : Shape {
        // Constants
        static constexpr std::string name() { return "convex_hull"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return false; }

        // Constructors
        ConvexHull(const std::vector<vec3>& points, const std::vector<std::array<size_t, 3>>& faces);
        ConvexHull(const std::vector<vec3>& points, const std::vector<uint32_t>& indices);
        ~ConvexHull() override = default;

        ConvexHull(const ConvexHull& other) : m_points(other.m_points), m_faces(other.m_faces) {}

        // Getters
        [[nodiscard]] const std::vector<vec3>& points() const { return m_points; }
        [[nodiscard]] const std::vector<std::array<size_t, 3>>& faces() const { return m_faces; }

        // Methods
        [[nodiscard]] vec3 support(vec3 direction) const override;
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override;
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;

#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override {
            btConvexHullShape* shape = new btConvexHullShape();
            for (const auto& point : m_points) {
                shape->addPoint(btVector3(point.x, point.y, point.z));
            }
            return shape;
        }
#endif

    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override { return m_points; }
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override { return m_faces; }

    private:
        std::vector<vec3> m_points;
        std::vector<std::array<size_t, 3>> m_faces;
    };

    struct LineSegment final : Shape {
        // Constants
        static constexpr std::string name() { return "line_segment"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return true; }

        // Constructors
        LineSegment(vec3 a, vec3 b) : m_from(a), m_to(b) {}
        ~LineSegment() override = default;

        LineSegment(const LineSegment& other) : m_from(other.m_from), m_to(other.m_to) {}

        // Getters
        [[nodiscard]] vec3 from() const { return m_from; }
        [[nodiscard]] vec3 to() const { return m_to; }
        [[nodiscard]] vec3 direction() const { return m_to - m_from; }
        [[nodiscard]] vec3 direction_normalized() const { return normalize(direction()); }
        [[nodiscard]] vec3 center() const { return (m_from + m_to) / 2.0f; }

        // Setters
        void set_from(vec3 a) { m_from = a; }
        void set_to(vec3 b) { m_to = b; }

        // Methods
        [[nodiscard]] vec3 support(vec3 support_vector) const override;
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override;
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;

#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override {
            return new btCapsuleShape(0.0f, 0.0f);
        }
#endif
    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override { return {m_from, m_to}; }
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override { return {}; }

    private:
        void generate_mesh() const override;

        vec3 m_from{};
        vec3 m_to{};
    };

    struct Point final : Shape {
        // Constants
        static constexpr std::string name() { return "point"; }
        bool is_convex() const override { return true; }
        bool is_implicit() const override { return true; }

        // Constructors
        explicit Point(vec3 position) : m_position(position) {}
        ~Point() override = default;

        // Getters
        [[nodiscard]] vec3 position() const { return m_position; }

        // Setters
        void set_point(vec3 point) { m_position = point; }

        // Methods
        [[nodiscard]] vec3 support(vec3 support_vector) const override { return m_position; }
        [[nodiscard]] AABB calculate_aabb(mat4x4 transform) const override { return AABB{m_position, m_position}; }
        [[nodiscard]] std::set<RayIntersection> ray_cast(Ray ray, mat4x4 transform = mat4x4(1.0f)) const override;

#if USE_BULLET_PHX
        [[nodiscard]] btCollisionShape* to_bullet_type() const override {
            return new btSphereShape(0.0f);
        }
#endif
    protected:
        [[nodiscard]] std::vector<vec3> generate_vertices() const override { return {m_position}; }
        [[nodiscard]] std::vector<std::array<size_t, 3>> generate_indices() const override { return {}; }

    private:
        void generate_mesh() const override;

        vec3 m_position{};
    };

    inline AABB calculate_aabb_from_obb(vec3 half_extents, mat4x4 transform) {
        const auto center = vec3(column(transform, 3));
        const auto upper_left = mat3x3{transform};
        vec3 new_half_extents;
        new_half_extents.x = abs(upper_left[0][0]) * half_extents.x +
                             abs(upper_left[1][0]) * half_extents.y +
                             abs(upper_left[2][0]) * half_extents.z;

        new_half_extents.y = abs(upper_left[0][1]) * half_extents.x +
                             abs(upper_left[1][1]) * half_extents.y +
                             abs(upper_left[2][1]) * half_extents.z;

        new_half_extents.z = abs(upper_left[0][2]) * half_extents.x +
                             abs(upper_left[1][2]) * half_extents.y +
                             abs(upper_left[2][2]) * half_extents.z;
        return AABB{center - new_half_extents, center + new_half_extents};
    }
}

#   endif // AGE_MATH_SHAPE_HPP_INCLUDED
