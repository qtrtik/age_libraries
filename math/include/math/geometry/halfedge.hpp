#ifndef AGE_MATH_HEDGE_HPP_INCLUDED
#   define AGE_MATH_HEDGE_HPP_INCLUDED

#   include <math/math.hpp>
#   include <math/geometry/feature.hpp>
#   include <math/geometry/vertex.hpp>
#   include <math/geometry/face.hpp>
#include <utility>

namespace geometry {
    struct HalfEdge final : Feature {
        HalfEdge(std::weak_ptr<Vertex> start, std::weak_ptr<Vertex> end)
            : m_start(std::move(start)), m_end(std::move(end)) {}

        friend bool operator==(const HalfEdge& lhs, const HalfEdge& rhs) {
            return static_cast<const Feature&>(lhs) == static_cast<const Feature&>(rhs)
                   && lhs.m_start.lock() == rhs.m_start.lock()
                   && lhs.m_end.lock() == rhs.m_end.lock()
                   && lhs.m_twin.lock() == rhs.m_twin.lock()
                   && lhs.m_next.lock() == rhs.m_next.lock()
                   && lhs.m_prev.lock() == rhs.m_prev.lock()
                   && lhs.m_face.lock() == rhs.m_face.lock();
        }

        friend bool operator!=(const HalfEdge& lhs, const HalfEdge& rhs) {
            return !(lhs == rhs);
        }

        // Getters
        [[nodiscard]] Vertex* start() const { ASSUMPTION(!m_start.expired()); return m_start.lock().get(); }
        [[nodiscard]] Vertex* end() const { ASSUMPTION(!m_end.expired()); return m_end.lock().get(); }
        [[nodiscard]] HalfEdge* twin() const { ASSUMPTION(!m_twin.expired()); return m_twin.lock().get(); }
        [[nodiscard]] HalfEdge* next() const { ASSUMPTION(!m_next.expired()); return m_next.lock().get(); }
        [[nodiscard]] HalfEdge* prev() const { ASSUMPTION(!m_prev.expired()); return m_prev.lock().get(); }
        [[nodiscard]] Face* face() const { ASSUMPTION(!m_face.expired()); return m_face.lock().get(); }

        [[nodiscard]] vec3 direction() const {
            ASSUMPTION(!m_start.expired() && !m_end.expired());
            return m_end.lock()->position() - m_start.lock()->position();
        }

        [[nodiscard]] vec3 direction_ws() const {
            ASSUMPTION(!m_start.expired() && !m_end.expired());
            return m_end.lock()->position_ws() - m_start.lock()->position_ws();
        }

        [[nodiscard]] DistanceQueryResult distance_to(Feature* other) const override;

        [[nodiscard]] std::string to_string() const override;

    protected:
        [[nodiscard]] std::vector<Vertex*> vertex_neighbours() const override;
        [[nodiscard]] std::vector<Face*> face_neighbours() const override;

    private:
        std::weak_ptr<Vertex> m_start{};
        std::weak_ptr<Vertex> m_end{};
        std::weak_ptr<HalfEdge> m_twin{};
        std::weak_ptr<HalfEdge> m_next{};
        std::weak_ptr<HalfEdge> m_prev{};
        std::weak_ptr<Face> m_face{};

        friend struct Mesh;
        friend struct LineSegment;
    };
}

#   endif // AGE_MATH_HEDGE_HPP_INCLUDED
