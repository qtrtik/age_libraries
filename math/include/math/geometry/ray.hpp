#ifndef AGE_MATH_RAY_HPP_INCLUDED
#   define AGE_MATH_RAY_HPP_INCLUDED

#   include <math/math.hpp>

namespace geometry {
    struct ParametrisedPoint {
        vec3 point;
        scalar t;

        explicit operator vec3() const { return point; }

        bool operator<(const ParametrisedPoint& other) const {
            return t < other.t;
        }
    };

    struct RayIntersection {
        ParametrisedPoint point;
        vec3 normal;

        bool operator<(const RayIntersection& other) const {
            return point < other.point;
        }
    };

    struct Ray {
        static constexpr scalar DEFAULT_LENGTH = 1000.0f;

        Ray(const vec3& origin, const vec3& direction, scalar length = DEFAULT_LENGTH);

        [[nodiscard]] vec3 from() const { return m_origin; }
        [[nodiscard]] vec3 to() const { return m_origin + direction_unscaled(); }
        [[nodiscard]] vec3 at(const scalar t) const { return m_origin + m_direction * t * m_length; }
        [[nodiscard]] scalar parameter_at(const vec3& point) const;

        [[nodiscard]] vec3 origin() const { return m_origin; }
        [[nodiscard]] vec3 direction() const { return m_direction; }
        [[nodiscard]] vec3 direction_unscaled() const { return m_direction * m_length; }
        [[nodiscard]] scalar length() const { return m_length; }

        [[nodiscard]] Ray clip(scalar t_min, scalar t_max) const;
        [[nodiscard]] Ray transform(const mat4x4& transform) const;

        static Ray from_two_points(const vec3& from, const vec3& to);

    private:
        vec3 m_origin;
        vec3 m_direction;
        scalar m_length;
    };
}

#   endif // AGE_MATH_RAY_HPP_INCLUDED
