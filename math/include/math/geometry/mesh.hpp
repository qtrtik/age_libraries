#ifndef AGE_MATH_MESH_HPP_INCLUDED
#   define AGE_MATH_MESH_HPP_INCLUDED

#include <ranges>
#   include <shared_mutex>
#   include <vector>
#   include <math/math.hpp>
#   include <com/context.hpp>

#   include <math/geometry/vertex.hpp>
#   include <math/geometry/face.hpp>
#   include <math/geometry/halfedge.hpp>

namespace geometry {
    // Forward declarations
    struct Mesh {
        // Constructors
        Mesh() = default;
        Mesh(const std::vector<vec3>& vertex_positions, const std::vector<std::array<size_t, 3>>& triangle_indices);
        Mesh(const std::vector<std::shared_ptr<Vertex>>& vertices, const std::vector<std::shared_ptr<HalfEdge>>& edges, const std::vector<std::shared_ptr<Face>>& faces);
        ~Mesh() = default;

        Mesh(const Mesh& other);
        Mesh(Mesh&& other) noexcept;
        Mesh& operator=(const Mesh& other);
        Mesh& operator=(Mesh&& other) noexcept;

        // Operators
        friend bool operator==(const Mesh& lhs, const Mesh& rhs);
        friend bool operator!=(const Mesh& lhs, const Mesh& rhs);

        // Getters
        [[nodiscard]] auto vertices() const {
            return m_vertices | std::ranges::views::transform([](const std::shared_ptr<Vertex>& sp) -> std::weak_ptr<Vertex> {
                return sp;
            });
        }

        [[nodiscard]] auto half_edges() const {
            return m_edges | std::ranges::views::transform([](const std::shared_ptr<HalfEdge>& sp) -> std::weak_ptr<HalfEdge> {
                return sp;
            });
        }

        [[nodiscard]] auto faces() const {
            return m_faces | std::ranges::views::transform([](const std::shared_ptr<Face>& sp) -> std::weak_ptr<Face> {
                return sp;
            });
        }

        [[nodiscard]] std::weak_ptr<Vertex> vertex(size_t index) const;
        [[nodiscard]] std::weak_ptr<HalfEdge> half_edge(size_t index) const;
        [[nodiscard]] std::weak_ptr<Face> face(size_t index) const;

        [[nodiscard]] std::optional<std::weak_ptr<HalfEdge>> half_edge_from_vertices(const std::shared_ptr<Vertex>& start, const std::shared_ptr<Vertex>& end) const;
        [[nodiscard]] std::optional<std::weak_ptr<HalfEdge>> half_edge_from_vertices(size_t start, size_t end) const { return half_edge_from_vertices(vertex(start).lock(), vertex(end).lock()); }

        [[nodiscard]] std::optional<std::weak_ptr<Face>> face_from_vertices(const std::shared_ptr<Vertex>& v1, const std::shared_ptr<Vertex>& v2, const std::shared_ptr<Vertex>& v3) const;
        [[nodiscard]] std::optional<std::weak_ptr<Face>> face_from_vertices(size_t v1, size_t v2, size_t v3) const { return face_from_vertices(vertex(v1).lock(), vertex(v2).lock(), vertex(v3).lock()); }

        [[nodiscard]] const mat4x4& transform() const { return m_transform; }

        // Setters
        void set_transform(const mat4x4& transform);

        // Methods
        void build_dcel(const std::vector<vec3>& vertex_positions, const std::vector<std::array<size_t, 3>>& triangle_indices);
        void clear_cache() const;
        [[nodiscard]] std::string get_debug_data(std::string name) const;

    protected:
        friend struct Vertex;

        [[nodiscard]] vec3 get_vertex_position_ws(size_t vertex_id) const;

    private:
        void add_vertex(std::shared_ptr<Vertex>&& vertex);
        void add_face(std::shared_ptr<Face>&& face);
        void add_half_edge(std::shared_ptr<HalfEdge>&& hedge);

        void clear();

        void copy_from(const Mesh& other);
        void move_from(Mesh&& other);

        std::vector<std::shared_ptr<Vertex>> m_vertices;
        std::vector<std::shared_ptr<HalfEdge>> m_edges;
        std::vector<std::shared_ptr<Face>> m_faces;

        mat4x4 m_transform = mat4x4(1.0f);

        // Global position cache
        mutable std::unordered_map<size_t, vec3> m_pos_cache{};
        mutable std::shared_mutex m_pos_cache_mutex{};
    };
}

#   endif // AGE_MATH_MESH_HPP_INCLUDED
