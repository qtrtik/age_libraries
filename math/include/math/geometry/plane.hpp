#ifndef AGE_MATH_PLANE_HPP_INCLUDED
#   define AGE_MATH_PLANE_HPP_INCLUDED

#   include <math/math.hpp>

namespace geometry {
    struct Plane {
        // Constructors
        Plane() = default;

        Plane(const vec3& normal, scalar d)
            : m_normal(normal),
              m_d(d) {
        }

        explicit Plane(const vec4& vec)
            : m_normal(vec),
              m_d(vec.w) {
        }

        Plane(const vec3& normal, const vec3& point)
            : m_normal(normal),
              m_d(dot(normal, point)) {
        }

        // Getters
        [[nodiscard]] vec3 get_normal() const {
            return m_normal;
        }

        [[nodiscard]] scalar get_d() const {
            return m_d;
        }

        // Operators
        friend bool operator==(const Plane& lhs, const Plane& rhs) = default;
        friend bool operator!=(const Plane& lhs, const Plane& rhs) = default;

        [[nodiscard]] Plane operator-() const {
            return {-m_normal, -m_d};
        }

        [[nodiscard]] Plane operator*(const mat4x4& matrix) const {
            const auto normal = vec3(matrix * vec4(this->m_normal, 0.0));
            const auto point = vec3(matrix * vec4(this->m_normal * this->m_d, 1.0));
            return {normal, point};
        }

        friend Plane operator*(const mat4x4& matrix, const Plane& plane) {
            return plane * matrix;
        }

        // Methods
        [[nodiscard]] vec4 to_vec4() const {
            return {m_normal, m_d};
        }

        [[nodiscard]] scalar distance_to(const vec3& point) const {
            return dot(m_normal, point) - m_d;
        }

        [[nodiscard]] bool is_above(const vec3& point) const {
            return distance_to(point) >= 0;
        }

        [[nodiscard]] scalar get_intersection_parameter(const vec3& point_a, const vec3& point_b) const;

        static vec3 get_plane_normal(const vec3& a, const vec3& b, const vec3& c) {
            return normalize(cross(b - a, c - a));
        }

        [[nodiscard]] std::string to_string() const {
            return "Plane(normal: (" + std::to_string(m_normal.x) + ", " + std::to_string(m_normal.y) + ", " + std::to_string(m_normal.z) + "), d: " + std::to_string(m_d) + ")";
        }

    protected:
        vec3 m_normal;
        scalar m_d;
    };
}

#   endif // AGE_MATH_PLANE_HPP_INCLUDED
