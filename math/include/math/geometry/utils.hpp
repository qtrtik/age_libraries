#ifndef AGE_MATH_UTILS_HPP_INCLUDED
#   define AGE_MATH_UTILS_HPP_INCLUDED

#   include <array>
#   include <vector>
#   include <optional>
#   include <math/math.hpp>

namespace geometry {
    struct DistanceQueryResult {
        scalar distance;
        vec3 closest_point_a;
        vec3 closest_point_b;

        auto operator<=>(const DistanceQueryResult& other) const {
            return distance <=> other.distance;
        }

        bool operator==(const DistanceQueryResult& other) const = default;

        static DistanceQueryResult max() {
            return {std::numeric_limits<scalar>::max(), vec3{}, vec3{}};
        }
    };

    inline DistanceQueryResult distance_point_line(const vec3& point, const vec3& a, const vec3& b, const scalar epsilon = Num::epsilon) {
        const auto ab = b - a;
        const auto ap = point - a;

        const scalar ab_length2 = dot(ab, ab);

        // Handle degenerate case where a and b are the same point
        if (ab_length2 < epsilon) {
            return {distance(point, a), point, a};
        }

        const auto t = dot(ap, ab) / ab_length2;
        const auto clamped_t = clamp(t, 0.0f, 1.0f);
        const auto projection = a + ab * clamped_t;
        return {distance(point, projection), point, projection};
    }

    inline DistanceQueryResult distance_point_triangle(const vec3& point, const vec3& a, const vec3& b, const vec3& c) {
        // Project point to the plane of the triangle
        const auto normal = normalize(cross(b - a, c - a));
        const auto d = -dot(normal, a);
        const auto projected_point = point - normal * (dot(normal, point) + d);

        // Calculate the barycentric coordinates
        const auto v0 = b - a;
        const auto v1 = c - a;
        const auto v2 = projected_point - a;

        const auto den = v0.x * v1.y - v1.x * v0.y;
        const auto v = (v2.x * v1.y - v1.x * v2.y) / den;
        const auto w = (v0.x * v2.y - v2.x * v0.y) / den;
        const auto u = 1.0f - v - w;

        // Check if the point is inside the triangle
        if (v >= 0 && w >= 0 && u >= 0) {
            return {distance(point, projected_point), point, projected_point};
        }

        // Calculate the distance to the edges
        const auto d0 = distance_point_line(point, a, b);
        const auto d1 = distance_point_line(point, b, c);
        const auto d2 = distance_point_line(point, c, a);

        return std::min({d0, d1, d2});
    }

    inline DistanceQueryResult distance_point_face(const vec3& point, const std::vector<vec3>& vertices) {
        // Split the face into triangles and calculate the distance to each of them
        DistanceQueryResult min_distance = DistanceQueryResult::max();
        for (size_t i = 1; i < vertices.size() - 1; i++) {
            const auto distance = distance_point_triangle(point, vertices[0], vertices[i], vertices[i + 1]);
            min_distance = std::min(min_distance, distance);
        }
        return min_distance;
    }

    inline DistanceQueryResult distance_line_line(const vec3& a1, const vec3& a2, const vec3& b1, const vec3& b2, const scalar epsilon = Num::epsilon) {
        const auto d1 = a2 - a1;
        const auto d2 = b2 - b1;

        const auto r = a1 - b1;
        const auto a = dot(d1, d1);
        const auto e = dot(d2, d2);
        const auto f = dot(d2, r);

        // Check if some or both lines are degenerate
        if (a < epsilon && e < epsilon) {
            return {distance(a1, b1), a1, b1};
        }
        if (a < epsilon) {
            return distance_point_line(a1, b1, b2);
        }
        if (e < epsilon) {
            return distance_point_line(b1, a1, a2);
        }

        // If the lines are not degenerate, calculate the closest points
        scalar b = dot(d1, d2);
        scalar c = dot(d1, r);
        scalar denom = a * e - b * b;

        // Calculate the parameters of the closest points
        scalar s;
        if (denom > epsilon) {
            s = clamp((b * f - c * e) / denom, 0.0f, 1.0f);
        } else {
            s = 0.0f;
        }

        scalar t = (b * s + f) / e;
        if (t < 0.0f) {
            t = 0.0f;
            s = clamp(-c / a, 0.0f, 1.0f);
        } else if (t > 1.0f) {
            t = 1.0f;
            s = clamp((b - c) / a, 0.0f, 1.0f);
        }

        // Get the closest points on the lines
        const auto c1 = a1 + d1 * s;
        const auto c2 = b1 + d2 * t;

        return {distance(c1, c2), c1, c2};
    }

    inline std::optional<vec3> line_triangle_intersection(const vec3& p0, const vec3& p1, const vec3& v0, const vec3& v1, const vec3& v2, const scalar epsilon = Num::epsilon) {
        // Möller–Trumbore intersection algorithm
        const vec3 edge1 = { v1.x - v0.x, v1.y - v0.y, v1.z - v0.z };
        const vec3 edge2 = { v2.x - v0.x, v2.y - v0.y, v2.z - v0.z };

        // Compute the intersection normal
        const auto h = cross(p1 - p0, edge2);
        const auto a = dot(edge1, h);

        if (std::abs(a) < epsilon)
            return std::nullopt; // Line is parallel or coplanar

        const auto f = 1.0f / a;
        vec3 s = { p0.x - v0.x, p0.y - v0.y, p0.z - v0.z };
        const auto u = f * dot(s, h);

        if (u < 0.0f || u > 1.0f)
            return std::nullopt; // Intersection is outside the triangle

        const auto q = cross(s, edge1);
        const auto v = f * dot(p1 - p0, q);

        if (v < 0.0f || u + v > 1.0f)
            return std::nullopt; // Intersection is outside the triangle

        const auto t = f * dot(edge2, q);

        if (t < 0.0f || t > 1.0f)
            return std::nullopt; // Intersection is outside the line segment

        // Intersection point
        vec3 intersection = {
            p0.x + (p1.x - p0.x) * t,
            p0.y + (p1.y - p0.y) * t,
            p0.z + (p1.z - p0.z) * t
        };

        return intersection;
    }

    inline DistanceQueryResult distance_line_triangle(const vec3& a, const vec3& b, const vec3& v0, const vec3& v1, const vec3& v2, const scalar epsilon = Num::epsilon) {
        // Check for intersection
        if (const auto intersection = line_triangle_intersection(a, b, v0, v1, v2, epsilon)) {
            return {0.0f, *intersection, *intersection};
        }

        // Compute distance between the line and each of the edges
        const auto d0 = distance_line_line(a, b, v0, v1, epsilon);
        const auto d1 = distance_line_line(a, b, v1, v2, epsilon);
        const auto d2 = distance_line_line(a, b, v2, v0, epsilon);

        // Compute distance between endpoints and the triangle
        const auto d3 = distance_point_triangle(a, v0, v1, v2);
        const auto d4 = distance_point_triangle(b, v0, v1, v2);

        // Return the minimum distance
        return std::min({d0, d1, d2, d3, d4});
    }

    inline DistanceQueryResult distance_line_face(const vec3& a, const vec3& b, const std::vector<vec3>& vertices, const scalar epsilon = Num::epsilon) {
        // Split the face into triangles and calculate the distance to each of them
        DistanceQueryResult min_distance = DistanceQueryResult::max();
        for (size_t i = 1; i < vertices.size() - 1; i++) {
            const auto distance = distance_line_triangle(a, b, vertices[0], vertices[i], vertices[i + 1], epsilon);
            min_distance = std::min(min_distance, distance);
        }
        return min_distance;
    }

    inline DistanceQueryResult distance_triangle_triangle(const vec3& a0, const vec3& a1, const vec3& a2, const vec3& b0, const vec3& b1, const vec3& b2, const scalar epsilon = Num::epsilon) {
        auto distances = std::vector<DistanceQueryResult>();
        const auto triangle_a = std::vector{a0, a1, a2};
        const auto triangle_b = std::vector{b0, b1, b2};

        // Compute the distance between each pair of edges
        for (size_t i = 0; i < 3; i++) {
            for (size_t j = 0; j < 3; j++) {
                const auto distance = distance_line_line(triangle_a[i], triangle_a[(i + 1) % 3], triangle_b[j], triangle_b[(j + 1) % 3], epsilon);
                distances.push_back(distance);
            }
        }

        // Compute the distance between the vertices and the opposite triangle
        for (size_t i = 0; i < 3; i++) {
            const auto distance = distance_point_triangle(triangle_a[i], b0, b1, b2);
            distances.push_back(distance);
        }
        for (size_t i = 0; i < 3; i++) {
            const auto distance = distance_point_triangle(triangle_b[i], a0, a1, a2);
            distances.push_back(distance);
        }

        return *std::ranges::min_element(distances);
    }

    inline DistanceQueryResult distance_face_face(const std::vector<vec3>& vertices_a, const std::vector<vec3>& vertices_b, const scalar epsilon = Num::epsilon) {
        // Split the faces into triangles and calculate the distance to each pair of triangles
        DistanceQueryResult min_distance = DistanceQueryResult::max();
        for (size_t i = 1; i < vertices_a.size() - 1; i++) {
            for (size_t j = 1; j < vertices_b.size() - 1; j++) {
                const auto distance = distance_triangle_triangle(vertices_a[0], vertices_a[i], vertices_a[i + 1], vertices_b[0], vertices_b[j], vertices_b[j + 1], epsilon);
                min_distance = std::min(min_distance, distance);
            }
        }
        return min_distance;
    }

    inline bool convex_test(const std::vector<vec3>& points, const std::vector<std::array<size_t, 3>>& indices) {
        for (const auto& face : indices) {
            const auto& v0 = points[face[0]];
            const auto& v1 = points[face[1]];
            const auto& v2 = points[face[2]];

            const auto edge1 = v1 - v0;
            const auto edge2 = v2 - v0;

            const auto normal = normalized(cross(edge1, edge2));

            double d = -dot(normal, v0);

            for (size_t i = 0; i < points.size(); ++i) {
                if (i == face[0] || i == face[1] || i == face[2])
                    continue;

                const auto p = points[i];
                const auto distance = dot(normal, p) + d;

                if (distance > Num::epsilon)
                    return false;
            }
        }

        return true;
    }

    inline std::optional<vec3> ray_triangle_intersection(vec3 origin, vec3 direction, vec3 a, vec3 b, vec3 c) {
        const vec3 edge1 = b - a;
        const vec3 edge2 = c - a;
        const vec3 h = cross(direction, edge2);
        const float a_dot_h = dot(edge1, h);

        // This means the ray is parallel to the triangle.
        if (a_dot_h > 0.0f && a_dot_h < 0.0f) {
            return std::nullopt;
        }

        const float f = 1.0f / a_dot_h;
        const vec3 s = origin - a;
        const float u = f * dot(s, h);

        // Intersection lies outside of the triangle.
        if (u < 0.0f || u > 1.0f) {
            return std::nullopt;
        }

        const vec3 q = cross(s, edge1);
        const float v = f * dot(direction, q);

        // Intersection lies outside of the triangle.
        if (v < 0.0f || u + v > 1.0f) {
            return std::nullopt;
        }

        // Compute the intersection point.
        const float t = f * dot(edge2, q);
        if (t > 0.0f) {  // ray intersection
            return origin + direction * t;
        }

        // No hit, ray intersection point lies behind the ray origin.
        return std::nullopt;
    }

    inline vec3 calculate_barycentric_vector(const vec3& a, const vec3& b, const vec3& c) {
        auto ab = b - a;
        auto ac = c - a;
        auto ap = -a;

        auto d1 = dot(ab, ap);
        auto d2 = dot(ac, ap);

        if (d1 <= 0.0 && d2 <= 0.0) {
            return {1, 0, 0};
        }

        vec3 bp = -b;
        auto d3 = dot(ab, bp);
        auto d4 = dot(ac, bp);
        if (d3 >= 0.0 && d4 <= d3) {
            return {0, 1, 0};
        }

        auto vc = d1 * d4 - d3 * d2;
        if (vc <= 0.0 && d1 >= 0.0 && d3 <= 0.0) {
            auto v = d1 / (d1 - d3);
            return {1.0 - v, v, 0};
        }

        vec3 cp = -c;
        auto d5 = dot(ab, cp);
        auto d6 = dot(ac, cp);
        if (d6 >= 0.0 && d5 <= d6) {
            return {0, 0, 1};
        }

        auto vb = d5 * d2 - d1 * d6;
        if (vb <= 0.0 && d2 >= 0.0 && d6 <= 0.0) {
            auto w = d2 / (d2 - d6);
            return {1.0 - w, 0, w};
        }

        auto va = d3 * d6 - d5 * d4;
        if (va <= 0.0 && (d4 - d3) >= 0.0 && (d5 - d6) >= 0.0) {
            auto w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
            return {0, 1.0 - w, w};
        }

        auto denom = va + vb + vc;
        auto d = 1.0 / denom;
        auto vf = vb * d;
        auto wf = vc * d;

        return {1.0 - vf - wf, vf, wf};
    }
}

#   endif // AGE_MATH_UTILS_HPP_INCLUDED
