#ifndef AGE_MATH_FACE_HPP_INCLUDED
#   define AGE_MATH_FACE_HPP_INCLUDED

#   include <math/geometry/feature.hpp>
#   include <math/geometry/plane.hpp>

#   include "mesh.hpp"
#   include "utils.hpp"

namespace geometry {
    // Forward declarations
    struct HalfEdge;
    struct Vertex;

    struct Face final : Feature {
        explicit Face(const Plane& plane) : m_plane(plane) {}
        Face(const vec3& normal, const scalar d) : m_plane(normal, d) {}

        friend bool operator==(const Face& lhs, const Face& rhs) {
            return static_cast<const Feature&>(lhs) == static_cast<const Feature&>(rhs)
                   && lhs.m_plane == rhs.m_plane;
        }

        friend bool operator!=(const Face& lhs, const Face& rhs) {
            return !(lhs == rhs);
        }

        [[nodiscard]] Plane plane() const;

        [[nodiscard]] std::vector<Vertex*> vertices() const;
        [[nodiscard]] std::vector<vec3> vertex_positions() const;

        [[nodiscard]] std::vector<HalfEdge*> edges() const;

        [[nodiscard]] DistanceQueryResult distance_to(Feature* other) const override;

        [[nodiscard]] std::string to_string() const override;

    protected:
        [[nodiscard]] std::vector<HalfEdge*> halfedge_neighbours() const override;

    private:
        Plane m_plane;
        std::vector<std::weak_ptr<HalfEdge>> m_edges;

        friend struct Mesh;
    };
}

#   endif // AGE_MATH_FACE_HPP_INCLUDED
