#ifndef AGE_MATH_VERTEX_HPP_INCLUDED
#   define AGE_MATH_VERTEX_HPP_INCLUDED

#   include <math/math.hpp>
#   include <math/geometry/feature.hpp>

namespace geometry {
    // Forward declarations
    struct HalfEdge;

    struct Vertex final : Feature {
        explicit Vertex(const vec3& position)
            : m_position(position) {
        }

        friend bool operator==(const Vertex& lhs, const Vertex& rhs) {
            return static_cast<const Feature&>(lhs) == static_cast<const Feature&>(rhs)
                   && lhs.m_position == rhs.m_position;
        }

        friend bool operator!=(const Vertex& lhs, const Vertex& rhs) {
            return !(lhs == rhs);
        }

        [[nodiscard]] vec3 position() const { return m_position; }
        [[nodiscard]] vec3 position_ws() const;
        [[nodiscard]] std::vector<std::weak_ptr<HalfEdge>> edges() const { return m_edges; }
        [[nodiscard]] std::vector<Face*> incident_faces() const;

        [[nodiscard]] DistanceQueryResult distance_to(Feature* other) const override;

        [[nodiscard]] std::string to_string() const override;

    protected:
        [[nodiscard]] std::vector<HalfEdge*> halfedge_neighbours() const override;

    private:
        vec3 m_position;
        std::vector<std::weak_ptr<HalfEdge>> m_edges;

        friend struct Mesh;
        friend struct LineSegment;
    };
}

#   endif // AGE_MATH_VERTEX_HPP_INCLUDED
