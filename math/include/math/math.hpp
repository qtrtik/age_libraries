#ifndef AGE_MATH_MATH_HPP_INCLUDED
#   define AGE_MATH_MATH_HPP_INCLUDED

#   include <utils/assumptions.hpp>
#   include <cmath>
#   include <cstdint>
#   include <limits>

// First we bind our math API to a chosen 3rd party math library.

#if defined(AGE_USE_GLM_MATH_LIB)
#    include <math/math_binding_glm.hpp>
#elif defined(AGE_USE_EIGEN3_MATH_LIB)
#    include <math/math_binding_eigen3.hpp>
#elif defined(AGE_USE_UBLAS_MATH_LIB)
#    include <math/math_binding_ublas.hpp>
#else
#    error "Unknown 3rd party math library."
#endif

// And we define the remainder of the math library.

inline bool equal(scalar const x, scalar const y, scalar const epsilon = Num::epsilon)
{
    return std::abs(x - y) < epsilon;
}

inline bool equal(vec2 const u, vec2 const v, scalar const epsilon = Num::epsilon)
{
    return  equal(get<0>(u), get<0>(v), epsilon) &&
            equal(get<1>(u), get<1>(v), epsilon) ;
}

inline bool equal(vec3 const u, vec3 const v, scalar const epsilon = Num::epsilon)
{
    return  equal(get<0>(u), get<0>(v), epsilon) &&
            equal(get<1>(u), get<1>(v), epsilon) &&
            equal(get<2>(u), get<2>(v), epsilon) ;
}

inline bool equal(vec4 const u, vec4 const v, scalar const epsilon = Num::epsilon)
{
    return  equal(get<0>(u), get<0>(v), epsilon) &&
            equal(get<1>(u), get<1>(v), epsilon) &&
            equal(get<2>(u), get<2>(v), epsilon) &&
            equal(get<3>(u), get<3>(v), epsilon) ;
}

struct basis3
{
    basis3()
        : m_axes{ ::axis<vec3, 0>(), ::axis<vec3, 1>(), ::axis<vec3, 2>() }
    {}

    basis3(vec3 const& i, vec3 const& j, vec3 const& k)
        : m_axes{ i, j, k }
    {
        ASSUMPTION(equal( len2(m_axes[0]), 1.0f) );
        ASSUMPTION(equal( len2(m_axes[1]), 1.0f) );
        ASSUMPTION(equal( len2(m_axes[2]), 1.0f) );
        ASSUMPTION(equal( dot(m_axes[0], m_axes[1]), 0.0f) );
        ASSUMPTION(equal( dot(m_axes[0], m_axes[2]), 0.0f) );
        ASSUMPTION(equal( dot(m_axes[1], m_axes[2]), 0.0f) );
        ASSUMPTION(equal( cross(m_axes[0], m_axes[1]), m_axes[2]) );
    }

    basis3(mat3x3 const& m)
        : basis3 { column<0>(m), column<1>(m), column<2>(m) }
    {}

    basis3(quat const& q)
        : basis3(to_mat3x3(q))
    {}

    vec3 const& axis(int const idx) const { return m_axes[idx]; }
    template<int idx> vec3 const& axis() const { return m_axes[idx]; }

private:
    vec3 m_axes[3];
};

template<int idx> inline vec3 const& axis(basis3 const& b) { return b.axis<idx>(); }
inline vec3 const& axis(basis3 const& b, int const idx) { return b.axis(idx); }

inline basis3 to_basis(mat3x3 const& m) { return basis3{ m }; }
inline mat3x3 to_mat3x3(basis3 const& b) { return mat3x3( axis<0>(b), axis<1>(b), axis<2>(b) ); }

inline mat4x4 matrix_out(vec3 const& origin, basis3 const& basis)
{
    return mat4x4 {
        vec4{ axis<0>(basis), 0.0f },
        vec4{ axis<1>(basis), 0.0f },
        vec4{ axis<2>(basis), 0.0f },
        vec4{ origin, 1.0f }
    };
}

inline mat4x4 matrix_in(vec3 const& origin, basis3 const& basis)
{
    return transposed( mat4x4 {
        vec4{ axis<0>(basis), -dot(axis<0>(basis), origin) },
        vec4{ axis<1>(basis), -dot(axis<1>(basis), origin) },
        vec4{ axis<2>(basis), -dot(axis<2>(basis), origin) },
        axis<vec4, 3>()
    } );
}

struct frame3
{
    frame3()
        : m_origin(zero<vec3>())
        , m_rotation(identity<quat>())
    {}

    frame3(vec3 const& origin)
        : m_origin(origin)
        , m_rotation(identity<quat>())
    {}

    frame3(quat const& rotation)
        : frame3(zero<vec3>(), rotation)
    {}

    frame3(vec3 const& origin, quat const& rotation)
        : m_origin(origin)
        , m_rotation(rotation)
    {
        ASSUMPTION(equal(len2(m_rotation), 1.0f));
    }

    vec3 const& origin() const { return m_origin; }
    quat const& rotation() const { return m_rotation; }

private:
    vec3 m_origin;
    quat m_rotation;
};

inline mat4x4 matrix_out(frame3 const& frame)
{
    mat4x4 m = to_mat4x4(frame.rotation());
    column<3>(m, vec4{ frame.origin(), 1.0f });
    return m;
}

inline mat4x4 matrix_in(frame3 const& frame)
{
    mat4x4 m = to_mat4x4(frame.rotation());
    row<3>(m,
        vec4 {
            -dot(column<0>(m), frame.origin()),
            -dot(column<1>(m), frame.origin()),
            -dot(column<2>(m), frame.origin()),
            1.0f
            }
        );
    return transposed(m);
}

#endif
