#ifndef AGE_MATH_GEOMETRY_HPP_INCLUDED
#   define AGE_MATH_GEOMETRY_HPP_INCLUDED

// Umbrella header for the geometry submodule

#   include <math/geometry/utils.hpp>
#   include <math/geometry/feature.hpp>
#   include <math/geometry/shape.hpp>
#   include <math/geometry/mesh.hpp>
#   include <math/geometry/plane.hpp>
#   include <math/geometry/face.hpp>
#   include <math/geometry/vertex.hpp>
#   include <math/geometry/halfedge.hpp>
#   include <math/geometry/ray.hpp>

#   endif // AGE_MATH_GEOMETRY_HPP_INCLUDED
