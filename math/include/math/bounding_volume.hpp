#ifndef AGE_MATH_BOUNDING_VOLUME_HPP_INCLUDED
#   define AGE_MATH_BOUNDING_VOLUME_HPP_INCLUDED

#   include <set>
#   include <math/math.hpp>

namespace geometry {
    struct Ray;
    struct ParametrisedPoint;
    struct RayIntersection;
}

struct AABB {
    vec3 lo;
    vec3 hi;

    bool operator==(AABB const& other) const;

    [[nodiscard]] scalar volume() const;
    [[nodiscard]] scalar surface_area() const;
    [[nodiscard]] vec3 center() const;
    [[nodiscard]] vec3 size() const;
    [[nodiscard]] vec3 half_size() const;

    friend AABB operator*(mat4x4 const& m, AABB const& aabb);

    [[nodiscard]] bool intersects(AABB const& other) const;
    [[nodiscard]] bool contains(vec3 const& point) const;
    [[nodiscard]] bool contains(AABB const& other) const;

    [[nodiscard]] bool projection_intersects_xy(vec2 const& point) const;
    [[nodiscard]] bool projection_intersects_xz(vec2 const& point) const;
    [[nodiscard]] bool projection_intersects_yz(vec2 const& point) const;

    void merge(AABB const& other);
    void merge(vec3 const& point);

    void intersection(AABB const& other);

    void fatten(vec3 const& margin);
    void shrink(vec3 const& margin);
    void displace(vec3 const& displacement);

    void to_points(vec3 points[8]) const;

    [[nodiscard]] std::set<geometry::RayIntersection> ray_cast(geometry::Ray const& ray) const;

    [[nodiscard]] static AABB merge(AABB const& left, AABB const& right);
    [[nodiscard]] static AABB merge(AABB const& aabb, vec3 const& point);

    [[nodiscard]] static AABB intersection(AABB const& left, AABB const& right);

    [[nodiscard]] static AABB fatten(AABB const& aabb, vec3 const& margin);
    [[nodiscard]] static AABB shrink(AABB const& aabb, vec3 const& margin);
    [[nodiscard]] static AABB displace(AABB const& aabb, vec3 const& displacement);

    [[nodiscard]] static AABB from_points(vec3 const* begin, vec3 const* end);

    [[nodiscard]] static bool intersects(AABB const& left, AABB const& right);
    [[nodiscard]] static bool contains(AABB const& left, AABB const& right);
    [[nodiscard]] static bool contains(AABB const& aabb, vec3 const& point);

    [[nodiscard]] static bool projection_intersects_xy(AABB const& aabb, vec2 const& point);
    [[nodiscard]] static bool projection_intersects_xz(AABB const& aabb, vec2 const& point);
    [[nodiscard]] static bool projection_intersects_yz(AABB const& aabb, vec2 const& point);
};

#endif
