#include <math/math_scalar_type.hpp>
#include <utils/config.hpp>
#if COMPILER() == COMPILER_CLANG()
#   pragma clang diagnostic push
#   pragma clang diagnostic ignored "-Wdeprecated-volatile"
#endif
#if COMPILER() == COMPILER_GCC()
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wvolatile"
#endif
#include <random>
#include <variant>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#if COMPILER() == COMPILER_CLANG()
#   pragma clang diagnostic pop
#endif
#if COMPILER() == COMPILER_GCC()
#   pragma GCC diagnostic pop
#endif

using vec2i = glm::vec<2, int, glm::defaultp>;
using vec3i = glm::vec<3, int, glm::defaultp>;

using vec2u = glm::vec<2, unsigned int, glm::defaultp>;
using vec3u = glm::vec<2, unsigned int, glm::defaultp>;
using vec4u = glm::vec<4, unsigned int, glm::defaultp>;

using vec2 = glm::vec<2, scalar, glm::defaultp>;
using vec3 = glm::vec<3, scalar, glm::defaultp>;
using vec4 = glm::vec<4, scalar, glm::defaultp>;

using mat2x2 = glm::mat<2, 2, scalar, glm::defaultp>;
using mat3x3 = glm::mat<3, 3, scalar, glm::defaultp>;
using mat4x4 = glm::mat<4, 4, scalar, glm::defaultp>;

using quat = glm::qua<scalar, glm::defaultp>;

namespace detail {
    template<typename T, int idx> struct __vec_get;
    template<> struct __vec_get<vec2, 2> {};
    template<> struct __vec_get<vec2, 3> {};
    template<> struct __vec_get<vec3, 4> {};
    template<> struct __vec_get<quat, 4> {};
    template<typename T> struct __vec_get<T, 0> { static inline scalar get(T const& v) { return v.x; } };
    template<typename T> struct __vec_get<T, 1> { static inline scalar get(T const& v) { return v.y; } };
    template<typename T> struct __vec_get<T, 2> { static inline scalar get(T const& v) { return v.z; } };
    template<typename T> struct __vec_get<T, 3> { static inline scalar get(T const& v) { return v.w; } };

    template<typename T, int idx> struct __vec_set;
    template<> struct __vec_set<vec2, 2> {};
    template<> struct __vec_set<vec2, 3> {};
    template<> struct __vec_set<vec3, 4> {};
    template<> struct __vec_set<quat, 4> {};
    template<typename T> struct __vec_set<T, 0> { static inline void set(T& v, scalar const a) { v.x = a; } };
    template<typename T> struct __vec_set<T, 1> { static inline void set(T& v, scalar const a) { v.y = a; } };
    template<typename T> struct __vec_set<T, 2> { static inline void set(T& v, scalar const a) { v.z = a; } };
    template<typename T> struct __vec_set<T, 3> { static inline void set(T& v, scalar const a) { v.w = a; } };
}

template<typename T> int constexpr dim() { return T::length(); }

template<int idx> inline scalar get(vec2 const& v) { return detail::__vec_get<vec2, idx>::get(v); }
template<int idx> inline scalar get(vec3 const& v) { return detail::__vec_get<vec3, idx>::get(v); }
template<int idx> inline scalar get(vec4 const& v) { return detail::__vec_get<vec4, idx>::get(v); }
template<int idx> inline void set(vec2& v, scalar const a) { detail::__vec_set<vec2, idx>::set(v, a); }
template<int idx> inline void set(vec3& v, scalar const a) { detail::__vec_set<vec3, idx>::set(v, a); }
template<int idx> inline void set(vec4& v, scalar const a) { detail::__vec_set<vec4, idx>::set(v, a); }

template<int column, int row> inline scalar get(mat2x2 const& m) { return get<column>(m[row]); }
template<int column, int row> inline scalar get(mat3x3 const& m) { return get<column>(m[row]); }
template<int column, int row> inline scalar get(mat4x4 const& m) { return get<column>(m[row]); }
template<int column, int row> inline void set(mat2x2& m, scalar const a) { set<column>(m[row], a); }
template<int column, int row> inline void set(mat3x3& m, scalar const a) { set<column>(m[row], a); }
template<int column, int row> inline void set(mat4x4& m, scalar const a) { set<column>(m[row], a); }

template<int idx> inline scalar get(quat const& q) { return detail::__vec_get<quat, idx>::get(q); }
template<int idx> inline void set(quat& q, scalar const a) { detail::__vec_set<quat, idx>::set(q, a); }
inline scalar getw(quat const& q) { return q.w; }
inline void setw(quat& q, scalar const a) { q.w = a; }

inline scalar get(vec2 const& v, int const idx) { return v[idx]; }
inline scalar get(vec3 const& v, int const idx) { return v[idx]; }
inline scalar get(vec4 const& v, int const idx) { return v[idx]; }
inline scalar get(mat2x2 const& m, int const column, int const row) { return m[column][row]; }
inline scalar get(mat3x3 const& m, int const column, int const row) { return m[column][row]; }
inline scalar get(mat4x4 const& m, int const column, int const row) { return m[column][row]; }
inline scalar get(quat const& q, int const idx) { return q[idx]; }
inline void set(vec2& v, int const idx, scalar const a) { v[idx] = a; }
inline void set(vec3& v, int const idx, scalar const a) { v[idx] = a; }
inline void set(vec4& v, int const idx, scalar const a) { v[idx] = a; }
inline void set(mat2x2& m, int const column, int const row, scalar const a) { m[column][row] = a; }
inline void set(mat3x3& m, int const column, int const row, scalar const a) { m[column][row] = a; }
inline void set(mat4x4& m, int const column, int const row, scalar const a) { m[column][row] = a; }
inline void set(quat& q, int const idx, scalar const a) { q[idx] = a; }

inline vec2 column(mat2x2 const& m, int const idx) { return glm::column(m, idx); }
inline vec3 column(mat3x3 const& m, int const idx) { return glm::column(m, idx); }
inline vec4 column(mat4x4 const& m, int const idx) { return glm::column(m, idx); }
inline vec2 row(mat2x2 const& m, int const idx) { return glm::row(m, idx); }
inline vec3 row(mat3x3 const& m, int const idx) { return glm::row(m, idx); }
inline vec4 row(mat4x4 const& m, int const idx) { return glm::row(m, idx); }

inline void column(mat2x2& m, int const idx, vec2 const& v) { m[idx] = v; }
inline void column(mat3x3& m, int const idx, vec3 const& v) { m[idx] = v; }
inline void column(mat4x4& m, int const idx, vec4 const& v) { m[idx] = v; }
inline void row(mat2x2& m, int const idx, vec2 const& v) { m[0][idx] = v[0]; m[1][idx] = v[1]; }
inline void row(mat3x3& m, int const idx, vec3 const& v) { m[0][idx] = v[0]; m[1][idx] = v[1]; m[2][idx] = v[2]; }
inline void row(mat4x4& m, int const idx, vec4 const& v) { m[0][idx] = v[0]; m[1][idx] = v[1]; m[2][idx] = v[2]; m[3][idx] = v[3]; }

template<int idx> inline typename mat2x2::col_type column(mat2x2 const& m) { return column(m, idx); }
template<int idx> inline typename mat3x3::col_type column(mat3x3 const& m) { return column(m, idx); }
template<int idx> inline typename mat4x4::col_type column(mat4x4 const& m) { return column(m, idx); }
template<int idx> inline typename mat2x2::col_type row(mat2x2 const& m) { return row(m, idx); }
template<int idx> inline typename mat3x3::col_type row(mat3x3 const& m) { return row(m, idx); }
template<int idx> inline typename mat4x4::col_type row(mat4x4 const& m) { return row(m, idx); }

template<int idx> inline void column(mat2x2& m, vec2 const& v) { return column(m, idx, v); }
template<int idx> inline void column(mat3x3& m, vec3 const& v) { return column(m, idx, v); }
template<int idx> inline void column(mat4x4& m, vec4 const& v) { return column(m, idx, v); }
template<int idx> inline void row(mat2x2& m, vec2 const& v) { return row(m, idx, v); }
template<int idx> inline void row(mat3x3& m, vec3 const& v) { return row(m, idx, v); }
template<int idx> inline void row(mat4x4& m, vec4 const& v) { return row(m, idx, v); }

inline scalar const* element_array_ptr(vec2 const& v) { return glm::value_ptr(v); }
inline scalar const* element_array_ptr(vec3 const& v) { return glm::value_ptr(v); }
inline scalar const* element_array_ptr(vec4 const& v) { return glm::value_ptr(v); }
inline scalar const* element_array_ptr(mat2x2 const& m) { return glm::value_ptr(m); }
inline scalar const* element_array_ptr(mat3x3 const& m) { return glm::value_ptr(m); }
inline scalar const* element_array_ptr(mat4x4 const& m) { return glm::value_ptr(m); }
inline scalar const* element_array_ptr(quat const& q) { return glm::value_ptr(q); }
inline scalar* element_array_ptr(vec2& v) { return glm::value_ptr(v); }
inline scalar* element_array_ptr(vec3& v) { return glm::value_ptr(v); }
inline scalar* element_array_ptr(vec4& v) { return glm::value_ptr(v); }
inline scalar* element_array_ptr(mat2x2& m) { return glm::value_ptr(m); }
inline scalar* element_array_ptr(mat3x3& m) { return glm::value_ptr(m); }
inline scalar* element_array_ptr(mat4x4& m) { return glm::value_ptr(m); }
inline scalar* element_array_ptr(quat& q) { return glm::value_ptr(q); }

struct Num
{
    static constexpr scalar zero{ static_cast<scalar>(0) };
    static constexpr scalar one{ static_cast<scalar>(1) };
    static constexpr scalar half{ static_cast<scalar>(0.5) };
    static constexpr scalar third{ static_cast<scalar>(1 / 3.0) };
    static constexpr scalar pi = glm::pi<scalar>();
    static constexpr scalar two_pi = glm::two_pi<scalar>();
    static constexpr scalar half_pi = glm::half_pi<scalar>();
    static constexpr scalar quarter_pi = glm::quarter_pi<scalar>();
    static constexpr scalar e = glm::e<scalar>();
    static constexpr scalar sqrt_2 = glm::root_two<scalar>();
    static constexpr scalar epsilon = static_cast<scalar>(1e-5);
    static constexpr scalar epsilon_sq = epsilon * epsilon;
    static constexpr scalar max = std::numeric_limits<scalar>::max();
    static constexpr scalar min = std::numeric_limits<scalar>::lowest();
};

template<typename T> T zero();

template<> inline int zero<int>() { return 0; }
template<> inline scalar zero<scalar>() { return Num::zero; }
template<> inline vec2 zero<vec2>() { return vec2{ 0, 0 }; }
template<> inline vec3 zero<vec3>() { return vec3{ 0, 0, 0 }; }
template<> inline vec4 zero<vec4>() { return vec4{ 0, 0, 0, 0 }; }
template<> inline mat2x2 zero<mat2x2>() { return mat2x2(0); }
template<> inline mat3x3 zero<mat3x3>() { return mat3x3(0); }
template<> inline mat4x4 zero<mat4x4>() { return mat4x4(0); }

template<typename T> T identity();

template<> inline mat2x2 identity<mat2x2>() { return mat2x2{ { 1, 0 }, { 0, 1} }; }
template<> inline mat3x3 identity<mat3x3>() { return mat3x3{ { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } }; }
template<> inline mat4x4 identity<mat4x4>() { return mat4x4{ { 1, 0, 0, 0 }, { 0, 1, 0, 0 }, { 0, 0, 1, 0 }, { 0, 0, 0, 1 } }; }
template<> inline quat identity<quat>() { return quat{ 1, 0, 0, 0 }; }

inline mat2x2 transposed(mat2x2 const& m) { return glm::transpose(m); }
inline mat3x3 transposed(mat3x3 const& m) { return glm::transpose(m); }
inline mat4x4 transposed(mat4x4 const& m) { return glm::transpose(m); }

inline mat2x2 inverted(mat2x2 const& m) { return glm::inverse(m); }
inline mat3x3 inverted(mat3x3 const& m) { return glm::inverse(m); }
inline mat4x4 inverted(mat4x4 const& m) { return glm::inverse(m); }
inline quat   inverted(quat const& q) { return glm::inverse(q); }

template<typename T, int idx> T axis();

template<> inline vec2 axis<vec2, 0>() { return vec2{ 1, 0 }; }
template<> inline vec2 axis<vec2, 1>() { return vec2{ 0, 1 }; }

template<> inline vec3 axis<vec3, 0>() { return vec3{ 1, 0, 0 }; }
template<> inline vec3 axis<vec3, 1>() { return vec3{ 0, 1, 0 }; }
template<> inline vec3 axis<vec3, 2>() { return vec3{ 0, 0, 1 }; }

template<> inline vec4 axis<vec4, 0>() { return vec4{ 1, 0, 0, 0 }; }
template<> inline vec4 axis<vec4, 1>() { return vec4{ 0, 1, 0, 0 }; }
template<> inline vec4 axis<vec4, 2>() { return vec4{ 0, 0, 1, 0 }; }
template<> inline vec4 axis<vec4, 3>() { return vec4{ 0, 0, 0, 1 }; }

inline scalar dot(vec2 const& u, vec2 const& v) { return glm::dot(u,v); }
inline scalar dot(vec3 const& u, vec3 const& v) { return glm::dot(u,v); }
inline scalar dot(vec4 const& u, vec4 const& v) { return glm::dot(u,v); }

inline vec3 cross(vec3 const& u, vec3 const& v) { return glm::cross(u,v); }

inline vec2 hadamard_product(vec2 const& u, vec2 const& v) { return u * v; }
inline vec3 hadamard_product(vec3 const& u, vec3 const& v) { return u * v; }
inline vec4 hadamard_product(vec4 const& u, vec4 const& v) { return u * v; }
inline mat2x2 hadamard_product(mat2x2 const& u, mat2x2 const& v) { return u * v; }
inline mat3x3 hadamard_product(mat3x3 const& u, mat3x3 const& v) { return u * v; }
inline mat4x4 hadamard_product(mat4x4 const& u, mat4x4 const& v) { return u * v; }

inline scalar len2(vec2 const& u) { return glm::dot(u,u); }
inline scalar len2(vec3 const& u) { return glm::dot(u,u); }
inline scalar len2(vec4 const& u) { return glm::dot(u,u); }
inline scalar len2(quat const& q) { return glm::dot(q,q); }

inline scalar len(vec2 const& u) { return glm::length(u); }
inline scalar len(vec3 const& u) { return glm::length(u); }
inline scalar len(vec4 const& u) { return glm::length(u); }
inline scalar len(quat const& q) { return glm::length(q); }

inline scalar distance(vec2 const& u, vec2 const& v) { return glm::distance(u,v); }
inline scalar distance(vec3 const& u, vec3 const& v) { return glm::distance(u,v); }
inline scalar distance(vec4 const& u, vec4 const& v) { return glm::distance(u,v); }

inline vec2 normalized(vec2 const& u) { return glm::normalize(u); }
inline vec3 normalized(vec3 const& u) { return glm::normalize(u); }
inline vec4 normalized(vec4 const& u) { return glm::normalize(u); }

inline quat normalized(quat const& q) { return glm::normalize(q); }
inline quat conjugate(quat const& q) { return glm::conjugate(q); }

inline quat slerp(quat const& q0, quat const& q1, scalar const t) { return glm::mix(q0, q1, t); }

inline vec3 expand(vec2 const& u, scalar h=scalar(1.0)) { return vec3(u, h); }
inline vec4 expand(vec3 const& u, scalar h=scalar(1.0)) { return vec4(u, h); }
inline mat4x4 expand(mat3x3 const& m) { return mat4x4(m); }

inline vec2 contract(vec3 const& u) { return vec2(u); }
inline vec3 contract(vec4 const& u) { return vec3(u); }
inline mat3x3 contract(mat4x4 const& m) { return mat3x3(m); }

inline quat to_quat(scalar const radians) { return glm::angleAxis(radians, axis<vec3, 2>()); }
inline quat to_quat(scalar const radians, vec3 const& unit_axis) { return glm::angleAxis(radians, unit_axis); }
inline quat to_quat(vec3 const& euler_angles) { return quat(euler_angles); }

inline scalar rotation_angle(quat const& q) { return glm::angle(q); }
inline vec3 rotation_axis(quat const& q) { return glm::axis(q); }
inline vec3 euler_angles(quat const& q) { return glm::eulerAngles(q); }

inline quat to_quat(mat3x3 const& m) { return glm::quat_cast(m); }
inline quat to_quat(mat4x4 const& m) { return glm::quat_cast(m); }

inline mat3x3 to_mat3x3(quat const& q) { return glm::mat3_cast(q); }
inline mat4x4 to_mat4x4(quat const& q) { return glm::mat4_cast(q); }

inline mat3x3 to_translation_matrix(vec2 const u) { return mat3x3{ axis<vec3, 0>(), axis<vec3, 1>(), expand(u) }; }
inline mat4x4 to_translation_matrix(vec3 const u) { return mat4x4{ axis<vec4, 0>(), axis<vec4, 1>(), axis<vec4, 2>(), expand(u) }; }

inline mat3x3 to_rotation_matrix(scalar const radians) { return to_mat3x3(to_quat(radians)); }
inline mat4x4 to_rotation_matrix(quat const& q) { return to_mat4x4(q); }

inline mat3x3 to_scale_matrix(vec2 const u) { return mat3x3{ { u[0], 0, 0 }, { 0, u[1], 0 }, axis<vec3, 2>() }; }
inline mat4x4 to_scale_matrix(vec3 const u) { return mat4x4{ { u[0], 0, 0, 0 }, { 0, u[1], 0, 0 }, { 0, 0, u[2], 0 }, axis<vec4, 3>() }; }

inline scalar min(scalar const a, scalar const b) { return glm::min(a, b); }
inline vec2 min(vec2 const& u, vec2 const& v) { return glm::min(u, v); }
inline vec3 min(vec3 const& u, vec3 const& v) { return glm::min(u, v); }
inline vec4 min(vec4 const& u, vec4 const& v) { return glm::min(u, v); }

inline scalar max(scalar const a, scalar const b) { return glm::max(a, b); }
inline vec2 max(vec2 const& u, vec2 const& v) { return glm::max(u, v); }
inline vec3 max(vec3 const& u, vec3 const& v) { return glm::max(u, v); }
inline vec4 max(vec4 const& u, vec4 const& v) { return glm::max(u, v); }

inline scalar clamp(scalar const x, scalar const min, scalar const max) { return glm::clamp(x, min, max); }
inline vec2 clamp(vec2 const& u, vec2 const& min, vec2 const& max) { return glm::clamp(u, min, max); }
inline vec3 clamp(vec3 const& u, vec3 const& min, vec3 const& max) { return glm::clamp(u, min, max); }
inline vec4 clamp(vec4 const& u, vec4 const& min, vec4 const& max) { return glm::clamp(u, min, max); }

inline scalar sign(scalar const x) { return glm::sign(x); }
inline vec2 sign(vec2 const& u) { return glm::sign(u); }
inline vec3 sign(vec3 const& u) { return glm::sign(u); }
inline vec4 sign(vec4 const& u) { return glm::sign(u); }

inline quat look_at(vec3 const& direction, vec3 const& up) { return glm::quatLookAt(direction, up); }
inline quat look_at(vec3 const& direction, vec3 const& up, vec3 const& right) {
    if(std::fabs(dot(direction, up)) > static_cast<scalar>(1.0) - Num::epsilon) {
        return look_at(direction, right);
    }
    return look_at(direction, up);
}

// Swizzling
enum class Axis : uint8_t { X = 0, Y = 1, Z = 2, W = 3, ZERO = 254, ONE = 255 };

template <Axis idx, typename V>
constexpr scalar get_component(const V& v) {
    if constexpr (idx == Axis::X) return v.x;
    else if constexpr (idx == Axis::Y) return v.y;
    else if constexpr (idx == Axis::Z) return v.z;
    else if constexpr (idx == Axis::W) return v.w;
    else if constexpr (idx == Axis::ZERO) return static_cast<scalar>(0);
    else if constexpr (idx == Axis::ONE) return static_cast<scalar>(1);
    return static_cast<scalar>(0);
}

template <Axis... Args, typename V>
constexpr auto swizzle(const V& v) {
    constexpr size_t size = sizeof...(Args);
    auto result = std::tuple{
        get_component<Args>(v)...
    };

    if constexpr (size == 1) {
        return std::get<0>(result);
    } else if constexpr (size == 2) {
        return vec2{ std::get<0>(result), std::get<1>(result) };
    } else if constexpr (size == 3) {
        return vec3{ std::get<0>(result), std::get<1>(result), std::get<2>(result) };
    } else if constexpr (size == 4) {
        return vec4{ std::get<0>(result), std::get<1>(result), std::get<2>(result), std::get<3>(result) };
    }
}