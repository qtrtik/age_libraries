#include <com/context.hpp>
#include <utils/invariants.hpp>
#include <utils/msgstream.hpp>
#include <utils/fail_message.hpp>
#include <utils/timeprof.hpp>
#include <algorithm>
#include <sstream>
#include <utils/context_utils.hpp>
namespace osi::detail { com::Folder* get_root_folder(); }

namespace com::detail {

Linkage::Linkage(ContextItem* const target, Link* const link)
    : m_target{ target }
    , m_links{ link }
{
    ASSUMPTION(m_target != nullptr && *m_links.begin() != nullptr);
}

}

namespace com {

ContextItem::ContextItem(std::string const& name)
    : m_name{ name }
    , m_folder{ nullptr }
    , m_linkage{ nullptr }
    , m_handlers{ nullptr }
{
    ASSUMPTION(!m_name.empty());
}

ContextItem::~ContextItem()
{
    INVARIANT(m_folder == nullptr);
    INVARIANT(m_linkage == nullptr);
    INVARIANT(m_handlers == nullptr);
}

bool ContextItem::is_under(Folder* const f) const
{
    TMPROF_BLOCK();
    for (Folder* parent = folder(); parent != nullptr; parent = parent->folder())
        if (parent == f)
            return true;
    return false;
}

bool ContextItem::is_folder() const { return dynamic_cast<Folder const*>(this) != nullptr; }
bool ContextItem::is_file() const { return dynamic_cast<File const*>(this) != nullptr; }
bool ContextItem::is_link() const { return dynamic_cast<Link const*>(this) != nullptr; }

ContextPath ContextItem::path(Folder* end) const
{
    ContextPath path{ name() };
    for (Folder* f = folder(); f != end; f = f->folder())
        path.push_back(f == Folder::root() ? "/" : f->name());
    std::reverse(path.begin(), path.end());
    return path;
}

void ContextItem::register_handler(EventType const type, ContextItem* const item)
{
    ASSUMPTION(item != nullptr);
    if (m_handlers == nullptr)
        m_handlers = new std::unordered_map<EventType, std::unordered_set<ContextItem*> >;
    auto const it_and_bool = (*m_handlers)[type].insert(item);
    ASSUMPTION(it_and_bool.second);
}

void ContextItem::unregister_handler(EventType const type, ContextItem* const item)
{
    ASSUMPTION(item != nullptr);
    ASSUMPTION(m_handlers != nullptr);
    auto const it_set = m_handlers->find(type);
    auto const it = it_set->second.find(item);
    ASSUMPTION(it != it_set->second.end());
    it_set->second.erase(it);
    if (it_set->second.empty())
    {
        m_handlers->erase(it_set);
        if (m_handlers->empty())
        {
            delete m_handlers;
            m_handlers = nullptr;
        }
    }
}

void ContextItem::send_on_erase()
{
    if (m_handlers == nullptr)
        return;
    auto it = m_handlers->find(ON_ERASE);
    if (it == m_handlers->end())
        return;
    
    std::vector<ContextItem*> const handlers { it->second.begin(), it->second.end() };
    for (ContextItem* const handler : handlers)
        handler->on_erase(this);
}



Folder::Folder(std::string const& name)
    : ContextItem{ name }

    , m_items{}
    , m_dictionary{}

    , m_handlers{ nullptr }

    , m_subfolders{ nullptr }
    , m_files{ nullptr }
    , m_links{ nullptr }

    , m_fresh_name_id{ 0ULL }
{}

Folder::~Folder()
{
    INVARIANT(m_items.empty());
    INVARIANT(m_dictionary.empty());
    INVARIANT(m_handlers == nullptr);
    delete_all_caches();
}

ContextItem* Folder::copy_front(ContextItem* const item, std::string const &unique_name)
{
    std::unordered_map<ContextItem*, ContextItem*> orig_clone;
    auto result = copy_front_impl(item, unique_name, orig_clone);
    notify_clone_integration_impl(orig_clone);
    return result;
}

ContextItem* Folder::copy_back(ContextItem* const item, std::string const &unique_name)
{
    std::unordered_map<ContextItem*, ContextItem*> orig_clone;
    auto result = copy_back_impl(item, unique_name, orig_clone);
    notify_clone_integration_impl(orig_clone);
    return result;
}

ContextItem* Folder::copy_before(ContextItem* const item, std::string const& name, std::string const &unique_name)
{
    std::unordered_map<ContextItem*, ContextItem*> orig_clone;
    auto result = copy_before_impl(item, name, unique_name, orig_clone);
    notify_clone_integration_impl(orig_clone);
    return result;
}

ContextItem* Folder::copy_after(std::string const& name, ContextItem* const item, std::string const &unique_name)
{
    std::unordered_map<ContextItem*, ContextItem*> orig_clone;
    auto result = copy_after_impl(name, item, unique_name, orig_clone);
    notify_clone_integration_impl(orig_clone);
    return result;
}

ContextItem* Folder::extract(std::string const& name)
{
    auto const  it = m_dictionary.find(name);
    if (it == m_dictionary.end())
        return nullptr;

    ContextItem* const item = *it->second;
    ASSUMPTION(item->folder() == this);

    call_handler(item, false);

    item->m_folder = nullptr;
    m_items.erase(it->second);
    m_dictionary.erase(it);
    delete_cache(item);

    return item;
}

void Folder::erase(std::string const& name)
{
    auto const  it = m_dictionary.find(name);
    if (it == m_dictionary.end())
        return;

    ContextItem* const item = *it->second;
    ASSUMPTION(item->folder() == this);

    if (Folder* const folder = dynamic_cast<Folder*>(item))
        while (!folder->empty())
            folder->erase(*folder->items().begin());

    call_handler(item, false);
 
    if (Link* const link = dynamic_cast<Link*>(item))
        link->set_target(nullptr);
    else
        while (item->m_linkage != nullptr)
        {
            INVARIANT(item->m_linkage->m_target == item);
            INVARIANT(!item->m_linkage->m_links.empty());
            Link* const link = *item->m_linkage->m_links.begin();
            link->set_target(nullptr);
        }
    
    item->send_on_erase();
    item->release();

    item->m_folder = nullptr;
    m_items.erase(it->second);
    m_dictionary.erase(it);
    delete_cache(item);
    delete item;
}

void Folder::erase_empty(Folder* const start, Folder* const end)
{
    if (start == end)
        return;
    if (!start->empty())
        return;
    Folder* const folder_of_start{ start->folder() };
    folder_of_start->erase(start);
    erase_empty(folder_of_start, end);
}

ContextItem* Folder::find(std::string const& name) const
{
    auto const it = m_dictionary.find(name);
    return it == m_dictionary.end() ? nullptr : *it->second;
}

ContextItem* Folder::locate(ContextPath const& path) const
{
    ContextItem* item = const_cast<Folder*>(this);
    for (auto it = path.begin(); it != path.end(); ++it)
        if (*it == "/")
            while (item->folder() != nullptr)
                item = item->folder();
        else if (*it == ".")
            continue;
        else if (*it == "..")
        {
            if (item->folder() == nullptr)
                return nullptr;
            item = item->folder();
        }
        else if (Folder* folder = dynamic_cast<Folder*>(item))
        {
            ContextItem* const sub_item = folder->find(*it);
            if (sub_item == nullptr)
                return nullptr;
            item = sub_item;
        }
        else
            return nullptr;
    return item;
}

Folder* Folder::root()
{
    return osi::detail::get_root_folder();
}

Folder* Folder::clone() const
{
    return new Folder(name());
}

std::string Folder::generate_unique_name(std::string const& prefix, std::string const& suffix) const
{
    std::string name = prefix + suffix;
    while (contains(name))
    {
        ASSUMPTION(m_fresh_name_id != std::numeric_limits<std::uint64_t>::max());

        std::stringstream sstr;
        sstr << prefix  << m_fresh_name_id++ << suffix;
        name = sstr.str();
    }
    return name;
}

void Folder::delete_cache(ContextItem* const item) const
{
    if (item->is_folder())
    {
        delete m_subfolders;
        m_subfolders = nullptr;
    }
    else if (item->is_file())
    {
        delete m_files;
        m_files = nullptr;
    }
    else
    {
        INVARIANT(item->is_link());
        delete m_links;
        m_links = nullptr;
    }
}

void Folder::delete_all_caches() const
{
    delete m_subfolders;
    m_subfolders = nullptr;
    delete m_files;
    m_files = nullptr;
    delete m_links;
    m_links = nullptr;
}

void Folder::insert(ContextItem* item, std::function<void(ContextItem*)> const& inserter)
{
    ASSUMPTION(item != nullptr && item->folder() == nullptr && !contains(item->name()));
    inserter(item);
    delete_cache(item);
    item->m_folder = this;
    item->initialize();
    call_handler(item, true);
}

ContextItem* Folder::copy(ContextItem* const item, std::function<void(ContextItem*)> const& inserter, std::string const & unique_name, std::unordered_map<ContextItem*, ContextItem*>& orig_clone)
{
    ContextItem* const item_copy{ item->clone() };
    orig_clone.insert({item, item_copy});
    if (item_copy == nullptr)
    {
        return nullptr;
    }
    item_copy->m_name = unique_name;
    ASSUMPTION(item_copy->folder() == nullptr && !contains(item_copy->name()));
    inserter(item_copy);
    delete_cache(item_copy);
    item_copy->m_folder = this;
    if (Folder* const src_folder = dynamic_cast<Folder*>(item))
    {
        Folder* const dst_folder = dynamic_cast<Folder*>(item_copy);
        for (com::ContextItem* const sub_item : src_folder->items())
            dst_folder->copy_back_impl(sub_item, sub_item->name(), orig_clone);
    }
    return item_copy;
}

void Folder::notify_clone_integration_impl(std::unordered_map<ContextItem*, ContextItem*> & orig_clone)
{
    for (auto &pair : orig_clone)
    {
        if (pair.second != nullptr)
        {
            notify_clone_integration(pair.first, pair.second);
        }
    }
}

void Folder::notify_clone_integration(ContextItem* const original, ContextItem* const clone)
{
    clone->initialize_clone();
    call_handler(clone, true);
    clone->on_clone_integrated(original);
}

void Folder::push_front(ContextItem* const item)
{
    m_items.push_front(item);
    auto const it_and_state = m_dictionary.insert({ item->name(), m_items.begin() });
    ASSUMPTION(it_and_state.second);
}

void Folder::push_back(ContextItem* const item)
{
    m_items.push_back(item);
    auto const it_and_state = m_dictionary.insert({ item->name(), std::prev(m_items.end()) });
    ASSUMPTION(it_and_state.second);
}

void Folder::insert_before(ContextItem* const item, std::string const& name)
{
    auto const it_dict = m_dictionary.find(name);
    auto const it_list = m_items.insert(it_dict->second, item);
    auto const it_and_state = m_dictionary.insert({ item->name(), it_list });
    ASSUMPTION(it_and_state.second);
}

void Folder::insert_after(std::string const& name, ContextItem* item)
{
    auto const it_dict = std::next(m_dictionary.find(name));
    auto const it_list = m_items.insert(it_dict->second, item);
    auto const it_and_state = m_dictionary.insert({ item->name(), it_list });
    ASSUMPTION(it_and_state.second);
}

Folder* Folder::insert_folders(ContextPath const& path, std::function<void(Folder*, ContextItem*)> const& inserter)
{
    ContextItem* item = this;
    for (auto it = path.begin(); it != path.end(); ++it)
        if (*it == "/")
            while (item->folder() != nullptr)
                item = item->folder();
        else if (*it == ".")
            continue;
        else if (*it == "..")
        {
            if (item->folder() == nullptr)
                return nullptr;
            item = item->folder();
        }
        else if (Folder* folder = dynamic_cast<Folder*>(item))
        {
            ContextItem* sub_item = folder->find(*it);
            if (sub_item == nullptr)
                sub_item = folder->create_and_insert_item<com::Folder>(
                    [folder, &inserter](ContextItem* item){ inserter(folder, item); },
                    *it
                    );
            item = sub_item;
        }
        else
            return nullptr;
    return dynamic_cast<Folder*>(item);
}

void Folder::register_handler(EventType const type, ContextItem* const item)
{
    ASSUMPTION(item != nullptr);
    if (m_handlers == nullptr)
        m_handlers = new std::unordered_map<EventType, std::unordered_set<ContextItem*> >;
    auto const it_and_bool = (*m_handlers)[type].insert(item);
    ASSUMPTION(it_and_bool.second);
}

void Folder::unregister_handler(EventType const type, ContextItem* const item)
{
    ASSUMPTION(item != nullptr);
    ASSUMPTION(m_handlers != nullptr);
    auto const it_set = m_handlers->find(type);
    auto const it = it_set->second.find(item);
    ASSUMPTION(it != it_set->second.end());
    it_set->second.erase(it);
    if (it_set->second.empty())
    {
        m_handlers->erase(it_set);
        if (m_handlers->empty())
        {
            delete m_handlers;
            m_handlers = nullptr;
        }
    }
}

void Folder::call_handler(ContextItem* item, bool const insertion)
{
    ASSUMPTION(item != nullptr);

    if (m_handlers == nullptr)
        return;

    if (Folder* const subject = dynamic_cast<Folder*>(item))
    {
        auto const it = m_handlers->find(insertion ? FOLDER_INSERTED : FOLDER_ERASE);
        if (it != m_handlers->end())
        {
            void (ContextItem::* const method)(Folder*, Folder*) = insertion ? &ContextItem::on_folder_inserted : &ContextItem::on_folder_erase;
            std::vector<ContextItem*> const handlers { it->second.begin(), it->second.end() };
            for (ContextItem* const handler : handlers)
                (handler->*method)(this, subject);
        }
    }
    else if (File* const subject = dynamic_cast<File*>(item))
    {
        auto const it = m_handlers->find(insertion ? FILE_INSERTED : FILE_ERASE);
        if (it != m_handlers->end())
        {
            void (ContextItem::* const method)(Folder*, File*) = insertion ? &ContextItem::on_file_inserted : &ContextItem::on_file_erase;
            std::vector<ContextItem*> const handlers { it->second.begin(), it->second.end() };
            for (ContextItem* const handler : handlers)
                (handler->*method)(this, subject);
        }
    }
    else if (Link* const subject = dynamic_cast<Link*>(item))
    {
        auto const it = m_handlers->find(insertion ? LINK_INSERTED : LINK_ERASE);
        if (it != m_handlers->end())
        {
            void (ContextItem::* const method)(Folder*, Link*) = insertion ? &ContextItem::on_link_inserted : &ContextItem::on_link_erase;
            std::vector<ContextItem*> const handlers { it->second.begin(), it->second.end() };
            for (ContextItem* const handler : handlers)
                (handler->*method)(this, subject);
        }
    }
    else UNREACHABLE();
}

File::File(std::string const& name)
    : ContextItem{ name }
    , m_handlers{ nullptr }
{}

File::~File()
{
    INVARIANT(m_handlers == nullptr);
}

File* File::clone() const
{
    return new File(name());
}

void File::register_on_content_changed(ContextItem* const event_receiver)
{
    ASSUMPTION(event_receiver != nullptr);
    if (m_handlers == nullptr)
        m_handlers = new std::unordered_set<ContextItem*>;
    auto const it_and_bool = m_handlers->insert(event_receiver);
    ASSUMPTION(it_and_bool.second);
}

std::vector<ContextItem*> File::registered_on_content_changed() const {
    if (m_handlers == nullptr) return {};
    return { m_handlers->begin(), m_handlers->end() };
}

void File::unregister_on_content_changed(ContextItem* const event_receiver)
{
    ASSUMPTION(event_receiver != nullptr);
    ASSUMPTION(m_handlers != nullptr);
    auto const it = m_handlers->find(event_receiver);
    ASSUMPTION(it != m_handlers->end());
    m_handlers->erase(it);
    if (m_handlers->empty())
    {
        delete m_handlers;
        m_handlers = nullptr;
    }
}

void File::notify_content_changed()
{
    if (m_handlers != nullptr)
    {
        std::vector<ContextItem*> const handlers { registered_on_content_changed() };
        for (ContextItem* const handler : handlers)
            handler->on_content_changed(this);
    }
}

Link::Link(std::string const& name, ContextItem* const target)
    : ContextItem{ name }
    , m_handlers{ nullptr }
{
    set_target(target);
}

Link::~Link()
{
    INVARIANT(m_handlers == nullptr);
}

void Link::set_target(ContextItem* target)
{
    ASSUMPTION(target == nullptr || !target->is_link());

    bool change = false;

    if (m_linkage != nullptr)
    {
        if (m_linkage->m_target == target)
            return;

        INVARIANT(m_linkage->m_target->m_linkage == m_linkage);

        auto const it = m_linkage->m_links.find(this);
        INVARIANT(it != m_linkage->m_links.end());
        m_linkage->m_links.erase(it);

        send_on_linkage_changed();

        if (m_linkage->m_links.empty())
        {
            m_linkage->m_target->m_linkage = nullptr;
            delete m_linkage;
        }

        m_linkage = nullptr;

        change = true;
    }

    if (target != nullptr)
    {
        if (target->m_linkage == nullptr)
            target->m_linkage = new detail::Linkage{ target, this };
        else
        {
            auto const it_and_bool = target->m_linkage->m_links.insert(this);
            INVARIANT(it_and_bool.second);
        }

        m_linkage = target->m_linkage;
        INVARIANT(m_linkage->m_target->m_linkage == m_linkage);

        send_on_linkage_changed();

        change = true;
    }

    if (change)
        send_on_target_changed();
}

Link* Link::clone() const
{
    return new Link(name(), target());
}

void Link::register_on_target_changed(ContextItem* const event_receiver)
{
    ASSUMPTION(event_receiver != nullptr);
    if (m_handlers == nullptr)
        m_handlers = new std::unordered_set<ContextItem*>;
    auto const it_and_bool = m_handlers->insert(event_receiver);
    ASSUMPTION(it_and_bool.second);
}

void Link::unregister_on_target_changed(ContextItem* const event_receiver)
{
    ASSUMPTION(event_receiver != nullptr);
    ASSUMPTION(m_handlers != nullptr);
    auto const it = m_handlers->find(event_receiver);
    ASSUMPTION(it != m_handlers->end());
    m_handlers->erase(it);
    if (m_handlers->empty())
    {
        delete m_handlers;
        m_handlers = nullptr;
    }
}

void Link::send_on_target_changed()
{
    if (m_handlers != nullptr)
    {
        std::vector<ContextItem*> const handlers { m_handlers->begin(), m_handlers->end() };
        for (ContextItem* const handler : handlers)
            handler->on_target_changed(this);
    }
}

void Link::send_on_linkage_changed()
{
    if (m_linkage->m_target->m_handlers == nullptr)
        return;
    auto const it = m_linkage->m_target->m_handlers->find(ON_LINKAGE_CHANGED);
    if (it == m_linkage->m_target->m_handlers->end())
        return;
    std::vector<ContextItem*> const handlers { it->second.begin(), it->second.end() };
    for (ContextItem* const handler : handlers)
        handler->on_linkage_changed(this, m_linkage->m_target);
}

void DynamicLink::on_clone_integrated(ContextItem *const original_item)
{
    com::DynamicLink *const link{dynamic_cast<DynamicLink *>(original_item)};
    INVARIANT(link != nullptr);

    com::ContextPath relative_path = find_relative_path(link, link->target());
    com::ContextItem *located_item = folder()->locate(relative_path);

    if (located_item && located_item->name() == link->target()->name())
    {
        set_target(located_item);
    }
}

}
