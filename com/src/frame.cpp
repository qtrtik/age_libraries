#include <com/frame.hpp>
#include <utils/invariants.hpp>

namespace com {

Frame::Frame()
    : com::File{ self_file_name() }
    , m_frame {}
    , m_scale { nullptr }
    , m_basis { nullptr }
    , m_world_space_cache { nullptr }
    , m_in { nullptr }
    , m_out { nullptr }
    , m_parent { nullptr }
{}

Frame::Frame(Frame const& source)
    : com::File{ self_file_name() }
    , m_frame { source.m_frame}
    , m_scale { source.m_scale == nullptr ? nullptr : new vec3{ *source.m_scale } }
    , m_in { nullptr }
    , m_out { nullptr }
    , m_parent { nullptr }
    , m_basis { nullptr }
    , m_world_space_cache  { nullptr }
{}

Frame::~Frame()
{
    delete_mutable_data();
    delete m_scale;
}

void Frame::initialize()
{
    folder()->register_on_link_inserted(this);
    folder()->register_on_link_erase(this);

    com::Link* const link = folder()->find<com::Link>(parent_link_file_name());
    if (link != nullptr)
        Frame::on_link_inserted(folder(), link);
}

void Frame::release()
{
    folder()->unregister_on_link_inserted(this);
    folder()->unregister_on_link_erase(this);

    com::Link* const link = folder()->find<com::Link>(parent_link_file_name());
    if (link != nullptr)
        link->unregister_on_target_changed(this);

    if (m_parent != nullptr)
        m_parent->unregister_on_content_changed(this);
}

basis3 const& Frame::basis() const
{
    if (m_basis == nullptr)
        m_basis = new basis3{ frame().rotation() };
    return *m_basis;
}

quat const& Frame::rotation_in_world_space() const
{
    update_world_space_cache();
    return std::get<0>(*m_world_space_cache);
}

basis3 const& Frame::basis_in_world_space() const
{
    update_world_space_cache();
    return std::get<1>(*m_world_space_cache);
}

vec3 const& Frame::position_in_world_space() const {
    update_world_space_cache();
    return std::get<2>(*m_world_space_cache);
}

vec3 const& Frame::scale_in_world_space() const
{
    update_world_space_cache();
    return std::get<3>(*m_world_space_cache);
}

void Frame::update_world_space_cache() const
{
    if (m_world_space_cache == nullptr)
    {
        if (m_parent == nullptr)
            m_world_space_cache = new std::tuple( rotation(), basis(), origin(), scale() );
        else
        {
            m_parent->update_world_space_cache();

            quat const& parent_global_rotation = std::get<0>(*m_parent->m_world_space_cache);
            vec3 const& parent_global_position = std::get<2>(*m_parent->m_world_space_cache);
            vec3 const& parent_global_scale    = std::get<3>(*m_parent->m_world_space_cache);

            vec3 const global_position = m_parent->out() * vec4(origin(), 1.0f);
            quat const global_rotation = normalized(parent_global_rotation * rotation());
            vec3 const global_scale    = hadamard_product(parent_global_scale, scale());
            basis3 const global_basis  = basis3 { global_rotation };
            m_world_space_cache = new std::tuple{ global_rotation, global_basis, global_position, global_scale };
        }
    }
}

mat4x4 const& Frame::in() const
{
    if (m_in == nullptr)
    {
        if (m_scale == nullptr)
        {
            if (m_parent == nullptr)
                m_in = new mat4x4{ matrix_in(frame()) };
            else
                m_in = new mat4x4{ matrix_in(frame()) * parent()->in() };
        }
        else
        {
            if (m_parent == nullptr)
                m_in = new mat4x4{ to_scale_matrix(*m_scale) * matrix_in(frame()) };
            else
                m_in = new mat4x4{ to_scale_matrix(*m_scale) * matrix_in(frame()) * parent()->in() };
        }
    }
    return *m_in;
}

mat4x4 const& Frame::out() const
{
    if (m_out == nullptr)
    {
        if (m_scale == nullptr)
        {
            if (m_parent == nullptr)
                m_out = new mat4x4{ matrix_out(frame()) };
            else
                m_out = new mat4x4{ parent()->out() * matrix_out(frame()) };
        }
        else
        {
            if (m_parent == nullptr)
                m_out = new mat4x4{ matrix_out(frame()) * to_scale_matrix(*m_scale) };
            else
                m_out = new mat4x4{ parent()->out() * matrix_out(frame()) * to_scale_matrix(*m_scale) };
        }
    }
    return *m_out;
}

void Frame::set_frame(frame3 const& new_frame)
{
    m_frame = new_frame;
    if (delete_mutable_data())
        notify_content_changed();
}

void Frame::set_scale(vec3 const& scale)
{
    if (equal(scale, {1,1,1}))
    {
        if (m_scale == nullptr)
            return;

        delete m_scale;
        m_scale = nullptr;
    }
    else if (m_scale == nullptr)
        m_scale = new vec3{ scale };
    else
        *m_scale = scale;

    if (delete_mutable_data())
        notify_content_changed();
}

void Frame::on_link_inserted(Folder* const sender, Link* const subject)
{
    ASSUMPTION(sender == folder());
    if (subject->name() != parent_link_file_name())
        return;
    subject->register_on_target_changed(this);
    if (set_parent(subject->target()))
        if (delete_mutable_data())
            notify_content_changed();
}

void Frame::on_link_erase(Folder* const sender, Link* const subject)
{
    ASSUMPTION(sender == folder());
    if (subject->name() != parent_link_file_name())
        return;
    subject->unregister_on_target_changed(this);
    if (set_parent(nullptr))
        if (delete_mutable_data())
            notify_content_changed();
}

void Frame::on_content_changed(File* const sender)
{
    ASSUMPTION(sender->folder() != folder() && sender->name() == name() && dynamic_cast<Frame*>(sender) != nullptr);
    if (delete_mutable_data())
        notify_content_changed();
}

void Frame::on_target_changed(Link* const sender)
{
    ASSUMPTION(sender->folder() == folder() && sender->name() == parent_link_file_name());
    if (set_parent(sender->target()))
        if (delete_mutable_data())
            notify_content_changed();
}

bool Frame::set_parent(ContextItem* const new_parent)
{
    ASSUMPTION(new_parent == nullptr || new_parent->folder() != folder());

    if (new_parent == m_parent)
        return false;

    if (m_parent != nullptr)
        m_parent->unregister_on_content_changed(this);

    m_parent = dynamic_cast<Frame*>(new_parent);
    INVARIANT(new_parent == nullptr || m_parent != nullptr);

    if (m_parent != nullptr)
        m_parent->register_on_content_changed(this);

    return true;
}

bool Frame::delete_mutable_data()
{
    bool const retval = m_basis != nullptr || m_world_space_cache != nullptr || m_in != nullptr || m_out != nullptr;

    delete m_basis;
    m_basis = nullptr;

    delete m_world_space_cache;
    m_world_space_cache = nullptr;

    delete m_in;
    m_in = nullptr;

    delete m_out;
    m_out = nullptr;

    return retval;
}

void Frame::set_position_in_world_space(vec3 const &pos)
{
    set_origin(parent()->in() * vec4(pos, 1.0f));
}

void Frame::move_position_in_world_space(vec3 const &u)
{
    set_position_in_world_space(position_in_world_space() + u);
}

void Frame::set_rotation_in_world_space(quat const &q)
{
    set_rotation(normalized(to_quat(parent()->in()) * q));
}

void Frame::move_rotation_in_world_space(quat const &q)
{
    set_rotation_in_world_space(q * rotation_in_world_space());
}

void Frame::set_scale_in_world_space(vec3 const &scale)
{
    set_scale(parent()->in() * vec4(scale, 1.0f));
}

}
