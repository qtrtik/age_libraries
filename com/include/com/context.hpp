#ifndef COM_CONTEXT_HPP_INCLUDED
#   define COM_CONTEXT_HPP_INCLUDED

#   include <utils/assumptions.hpp>
#   include <unordered_map>
#   include <unordered_set>
#   include <functional>
#   include <string>
#   include <vector>
#   include <list>
#   include <memory>
#   include <cstdint>
#   include <type_traits>
#   include <iostream>

namespace osi { struct Simulator; }

namespace com {

struct ContextItem;
struct Folder;
struct File;
struct Link;

using ContextPath = std::vector<std::string>;

struct context_error : public std::logic_error
{
    explicit context_error(std::string const& msg) : std::logic_error(msg) {}
};

namespace detail {
    struct Linkage
    {
        Linkage(ContextItem* const target, Link* const link);
        ContextItem* m_target;
        std::unordered_set<Link*> m_links;
    };
}

struct ContextItem
{
    ContextItem(std::string const& name);
    virtual ~ContextItem() = 0;

    std::string const& name() const { return m_name; }
    Folder* folder() const { return m_folder; }

    bool is_under(Folder* f) const;

    bool is_folder() const;
    bool is_file() const;
    bool is_link() const;

    void register_on_linkage_changed(ContextItem* event_receiver) { register_handler(ON_LINKAGE_CHANGED, event_receiver); }
    void unregister_on_linkage_changed(ContextItem* event_receiver) { unregister_handler(ON_LINKAGE_CHANGED, event_receiver); }
    void register_on_erase(ContextItem* event_receiver) { register_handler(ON_ERASE, event_receiver); }
    void unregister_on_erase(ContextItem* event_receiver) { unregister_handler(ON_ERASE, event_receiver); }

    std::unordered_set<Link*> const* linkage() const { return m_linkage == nullptr ? nullptr : &m_linkage->m_links; }

    ContextPath path(Folder* end = nullptr) const;

protected:
    virtual void initialize() {}
    virtual void initialize_clone() { initialize(); }
    virtual void release() {}

    virtual void on_erase(ContextItem* subject) {}

    virtual ContextItem* clone() const = 0;
    virtual void on_clone_integrated(ContextItem* original) {} // Called after the clone is inserted into some Context's folder.

    virtual void on_folder_inserted(Folder* sender, Folder* subject) {}
    virtual void on_file_inserted(Folder* sender, File* subject) {}
    virtual void on_link_inserted(Folder* sender, Link* subject) {}

    virtual void on_folder_erase(Folder* sender, Folder* subject) {}
    virtual void on_file_erase(Folder* sender, File* subject) {}
    virtual void on_link_erase(Folder* sender, Link* subject) {}

    virtual void on_content_changed(File* sender) {}

    virtual void on_target_changed(Link* sender) {}
    virtual void on_linkage_changed(Link* sender, ContextItem* subject) {}

private:
    enum EventType
    {
        ON_LINKAGE_CHANGED,
        ON_ERASE
    };
    
    friend struct ::com::Folder;
    friend struct ::com::File;
    friend struct ::com::Link;

    std::string m_name;
    Folder* m_folder;
    detail::Linkage* m_linkage;
    std::unordered_map<EventType, std::unordered_set<ContextItem*>>* m_handlers;

    void register_handler(EventType type, ContextItem* item);
    void unregister_handler(EventType type, ContextItem* item);
    void send_on_erase();
};

struct Folder : public ContextItem
{
    Folder(std::string const& name);
    ~Folder() override;

    std::list<ContextItem*> const& items() const { return m_items; }
    std::vector<Folder*> const& subfolders() const { update_cache(m_subfolders); return *m_subfolders; }
    std::vector<File*> const& files() const { update_cache(m_files); return *m_files; }
    std::vector<Link*> const& links() const { update_cache(m_links); return *m_links; }

    template<typename ItemType, class... ParameterTypes>
    ItemType* push_front(ParameterTypes... args)
    { return create_and_insert_item<ItemType>([this](ContextItem* item){ push_front(item); }, args...); }

    Folder* push_front_folders(ContextPath const& path) { return insert_folders(path, [](Folder* folder, ContextItem* item){ folder->push_front(item); }); }

    template<typename ItemType, class... ParameterTypes>
    ItemType* push_back(ParameterTypes... args)
    { return create_and_insert_item<ItemType>([this](ContextItem* item){ push_back(item); }, args...); }

    Folder* push_back_folders(ContextPath const& path) { return insert_folders(path, [](Folder* folder, ContextItem* item){ folder->push_back(item); }); }

    template<typename ItemType, class... ParameterTypes>
    ItemType* insert_before(std::string const& name, ParameterTypes... args)
    { return create_and_insert_item<ItemType>([this, &name](ContextItem* item){ insert_before(item, name); }, args...); }

    template<typename ItemType, class... ParameterTypes>
    ItemType* insert_after(std::string const& name, ParameterTypes... args)
     { return create_and_insert_item<ItemType>([this, &name](ContextItem* item){ insert_after(name, item); }, args...); }

    void move_front(ContextItem* const item)
    { insert(item->folder() == nullptr ? item : item->folder()->extract(item), [this](ContextItem* item){ push_front(item); }); }

    void move_back(ContextItem* const item)
    { insert(item->folder() == nullptr ? item : item->folder()->extract(item), [this](ContextItem* item){ push_back(item); }); }

    void move_before(ContextItem* const item, std::string const& name)
    { insert(item->folder() == nullptr ? item : item->folder()->extract(item), [this, &name](ContextItem* item){ insert_before(item, name); }); }

    void move_after(std::string const& name, ContextItem* const item)
    { insert(item->folder() == nullptr ? item : item->folder()->extract(item), [this, &name](ContextItem* item){ insert_after(name, item); }); }

    ContextItem* copy_front(ContextItem* const item, std::string const &unique_name);
    ContextItem* copy_back(ContextItem* const item, std::string const &unique_name);
    ContextItem* copy_before(ContextItem* const item, std::string const& name, std::string const &unique_name);
    ContextItem* copy_after(std::string const& name, ContextItem* const item, std::string const &unique_name);

    ContextItem* extract(ContextItem* item) { return extract(item->name()); }
    ContextItem* extract(std::string const& name);

    void erase(ContextItem* item) { erase(item->name()); }
    void erase(std::string const& name);

    static void erase_empty(Folder* start, Folder* end = root());

    void clear() { while (!m_items.empty()) erase(m_items.front()); }

    bool empty() const { return m_items.empty(); }
    bool contains(std::string const& name) const { return m_dictionary.count(name) != 0; }

    ContextItem* find(std::string const& name) const;

    template<typename ItemType>
    ItemType* find(std::string const& name) const { return dynamic_cast<ItemType*>(find(name)); }

    ContextItem* locate(ContextPath const& path) const;

    template<typename ItemType>
    ItemType* locate(ContextPath const& path) const { return dynamic_cast<ItemType*>(locate(path)); }

    static Folder* root();

    Folder* clone() const override;

    std::string generate_unique_name(std::string const& prefix = {}, std::string const& suffix = {}) const;

    void register_on_folder_inserted(ContextItem* const event_receiver) { register_handler(FOLDER_INSERTED, event_receiver); }
    void register_on_file_inserted(ContextItem* const event_receiver) { register_handler(FILE_INSERTED, event_receiver); }
    void register_on_link_inserted(ContextItem* const event_receiver) { register_handler(LINK_INSERTED, event_receiver); }

    void register_on_folder_erase(ContextItem* const event_receiver) { register_handler(FOLDER_ERASE, event_receiver); }
    void register_on_file_erase(ContextItem* const event_receiver) { register_handler(FILE_ERASE, event_receiver); }
    void register_on_link_erase(ContextItem* const event_receiver) { register_handler(LINK_ERASE, event_receiver); }

    void unregister_on_folder_inserted(ContextItem* const event_receiver) { unregister_handler(FOLDER_INSERTED, event_receiver); }
    void unregister_on_file_inserted(ContextItem* const event_receiver) { unregister_handler(FILE_INSERTED, event_receiver); }
    void unregister_on_link_inserted(ContextItem* const event_receiver) { unregister_handler(LINK_INSERTED, event_receiver); }

    void unregister_on_folder_erase(ContextItem* const event_receiver) { unregister_handler(FOLDER_ERASE, event_receiver); }
    void unregister_on_file_erase(ContextItem* const event_receiver) { unregister_handler(FILE_ERASE, event_receiver); }
    void unregister_on_link_erase(ContextItem* const event_receiver) { unregister_handler(LINK_ERASE, event_receiver); }

private:
    enum EventType {
        FOLDER_INSERTED,
        FILE_INSERTED,
        LINK_INSERTED,

        FOLDER_ERASE,
        FILE_ERASE,
        LINK_ERASE,
    };

    template<typename ItemType, class... ParameterTypes>
    ItemType* create_and_insert_item(std::function<void(ContextItem*)> const& inserter, ParameterTypes... args)
    {
        static_assert(std::is_base_of<::com::ContextItem, ItemType>::value, "The template param ItemType must be derived from ::com::ContextItem.");
        std::unique_ptr<ItemType> item = std::make_unique<ItemType>(args...);
        insert(item.get(), inserter);
        return item.release();
    }

    template<typename T>
    void update_cache(std::vector<T*>*& cache) const
    {
        if (cache == nullptr)
        {
            cache = new std::vector<T*>;
            for (ContextItem* item : m_items)
                if (T* const ptr = dynamic_cast<T*>(item))
                    cache->push_back(ptr);
        }
    }

    void delete_cache(ContextItem* item) const;
    void delete_all_caches() const;

    void insert(ContextItem* item, std::function<void(ContextItem*)> const& inserter);
    void push_front(ContextItem* item);
    void push_back(ContextItem* item);
    void insert_before(ContextItem* item, std::string const& name);
    void insert_after(std::string const& name, ContextItem* item);

    Folder* insert_folders(ContextPath const& path, std::function<void(Folder*, ContextItem*)> const& inserter);

    ContextItem* copy(ContextItem* item, std::function<void(ContextItem*)> const& inserter, std::string const & unique_name, std::unordered_map<ContextItem*, ContextItem*>& orig_clone);
    void notify_clone_integration_impl(std::unordered_map<ContextItem*, ContextItem*>& orig_clone);
    void notify_clone_integration(ContextItem* original, ContextItem* clone);
    ContextItem* copy_front_impl(ContextItem* const item, std::string const & unique_name, std::unordered_map<ContextItem*, ContextItem*>& orig_clone) { return copy(item, [this](ContextItem* item){ push_front(item); }, unique_name, orig_clone); }
    ContextItem* copy_back_impl(ContextItem* const item, std::string const & unique_name, std::unordered_map<ContextItem*, ContextItem*>& orig_clone) { return copy(item, [this](ContextItem* item){ push_back(item); }, unique_name, orig_clone); }
    ContextItem* copy_before_impl(ContextItem* const item, std::string const& name, std::string const & unique_name, std::unordered_map<ContextItem*, ContextItem*>& orig_clone) { return copy(item, [this, &name](ContextItem* item){ insert_before(item, name); }, unique_name, orig_clone); }
    ContextItem* copy_after_impl(std::string const& name, ContextItem* const item, std::string const & unique_name, std::unordered_map<ContextItem*, ContextItem*>& orig_clone) { return copy(item, [this, &name](ContextItem* item){ insert_after(name, item); }, unique_name, orig_clone); }

    void register_handler(EventType type, ContextItem* item);
    void unregister_handler(EventType type, ContextItem* item);
    void call_handler(ContextItem* item, bool insertion);

    std::list<ContextItem*> m_items;
    std::unordered_map<std::string, std::list<ContextItem*>::iterator> m_dictionary;

    std::unordered_map<EventType, std::unordered_set<ContextItem*> >* m_handlers;

    mutable std::vector<Folder*>* m_subfolders;
    mutable std::vector<File*>* m_files;
    mutable std::vector<Link*>* m_links;

    mutable std::uint64_t m_fresh_name_id;
};

struct File : public ContextItem
{
    File(std::string const& name);
    ~File() override;

    File* clone() const override;

    void register_on_content_changed(ContextItem* event_receiver);
    std::vector<ContextItem*> registered_on_content_changed() const;
    void unregister_on_content_changed(ContextItem* event_receiver);

    void notify_content_changed();

private:
    std::unordered_set<ContextItem*>* m_handlers;
};

struct Link : public ContextItem
{
    Link(std::string const& name, ContextItem* target);
    ~Link() override;

    template<typename ItemType>
    ItemType* target() const { return dynamic_cast<ItemType*>(target()); }

    ContextItem* target() const { return m_linkage == nullptr ? nullptr : m_linkage->m_target; }
    void set_target(ContextItem* target);

    Link* clone() const override;

    void register_on_target_changed(ContextItem* event_receiver);
    void unregister_on_target_changed(ContextItem* event_receiver);

private:
    void send_on_target_changed();
    void send_on_linkage_changed();

    std::unordered_set<ContextItem*>* m_handlers;
};

struct DynamicLink : Link
{
    DynamicLink(std::string const &name, ContextItem *target) : Link(name, target) {}

    DynamicLink* clone() const override { return new DynamicLink(name(), target()); }

protected:
    void on_clone_integrated(ContextItem* original_item) override;
};

}

#endif
