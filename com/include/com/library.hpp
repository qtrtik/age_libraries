#ifndef COM_LIBRARY_HPP_INCLUDED
#   define COM_LIBRARY_HPP_INCLUDED

#   include <com/context.hpp>

namespace com {

struct Library : public com::File
{
    Library(std::string const& name) : com::File{ name } {}
    ~Library() override {}

private:
};

}

#endif
