#ifndef COM_MATH_FILES_HPP_INCLUDED
#   define COM_MATH_FILES_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>
#   include <math/bounding_volume.hpp>
#   include <cstdint>

namespace com {

template<typename T>
struct MathFile final : public File
{
    using value_type = T;

    MathFile(std::string const& name)
        : MathFile{ name, zero<value_type>() }
    {}

    MathFile(std::string const& name, value_type const& value)
        : File{ name }
        , m_value{ value }
    {}

    value_type const& get() const { return m_value; }
    void set(value_type const& value) { m_value = value; notify_content_changed(); }

private:
    value_type m_value;
};

using FileBool = MathFile<bool>;
using FileUInt = MathFile<std::uint32_t>;
using FileInt = MathFile<std::int32_t>;
using FileScalar = MathFile<scalar>;
using FileVec2i = MathFile<vec2i>;
using FileVec3i = MathFile<vec3i>;
using FileVec2u = MathFile<vec2u>;
using FileVec3u = MathFile<vec3u>;
using FileVec4u = MathFile<vec4u>;
using FileVec2 = MathFile<vec2>;
using FileVec3 = MathFile<vec3>;
using FileVec4 = MathFile<vec4>;
using FileMat2x2 = MathFile<mat2x2>;
using FileMat3x3 = MathFile<mat3x3>;
using FileMat4x4 = MathFile<mat4x4>;
using FileAABB = MathFile<AABB>;

}

#endif
