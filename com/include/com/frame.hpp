#ifndef COM_FRAME_HPP_INCLUDED
#   define COM_FRAME_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>
#   include <vector>
#   include <unordered_set>
#   include <limits>

namespace com {

struct Frame final : public com::File
{
    Frame();
    ~Frame() override;

    static inline std::string self_file_name() { return "frame"; }
    static inline std::string parent_link_file_name() { return "frame_parent.link"; }

    frame3 const& frame() const { return m_frame; }
    quat const &rotation() const { return m_frame.rotation(); }
    basis3 const& basis() const;
    vec3 const &origin() const { return m_frame.origin(); };
    vec3 scale() const { return m_scale == nullptr ? vec3{ 1,1,1 } : *m_scale; }

    quat const& rotation_in_world_space() const;
    basis3 const& basis_in_world_space() const;
    vec3 const& position_in_world_space() const;
    vec3 const& scale_in_world_space() const;

    mat4x4 const& in() const;
    mat4x4 const& out() const;
    Frame* parent() const { return m_parent; }

    void set_frame(frame3 const& new_frame);
    void set_scale(vec3 const& scale);
    void set_origin(vec3 const& p) { set_frame({ p, m_frame.rotation() }); }
    void set_rotation(quat const& q) { set_frame({ m_frame.origin(), q }); }
    void move_origin(vec3 const& u) { set_frame({ m_frame.origin() + u, m_frame.rotation() }); }
    void move_rotation(quat const& q) { set_frame({ m_frame.origin(), normalized(q * m_frame.rotation()) }); }

    void set_position_in_world_space(vec3 const &p);
    void move_position_in_world_space(vec3 const &u);
    void set_rotation_in_world_space(quat const &q);
    void move_rotation_in_world_space(quat const &q);
    void set_scale_in_world_space(vec3 const &scale);

protected:
    void initialize() override;
    void release() override;

    Frame(Frame const& source);
    Frame* clone() const override { return new Frame(*this); }

    void on_link_inserted(Folder* sender, Link* subject) override;
    void on_link_erase(Folder* sender, Link* subject) override;
    void on_content_changed(File* sender) override;
    void on_target_changed(Link* sender) override;

private:
    bool set_parent(ContextItem* new_parent);
    bool delete_mutable_data();

    void update_world_space_cache() const;

    frame3 m_frame;
    vec3* m_scale;
    mutable basis3* m_basis;
    mutable std::tuple<quat, basis3, vec3, vec3>* m_world_space_cache;
    mutable mat4x4* m_in;
    mutable mat4x4* m_out;
    Frame* m_parent;
};

}

#endif
