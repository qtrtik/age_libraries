#ifndef COM_MODULE_HPP_INCLUDED
#   define COM_MODULE_HPP_INCLUDED

namespace com {

struct Folder;

void boot(Folder* ctx_root);
void shutdown(Folder* ctx_root);

}

#endif
