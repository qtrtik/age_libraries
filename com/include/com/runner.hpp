#ifndef COM_RUNNER_HPP_INCLUDED
#   define COM_RUNNER_HPP_INCLUDED

#   include <com/context.hpp>

namespace com {

struct Runner : public com::File
{
    Runner(std::string const& name, bool const ignore_paused = false)
        : com::File{ name }
        , m_ignore_paused{ ignore_paused }
    {}

    ~Runner() override {}

    bool ignore_paused() const { return m_ignore_paused; }
    void set_ignore_paused(bool const state) { m_ignore_paused = state; }

    virtual void next_round() {}

private:
    bool m_ignore_paused;
};

}

#endif
