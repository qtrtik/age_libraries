#include <algorithm>
#include <com/math_files.hpp>
#include <gfx/index.hpp>
#include <utils/development.hpp>

#include <gfx/material.hpp>

namespace gfx {
Material::Material(std::string const &name, Shader *shader)
    : com::Folder{ name }, m_shader{ shader }, m_uniforms{ nullptr } {}

void Material::initialize() {
    m_shader->register_on_content_changed(this);

    m_uniforms = push_back_folders({ "uniforms" });
    update_uniforms_folder();
}

void Material::release() { m_shader->unregister_on_content_changed(this); }

void Material::on_content_changed(com::File *sender) {
    ASSUMPTION(sender != nullptr);
    ASSUMPTION(sender->is_under(shader_system()->shaders()));
    ASSUMPTION(dynamic_cast<ShaderGraph *>(sender) == m_shader);

    update_uniforms_folder();
}

void Material::activate() const {
    shader_system()->set_active_shader(m_shader);
    set_shader_uniforms();
}

static void elem_to_file(ShaderGraph::Node::DataType::ElementType e, com::ContextItem *item, auto lambda) {
    switch (e) {
    case ShaderGraph::Node::DataType::INT: {
        auto m_file = dynamic_cast<com::FileInt *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::SCALAR: {
        auto m_file = dynamic_cast<com::FileScalar *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::VEC2: {
        auto m_file = dynamic_cast<com::FileVec2 *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::VEC3: {
        auto m_file = dynamic_cast<com::FileVec3 *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::VEC4: {
        auto m_file = dynamic_cast<com::FileVec4 *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::MAT2X2: {
        auto m_file = dynamic_cast<com::FileMat2x2 *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::MAT3X3: {
        auto m_file = dynamic_cast<com::FileMat3x3 *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::MAT4X4: {
        auto m_file = dynamic_cast<com::FileMat4x4 *>(item);
        INVARIANT(m_file != nullptr);
        lambda(m_file);
        break;
    }
    default: {
        UNREACHABLE();
        break;
    } // Other type of uniforms are not supported (yet).
    }
}

void Material::set_shader_uniforms() const {
    if (auto shader = dynamic_cast<ShaderGraph*>(m_shader)) {
        for (auto &shader_uniform : shader->m_uniforms) {
            if (shader_uniform->is_built_in())
                continue;

            auto u_file = m_uniforms->find(shader_uniform->name());
            INVARIANT(u_file != nullptr);

            elem_to_file(shader_uniform->output_type().element_type, u_file,
                         [&](auto m_file) { shader_uniform->set_uniform(m_file->get()); });
        }
    }
}

void Material::insert_uniform(const std::string &name, ShaderGraph::Node::DataType::ElementType e,
                              std::unordered_set<ContextItem *> &uniforms_in_graph) {
    switch (e) {
    case ShaderGraph::Node::DataType::INT: {
        auto m_file = m_uniforms->push_back<com::FileInt>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::SCALAR: {
        auto m_file = m_uniforms->push_back<com::FileScalar>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::VEC2: {
        auto m_file = m_uniforms->push_back<com::FileVec2>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::VEC3: {
        auto m_file = m_uniforms->push_back<com::FileVec3>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::VEC4: {
        auto m_file = m_uniforms->push_back<com::FileVec4>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::MAT2X2: {
        auto m_file = m_uniforms->push_back<com::FileMat2x2>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::MAT3X3: {
        auto m_file = m_uniforms->push_back<com::FileMat3x3>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    case ShaderGraph::Node::DataType::MAT4X4: {
        auto m_file = m_uniforms->push_back<com::FileMat4x4>(name);
        INVARIANT(m_file != nullptr);
        uniforms_in_graph.insert(m_file);
        break;
    }
    default: {
        UNREACHABLE();
        break;
    } // Other type of uniforms are not supported (yet).
    }
}

void Material::update_uniforms_folder() {
    if (auto shader = dynamic_cast<ShaderGraph*>(m_shader)) {
        std::unordered_set<ContextItem *> uniforms_in_graph;

        // insert missing uniform files
        for (auto &shader_uniform : shader->m_uniforms) {
            if (shader_uniform->is_built_in())
                continue;

            auto u_file = m_uniforms->find(shader_uniform->name());
            if (u_file == nullptr) {
                insert_uniform(shader_uniform->name(), shader_uniform->output_type().element_type, uniforms_in_graph);
            } else {
                uniforms_in_graph.insert(u_file);
            }
        }

        // erase unused uniform files
        for (auto item : m_uniforms->items())
            if (!uniforms_in_graph.contains(item))
                m_uniforms->erase(item);
    }
}

} // namespace gfx