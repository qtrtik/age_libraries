#define STBI_FAILURE_USERMSG
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <gfx/image_utils.hpp>

namespace gfx {


std::uint8_t*  decode_png_image_from_bytes(
        std::uint8_t const* const  bytes,
        std::size_t const  num_bytes,
        int&  out_width,
        int&  out_height,
        int&  out_num_channels
        )
{
    return stbi_load_from_memory(bytes, (int)num_bytes, &out_width, &out_height, &out_num_channels, 0);
}


void  free_memory_of_decoded_png_image(std::uint8_t* const  png_bytes)
{
    stbi_image_free((void*)png_bytes);
}


}
