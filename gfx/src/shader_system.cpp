#include <gfx/light.hpp>
#include <gfx/shader_nodes.hpp>
#include <gfx/shader_system.hpp>
#include <gfx/text_shader.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>

namespace gfx::detail {

// clang-format off

static std::string deferred_lighting_vertex() {
    return R"(#version 330 core

const vec2 tex_coords[3] = vec2[3] (
    vec2(0., 0.),
    vec2(2., 0.),
    vec2(0., 2.)
);

out vec2 tex_coord;

void main() {
    tex_coord = tex_coords[gl_VertexID];
    gl_Position = vec4(tex_coord * 2. - 1., 0., 1.);
}
)";
}

static std::string deferred_lighting_spheres_vertex() {
    return "#version 330 core\n"
           "layout (location = 0) in vec3 position;\n"
           "uniform vec3 positions[" + std::to_string(Shader::max_point_lights) + "];\n"
           "uniform float radii[" + std::to_string(Shader::max_point_lights) + "];\n" + R"(
out VertexData {
    noperspective vec2 tex_coord;
    flat int light_idx;
} out_data;

uniform mat4 )" + Shader::view_matrix_uniform + R"(;
uniform mat4 )" + Shader::projection_matrix_uniform + R"(;

void main() {
    vec3 pos_ws = position.xyz * radii[gl_InstanceID] + positions[gl_InstanceID].xyz;
    vec4 pos_cs = projection * view * vec4(pos_ws, 1.0);
    out_data.light_idx = gl_InstanceID;
    out_data.tex_coord = pos_cs.xy / pos_cs.w * 0.5 + 0.5;
    gl_Position = pos_cs;
})";
}

static std::string deferred_lighting_fragment() {
    auto a_name = AmbientLight::glsl::u_name();
    auto a_name_idx = a_name + "[i]";
    auto d_name = DirectionalLight::glsl::u_name();
    auto d_name_idx = d_name + "[i]";

    auto calc_a = AmbientLight::function_call(a_name_idx, "ambient");
    auto calc_d = DirectionalLight::function_call(d_name_idx, "diffuse", "specular", "normal", "viewDir");

    return "#version 330 core\n\n"
        "out vec4 fragColor;\n"
        "in vec2 tex_coord;\n"
        "uniform sampler2D gPosition;\n"
        "uniform sampler2D gNormal;\n"
        "uniform sampler2D gAmbient;\n"
        "uniform sampler2D gDiffuse;\n"
        "uniform sampler2D gSpecular;\n"
        "uniform sampler2D gEmission;\n"
        "uniform vec3 " + Shader::background_color_uniform + ";\n\n"
        + AmbientLight::declarations() + "\n"
        + DirectionalLight::declarations() + "\n"
        "uniform vec3 " + Shader::camera_pos_uniform + ";\n"
        "uniform int " + AmbientLight::glsl::nr_lights() + ";\n"
        "uniform " + AmbientLight::glsl::type() + ' ' + a_name + '[' + std::to_string(Shader::max_ambient_lights) + "];\n"
        "uniform int " + DirectionalLight::glsl::nr_lights() + ";\n"
        "uniform " + DirectionalLight::glsl::type() + ' ' + d_name + '[' + std::to_string(Shader::max_directional_lights) + "];\n"
        "void main() {\n"
        "    vec3 ambient = texture(gAmbient, tex_coord).rgb;\n"
        "    vec3 diffuse = texture(gDiffuse, tex_coord).rgb;\n"
        "    vec3 specular = texture(gSpecular, tex_coord).rgb;\n"
        "    vec3 emission = texture(gEmission, tex_coord).rgb;\n"
        "    vec3 position = texture(gPosition, tex_coord).rgb;\n"
        "    vec3 normal = texture(gNormal, tex_coord).rgb;\n"
        "    vec3 viewDir = normalize(" + Shader::camera_pos_uniform + " - position);\n"
        "    if (texture(gPosition, tex_coord).w < 0.01) {\n" // position was not written to, so it's background.
        "        fragColor = vec4(" + Shader::background_color_uniform + ", 1.);\n"
        "        return;\n"
        "    }\n"
        "    vec3 result = emission;\n"
        "    for (int i = 0; i < " + AmbientLight::glsl::nr_lights() + "; i++)\n"
        "        result += " + calc_a + ";\n"
        "    for (int i = 0; i < " + DirectionalLight::glsl::nr_lights() + "; i++)\n"
        "        result += " + calc_d + ";\n"
        "    fragColor = vec4(result, 1.);\n"
        // "    fragColor = vec4(specular, 1. + result.r);\n" // Buffer debugging
        "}\n";
}

static std::string deferred_lighting_spheres_fragment() {
    auto p_name = PointLight::glsl::u_name();
    auto p_name_idx = p_name + "[in_data.light_idx]";

    auto calc_p = PointLight::function_call(
        p_name_idx, "diffuse",
        "specular", "normal", "position", "viewDir");

    return R"(#version 330 core
out vec4 fragColor;

in VertexData {
    noperspective vec2 tex_coord;
    flat int light_idx;
} in_data;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAmbient;
uniform sampler2D gDiffuse;
uniform sampler2D gSpecular;
)"
        + PointLight::declarations() + "\n\n"
        "uniform vec3 " + Shader::camera_pos_uniform + ";\n"
        "uniform " + PointLight::glsl::type() + ' ' + p_name + '[' + std::to_string(Shader::max_point_lights) + "];\n\n"
        "void main() {\n"
        "    vec3 ambient = texture(gAmbient, in_data.tex_coord).rgb;\n"
        "    vec3 diffuse = texture(gDiffuse, in_data.tex_coord).rgb;\n"
        "    vec3 specular = texture(gSpecular, in_data.tex_coord).rgb;\n"
        "    vec3 position = texture(gPosition, in_data.tex_coord).rgb;\n"
        "    vec3 normal = texture(gNormal, in_data.tex_coord).rgb;\n"
        "    vec3 viewDir = normalize(" + Shader::camera_pos_uniform + " - position);\n"
        "    if (texture(gPosition, in_data.tex_coord).w < 0.01 || length("
            + p_name_idx + PointLight::glsl::position() + " - position) > " + p_name_idx + PointLight::glsl::radius() + ")\n"
        "        discard;\n"
        "    vec3 result = " + calc_p + ";\n"
        "    fragColor = vec4(result, 1.);\n"
        // "    fragColor = vec4(pointLights[in_data.light_idx].color, 1. + result.r);\n" // Buffer debugging
        "}\n";
}

static std::string PBR_helpers() {
    return R"(
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}
)";
}

static std::string deferred_lighting_fragment_PBR() {
    auto d_name = DirectionalLight::glsl::u_name();
    auto d_name_idx = d_name + "[i]";
    auto p_name = PointLight::glsl::u_name();
    auto p_name_idx = p_name + "[i]";

    // Adapted from https://learnopengl.com/PBR/Lighting
    return R"(#version 330 core
out vec4 fragColor;
in vec2 tex_coord;)"
        + DirectionalLight::struct_decl() + ";\n"
        "uniform vec3 " + Shader::background_color_uniform + ";\n"
        "uniform vec3 " + Shader::camera_pos_uniform + ";\n"
        "uniform int " + DirectionalLight::glsl::nr_lights() + ";\n"
        "uniform " + DirectionalLight::glsl::type() + ' ' + d_name + '[' + std::to_string(Shader::max_directional_lights) + "];\n"
        + R"(;
uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedo;
uniform sampler2D gMetRoughAo;
uniform sampler2D gEmission;
const float PI = 3.14159265359; )" + PBR_helpers() + R"(
void main() {
    if (texture(gPosition, tex_coord).w < 0.01) {
        fragColor = vec4()" + Shader::background_color_uniform + R"(, 1.);
        return;
    }

    vec3 position = texture(gPosition, tex_coord).rgb;
    vec3 albedo = texture(gAlbedo, tex_coord).rgb;
    vec3 emission = texture(gEmission, tex_coord).rgb;
    float metallic = texture(gMetRoughAo, tex_coord).r;
    float roughness = texture(gMetRoughAo, tex_coord).g;
    float ao = texture(gMetRoughAo, tex_coord).b;
    vec3 N = texture(gNormal, tex_coord).rgb;
    vec3 V = normalize()" + Shader::camera_pos_uniform + R"( - position);

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    for(int i = 0; i < )" + DirectionalLight::glsl::nr_lights() + R"(; ++i) {
        vec3 L = normalize(-)" + d_name_idx + DirectionalLight::glsl::direction() + R"();
        vec3 H = normalize(V + L);
        vec3 radiance = )" + d_name_idx + DirectionalLight::glsl::color() + R"(;
        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001;
        vec3 specular = numerator / denominator;
        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;
        float NdotL = max(dot(N, L), 0.0);
        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
    }

    vec3 ambient = vec3(0.03) * albedo * ao;
    fragColor = vec4(ambient + Lo + emission, 1.);
})";
    // fragColor = vec4(position, 1. + color.r); // Buffer debugging
}

static std::string deferred_lighting_spheres_fragment_PBR() {
    auto p_name = PointLight::glsl::u_name();
    auto p_name_idx = p_name + "[in_data.light_idx]";

    // Adapted from https://learnopengl.com/PBR/Lighting
    return R"(#version 330 core
out vec4 fragColor;

in VertexData {
    noperspective vec2 tex_coord;
    flat int light_idx;
} in_data;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedo;
uniform sampler2D gMetRoughAo;
)"      + PointLight::struct_decl() + ";\n"
        "uniform vec3 " + Shader::camera_pos_uniform + ";\n"
        "uniform " + PointLight::glsl::type() + ' ' + p_name + '[' + std::to_string(Shader::max_point_lights) + R"(];
const float PI = 3.14159265359;)" + PBR_helpers() + R"(
void main() {
    vec3 position = texture(gPosition, in_data.tex_coord).rgb;
    vec3 albedo = texture(gAlbedo, in_data.tex_coord).rgb;
    float metallic = texture(gMetRoughAo, in_data.tex_coord).r;
    float roughness = texture(gMetRoughAo, in_data.tex_coord).g;
    float ao = texture(gMetRoughAo, in_data.tex_coord).b;

    if (texture(gPosition, in_data.tex_coord).w < 0.01 || length()"
            + p_name_idx + PointLight::glsl::position() + " - position) > " + p_name_idx + PointLight::glsl::radius() + R"()
        discard;

    vec3 N = texture(gNormal, in_data.tex_coord).rgb;
    vec3 V = normalize()" + Shader::camera_pos_uniform + R"( - position);

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    vec3 L = normalize()" + p_name_idx + PointLight::glsl::position() + R"( - position);
    vec3 H = normalize(V + L);
    float distance = length()" + p_name_idx + PointLight::glsl::position() + R"( - position);
    float attenuation = 1.0 / ()" +
        p_name_idx + PointLight::glsl::atten_const() +
        " + " + p_name_idx + PointLight::glsl::atten_lin() + " * distance + " +
        p_name_idx + PointLight::glsl::atten_quad() + R"( * distance * distance);
    vec3 radiance = )" + p_name_idx + PointLight::glsl::color() + R"( * attenuation;

    float NDF = DistributionGGX(N, H, roughness);
    float G   = GeometrySmith(N, V, L, roughness);
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

    vec3 numerator    = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001; // + 0.0001 to prevent divide by zero
    vec3 specular = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - metallic;

    float NdotL = max(dot(N, L), 0.0);

    Lo += (kD * albedo / PI + specular) * radiance * NdotL;

    fragColor = vec4(Lo, 1.);
})";
    // fragColor = vec4(position, 1. + color.r); // Buffer debugging
}

// clang-format on

} // namespace gfx::detail

namespace gfx {

ShaderSystem::ShaderSystem()
    : com::Library{ self_name() }, m_shaders{ nullptr }, m_shader_default{ nullptr },
      m_forward_unlit_vertex_color_shader{ nullptr },
      m_deferred_lighting_shader{ nullptr }, m_deferred_lighting_shader_PBR{ nullptr },
      m_light_volumes_shader{ nullptr }, m_light_volumes_shader_PBR{ nullptr },
      m_active_shader{ nullptr } {}

ShaderSystem::~ShaderSystem() {}

void ShaderSystem::initialize() {
    m_shaders = folder()->push_back<com::Folder>("shaders");
    m_active_shader = shaders()->push_back<com::Link>("active_shader.link", nullptr);
}

void ShaderSystem::release() { folder()->erase(shaders()); }

void ShaderSystem::set_active_shader(Shader *const shader) {
#ifndef MEASURING_COMPILE
    if (active_shader() != shader)
#endif
    {
        shader->compile();
        shader->activate();
        m_active_shader->set_target(shader);
    }
}

void ShaderSystem::erase_shader(Shader *const shader) {
    ASSUMPTION(shader->is_under(shaders()));
    shader->folder()->erase(shader);
}

Shader *ShaderSystem::default_shader() {
    if (m_shader_default == nullptr) {
        auto shader = insert_shader<ShaderGraph>({ "built-in-shaders" }, "default-shader", ShaderGraph::UNLIT);
        auto color = shader->insert<ShaderGraph::UniformNode>("color", ShaderGraph::Node::DataType::VEC3);
        shader->connect(shader->root(), ShaderGraph::MasterNodeUnlit::inputs::color, color, 0);
        auto emission = shader->insert<ShaderGraph::ConstantNode>(vec3{0.});
        shader->connect(shader->root(), ShaderGraph::MasterNodeUnlit::inputs::emission, emission, 0);
        m_shader_default = shader;
    }
    return m_shader_default;
}

Shader *ShaderSystem::forward_unlit_vertex_color_shader() {
    if (m_forward_unlit_vertex_color_shader == nullptr) {
        auto shader = insert_shader<ShaderGraph>({ "built-in-shaders" }, "forward-unlit-vertex-color-shader", ShaderGraph::UNLIT);
        auto color = shader->insert<ShaderGraph::ConstantNode>(vec3{0.});
        shader->connect(shader->root(), ShaderGraph::MasterNodeUnlit::inputs::color, color, 0);
        auto emission = shader->insert<ShaderGraph::VaryingColorNode>();
        shader->connect(shader->root(), ShaderGraph::MasterNodeUnlit::inputs::emission, emission, 0);
        m_forward_unlit_vertex_color_shader = shader;
    }
    return m_forward_unlit_vertex_color_shader;
}

Shader *ShaderSystem::deferred_lighting_shader() {
    if (m_deferred_lighting_shader == nullptr) {
        m_deferred_lighting_shader =
            insert_shader<TextShader>({ "built-in-shaders" }, "deferred-lighting-shader", Shader::DEFERRED,
                                      detail::deferred_lighting_vertex(), detail::deferred_lighting_fragment());
    }

    return m_deferred_lighting_shader;
}

Shader * ShaderSystem::light_volumes_shader() {
    if (m_light_volumes_shader == nullptr) {
        m_light_volumes_shader =
            insert_shader<TextShader>({ "built-in-shaders" }, "light-volumes-shader", Shader::DEFERRED,
                                      detail::deferred_lighting_spheres_vertex(), detail::deferred_lighting_spheres_fragment());
    }

    return m_light_volumes_shader;
}

Shader *ShaderSystem::deferred_lighting_shader_PBR() {
    if (m_deferred_lighting_shader_PBR == nullptr) {
        m_deferred_lighting_shader_PBR =
            insert_shader<TextShader>({ "built-in-shaders" }, "deferred-lighting-shader-PBR", Shader::DEFERRED,
                                      detail::deferred_lighting_vertex(), detail::deferred_lighting_fragment_PBR());
    }

    return m_deferred_lighting_shader_PBR;
}

Shader * ShaderSystem::light_volumes_shader_PBR() {
    if (m_light_volumes_shader_PBR == nullptr) {
        m_light_volumes_shader_PBR =
            insert_shader<TextShader>({ "built-in-shaders" }, "light-volumes-shader-PBR", Shader::DEFERRED,
                                      detail::deferred_lighting_spheres_vertex(), detail::deferred_lighting_spheres_fragment_PBR());
    }

    return m_light_volumes_shader_PBR;
}

} // namespace gfx

