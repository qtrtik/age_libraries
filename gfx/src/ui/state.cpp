# include <gfx/ui/state.hpp>

namespace gfx {

    State::State() : com::File{ self_name() } {}
    State::~State() {}

    void State::set_state(UI_STATE new_state) {
        if (state != new_state) {
            state = new_state;
            notify_content_changed();
        }
    }

    void State::set_state_always(UI_STATE new_state) {
        state = new_state;
        notify_content_changed();
    }

    void State::initialize() {
        state = ENABLED;
    }

    void State::release() {
        state = DISABLED;
    }

}