# include <gfx/ui/ui_updater.hpp>
# include <osi/index.hpp>
# include <gfx/index.hpp>

namespace gfx {

    UiUpdater::UiUpdater() : com::Runner{ self_name() } {}
    UiUpdater::~UiUpdater() {};

    void UiUpdater::initialize() {
        osi::updaters()->push_front<com::Link>("gfx_" + self_name() + ".link", this);
        seconds = 0;
        ticks = 0;
    }

    void UiUpdater::release() {
        osi::updaters()->erase("gfx_" + self_name() + ".link");
    }

    void UiUpdater::next_round() {
        if (!osi::keyboard()->text().empty()) {
            ui_system()->on_text_entered(osi::keyboard()->text());
        }
        for (std::string const& character : osi::keyboard()->just_pressed()) {
            ui_system()->on_key_pressed(character);
        }
        if (osi::mouse()->pos_delta() != vec2i(0, 0)) {
            ui_system()->handle_mouse_move(osi::mouse()->pos());
        }
        if (osi::mouse()->just_pressed().contains("MouseLeft")) {
            ui_system()->handle_mouse_press(osi::mouse()->pos(), LEFT);
        }
        if (osi::mouse()->just_released().contains("MouseLeft")) {
            ui_system()->handle_mouse_release(osi::mouse()->pos(), LEFT);
        }
    }

}

