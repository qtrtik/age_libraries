# include <gfx/ui/anchor.hpp>

namespace gfx {

Anchor::Anchor(scalar bottom, scalar left, scalar top, scalar right) : File(self_name()), m_bottom(bottom), m_left(left), m_top(top), m_right(right), m_init(false) {
}

Anchor::~Anchor() {
}

void Anchor::set_anchors(scalar new_bottom, scalar new_left, scalar new_top, scalar new_right) {
    m_bottom = std::clamp(new_bottom, 0.f, 1.0f);
    m_left = std::clamp(new_left, 0.f, 1.0f);
    m_top = std::clamp(new_top, 0.f, 1.0f);
    m_right = std::clamp(new_right, 0.f, 1.0f);
    m_init = true;
    notify_content_changed();
}

void Anchor::disable_anchors() {
    m_bottom = 0;
    m_left = 0;
    m_top = 0;
    m_right = 0;
    m_init = false;
}

bool Anchor::is_zero() const {
    return m_bottom == 0 && m_left == 0 && m_top == 0 && m_right == 0;
}

}