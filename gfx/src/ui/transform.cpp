# include <gfx/ui/transform.hpp>
# include <gfx/index.hpp>
# include <gfx/ui/anchor.hpp>

namespace gfx {

    Transform::Transform() : com::Folder { self_name() } {}

    Transform::~Transform() {}

    void Transform::initialize() {
        push_back<Anchor>(0.f, 0.f, 0.f, 0.f);
        push_back<com::FileAABB>(aabb_name(), AABB { vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 0.0f) });
    }

    void Transform::release() {
        erase(anchor_file_name());
        erase(aabb_name());
    }

    AABB Transform::get_aabb() {
        return find<com::FileAABB>(aabb_name())->get();
    }

    Anchor *Transform::get_anchor() {
        return find<Anchor>(anchor_file_name());
    }

    void Transform::set_anchor(scalar bottom, scalar left, scalar top, scalar right) {
        find<Anchor>(anchor_file_name())->set_anchors(bottom, left, top, right);
    }

    void Transform::set_aabb(AABB new_aabb) {
        find<com::FileAABB>(aabb_name())->set(new_aabb);
    }

    template<typename ParamType>
    void set_param(std::string const& name, ParamType value) {
        static_assert(std::is_base_of<com::MathFile<typename ParamType::value_type>, ParamType>::value,
                  "ParamType must be a specialization of MathFile");

        ParamType* parameter = find<ParamType>(name);
        parameter->set(value);
        parameter->notify_content_changed();
    }

}