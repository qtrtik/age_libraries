# include "utils/context_utils.hpp"
# include "utils/timeprof.hpp"

# include <ranges>
# include <gfx/ui/ui_element.hpp>
# include <gfx/ui/transform.hpp>
# include <gfx/ui/state.hpp>
# include <gfx/object_system.hpp>
# include <gfx/index.hpp>
# include <osi/index.hpp>
# include <utility>
# include <regex>

namespace gfx {

UiElement::UiElement() : Folder{ self_name() } {
}

UiElement::UiElement(std::string const &name, Folder* scene_folder) : Folder{ name }, m_scene_folder{scene_folder}, m_size(vec2i(0, 0)), m_parent(nullptr) {
}

UiElement::UiElement(std::string const &name, Folder* scene_folder, vec2 size) : Folder{ name }, m_scene_folder{scene_folder}, m_size(size), m_parent(nullptr) {
}

UiElement::~UiElement() {
}

void UiElement::initialize() {
    recalculate_size_in_meters();
    m_font_name = "OpenSans";
    m_font_size = 48U;

    auto *frame_folder = push_back<Folder>(frame_folder_name());
    frame_folder->push_back<com::Frame>()->register_on_content_changed(this);
    auto* frame_parent_link = frame_folder->push_back<com::Link>(frame_parent_link_name(), nullptr);
    frame_parent_link->register_on_target_changed(this);

    // Set default rect to half size
    push_back<com::FileAABB>(rectangle_file_name(),
                             AABB{ -expand(get_size_in_meters(), 0.f) / 2.0f,
                                   expand(get_size_in_meters(), 0.f) / 2.0f });

    push_back<Folder>(background_folder_name())->push_back<com::FileVec4u>(scissor_file_name(), vec4u(0, 0, osi::window()->size()));
    push_back<Folder>(foreground_folder_name())->push_back<com::FileVec4u>(scissor_file_name(), vec4u(0, 0, osi::window()->size()));
    push_back<Folder>(lights_folder_name());

    push_back<Transform>()->find<Anchor>(Transform::anchor_file_name())->register_on_content_changed(this);
    push_back<State>()->register_on_content_changed(this);

    mouse_mask = LEFT;
}

void UiElement::release() {
    erase(frame_folder_name());
    erase(background_folder_name());
    erase(foreground_folder_name());
    erase(lights_folder_name());

    erase(transform_folder_name());
    erase(buffer_link_file_name());
}

void UiElement::on_target_changed(com::Link *sender) {
    if (sender->name() == frame_parent_link_name() && sender->target<com::Frame>() != nullptr) {
        m_parent = dynamic_cast<UiElement*>(sender->target<com::Frame>()->folder()->folder());
    }
}

void UiElement::on_state_changed(State *state) {
    update_objects();
}

void UiElement::on_anchor_changed(Anchor* anchor) {
    if (!anchor->is_init()) return;
    vec2 parent_size = m_parent == nullptr
        ? ui_system()->get_size_in_meters(viewport_system()->active_viewport()->size_absolute())
        : vec2(m_parent->get_normalized_size().hi) * 2.f;

    vec2 half_sizes = vec2{(1.0f - anchor->get_left() - anchor->get_right()) / 2.0f * parent_size.x,
                             (1.0f - anchor->get_top() - anchor->get_bottom()) / 2.0f * parent_size.y};
    if (anchor->get_left() + anchor->get_right() >= 1.f) {
        half_sizes.x = get_size_in_meters().x / 2.f;
    }
    if (anchor->get_bottom() + anchor->get_top() >= 1.f) {
        half_sizes.y = get_size_in_meters().y / 2.f;
    }
    half_sizes = max(half_sizes, get_size_in_meters() / 2.f);

    scalar origin_x = (1.0f - anchor->get_left() - anchor->get_right()) / 2.0f * parent_size.x + anchor->get_left() * parent_size.x;
    scalar origin_y = (1.0f - anchor->get_top() - anchor->get_bottom()) / 2.0f * parent_size.y + anchor->get_bottom() * parent_size.y;

    if (m_parent != nullptr) {
        origin_x = std::clamp(origin_x, 0.f, parent_size.x);
        origin_y = std::clamp(origin_y, 0.f, parent_size.y);
        origin_x -= parent_size.x / 2.f;
        origin_y -= parent_size.y / 2.f;
    } else {
        origin_x = std::clamp(origin_x, half_sizes.x, parent_size.x - half_sizes.x);
        origin_y = std::clamp(origin_y, half_sizes.y, parent_size.y - half_sizes.y);
    }

    if (half_sizes.x != get_normalized_size().hi.x || half_sizes.y != get_normalized_size().hi.y ||
        -half_sizes.x != get_normalized_size().lo.x || -half_sizes.y != get_normalized_size().lo.y) {
        set_normalized_size(-expand(half_sizes, 0.f), expand(half_sizes, 0.f));
    }
    if (get_frame()->frame().origin().x != origin_x || get_frame()->frame().origin().y != origin_y)
        get_frame()->set_origin(vec3{origin_x, origin_y, 0.f});

}

void UiElement::on_content_changed(com::File *sender) {
    if (sender->name() == State::self_name()) {
        on_state_changed(dynamic_cast<State *>(sender));
    }
    // NOTE: since currently ui_system is ticking every frame, there is no need for there events,
    // if that changes in the future, uncomment
    // else if (auto *frame = dynamic_cast<com::Frame *>(sender)) {
    //     ui_system()->recalculate_element(this);
    // } else if (auto* anchor = dynamic_cast<com::FileAnchor *>(sender)) {
    //     ui_system()->recalculate_element(this);
    // }
}

Transform *UiElement::get_transform() const {
    return find<Transform>(transform_folder_name());
}

void UiElement::set_normalized_size(vec3 lo, vec3 hi) {
    auto* rect_file = get_norm_size_file();
    if (rect_file) {
        rect_file->set(AABB{lo, hi});
        on_size_changed();
    }
}

com::Link *UiElement::get_object_folder_link(Folder *ui_object) {
    ASSUMPTION(ui_object->is_under(this));
    return ui_object->find<com::Link>(object_folder_link_name());
}

com::Folder * UiElement::get_object_folder(Folder *ui_object) {
    ASSUMPTION(ui_object->is_under(this));
    return get_object_folder_link(ui_object)->target<Folder>();
}

com::Frame * UiElement::get_ui_object_frame(Folder *ui_object) {
    ASSUMPTION(ui_object->is_under(this));
    return ui_object->find<Folder>(frame_folder_name())->find<com::Frame>(frame_file_name());
}

com::Frame* UiElement::insert_frame(Folder *ui_object, com::Frame* parent) {
    ASSUMPTION(ui_object->is_under(this));
    auto *frame_folder = ui_object->push_back<Folder>(frame_folder_name());
    frame_folder->push_back<com::Link>(frame_parent_link_name(), parent == nullptr? get_frame() : parent);
    return frame_folder->push_back<com::Frame>();
}

com::FileVec4u * UiElement::get_scissor_file() const {
    return find<com::FileVec4u>(scissor_file_name());
}

com::FileVec4u * UiElement::get_foreground_scissor_file() const {
    return get_foreground_folder()->find<com::FileVec4u>(scissor_file_name());
}

com::FileVec4u * UiElement::get_background_scissor_file() const {
    return get_background_folder()->find<com::FileVec4u>(scissor_file_name());
}

com::Folder * UiElement::set_background_object_buffer(Material *enabled_material, Material *hovered_material,
    Material *pressed_material) {
    Buffer *buffer = buffer_generators()->insert_procedural_rectangle(get_size_in_meters() / 2.0f, name() + "_buffer",
                                                                      { "ui", get_scene_folder()->name(), name() });
    auto *bg_object_folder = set_background_object_buffer(buffer);
    set_object_materials_enabled(bg_object_folder, enabled_material);
    set_object_materials_hovered(bg_object_folder, hovered_material);
    set_object_materials_pressed(bg_object_folder, pressed_material);
    return bg_object_folder;
}

com::Folder * UiElement::set_background_object_buffer(Material* material) {
     Buffer *buffer = buffer_generators()->insert_procedural_rectangle(get_size_in_meters() / 2.0f, name() + "_buffer",
                                                                      { "ui", get_scene_folder()->name(), name() });
    auto *bg_object_folder = set_background_object_buffer(buffer);
    set_object_materials_enabled(bg_object_folder, material);
    set_object_materials_hovered(bg_object_folder, material);
    set_object_materials_pressed(bg_object_folder, material);
    return bg_object_folder;
}

com::Folder * UiElement::set_background_object_buffer(Buffer *buffer) {
    auto* background_folder = get_background_folder()->find<Folder>(background_ui_object_name());
    if (!background_folder) {
        background_folder = get_background_folder()->push_back<Folder>(background_ui_object_name());
        insert_frame(background_folder);
    }
    set_object_buffers_all(background_folder, buffer);
    m_background_object = background_folder;
    return background_folder;
}

com::Folder * UiElement::set_border_object_buffer(Buffer *buffer) {
    auto* border_folder = get_background_folder()->find<Folder>(border_ui_object_name());
    if (!border_folder) {
        border_folder= get_background_folder()->push_back<Folder>(border_ui_object_name());
        insert_frame(border_folder);
    }
    set_object_buffers_all(border_folder, buffer);
    m_border_object = border_folder;
    return border_folder;
}

com::Folder * UiElement::set_border_object_buffer(Material* material) {
    Buffer *buffer = buffer_generators()->insert_procedural_rectangle_wireframe(get_size_in_meters() / 2.0f, name() + "_border_buffer",
                                                                     { "ui", get_scene_folder()->name(), name() });
    auto *bg_object_folder = set_border_object_buffer(buffer);
    set_object_materials_enabled(bg_object_folder, material);
    set_object_materials_hovered(bg_object_folder, material);
    set_object_materials_pressed(bg_object_folder, material);
    return bg_object_folder;
}

com::Folder *UiElement::set_object_all(Folder* ui_object, Material *material, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                enabled_link_name(), material, buffer);
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                hovered_link_name(), material, buffer);
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                pressed_link_name(), material, buffer);

    return ui_object;
}

com::Folder* UiElement::set_object_enabled(Folder* ui_object, Material *material, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                enabled_link_name(), material, buffer);
    return ui_object;
}

com::Folder* UiElement::set_object_pressed(Folder* ui_object, Material *material, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                pressed_link_name(), material, buffer);
    return ui_object;
}

com::Folder * UiElement::set_object_materials_all(Folder *ui_object, Material *material) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                hovered_link_name(), material, nullptr);
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                enabled_link_name(), material, nullptr);
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                pressed_link_name(), material, nullptr);
    return ui_object;
}

com::Folder * UiElement::set_object_materials_enabled(Folder *ui_object, Material *material) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                enabled_link_name(), material, nullptr);
    return ui_object;
}

com::Folder * UiElement::set_object_materials_hovered(Folder *ui_object, Material *material) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                hovered_link_name(), material, nullptr);
    return ui_object;
}

com::Folder * UiElement::set_object_materials_pressed(Folder *ui_object, Material *material) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                pressed_link_name(), material, nullptr);
    return ui_object;
}

com::Folder* UiElement::set_object_hovered(Folder* ui_object, Material *material, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                hovered_link_name(), material, buffer);
    return ui_object;
}

void UiElement::_set_object(Folder *materials_folder, Folder *buffers_folder, std::string const &state_name,
                            Material *material, Buffer *buffer) {
    ASSUMPTION(state_name == enabled_link_name()
            || state_name == hovered_link_name()
            || state_name == pressed_link_name());

    if (material != nullptr) {
        auto* material_link =  materials_folder->find<com::Link>(state_name);
        if (material_link == nullptr)
            material_link = materials_folder->push_back<com::Link>(state_name, nullptr);
        material_link->set_target(material);
    }

    if (buffer != nullptr) {
        auto* buffer_link =  buffers_folder->find<com::Link>(state_name);
        if (buffer_link == nullptr)
            buffer_link = buffers_folder->push_back<com::Link>(state_name, nullptr);
        buffer_link->set_target(buffer);
    }
}

FontProps * UiElement::get_font_props(std::string const &font_name, size_t font_size) const {
    ASSUMPTION(!m_font_name.empty());
    ASSUMPTION(m_font_size > 0);
    if (!m_required_fonts.empty())
        return nullptr;
    return font_loader()->get_font(font_name, font_size);
}

Material * UiElement::get_font_material(std::string const &name, size_t font_size) const {
    ASSUMPTION(m_required_fonts.empty());
    return font_loader()->get_material_for_font(name, font_size);
}

com::Folder * UiElement::set_object_buffers_all(Folder *ui_object, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                hovered_link_name(), nullptr, buffer);
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                enabled_link_name(), nullptr, buffer);
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                pressed_link_name(), nullptr, buffer);
    return ui_object;
}

com::Folder * UiElement::set_object_buffers_enabled(Folder *ui_object, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                enabled_link_name(), nullptr, buffer);
    return ui_object;
}

com::Folder * UiElement::set_object_buffers_hovered(Folder *ui_object, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                hovered_link_name(), nullptr, buffer);
    return ui_object;
}

com::Folder * UiElement::set_object_buffers_pressed(Folder *ui_object, Buffer *buffer) {
    _set_object(ui_system()->get_materials_folder(ui_object),
                ui_system()->get_buffers_folder(ui_object),
                pressed_link_name(), nullptr, buffer);
    return ui_object;
}

void UiElement::recalculate_size_in_meters() {
    vec2 size_normalized = (vec2)m_size / (vec2)osi::window()->size();
    m_size_meters = osi::window()->to_absolute_meters(size_normalized);
}

void UiElement::recalculate_scissor() {
    vec2 pixel_size = osi::window()->pixel_size_in_meters();
    vec3 absolute_origin = get_origin_from_scene_frame_link();

    auto rect_intersect = get_normalized_size();
    rect_intersect.lo += absolute_origin;
    rect_intersect.hi += absolute_origin;

    auto* parent = get_parent();
    while (parent != nullptr) {
        vec3 parent_absolute_origin = parent->get_origin_from_scene_frame_link();
        auto parent_rect = parent->get_normalized_size();
        parent_rect.lo += parent_absolute_origin;
        parent_rect.hi += parent_absolute_origin;

        rect_intersect = AABB::intersection(parent_rect, rect_intersect);

        parent = parent->get_parent();
    }

    vec2 frame_offset = rect_intersect.lo;
    vec2 scissor_origin = round(frame_offset / pixel_size);
    vec2 size = round(contract(rect_intersect.hi - rect_intersect.lo) / pixel_size);
    //round

    get_foreground_folder()->find<com::FileVec4u>(scissor_file_name())->set(vec4u(scissor_origin, size));
    get_background_folder()->find<com::FileVec4u>(scissor_file_name())->set(vec4u(scissor_origin, size));

    if (auto* object_folder = ui_system()->get_object(this)) {
        auto* fg_scissor = object_folder->find<Folder>(foreground_folder_name())->find<com::FileVec4u>(ObjectSystem::scissor_file_name());
        if (fg_scissor) fg_scissor->set(vec4u(scissor_origin, size));
        auto* bg_scissor = object_folder->find<Folder>(background_folder_name())->find<com::FileVec4u>(ObjectSystem::scissor_file_name());
        if (bg_scissor) bg_scissor->set(vec4u(scissor_origin, size));
    }
}

void UiElement::update_objects() {
    update_objects(get_foreground_folder(), get_state_file()->get_state());
    update_objects(get_background_folder(), get_state_file()->get_state());
}

void UiElement::request_fonts_to_load(const std::string& font_name, size_t font_size , bool bold, bool italic) {
    m_font_name = font_name;
    m_font_size = font_size;

    ASSUMPTION(!m_required_fonts.contains(FontLoader::font_regular_name(font_name, font_size)));
    m_required_fonts.insert(FontLoader::font_regular_name(font_name, font_size));
    if (bold) {
        ASSUMPTION(!m_required_fonts.contains(FontLoader::font_bold_name(font_name, font_size)));
        m_required_fonts.insert(FontLoader::font_bold_name(font_name, font_size));
    }
    if (italic) {
        ASSUMPTION(!m_required_fonts.contains(FontLoader::font_italic_name(font_name, font_size)));
        m_required_fonts.insert(FontLoader::font_italic_name(font_name, font_size));
    }

    font_loader()->load_font_for_element(this, font_name + FontLoader::font_regular_suffix(), font_size);

    if (bold)
        font_loader()->load_font_for_element(this, font_name + FontLoader::font_bold_suffix(), font_size);
    if (italic)
        font_loader()->load_font_for_element(this, font_name + FontLoader::font_italic_suffix(), font_size);
}

void UiElement::on_font_loaded(const std::string &name) {
    if (m_required_fonts.contains(name)) {
        m_required_fonts.erase(name);
        if (m_required_fonts.empty())
            on_all_fonts_loaded();
    }
}

void UiElement::on_all_fonts_loaded() {

}

FontProps * UiElement::get_regular_font_props() const {
    return get_font_props(m_font_name + FontLoader::font_regular_suffix(), m_font_size);
}

Material * UiElement::get_regular_font_material() const {
    return get_font_material(m_font_name + FontLoader::font_regular_suffix(), m_font_size);
}

FontProps * UiElement::get_bold_font_props() const {
    return get_font_props(m_font_name + FontLoader::font_bold_suffix(), m_font_size);
}

Material * UiElement::get_bold_font_material() const {
    return get_font_material(m_font_name + FontLoader::font_bold_suffix(), m_font_size);
}

FontProps * UiElement::get_italic_font_props() const {
    return get_font_props(m_font_name + FontLoader::font_italic_suffix(), m_font_size);
}

Material * UiElement::get_italic_font_material() const {
    return get_font_material(m_font_name + FontLoader::font_italic_suffix(), m_font_size);
}

vec3 UiElement::get_origin_from_scene_frame_link() {
    ASSUMPTION(this->is_under(m_scene_folder));
    vec3 origin = get_frame()->frame().origin();
    auto* scene_frame = ui_system()->get_scene_frame(m_scene_folder);
    auto* parent_frame = get_frame()->parent();
    while (parent_frame != nullptr && parent_frame != scene_frame) {
        auto parent_origin = parent_frame->frame().origin();
        origin += parent_origin;
        parent_frame = parent_frame->parent();
    }
    return origin;
}

void UiElement::update_objects(Folder *ui_objects, STATE state) {
    for (Folder *ui_object : ui_objects->subfolders()) {
        if (auto* ui_element = dynamic_cast<UiElement*>(ui_object))
            continue;

        auto *object_link = ui_system()->get_object_link(ui_object);
        if (!object_link)
            continue;
        auto *object = object_link->target<Folder>();
        if (!object)
            continue;
        auto *material_link = object->find<com::Link>(ObjectSystem::material_link_name());
        auto *buffer_link = object->find<com::Link>(ObjectSystem::buffer_link_name());
        switch (state) {
        case HOVERED:
            material_link->set_target(ui_system()->get_hovered_material(ui_object));
            buffer_link->set_target(ui_system()->get_hovered_buffer(ui_object));
            break;
        case PRESSED:
            material_link->set_target(ui_system()->get_pressed_material(ui_object));
            buffer_link->set_target(ui_system()->get_pressed_buffer(ui_object));
            break;
        case ENABLED:
            material_link->set_target(ui_system()->get_enabled_material(ui_object));
            buffer_link->set_target(ui_system()->get_enabled_buffer(ui_object));
            break;
        default:
            UNREACHABLE();
        }
    }
}

BaseButton::BaseButton(std::string const &name, Folder* scene_folder) : UiElement{ name, scene_folder }, m_toggleable{ false }, m_toggled{ false } {
}

BaseButton::BaseButton(std::string const &name, Folder* scene_folder, vec2 size) : UiElement{ name, scene_folder, size }, m_toggleable{ false },
                                                             m_toggled{ false } {
}

BaseButton::BaseButton(std::string const &name, Folder* scene_folder, bool toggleable) : UiElement{ name, scene_folder }, m_toggleable{ toggleable },
                                                                   m_toggled{ false } {
}

BaseButton::BaseButton(std::string const &name, Folder* scene_folder, vec2 size, bool toggleable) : UiElement{ name, scene_folder, size },
                                                                              m_toggleable{ toggleable },
                                                                              m_toggled{ false } {
}

BaseButton::~BaseButton() {
}

LineTextEdit::LineTextEdit(std::string const &name, Folder* scene_folder, vec2i size, std::string text) : UiElement{ name, scene_folder, size },
                                                                          m_text(std::move(text)) {
}

LineTextEdit::~LineTextEdit() {
}

void LineTextEdit::initialize() {
    UiElement::initialize();
    if (m_text.empty())
        m_cursor_idx = -1;
    else m_cursor_idx = m_text.size();
}

void LineTextEdit::release() {
    UiElement::release();
}

void LineTextEdit::recalculate_objects() {
    UiElement::recalculate_objects();
    if (!m_text_object)
        return;
    auto* object_ws_aabb = m_text_object->find<com::FileAABB>(ws_aabb_file_name());
    auto* object_ss_aabb = m_text_object->find<com::FileAABB>(ss_aabb_file_name());
    object_ss_aabb->set(ui_system()->calculate_screen_space_aabb(m_text_object, get_scene_folder(), object_ws_aabb->get()));
    on_cursor_changed(m_cursor_idx, m_cursor_props.cursorX);
}

void LineTextEdit::on_all_fonts_loaded() {
    UiElement::on_all_fonts_loaded();
    if (m_text.empty())
        return;

    // Setup text object
    FontProps* props = get_regular_font_props();
    std::string text_name = name() + "_text";
    Buffer* text_buffer = buffer_generators()->create_text_buffer(m_text, *props, m_cursor_props, 100000, text_name + "_buffer");
    Material* text_material = get_regular_font_material();
    auto* text_folder = get_foreground_folder()->push_back<Folder>(text_name);
    auto* frame = insert_frame(text_folder);
    frame->set_origin(vec3{get_normalized_size().lo.x, get_normalized_size().hi.y, 0.f});
    set_object_all(text_folder, text_material, text_buffer);
    if (ui_system()->get_object(this))
    ui_system()->insert_object(this, text_folder, get_scene_folder(), frame, text_material, text_buffer);

    AABB ss_aabb = ui_system()->calculate_screen_space_aabb(text_folder, get_scene_folder(), text_buffer->aabb());
    text_folder->push_back<com::FileAABB>(ws_aabb_file_name(), text_buffer->aabb());
    text_folder->push_back<com::FileAABB>(ss_aabb_file_name(), ss_aabb);

    m_cursor_idx = m_text.size() - 1;
    m_text_object = text_folder;

    auto* cursor_folder = get_foreground_folder()->find<Folder>(name() + "_cursor");
    if (cursor_folder == nullptr) {

        cursor_folder = get_foreground_folder()->push_back<Folder>(name() + "_cursor");
        auto* cursor_frame = insert_frame(cursor_folder, ui_system()->get_frame(text_folder));
        cursor_frame->move_origin(vec3{0.f, -(get_normalized_size().hi.y - get_normalized_size().lo.y) / 2.f, 0.f});
        vec2 pixel_size = osi::window()->pixel_size_in_meters();
        Buffer* cursor_buffer = buffer_generators()->insert_procedural_rectangle(vec2{props->minimal_advance * pixel_size.x / 10.f, (get_normalized_size().hi.y - get_normalized_size().lo.y) / 2.f}, cursor_folder->name() + "_buffer", {"cursor_buffers"});
        Material* cursor_material = ui_system()->get_default_enabled_material();
        set_object_all(cursor_folder, cursor_material, cursor_buffer);
        if (ui_system()->get_object(this))
            ui_system()->insert_object(this, cursor_folder, get_scene_folder(), cursor_frame, cursor_material, cursor_buffer);

        m_cursor_object = cursor_folder;
    }

    recalculate_objects();
}

void LineTextEdit::on_text_entered(std::string const &text) {
    UiElement::on_text_entered(text);
    if (m_text_object == nullptr) {
        ASSUMPTION(m_cursor_idx == -1);
        m_text.insert(m_text.begin(), text.begin(), text.end());
        on_all_fonts_loaded();
        return;
    }

    vec2 pixel_size = osi::window()->pixel_size_in_meters();
    FontProps *atlas_props = get_regular_font_props();

    m_text.insert(m_text.begin() + m_cursor_idx + 1, text.begin(), text.end());

    scalar offset_x = 0.f;
    for (char c : text) {
        offset_x += std::max((scalar)atlas_props->character_props[c].advance, atlas_props->get_minimal_advance()) * pixel_size.x;
    }
    scalar cursorX = m_cursor_props.cursorX + offset_x;

    auto *text_buffer = ui_system()->get_buffers_folder(m_text_object)->find<com::Link>(enabled_link_name())->target<Buffer>();
    if (text_buffer != nullptr) {
        text_buffer->clear();
        buffer_system()->erase_buffer(text_buffer);
    }
    text_buffer = insert_new_text_buffer();
    set_cursor_index(m_cursor_idx + text.size(), cursorX);
    update_objects();
}

void LineTextEdit::on_key_pressed(std::string const &key) {
    UiElement::on_key_pressed(key);
    FontProps* font_props = get_regular_font_props();
    vec2 pixel_size = osi::window()->pixel_size_in_meters();
    if (key == "Backspace") {
        if (m_cursor_idx == -1) return;

        scalar offset_x = -std::max((scalar)font_props->character_props[m_text[m_cursor_idx]].advance, font_props->get_minimal_advance()) * pixel_size.x;
        int cursor_idx = m_cursor_idx - 1;
        scalar cursor_offset = m_cursor_props.cursorX + offset_x;

        m_cursor_props.cursorX = 0.f;
        m_cursor_props.prevCursorX = 0.f;
        m_cursor_props.cursorY = 0.f;

        auto* text_buffer = ui_system()->get_buffers_folder(m_text_object)->find<com::Link>(enabled_link_name())->target<Buffer>();
        text_buffer->clear();
        m_text.erase(m_text.begin() + m_cursor_idx, m_text.begin() + m_cursor_idx + 1);
        if (!m_text.empty()) {
            buffer_generators()->create_text_buffer(m_text, *font_props, m_cursor_props, 100000, text_buffer->name());
        } else {
            erase_text_object();
        }
        set_cursor_index(cursor_idx, cursor_offset);
    } else if (key == "Delete") {
        if (m_cursor_idx == m_text.size() - 1)
            return;

        int cursor_idx = m_cursor_idx;
        scalar cursor_offset = m_cursor_props.cursorX;

        m_cursor_props.cursorX = 0.f;
        m_cursor_props.prevCursorX = 0.f;
        m_cursor_props.cursorY = 0.f;

        auto* text_buffer = ui_system()->get_buffers_folder(m_text_object)->find<com::Link>(enabled_link_name())->target<Buffer>();
        text_buffer->clear();
        m_text.erase(m_text.begin() + m_cursor_idx + 1, m_text.begin() + m_cursor_idx + 2);

        if (!m_text.empty()) {
            buffer_generators()->create_text_buffer(m_text, *font_props, m_cursor_props, 100000, text_buffer->name());
        } else {
            erase_text_object();
        }

        set_cursor_index(cursor_idx, cursor_offset);
    } else if (key == "Left") {
        if (m_cursor_idx == -1) return;
        set_cursor_index(m_cursor_idx - 1, m_cursor_props.cursorX - std::max((scalar)font_props->character_props[m_text[m_cursor_idx]].advance, font_props->get_minimal_advance()) * pixel_size.x);
    } else if (key == "Right") {
        if (m_cursor_idx == m_text.size() - 1)
            return;
        set_cursor_index(m_cursor_idx + 1, m_cursor_props.cursorX + std::max((scalar)font_props->character_props[m_text[m_cursor_idx + 1]].advance, font_props->get_minimal_advance()) * pixel_size.x);
    }
}

void LineTextEdit::on_mouse_pressed(vec2 position) {
    UiElement::on_mouse_pressed(position);
    if (m_text_object == nullptr)
        return;
    AABB ws_aabb = get_ws_aabb()->get();

    calculate_cursor_position(ws_aabb, position);
}

void LineTextEdit::on_cursor_changed(int new_idx, scalar new_cursorX) {
    auto* cursor_frame = ui_system()->get_frame(m_cursor_object);
    auto* text_frame = ui_system()->get_frame(m_text_object);

    auto* text_buffer = ui_system()->get_buffers_folder(m_text_object)->find<com::Link>(enabled_link_name())->target<Buffer>();
    scalar text_width;
    if (!text_buffer) text_width = 0.f;
    else text_width = text_buffer->aabb().hi.x - text_buffer->aabb().lo.x;

    scalar origin_y = cursor_frame->frame().origin().y;
    scalar origin_z = cursor_frame->frame().origin().z;
    cursor_frame->set_origin(vec3{new_cursorX, origin_y, origin_z});

    scalar rect_size_x = get_normalized_size().hi.x - get_normalized_size().lo.x;

    scalar text_offset_x = text_frame->frame().origin().x;
    scalar rect_frame_dst = -text_offset_x + get_normalized_size().lo.x;

    if (new_cursorX <= m_cursor_props.prevCursorX) {
        if (text_width > rect_size_x && rect_frame_dst > new_cursorX) {
            text_offset_x -= new_cursorX - m_cursor_props.prevCursorX;
        }
    }
    if (rect_frame_dst > new_cursorX) {
        text_offset_x += rect_frame_dst - new_cursorX;
    }
    if (rect_frame_dst + rect_size_x < new_cursorX) {
        text_offset_x -= new_cursorX - (rect_frame_dst + rect_size_x);
    }
    if (text_width < rect_size_x) {
        text_offset_x = get_normalized_size().lo.x;
    }

    origin_y = text_frame->frame().origin().y;
    origin_z = text_frame->frame().origin().z;
    text_frame->set_origin(vec3{std::min(text_offset_x, get_normalized_size().lo.x), origin_y, origin_z});
}

Buffer *LineTextEdit::insert_new_text_buffer() {
    m_cursor_props.cursorX = 0.f;
    m_cursor_props.prevCursorX= 0.f;
    FontProps* props = get_regular_font_props();
    std::string text_name = name() + "_text";
    Buffer* text_buffer = buffer_generators()->create_text_buffer(m_text, *props, m_cursor_props, 100000, text_name + "_buffer");
    set_object_buffers_all(m_text_object, text_buffer);
    if (!ui_system()->get_object(m_text_object))
        ui_system()->insert_object(this, m_text_object, get_scene_folder(), ui_system()->get_frame(m_text_object), get_regular_font_material(), text_buffer);
    return text_buffer;
}

void LineTextEdit::erase_text_object() {
    auto* text_buffer = m_text_object->find<Folder>(buffers_folder_name())->find<com::Link>(enabled_link_name())->target<Buffer>();
    set_object_buffers_all(m_text_object, nullptr);
    buffer_system()->erase_buffer(text_buffer);
    object_system()->erase_object(ui_system()->get_object(m_text_object));
    m_text_object->erase(object_folder_link_name());
    set_cursor_index(-1, 0.f);
}

void LineTextEdit::calculate_cursor_position(AABB text_ws_aabb, vec2 ss_mouse_pos) {
    vec2 ws_mouse_pos = ui_system()->calculate_world_space_coords(m_text_object, ss_mouse_pos, get_scene_folder());
    scalar offset_x_ws = ws_mouse_pos.x - text_ws_aabb.lo.x;
    vec2 pixel_size = osi::window()->pixel_size_in_meters();
    if (offset_x_ws < 0.0f) return;
    FontProps* props = get_regular_font_props();
    scalar total_advance = 0.f;
    for (int i = 0 ; i < m_text.size() ; i++) {
        scalar char_advance_normalized = std::max((scalar)props->character_props[m_text.at(i)].advance, props->get_minimal_advance()) * pixel_size.x;
        total_advance += char_advance_normalized;
        if (total_advance > offset_x_ws) {
            set_cursor_index(i, total_advance);
            return;
        }
    }
    set_cursor_index(m_text.size() - 1, total_advance);
}

StaticText::StaticText(std::string const &name, Folder* scene_folder, std::string text) : UiElement{ name, scene_folder },
    m_text_wrap(false), m_text(std::move(text)) {}

StaticText::StaticText(std::string const &name, Folder* scene_folder, std::string text, bool text_wrap) : UiElement{ name, scene_folder },
    m_text_wrap(text_wrap), m_text(std::move(text)) {}

StaticText::StaticText(std::string const &name, Folder* scene_folder, vec2i size, std::string text) : UiElement{ name, scene_folder, size },
    m_text_wrap(false), m_text(std::move(text)) {}

StaticText::StaticText(std::string const &name, Folder* scene_folder, vec2i size, std::string text, bool text_wrap) : UiElement{ name, scene_folder, size },
    m_text_wrap(text_wrap), m_text(std::move(text)) {}

StaticText::~StaticText() {
}

void StaticText::initialize_text_objects() {
    TMPROF_BLOCK();
    size_t width = get_size().x;
    AABB _union = AABB{};
    FontProps* regular_props = get_regular_font_props();
    FontProps* props;
    Material *text_material;

    std::stringstream m_text_stream(m_text);
    std::string segment;
    std::vector<std::string> lines;

    while(std::getline(m_text_stream, segment, '\n'))
    {
        lines.push_back(segment);
    }

    for (const std::string& line : lines) {
        std::vector<TextSegment> segments = batch_formatted_text(line);

        for (const auto& segment : segments) {
            props = get_font_props_based_on_segment(segment);
            text_material = get_font_material_based_on_segment(segment);
            for (size_t idx = 0; idx < segment.text.size(); idx += m_max_text_size) {
                std::string text = segment.text.substr(idx, m_max_text_size);
                std::string id = name() + "_text_" + std::to_string(m_text_objects.size());
                Buffer* text_buffer = buffer_generators()->create_text_buffer(text, *props, m_cursor_props, width, id, segment.style.color, m_text_wrap);
                auto *text_folder = get_foreground_folder()->push_back<Folder>(id);
                set_object_all(text_folder, text_material, text_buffer);
                auto* text_frame = insert_frame(text_folder);
                if (ui_system()->get_object(this))
                    ui_system()->insert_object(this, text_folder, get_scene_folder(), text_frame, text_material, text_buffer, true);

                // Calculate screen space aabb of text buffer
                AABB ss_aabb = ui_system()->calculate_screen_space_aabb(text_folder, get_scene_folder(), text_buffer->aabb());
                text_folder->push_back<com::FileAABB>(aabb_file_name(), text_buffer->aabb());
                text_folder->push_back<com::FileAABB>(rect_file_name(), ss_aabb);
                if (_union.lo == vec3{0.f} && _union.hi == vec3{0.f})
                    _union = text_buffer->aabb();
                else _union = AABB::merge(text_buffer->aabb(), _union);

                // Insert text and object
                m_text_objects.push_back(text_folder);
                m_texts.push_back(text);
            }
        }

        m_cursor_props.cursorY -= regular_props->m_font_size * osi::window()->pixel_size_in_meters().y;
        m_cursor_props.cursorX = 0.f;
    }

    push_back<com::FileAABB>(aabb_union_file_name(), _union);
    recalculate_based_on_value(1.0f);
}

void StaticText::initialize() {
    UiElement::initialize();
}

void StaticText::release() {
    UiElement::release();
}

void StaticText::on_element_value_changed(Interval *child_interval, scalar value) {
    UiElement::on_element_value_changed(child_interval, value);
    recalculate_based_on_value(value);
}

void StaticText::recalculate_objects() {
    UiElement::recalculate_objects();
    if (!m_required_fonts.empty()) return;
    FontProps* regular_props = get_regular_font_props();
    if (regular_props == nullptr)
        return;
    auto* union_file = find<com::FileAABB>(aabb_union_file_name());
    AABB _union = union_file ? union_file->get() : AABB{};
    vec2 default_pos = vec2(get_normalized_size().lo.x, get_normalized_size().hi.y);
    for (Folder* text_object : m_text_objects) {
        // Set alignment
        auto* frame = ui_system()->get_frame(text_object);
        scalar current_pos_x = frame->frame().origin().x;
        scalar current_pos_y = frame->frame().origin().y;
        vec2 size = _union.hi - _union.lo;
        size.y = std::max(size.y, (scalar)regular_props->m_font_size * osi::window()->pixel_size_in_meters().y);
        vec2 normalized_size = get_normalized_size().hi - get_normalized_size().lo;
            scalar frame_x = default_pos.x;
            scalar frame_y = default_pos.y;
            if (size.x < normalized_size.x) {
                switch (m_h_alignment) {
                case H_CENTER:
                    frame_x = -size.x / 2.f;
                    break;
                case RIGHT:
                    frame_x = get_normalized_size().hi.x;
                    frame_x -= size.x;
                    break;
                case LEFT:
                    frame_x = get_normalized_size().lo.x;
                    break;
                default:
                    UNREACHABLE();
                }
            }
            if (size.y < normalized_size.y) {
                switch (m_v_alignment) {
                case V_CENTER:
                    frame_y = size.y / 2.f;
                    break;
                case TOP:
                    frame_y = get_normalized_size().hi.y;
                    break;
                case BOTTOM:
                    frame_y = get_normalized_size().lo.y;
                    frame_y += size.y;
                    break;
                }
            }
            if (size.y > normalized_size.y)
                frame_y = current_pos_y;
            if (size.x > normalized_size.x)
                frame_x = current_pos_x;
            ui_system()->get_frame(text_object)->set_origin({frame_x, frame_y, 0});
    }
}

void StaticText::on_all_fonts_loaded() {
    UiElement::on_all_fonts_loaded();
    initialize_text_objects();
}

void StaticText::on_text_changed() {
    if (!m_required_fonts.empty()) {
        return;
    }
    m_texts.clear();
    m_cursor_props.cursorX = 0.f;
    m_cursor_props.prevCursorX = 0.f;
    m_cursor_props.cursorY = 0.f;
    erase(aabb_union_file_name());
    for (auto* text_segment : m_text_objects) {
        // Remove from object system
        auto* text_object = ui_system()->get_object(text_segment);
        text_object->clear();
        erase_empty(text_object);

        // Erase buffer
        buffer_system()->erase_buffer(ui_system()->get_enabled_buffer(text_segment));

        // Remove from ui system
        text_segment->clear();
        erase_empty(text_segment);
    }
    m_text_objects.clear();
    // Recreate text
    initialize_text_objects();
}

void StaticText::recalculate_based_on_value(scalar value) {
    auto* union_file = find<com::FileAABB>(aabb_union_file_name());
    if (union_file == nullptr)
        return;
    AABB _union = union_file->get();
    FontProps* props = get_regular_font_props();
    scalar total_text_height = _union.hi.y - (_union.lo.y - props->max_height * osi::window()->pixel_size_in_meters().y);
    scalar view_height = 2.f * get_normalized_size().hi.y;

    scalar scroll_offset = (total_text_height - view_height) * (1.0f - value);

    for (auto* text_segment : m_text_objects) {
        auto* text_frame = ui_system()->get_frame(text_segment);
        scalar origin_x = text_frame->frame().origin().x;
        scalar origin_z = text_frame->frame().origin().z;
        text_frame->set_origin(vec3{origin_x, get_normalized_size().hi.y + scroll_offset, origin_z});
        auto* text_buffer = ui_system()->get_buffers_folder(text_segment)->find<com::Link>(enabled_link_name())->target<Buffer>();
        auto* text_material = ui_system()->get_materials_folder(text_segment)->find<com::Link>(enabled_link_name())->target<Material>();
        if (get_normalized_size().hi.y + scroll_offset + text_buffer->aabb().lo.y > get_normalized_size().hi.y ||
            get_normalized_size().hi.y + scroll_offset + text_buffer->aabb().hi.y < get_normalized_size().lo.y) {
            // Delete ui object, it is out of bounds of the StaticText widget
            if (auto* ui_object = ui_system()->get_object(text_segment)) {
                ui_object->clear();
                erase_empty(ui_object);
                text_segment->erase(object_folder_link_name());
            }
        } else {
            // Reinsert ui object for rendering
            if (ui_system()->get_object(this) && ui_system()->get_object_link(text_segment) == nullptr) {
                ui_system()->insert_object(this, text_segment, get_scene_folder(), ui_system()->get_frame(text_segment), text_material, text_buffer, true);
            }
        }
    }

    recalculate_objects();
}

FontProps* StaticText::get_font_props_based_on_segment(const TextSegment &segment) const {
    switch (segment.style.font_style) {
    case BOLD:
        return get_bold_font_props();
    case REGULAR:
        return get_regular_font_props();
    case ITALIC:
        return get_italic_font_props();
    default:
        UNREACHABLE();
        return nullptr;
    }
}

Material* StaticText::get_font_material_based_on_segment(const TextSegment &segment) const {
    switch (segment.style.font_style) {
    case BOLD:
        return get_bold_font_material();
    case REGULAR:
        return get_regular_font_material();
    case ITALIC:
        return get_italic_font_material();
    default:
        UNREACHABLE();
        return nullptr;
    }
}

std::vector<StaticText::TextSegment> StaticText::batch_formatted_text(const std::string &input) {
    std::regex command_regex(R"((\\c\[(\d+,\d+,\d+)\])?(b|i|u)?\{([^{}]*)\}|\\(b|i|u)\{([^{}]*)\})");

    std::vector<TextSegment> segments;
    std::smatch match;
    std::string::const_iterator search_start = input.cbegin();

    // Default style
    TextStyle default_style;
    default_style.color = vec3(255.0f, 255.0f, 255.0f);
    default_style.font_style = REGULAR;

    if (input.empty()) return segments;
    if (input.find_first_of("\\") == std::string::npos) {
        segments.push_back({ input, default_style });
        return segments;
    }

    while (std::regex_search(search_start, input.cend(), match, command_regex)) {
        // Capture any text before the match
        if (search_start != match[0].first) {
            std::string text_before(search_start, match[0].first);
            segments.push_back({ text_before, default_style });
        }

        TextSegment segment;
        TextStyle style = default_style;

        // Handle color
        if (match[2].matched) {
            std::string color_str = match[2].str();
            std::stringstream color_stream(color_str);
            std::string color_part;
            int color_val;
            int i = 0;

            while (std::getline(color_stream, color_part, ',') && i < 3) {
                color_val = std::stoi(color_part);
                style.color[i] = static_cast<float>(color_val);
                i++;
            }
        }

        // Handle font styles
        if (match[3].matched || match[5].matched) {
            std::string style_char = match[3].matched ? match[3].str() : match[5].str();
            if (style_char == "b") {
                style.font_style = BOLD;
            } else {
                style.font_style = ITALIC;
            }
        } else {
            style.font_style = REGULAR;
        }

        segment.text = match[4].matched ? match[4].str() : match[6].str();
        segment.style = style;
        segments.push_back(segment);

        search_start = match.suffix().first;
    }

    // Capture any remaining text
    if (search_start != input.cend()) {
        std::string text_after(search_start, input.cend());
        segments.push_back({ text_after, default_style });
    }

    return segments;
}

Button::Button(std::string const &name, Folder* scene_folder) : BaseButton{ name, scene_folder }, m_text("") {
}

Button::Button(std::string const &name, Folder* scene_folder, vec2 size, std::string const& text) : BaseButton{ name, scene_folder, size }, m_text(text) {
}

Button::~Button() {
}

void Button::initialize() {
    UiElement::initialize();

    // Buffer *buffer = buffer_generators()->insert_procedural_rectangle(get_size_in_meters() / 2.0f, name() + "_buffer",
    //                                                                   { "ui" });
    // auto *bg_object_folder = get_background_folder()->push_front<Folder>("background");
    // insert_frame(bg_object_folder);
    // set_object_buffers_all(bg_object_folder, buffer);
    // set_object_materials_enabled(bg_object_folder, ui_system()->get_default_enabled_material());
    // set_object_materials_hovered(bg_object_folder, ui_system()->get_default_hovered_material());
    // set_object_materials_pressed(bg_object_folder, ui_system()->get_default_pressed_material());
    // m_background_object = bg_object_folder;
}

void Button::release() {
    UiElement::release();
}

void Button::on_state_changed(State *state) {
    UiElement::on_state_changed(state);
}

void Button::on_all_fonts_loaded() {
    auto* layer = ui_system()->get_element_scene_layer(get_scene_folder(), this);
    auto* static_text = ui_system()->insert_child_element<StaticText>(name() + "_static_text", this, get_scene_folder(), layer, get_size(), m_text);
    static_text->request_fonts_to_load(m_font_name, m_font_size, true, true);
    static_text->get_transform()->set_anchor(0.f, 0.f, 0.f, 0.f);
    if (ui_system()->get_object(this))
        ui_system()->insert_objects(static_text);
}

CheckBox::CheckBox(std::string const &name, Folder* scene_folder, vec2i size) : BaseButton(name, scene_folder, size, true) {
}

CheckBox::~CheckBox() {
}

void CheckBox::initialize() {
    BaseButton::initialize();
    set_toggleable(true);

    // Buffer *buffer = buffer_generators()->insert_procedural_rectangle(get_size_in_meters() / 2.0f, name() + "_buffer",
    //                                                                   { "ui" });
    // auto *bg_object_folder = get_background_folder()->push_front<Folder>("background");
    // insert_frame(bg_object_folder);
    // auto *bg_object_materials_folder = bg_object_folder->push_back<Folder>(materials_folder_name());
    // auto *bg_object_buffers_folder = bg_object_folder->push_back<Folder>(buffers_folder_name());
    //
    // bg_object_buffers_folder->push_back<com::Link>(enabled_link_name(), buffer);
    // bg_object_buffers_folder->push_back<com::Link>(hovered_link_name(), buffer);
    // bg_object_buffers_folder->push_back<com::Link>(pressed_link_name(), buffer);
    //
    // bg_object_materials_folder->push_back<com::Link>(enabled_link_name(), material_system()->materials()->locate<Material>({"state_materials", "enabled_material"}));
    // bg_object_materials_folder->push_back<com::Link>(hovered_link_name(), material_system()->materials()->locate<Material>({"state_materials", "hovered_material"}));
    // bg_object_materials_folder->push_back<com::Link>(pressed_link_name(), material_system()->materials()->locate<Material>({"state_materials", "pressed_material"}));
}

void CheckBox::release() {
    UiElement::release();
}

void CheckBox::on_state_changed(State *state) {
    switch (state->get_state()) {
    case PRESSED:
        set_toggled(!is_toggled());
        break;
    case HOVERED:
    case ENABLED:
    default:
        break;
    }
}

void CheckBox::on_toggle_changed() {
    BaseButton::on_toggle_changed();
    if (is_toggled()) {
        update_objects(get_foreground_folder(), PRESSED);
        update_objects(get_background_folder(), PRESSED);
    } else {
        update_objects(get_foreground_folder(), ENABLED);
        update_objects(get_background_folder(), ENABLED);
    }
}


Interval::Interval(std::string const &name, Folder* scene_folder, vec2i size, scalar value) : UiElement{ name, scene_folder, size }, m_value(value) {
}

Interval::~Interval() {
}

ProgressBar::ProgressBar(std::string const &name, Folder* scene_folder, vec2i size, scalar value) : Interval(name, scene_folder, size, value) {}

ProgressBar::ProgressBar(std::string const &name, Folder* scene_folder, vec2i size) : Interval(name, scene_folder, size, 0.0f) {}

ProgressBar::~ProgressBar() {}


void ProgressBar::initialize() {
    Interval::initialize();
    auto* foreground_object = get_foreground_folder()->push_back<Folder>("foreground_object");
    auto* foreground_material = material_system()->materials()->locate<Material>({"state_materials", "hovered_material"});
    vec2 half_size = vec2(get_size_in_meters().x, get_size_in_meters().y / 2.0f);
    vec2 half_size_inv = vec2(0.0f, get_size_in_meters().y / 2.0f);
    Buffer* foreground_buffer = buffer_generators()->insert_procedural_rectangle_variable(half_size, half_size_inv, name() + "_buffer", {name(), "foreground_buffers"});

    set_object_all(foreground_object, foreground_material, foreground_buffer);
    auto* frame = insert_frame(foreground_object);
    frame->move_origin(vec3(get_normalized_size().lo.x, 0.f, 0.f));
    frame->set_scale(vec3{get_value(), 1.0f, 1.0f});
    m_foreground_folder = foreground_object;
}

void ProgressBar::release() {
    Interval::release();
}

void ProgressBar::on_size_changed() {
    Interval::on_size_changed();
    ui_system()->get_frame(m_foreground_folder)->set_origin(vec3{get_normalized_size().lo.x, 0.f, 0.f});
    ui_system()->get_frame(m_foreground_folder)->set_scale(vec3{get_value() * (get_normalized_size().hi.x / (get_size_in_meters().x / 2.f)), (get_normalized_size().hi.y / (get_size_in_meters().y / 2.f)), 1.f});
}

void ProgressBar::on_value_changed(scalar value) {
    Interval::on_value_changed(value);
    ui_system()->get_frame(get_foreground_folder()->find<Folder>("foreground_object"))->set_scale(vec3{get_value() * (get_normalized_size().hi.x / (get_size_in_meters().x / 2.f)), (get_normalized_size().hi.y / (get_size_in_meters().y / 2.f)), 1.f});
}

void ProgressBar::set_foreground_material(Material *material) {
    set_object_materials_all(m_foreground_folder, material);
}

Slider::Slider(
    std::string const &name, Folder* scene_folder, vec2i size
    ) : Interval{ name, scene_folder, size, 0.0f }, m_alignment{ gfx::HORIZONTAL } {
}

Slider::Slider(
    std::string const &name, Folder* scene_folder, vec2i size, scalar value
    ) : Interval{ name, scene_folder, size, value }, m_alignment{ gfx::HORIZONTAL } {
}

Slider::Slider(
    std::string const &name, Folder* scene_folder, vec2i size, Alignment alignment
    ) : Interval{ name, scene_folder, size, 0.0f }, m_alignment{ alignment } {
}

Slider::Slider(
    std::string const &name, Folder* scene_folder, vec2i size, scalar value, Alignment alignment
    ) : Interval{ name, scene_folder, size, value }, m_alignment{ alignment } {
}

Slider::~Slider() {
}

void Slider::initialize() {
    UiElement::initialize();

    Buffer* thumb_buffer;
    if (m_alignment == HORIZONTAL) {
        thumb_buffer = buffer_generators()->insert_procedural_rectangle(
        vec2(get_size_in_meters().x / 20.0f, get_size_in_meters().y / 1.5f), name() + "_thumb_buffer", { "ui" });
    } else {
        thumb_buffer = buffer_generators()->insert_procedural_rectangle(
        vec2(get_size_in_meters().x / 1.5f, get_size_in_meters().y / 20.0f), name() + "_thumb_buffer", { "ui" });
    }

    auto* thumb_material = ui_system()->get_default_rect_material();
    auto* thumb_folder = get_foreground_folder()->push_back<Folder>("thumb");
    insert_frame(thumb_folder);
    set_object_all(thumb_folder, thumb_material, thumb_buffer);
}

void Slider::release() {
    Interval::release();
}

void Slider::on_mouse_moved(vec2 position) {
    UiElement::on_mouse_moved(position);
    if (get_state_file()->get_state() == PRESSED) {
        scalar new_value;
        scalar low_value;
        scalar high_value;
        if (m_alignment == HORIZONTAL) {
            new_value = position.x;
            low_value = get_transform()->get_aabb().lo.x;
            high_value = get_transform()->get_aabb().hi.x;
        } else {
            new_value = position.y;
            low_value = get_transform()->get_aabb().lo.y;
            high_value = get_transform()->get_aabb().hi.y;
        }
        set_value(std::clamp((new_value - low_value) / (high_value - low_value), 0.0f, 1.0f));
    }
}

void Slider::on_mouse_pressed(vec2 position) {
    UiElement::on_mouse_pressed(position);
    on_mouse_moved(position);
}

void Slider::on_size_changed() {
    Interval::on_size_changed();
    auto* thumb = get_foreground_folder()->find<Folder>("thumb");
    auto* thumb_buffer_link = ui_system()->get_buffers_folder(thumb)->find<com::Link>(enabled_link_name());
    auto* thumb_buffer = thumb_buffer_link->target<Buffer>();
    thumb_buffer->clear();
    auto path = thumb_buffer->folder()->path();
    if (m_alignment == HORIZONTAL) {
        thumb_buffer = buffer_generators()->insert_procedural_rectangle(
        vec2(get_normalized_size().hi.x / 10.0f, get_normalized_size().hi.y), thumb_buffer->name(), path);
    } else {
        thumb_buffer = buffer_generators()->insert_procedural_rectangle(
        vec2(get_normalized_size().hi.x, get_normalized_size().hi.y / 10.0f), thumb_buffer->name(), path);
    }
    thumb_buffer_link->set_target(thumb_buffer);
}

void Slider::recalculate_objects() {
    Interval::recalculate_objects();
    auto* thumb = get_foreground_folder()->find<Folder>("thumb");
    auto* frame = thumb->find<Folder>(frame_folder_name())->find<com::Frame>("frame");

    vec3 origin = frame->frame().origin();
    if (m_alignment == HORIZONTAL) {
        frame->set_origin(vec3(2.0f * get_normalized_size().hi.x * get_value() + get_normalized_size().lo.x, origin.y, origin.z));
    } else {
        frame->set_origin(vec3(origin.x, 2.0f * get_normalized_size().hi.y * get_value() + get_normalized_size().lo.y, origin.z));
    }
}

ListBox::ListBox(std::string const &name, Folder* scene_folder, vec2i size, vec2i item_size) : UiElement(name, scene_folder, size), m_elements({}), m_item_size(item_size), m_idx(0), m_counter(0) {}
ListBox::~ListBox() {}

void ListBox::initialize() {
    UiElement::initialize();
    // Buffer* background_buffer = buffer_generators()->insert_procedural_rectangle(get_size_in_meters() / 2.0f, name() + "_background_buffer", { "list_box_buffers" });
    //
    // auto* background_folder = get_background_folder()->push_front<Folder>("background");
    // insert_frame(background_folder);
    // set_object_all(background_folder, material_system()->materials()->locate<Material>({"state_materials", "pressed_material"}), background_buffer);

    m_items_height = 0;
    m_new_item_frame_pos = vec2(0.0f, (get_size_in_meters().y / 2.0f) - (ui_system()->get_size_in_meters(m_item_size).y / 2.0f));
}

void ListBox::release() {
    UiElement::release();
}

void ListBox::on_element_pressed(UiElement *subelement, vec2 position) {
    UiElement::on_element_pressed(subelement, position);
    ASSUMPTION(subelement->is_under(this));
}

void ListBox::on_size_changed() {
    UiElement::on_size_changed();
    for (UiElement* element : m_elements) {
        AABB norm_size = element->get_normalized_size();
        element->set_normalized_size(
            vec3{get_normalized_size().lo.x, norm_size.lo.y, norm_size.lo.z},
            vec3{get_normalized_size().hi.x, norm_size.hi.y, norm_size.hi.z});
    }
    scalar value = 1.0f;
    if (m_slider != nullptr) {
        value = m_slider->get_value();
    }
    // move frames based on slider value
    scalar frame_y_pos = ui_system()->get_size_in_meters(vec2(m_item_size) / 2.0f).y - get_normalized_size().hi.y;
    for (UiElement* child_element : m_elements) {
        child_element->get_frame()->set_origin(vec3{0.0f, (ui_system()->get_size_in_meters(vec2(m_items_height)).y - get_normalized_size().hi.y * 2) * (1.0f - value) - frame_y_pos, 0.0f});
        frame_y_pos += ui_system()->get_size_in_meters(vec2(m_item_size)).y;
    }
}

UiElement * ListBox::insert_item_generic() {
    auto* scene = get_scene_folder();
    auto* layer = ui_system()->get_element_scene_layer(scene, this);
    auto* item = ui_system()->insert_child_element<UiElement>(
        this->name() + "_item_" + std::to_string(m_idx), this, scene,
        layer, m_item_size);
    item->get_frame()->set_origin({0.0f, m_new_item_frame_pos.y, 0.0f});
    m_idx++;
    m_new_item_frame_pos.y -= ui_system()->get_size_in_meters(m_item_size).y;
    m_items_height += m_item_size.y;
    ui_system()->set_parent_frame(item, get_frame());
    m_elements.insert(item);
    return item;
}

Slider * ListBox::insert_slider(vec2i size) {
    auto* slider = ui_generators()->create_ui_slider(name() + "_slider", size, Alignment::VERTICAL, this, get_scene_folder(), ui_system()->get_element_scene_layer(get_scene_folder(), this));
    slider->get_transform()->set_anchor(0.f, 1.0f, 0.f, 0.f);
    m_slider = slider;
    return slider;
}

void ListBox::on_element_value_changed(Interval *child_interval, scalar value) {
    UiElement::on_element_value_changed(child_interval, value);
    // Element items should not affect frames
    for (UiElement* item_element : m_elements) {
        if (child_interval->is_under(item_element))
            return;
    }
    if (m_elements.contains(child_interval)) {
        return;
    }
    // move frames based on slider value
    scalar frame_y_pos = ui_system()->get_size_in_meters(vec2(m_item_size) / 2.0f).y - get_normalized_size().hi.y;
    for (UiElement* child_element : m_elements) {
        child_element->get_frame()->set_origin(vec3{0.0f, (ui_system()->get_size_in_meters(vec2(m_items_height)).y - get_normalized_size().hi.y * 2) * (1.0f - value) - frame_y_pos, 0.0f});
        frame_y_pos += ui_system()->get_size_in_meters(vec2(m_item_size)).y;
    }
}

TabBar::TabBar(std::string const &name, Folder* scene_folder, vec2i size, vec2i tab_size) : UiElement(name, scene_folder, size),
                                                                      m_tab_size(tab_size), m_tabs({}), m_tab_contents{{}}, m_idx(0),
                                                                      m_new_tab_frame_pos(vec2{}), m_active_content(nullptr), m_active_tab(nullptr) {}
TabBar::~TabBar() {}

void TabBar::set_active_content(UiElement *content_element) {
    for (UiElement* tab : m_tabs) {
        UiElement* element = m_tab_contents[tab];
        if (element == nullptr)
            continue;
        auto* object = ui_system()->get_object(element);
        if (element != content_element && object != nullptr) {
            ui_system()->erase_object(get_scene_folder(), element);
        }
        if (element == content_element && object == nullptr) {
            ui_system()->insert_objects(content_element, true);
        }
    }
    // if (content_element != nullptr)
    //     content_element->get_frame()->set_origin(vec3(0.f, (get_normalized_size().hi.y - content_element->get_normalized_size().hi.y - ui_system()->get_size_in_meters(m_tab_size)).y, 0.f));
    m_active_content = content_element;
}

/**
 * Assigns content to corresponding tab which will show the content when pressed
 * Tab and Content must be a sub-element of this tab
 * @param tab
 * @param content
 */
void TabBar::assign_content_to_tab(UiElement *content, UiElement *tab) {
    ASSUMPTION(tab != nullptr && tab->is_under(get_foreground_folder()));
    ASSUMPTION(content != nullptr && content->is_under(get_foreground_folder()));
    m_tab_contents[tab] = content;
}

void TabBar::initialize() {
    UiElement::initialize();
    m_new_tab_frame_pos.x =  get_normalized_size().lo.x + ui_system()->get_size_in_meters(m_tab_size).x / 2.0f;
    m_new_tab_frame_pos.y =  get_normalized_size().hi.y - ui_system()->get_size_in_meters(m_tab_size).y / 2.0f;
}

void TabBar::release() {
    UiElement::release();
}

void TabBar::on_tab_changed(UiElement *tab) {
    ASSUMPTION(tab->is_under(this));
    set_active_content(m_tab_contents[tab]);
}

void TabBar::on_element_pressed(UiElement *subelement, vec2 position) {
    UiElement::on_element_pressed(subelement, position);
    ASSUMPTION(subelement->is_under(this));
    if (!m_tabs.contains(subelement)) return;
    set_active_tab(subelement);
}

UiElement *TabBar::add_tab(std::string const &name, Folder* scene_folder, Folder* layer_folder, std::string const& font_name, size_t font_size) {
    auto* button = ui_system()->insert_child_element<Button>(this->name() + "_" + name, this, scene_folder, layer_folder, m_tab_size, name);
    button->request_fonts_to_load(font_name, font_size, false, false);
    button->get_frame()->set_origin({m_new_tab_frame_pos.x, m_new_tab_frame_pos.y, 0.0f});
    scalar frame_x = m_new_tab_frame_pos.x - get_normalized_size().lo.x;
    vec2 tab_size_in_meters = ui_system()->get_size_in_meters(m_tab_size);
    vec2 size_in_meters = get_size_in_meters();
    scalar left = (frame_x - (tab_size_in_meters.x / 2.f)) / size_in_meters.x;
    scalar right = (size_in_meters.x - (frame_x + tab_size_in_meters.x / 2.f)) / size_in_meters.x;
    scalar top = 0.f;
    scalar bottom = (size_in_meters.y - tab_size_in_meters.y) / size_in_meters.y;
    button->get_transform()->set_anchor(bottom, left, top, right);
    ui_system()->set_parent_frame(button, get_frame());
    m_idx++;
    m_tabs.insert(button);
    m_new_tab_frame_pos.x += ui_system()->get_size_in_meters(m_tab_size).x;
    return button;
}

void TabBar::set_active_tab(UiElement *tab) {
    ASSUMPTION(tab->is_under(this));
    m_active_tab = tab;
    on_tab_changed(tab);
}

}
