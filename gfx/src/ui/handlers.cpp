# include "gfx/index.hpp"

# include <gfx/ui/handlers.hpp>
# include <gfx/ui/ui_element.hpp>
# include <iostream>

using namespace com;

namespace gfx {

    struct UiElement;
    struct Button;

    UiEventHandler::UiEventHandler(std::string const& name) : com::File {name} {}
    UiEventHandler::~UiEventHandler() {}

    ButtonHandler::ButtonHandler(std::string const& name) : UiEventHandler {name} {}
    ButtonHandler::~ButtonHandler() {}

    void ButtonHandler::on_click(UiElement* element) {
        Button* button = dynamic_cast<Button*>(element);
        ASSUMPTION(button != nullptr);
        // Implementation
        std::cout << "Button with name: " << button->name() << " was pressed." << std::endl;
    }

    CheckBoxHandler::CheckBoxHandler(std::string const& name) : UiEventHandler {name} {}
    CheckBoxHandler::~CheckBoxHandler() {}

    void CheckBoxHandler::on_toggle(UiElement* element) {
        CheckBox* check_box = dynamic_cast<CheckBox*>(element);
        ASSUMPTION(check_box != nullptr);
        if (check_box->is_toggled()) {
            // Do some action on toggle
        }
        else {
            // Do some action on un-toggle
        }
    }

    IntervalHandler::IntervalHandler(std::string const &name) : UiEventHandler(name) {}
    IntervalHandler::~IntervalHandler() {}

    void IntervalHandler::on_value_changed(UiElement *element) {
        auto* interval = dynamic_cast<Interval*>(element);
        ASSUMPTION(interval != nullptr);
        // Do something based on value changed
    }

    BufferHandler::BufferHandler(): UiEventHandler(self_name()) {}
    BufferHandler::~BufferHandler() {}

    void BufferHandler::on_size_changed(UiElement* element)
    {
        auto* border = element->get_border_object();
        auto* background = element->get_background_object();

        // Recreate background and border buffers if present
        if (background)
        {
            // Assuming buffers for all states are the same
            auto* buffer = ui_system()->get_buffers_folder(background)->find<Link>(UiElement::enabled_link_name())->target<Buffer>();
            buffer->clear();
            auto path = buffer->folder()->path();
            buffer_generators()->insert_procedural_rectangle(contract(element->get_normalized_size().hi), buffer->name(), path);
        }

        if (border)
        {
            // Assuming buffers for all states are the same
            auto* buffer = ui_system()->get_buffers_folder(border)->find<Link>(UiElement::enabled_link_name())->target<Buffer>();
            buffer->clear();
            auto path = buffer->folder()->path();
            buffer_generators()->insert_procedural_rectangle_wireframe(contract(element->get_normalized_size().hi), buffer->name(), path);
        }
        // Update objects to show updated buffer
        for (Folder* subfolder : element->get_foreground_folder()->subfolders()) {
            if (auto* sub_element = dynamic_cast<UiElement*>(subfolder)) {
                on_size_changed(sub_element);
            }
        }
        element->update_objects();
    }

}