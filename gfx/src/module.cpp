#include <com/context.hpp>
#include <gfx/background_system.hpp>
#include <gfx/buffer_generators.hpp>
#include <gfx/buffer_system.hpp>
#include <gfx/camera_system.hpp>
#include <gfx/index.hpp>
#include <gfx/light_system.hpp>
#include <gfx/module.hpp>
#include <gfx/object_system.hpp>
#include <gfx/renderer.hpp>
#include <gfx/shader_system.hpp>
#include <gfx/viewport_system.hpp>
#include <gfx/ui/ui_updater.hpp>
#include <osi/opengl.hpp>

namespace gfx {

void boot(com::Folder *const ctx_root) {
    com::Folder *const root_gfx = ctx_root->push_back<com::Folder>("gfx");
    root_gfx->push_back<ShaderSystem>();
    root_gfx->push_back<UiGenerators>();
    root_gfx->push_back<UiUpdater>();
    root_gfx->push_back<MaterialSystem>();
    root_gfx->push_back<BufferSystem>();
    root_gfx->push_back<BufferGenerators>();
    root_gfx->push_back<BackgroundSystem>();
    root_gfx->push_back<ViewportSystem>();
    root_gfx->push_back<ObjectSystem>();
    root_gfx->push_back<LightSystem>();
    root_gfx->push_back<CameraSystem>();
    root_gfx->push_back<FontLoader>();
    root_gfx->push_back<UiSystem>();
    root_gfx->push_back<Renderer>();
    gfx::index();
}

void shutdown(com::Folder *const ctx_root) {
    com::Folder *const root_gfx = ctx_root->find<com::Folder>("gfx");
    root_gfx->erase(Renderer::self_name());
    root_gfx->erase(UiSystem::self_name());
    root_gfx->erase(FontLoader::self_name());
    root_gfx->erase(CameraSystem::self_name());
    root_gfx->erase(LightSystem::self_name());
    root_gfx->erase(ObjectSystem::self_name());
    root_gfx->erase(ViewportSystem::self_name());
    root_gfx->erase(BackgroundSystem::self_name());
    root_gfx->erase(BufferGenerators::self_name());
    root_gfx->erase(BufferSystem::self_name());
    root_gfx->erase(MaterialSystem::self_name());
    root_gfx->erase(ShaderSystem::self_name());
    root_gfx->erase(UiGenerators::self_name());
    root_gfx->erase(UiUpdater::self_name());
    ctx_root->erase(root_gfx);
}

} // namespace gfx
