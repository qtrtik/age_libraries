#include <gfx/shader_graph.hpp>

#include <algorithm>
#include <com/frame.hpp>
#include <gfx/index.hpp>
#include <gfx/shader_nodes.hpp>
#include <lfs/index.hpp>
#include <utils/development.hpp>

namespace gfx {

std::string ShaderGraph::Node::SlotDefinition::type_name() const { return DataType::to_string(type.element_type); }

ShaderGraph::Node::Node(std::string const &name, std::string const &description, std::vector<SlotDefinition> const &inputs,
                   std::vector<SlotDefinition> const &outputs)
    : m_name{ name }, m_description{ description } {
    ASSUMPTION(!m_name.empty());

    m_inputs.reserve(inputs.size());
    std::ranges::transform(inputs, std::back_inserter(m_inputs), [](const auto &in) { return InputSlot{ in }; });
    m_outputs.reserve(outputs.size());
    std::ranges::transform(outputs, std::back_inserter(m_outputs), [](const auto &out) { return OutputSlot{ out }; });
}

void ShaderGraph::Node::add_code_helper(StagesCode &code, Stage stage, visited_t &visited) const {
    visited[stage].insert(this);

    for (std::size_t i = 0; i < m_inputs.size(); ++i) {
        const auto &in = m_inputs[i];
        if (!in.connected())
            throw std::logic_error{ "The input \"" + in.definition.name + "\" in node \"" + m_name +
                                    "\" is not connected." };

        auto s = output_to_input_stage(stage, i);
        if (!visited[s].contains(in.connection.node))
            in.connection.node->add_code_helper(code, s, visited);
    }

    add_code(code, stage);
}

ShaderGraph::Stage ShaderGraph::Node::output_to_input_stage(Stage out, std::size_t in) const { return out; }

std::string ShaderGraph::Node::variable_name(std::size_t out) const {
    ASSUMPTION(out < m_outputs.size());

    std::string n = m_name;
    std::ranges::replace(n, ' ', '_');

    std::stringstream ss;
    ss << this;
    return n + '_' + ss.str() + '_' + std::to_string(out);
}

std::string ShaderGraph::Node::input_name(std::size_t in) const {
    ASSUMPTION(in < m_inputs.size());
    ASSUMPTION(m_inputs[in].connected());

    const auto &c = m_inputs[in].connection;
    return c.node->variable_name(c.index);
}

std::string ShaderGraph::Node::make_initializer(const std::string &value, std::size_t out) const {
    return m_outputs[out].type_name() + ' ' + variable_name(out) + " = " + value;
}

void ShaderGraph::Node::pass_to_fragment(StagesCode &code, std::size_t out, const std::string &value) const {
    auto type = m_outputs[out].type_name();

    code[Stage::VERTEX].decl.push_back("out " + type + ' ' + interface_name());
    code[Stage::VERTEX].code.push_back(interface_name() + " = " + value);

    code[Stage::FRAGMENT].decl.push_back("in " + type + ' ' + interface_name());
    code[Stage::FRAGMENT].code.push_back(make_initializer(interface_name(), out));
}

std::string ShaderGraph::Node::interface_name() const { return variable_name() + "_inter"; }

ShaderGraph::ShaderGraph(std::string const &name, lighting l, lighting_model lm, transparency t) : Shader{ name, l }, m_root{ nullptr } {
    switch (l) {
    case LIT:
        switch (lm) {
        case PHONG:
            insert<MasterNodeLit>(t);

            // Insert and connect default nodes.
            connect(m_root, MasterNodeLit::inputs::position, insert<VaryingPositionNode>(), 0);
            connect(m_root, MasterNodeLit::inputs::normal, insert<VaryingNormalNode>(), 0);
            connect(m_root, MasterNodeLit::inputs::tangent, insert<VaryingTangentNode>(), 0);
            break;
        case PBR:
            insert<MasterNodePBR>(t);

            // Insert and connect default nodes.
            connect(m_root, MasterNodePBR::inputs::position, insert<VaryingPositionNode>(), 0);
            connect(m_root, MasterNodePBR::inputs::normal, insert<VaryingNormalNode>(), 0);
            connect(m_root, MasterNodePBR::inputs::tangent, insert<VaryingTangentNode>(), 0);
            break;
        default:
            UNREACHABLE();
        }
        break;
    case UNLIT:
        insert<MasterNodeUnlit>(t);

        // Insert and connect default nodes.
        connect(m_root, MasterNodeUnlit::inputs::position, insert<VaryingPositionNode>(), 0);
        break;
    case DEFERRED:
        ASSUMPTION(t == OPAQUE);

        switch (lm) {
        case PHONG:
            insert<MasterNodeDeferred>();

            // Insert and connect default nodes.
            connect(m_root, MasterNodeDeferred::inputs::position, insert<VaryingPositionNode>(), 0);
            connect(m_root, MasterNodeDeferred::inputs::normal, insert<VaryingNormalNode>(), 0);
            connect(m_root, MasterNodeDeferred::inputs::tangent, insert<VaryingTangentNode>(), 0);
            break;
        case PBR:
            insert<MasterNodeDeferredPBR>();

            // Insert and connect default nodes.
            connect(m_root, MasterNodeDeferredPBR::inputs::position, insert<VaryingPositionNode>(), 0);
            connect(m_root, MasterNodeDeferredPBR::inputs::normal, insert<VaryingNormalNode>(), 0);
            connect(m_root, MasterNodeDeferredPBR::inputs::tangent, insert<VaryingTangentNode>(), 0);
            break;
        default:
            UNREACHABLE();
        }
        break;
    default:
        UNREACHABLE();
    }
}

void ShaderGraph::erase(Node *const node) {
    ASSUMPTION(node != nullptr);

    auto const it = std::ranges::find(m_nodes, node, [](const auto &ptr) { return ptr.get(); });
    if (it == m_nodes.end())
        throw std::logic_error{ "The erased node is not in the shader." };
    INVARIANT(it->get() == node);

    disconnect(node);

    m_nodes.erase(it);

    if (auto ptr = dynamic_cast<UniformNode *>(node)) {
        auto u_it = std::ranges::find(m_uniforms, ptr);
        INVARIANT(u_it != m_uniforms.end());
        m_uniforms.erase(u_it);
    }

    on_change();
}

ShaderGraph::Node *ShaderGraph::connect(Node *dst_node, std::size_t const input_slot, Node *src_node,
                              std::size_t const output_slot) {
    ASSUMPTION(std::ranges::find(m_nodes, src_node, [&](const auto &ptr) { return ptr.get(); }) != m_nodes.end());
    ASSUMPTION(std::ranges::find(m_nodes, dst_node, [&](const auto &ptr) { return ptr.get(); }) != m_nodes.end());
    ASSUMPTION(output_slot < src_node->outputs().size());
    ASSUMPTION(input_slot < dst_node->inputs().size());

    if (dst_node->m_inputs[input_slot].definition.type != src_node->m_outputs[output_slot].definition.type)
        throw std::logic_error{ "The slot types do not match: source - "
            + src_node->m_outputs[output_slot].definition.type_name() + ", destination - "
            + dst_node->m_inputs[input_slot].definition.type_name() };

    auto &input = dst_node->m_inputs[input_slot].connection;
    if (input.connected())
        throw std::logic_error{ "The input slot #" + std::to_string(input_slot) + " is already connected." };

    auto &outputs = src_node->m_outputs[output_slot].connections;

    input = { src_node, output_slot };
    outputs.push_back({ dst_node, input_slot });

    on_change();

    return dst_node;
}

ShaderGraph::Node *ShaderGraph::disconnect(Node *node, std::size_t const &input_slot) {
    ASSUMPTION(std::ranges::find(m_nodes, node, [&](const auto &ptr) { return ptr.get(); }) != m_nodes.end());
    ASSUMPTION(input_slot < node->inputs().size());

    auto &input = node->m_inputs[input_slot].connection;
    if (!input.connected())
        throw std::logic_error{ "The input slot #" + std::to_string(input_slot) + " is not connected." };

    INVARIANT(std::ranges::find(m_nodes, input.node, [&](const auto &ptr) { return ptr.get(); }) != m_nodes.end());
    INVARIANT(input.index < input.node->outputs().size());

    auto &outputs = input.node->m_outputs[input.index].connections;

    auto const it = std::ranges::find(outputs, Node::SlotConnection{ node, input_slot });
    INVARIANT(it != outputs.end());

    input = { nullptr, 0 };
    outputs.erase(it);

    on_change();

    return node;
}

ShaderGraph::Node *ShaderGraph::disconnect(Node *const node) {
    for (std::size_t i = 0; i != node->inputs().size(); ++i)
        if (node->inputs()[i].connected())
            disconnect(node, i);
    for (const auto &slot : node->outputs())
        while (!slot.connections.empty())
            disconnect(slot.connections.back().node, slot.connections.back().index);
    return node;
}

void ShaderGraph::on_content_changed(com::File *sender) {
    auto handle = dynamic_cast<lfs::ResourceHandle *>(sender);
    ASSUMPTION(handle != nullptr);

    switch (handle->state()) {
    case lfs::ResourceHandle::LOADING:
    case lfs::ResourceHandle::DISCARDED:
        break;
    case lfs::ResourceHandle::LOADED: {
        auto it = std::ranges::find_if(m_textures, [&](auto t) { return t->m_handle == handle; });
        ASSUMPTION(it != m_textures.end());
        (*it)->generate_texture();

        if (gl_shader() != nullptr) {
            activate();
        }
        break;
    }
    case lfs::ResourceHandle::ERROR:
        throw std::runtime_error{ "error loading file: " + handle->error_message() };
    default:
        UNREACHABLE();
    }
}

void ShaderGraph::activate() const {
    ASSUMPTION(gl_shader() != nullptr);
    gl_shader()->activate();
    bind_textures();
}

void ShaderGraph::bind_textures() const {
    GLint texture_units;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &texture_units);
    if (static_cast<int>(m_textures.size()) >= texture_units)
        throw std::runtime_error{ "Maximum texture count exceded. Max: " + std::to_string(texture_units) +
                                  ", Current: " + std::to_string(m_textures.size()) };

    int unit = 0;
    for (auto t : m_textures) {
        if (!t->m_outputs[0].connections.empty()) {
            t->activate(unit);
            ++unit;
        }
    }
}

} // namespace gfx
