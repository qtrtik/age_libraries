#include <gfx/index.hpp>
#include <gfx/material_system.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>

namespace gfx {

MaterialSystem::MaterialSystem() : com::Library{ self_name() }, m_materials{ nullptr }, m_active_material{ nullptr } {}

void MaterialSystem::initialize() {
    m_materials = folder()->push_back<com::Folder>("materials");
    m_active_material = materials()->push_back<com::Link>("active_material.link", nullptr);
}

void MaterialSystem::release() { folder()->erase(materials()); }

void MaterialSystem::set_active_material(Material *const material) {
#ifndef MEASURING_COMPILE
    if (active_material() != material || shader_system()->active_shader() != material->shader())
#endif
    {
        material->activate();
        m_active_material->set_target(material);
    }
}

Material *MaterialSystem::insert_material(std::string const &name, Shader *shader,
                                          com::ContextPath const &sub_folders) {
    com::Folder *const folder = materials()->push_back_folders(sub_folders);
    ASSUMPTION(folder == materials() || folder->is_under(materials()));
    com::ContextItem *const item = folder->find(name);
    if (item == nullptr)
        return folder->push_back<Material>(name, shader);
    Material *const material = dynamic_cast<Material *>(item);
    ASSUMPTION(material != nullptr);
    return material;
}

void MaterialSystem::erase_material(Material *const material) {
    ASSUMPTION(material == materials() || material->is_under(materials()));
    material->folder()->erase(material);
}

Material *MaterialSystem::insert_default_material(std::string const &name, com::ContextPath const &sub_folders,
                                                  vec3 color) {
    auto material = insert_material(name, shader_system()->default_shader(), sub_folders);
    material->set_uniform("color", color);
    return material;
}

} // namespace gfx
