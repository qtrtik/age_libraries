#include <com/frame.hpp>
#include <gfx/index.hpp>
#include <utils/invariants.hpp>

namespace gfx {

Index const &Index::instance() {
    static Index const idx;
    return idx;
}

Index::Index()
    : m_root { com::Folder::root()->find<com::Folder>("gfx") }
    , m_background_system{ m_root->find<BackgroundSystem>(BackgroundSystem::self_name()) }
    , m_viewport_system{ m_root->find<ViewportSystem>(ViewportSystem::self_name()) }
    , m_camera_system{ m_root->find<CameraSystem>(CameraSystem::self_name()) }
    , m_shader_system{ m_root->find<ShaderSystem>(ShaderSystem::self_name()) }
    , m_material_system{ m_root->find<MaterialSystem>(MaterialSystem::self_name()) }
    , m_buffer_system{ m_root->find<BufferSystem>(BufferSystem::self_name()) }
    , m_buffer_generators{ m_root->find<BufferGenerators>(BufferGenerators::self_name()) }
    , m_object_system{ m_root->find<ObjectSystem>(ObjectSystem::self_name()) }
    , m_light_system{ m_root->find<LightSystem>(LightSystem::self_name()) }
    , m_renderer{ m_root->find<Renderer>(Renderer::self_name()) }
    , m_ui_system{ m_root->find<UiSystem>(UiSystem::self_name()) }
    , m_ui_generators{ m_root->find<UiGenerators>(UiGenerators::self_name()) }
    , m_font_loader{ m_root->find<FontLoader>(FontLoader::self_name()) }
{
    ASSUMPTION(
        m_background_system != nullptr && 
        m_viewport_system != nullptr && 
        m_camera_system != nullptr && 
        m_shader_system != nullptr && 
        m_material_system != nullptr &&
        m_buffer_system != nullptr && 
        m_buffer_generators != nullptr && 
        m_object_system != nullptr && 
        m_light_system != nullptr && 
        m_renderer != nullptr &&
        m_ui_system != nullptr && 
        m_ui_generators != nullptr &&
        m_font_loader != nullptr
        );
}

} // namespace gfx
