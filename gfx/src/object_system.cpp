#include <com/frame.hpp>
#include <gfx/buffer.hpp>
#include <gfx/buffer_system.hpp>
#include <gfx/index.hpp>
#include <gfx/object_system.hpp>
#include <gfx/shader.hpp>
#include <gfx/shader_system.hpp>
#include <utils/assumptions.hpp>
#include <utils/development.hpp>
#include <utils/invariants.hpp>

namespace gfx {

ObjectSystem::ObjectSystem()
    : com::Library{ self_name() }
    , m_objects { nullptr }
{}

ObjectSystem::~ObjectSystem()
{}

void ObjectSystem::initialize()
{
    m_objects = folder()->push_back<com::Folder>("objects");
}

void ObjectSystem::release()
{
    folder()->erase(objects());
}

com::Folder *ObjectSystem::insert_object(com::ContextPath const &path, Material *material, Buffer *buffer) {
    com::Folder *const object = objects()->push_back_folders(path);
    ASSUMPTION(object->is_under(objects()));
    if (!object->empty()) {
        INVARIANT(object->find<com::Link>(material_link_name()) != nullptr &&
                  object->find<com::Link>(buffer_link_name()) != nullptr &&
                  object->find<com::Folder>(frames_folder_name()) != nullptr &&
                  object->find<com::Folder>(uniforms_folder_name()) != nullptr &&
                  object->find<com::Folder>(lights_folder_name()) != nullptr);
        return object;
    }
    object->push_back<com::Link>(material_link_name(), material);
    object->push_back<com::Link>(buffer_link_name(), buffer);
    object->push_back<com::Folder>(frames_folder_name());
    object->push_back<com::Folder>(uniforms_folder_name());
    object->push_back<com::Folder>(lights_folder_name());
    return object;
}

com::Folder *ObjectSystem::insert_object_front(com::ContextPath const &path, Material *material, Buffer *buffer) {
    com::Folder *const object = objects()->push_front_folders(path);
    ASSUMPTION(object->is_under(objects()));
    if (!object->empty()) {
        INVARIANT(object->find<com::Link>(material_link_name()) != nullptr &&
                  object->find<com::Link>(buffer_link_name()) != nullptr &&
                  object->find<com::Folder>(frames_folder_name()) != nullptr &&
                  object->find<com::Folder>(uniforms_folder_name()) != nullptr &&
                  object->find<com::Folder>(lights_folder_name()) != nullptr);
        return object;
    }
    object->push_back<com::Link>(material_link_name(), material);
    object->push_back<com::Link>(buffer_link_name(), buffer);
    object->push_back<com::Folder>(frames_folder_name());
    object->push_back<com::Folder>(uniforms_folder_name());
    object->push_back<com::Folder>(lights_folder_name());
    return object;
}

void ObjectSystem::erase_object(com::Folder* const object)
{
    ASSUMPTION(object->is_under(objects()));
    object->erase(material_link_name());
    object->erase(buffer_link_name());
    object->erase(frames_folder_name());
    object->erase(uniforms_folder_name());
    object->erase(lights_folder_name());
    com::Folder::erase_empty(object, objects());
}

void ObjectSystem::erase_object_only(com::Folder* const object)
{
    ASSUMPTION(object->is_under(objects()));
    object->erase(material_link_name());
    object->erase(buffer_link_name());
    object->erase(frames_folder_name());
    object->erase(uniforms_folder_name());
    object->erase(lights_folder_name());
    object->folder()->erase(object);
}

Material* ObjectSystem::get_material(com::Folder* const object) const
{
    return object->find<com::Link>(material_link_name())->target<Material>();
}

Buffer* ObjectSystem::get_buffer(com::Folder* const object) const
{
    return object->find<com::Link>(buffer_link_name())->target<Buffer>();
}

com::Folder* ObjectSystem::get_frames(com::Folder* const object) const
{
    return object->find<com::Folder>(frames_folder_name());
}

com::Folder* ObjectSystem::get_uniforms(com::Folder* const object) const
{
    return object->find<com::Folder>(uniforms_folder_name());
}

com::Folder* ObjectSystem::get_lights(com::Folder* const object) const
{
    return object->find<com::Folder>(lights_folder_name());
}

com::FileVec4u * ObjectSystem::get_scissor_file(com::Folder * const object) const {
    return object->find<com::FileVec4u>(scissor_file_name());
}

void ObjectSystem::set_material(com::Folder * const object, Material *material) const {
    ASSUMPTION(material != nullptr);
    auto link = object->find<com::Link>(material_link_name());
    ASSUMPTION(link != nullptr);
    link->set_target(material);
}

void ObjectSystem::push_frame_front(com::Folder* const object, com::Frame* const frame, std::string const& name)
{
    com::Folder* const frames = get_frames(object);
    ASSUMPTION(frames != nullptr && !has_frame(object, frame));
    frames->push_front<com::Link>(name.empty() ? frames->generate_unique_name("link", ".link") : name, frame);
}

void ObjectSystem::push_frame_back(com::Folder* const object, com::Frame* const frame, std::string const& name)
{
    com::Folder* const frames = get_frames(object);
    ASSUMPTION(frames != nullptr && !has_frame(object, frame));
    frames->push_back<com::Link>(name.empty() ? frames->generate_unique_name("link", ".link") : name, frame);
}

void ObjectSystem::insert_frame_before(
    com::Folder* const object, com::Frame* const frame, std::string const& pivot_frame_name, std::string const& name
    )
{
    com::Folder* const frames = get_frames(object);
    ASSUMPTION(frames != nullptr && !has_frame(object, frame));
    frames->insert_before<com::Link>(pivot_frame_name, name.empty() ? frames->generate_unique_name("link", ".link") : name, frame);
}

void ObjectSystem::insert_frame_after(
    com::Folder* const object, com::Frame* const frame, std::string const& pivot_frame_name, std::string const& name
    )
{
    com::Folder* const frames = get_frames(object);
    ASSUMPTION(frames != nullptr && !has_frame(object, frame));
    frames->insert_after<com::Link>(pivot_frame_name, name.empty() ? frames->generate_unique_name("link", ".link") : name, frame);
}

void ObjectSystem::erase_frame(com::Folder* const object, com::Frame* const frame)
{
    ASSUMPTION(object->is_under(objects()));
    com::Folder* const frames = get_frames(object);
    ASSUMPTION(frames != nullptr && object->linkage() != nullptr);
    for (com::Link* link : std::vector<com::Link*>{ frame->linkage()->begin(), frame->linkage()->end() })
        if (link->folder() == frames)
        {
            frames->erase(link);
            return;
        }
    UNREACHABLE();
}

bool ObjectSystem::has_frame(com::Folder* const object, com::Frame* const frame) const
{
    ASSUMPTION(object->is_under(objects()));
    com::Folder* const frames = get_frames(object);
    if (frame->linkage() != nullptr)
        for (com::Link* link : std::vector<com::Link*>{ frame->linkage()->begin(), frame->linkage()->end() })
            if (link->folder() == frames)
                return true;
    return false;
}

void ObjectSystem::insert_light(com::Folder* const object, com::Folder* const light)
{
    ASSUMPTION(object->is_under(objects()));
    ASSUMPTION(light->is_under(light_system()->lights()));
    ASSUMPTION(!has_light(object, light));
    com::Folder* const lights = get_lights(object);
    ASSUMPTION(lights != nullptr);
    lights->push_back<com::Link>(lights->generate_unique_name(light->name(), ".link"), light);
}

void ObjectSystem::erase_light(com::Folder* const object, com::Folder* const light)
{
    ASSUMPTION(object->is_under(objects()));
    ASSUMPTION(light->is_under(light_system()->lights()));
    com::Folder* const lights = get_lights(object);
    ASSUMPTION(lights != nullptr);
    if (light->linkage() != nullptr)
        for (com::Link* link : std::vector<com::Link*>{ light->linkage()->begin(), light->linkage()->end() })
            if (link->folder() == lights)
            {
                lights->erase(link);
                return;
            }
    UNREACHABLE();
}

bool ObjectSystem::has_light(com::Folder* const object, com::Folder* const light) const
{
    ASSUMPTION(object->is_under(objects()));
    ASSUMPTION(light->is_under(light_system()->lights()));
    com::Folder* const lights = get_lights(object);
    ASSUMPTION(lights != nullptr);
    if (light->linkage() != nullptr)
        for (com::Link* link : std::vector<com::Link*>{ light->linkage()->begin(), light->linkage()->end() })
            if (link->folder() == lights)
                return true;
    return false;
}

void ObjectSystem::insert_scissor_file(com::Folder *object, vec4u scissor) const {
    ASSUMPTION(object->is_under(objects()));
    object->push_back<com::FileVec4u>(scissor_file_name(), scissor);
}

}
