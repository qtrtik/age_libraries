#include <SDL2/SDL_timer.h>
#include <com/frame.hpp>
#include <com/math_files.hpp>
#include <gfx/index.hpp>
#include <gfx/renderer.hpp>
#include <osi/index.hpp>
#include <osi/opengl.hpp>
#include <utils/assumptions.hpp>
#include <utils/development.hpp>
#include <utils/invariants.hpp>
#include <algorithm>

namespace gfx::detail {

static auto window_size() -> std::pair<int, int> {
    auto size = osi::window()->size();
    return { size[0], size[1] };
}

} // namespace gfx::detail

namespace gfx {

Renderer::Renderer() : com::Library{ self_name() }, m_win_width{ 0 }, m_win_height{ 0 } {
    // configure g-buffer framebuffer
    GL_ASSUME_SUCCESS(glGenFramebuffers(1, &m_g_buffer));
    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, m_g_buffer));

    resize_fullscreen_textures();
    configure_face_culling(true);
}

Renderer::~Renderer() {
    GL_ASSUME_SUCCESS(glDeleteFramebuffers(1, &m_g_buffer));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_position));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_normal));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_ambient));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_diffuse));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_specular));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_emission));
    GL_ASSUME_SUCCESS(glDeleteRenderbuffers(1, &m_rbo_depth));
}

void Renderer::initialize() {
    m_empty_vao = buffer_system()->insert_buffer("empty-vao", Buffer::POINT, { "renderer" });
    m_light_sphere = buffer_generators()->insert_procedural_sphere_solid(1., 5, "light-sphere", { "renderer" });
}

void Renderer::release() {
    buffer_system()->erase_buffer(m_empty_vao);
    m_empty_vao = nullptr;
    buffer_system()->erase_buffer(m_light_sphere);
    m_light_sphere = nullptr;
}

void Renderer::clear_render_buffers(bool using_deferred) {
    if (using_deferred) {
        GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, m_g_buffer));
        GL_ASSUME_SUCCESS(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
        GL_ASSUME_SUCCESS(glClearDepth(1.0));
        GL_ASSUME_SUCCESS(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
    }

    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, 0));

    if (!using_deferred) {
        vec3 const &color = background_system()->background()->color();
        GL_ASSUME_SUCCESS(glClearColor(color.r, color.g, color.b, 0.0f));
    }

    GL_ASSUME_SUCCESS(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::present_collection(com::Folder *const collection, pipeline p) {
    ASSUMPTION(collection == object_system()->objects() || collection->is_under(object_system()->objects()));
    ASSUMPTION(camera_system()->active_frame() != nullptr);

    resize_fullscreen_textures();

    viewport_system()->active_viewport()->activate();

    CommonVisitData const common{ camera_system()->active_camera()->matrix(), camera_system()->active_frame()->in(),
                                  camera_system()->active_frame()->frame().origin() };

    unsigned framebuffer;
    switch (p) {
    case FORWARD:
    case TRANSPARENT:
        framebuffer = 0;
        break;
    case DEFERRED:
        framebuffer = m_g_buffer;
        break;
    default:
        UNREACHABLE();
        return; // silence linter
    }

    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, framebuffer));
    GL_ASSUME_SUCCESS(glEnable(GL_DEPTH_TEST));

    if (p == TRANSPARENT)
        present_collection_transparent(collection, common);
    else
        present_collection(collection, common);
}

void Renderer::lighting_pass(const std::vector<const com::Folder *> &lights, ShaderGraph::lighting_model lm) const {
    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, 0));
    GL_ASSUME_SUCCESS(glDisable(GL_DEPTH_TEST));

    configure_depth_writing(false);
    configure_blending(true, GL_ONE, GL_ONE);
    configure_face_culling(true, GL_FRONT);

    GL_ASSUME_SUCCESS(glActiveTexture(GL_TEXTURE0));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_position));
    GL_ASSUME_SUCCESS(glActiveTexture(GL_TEXTURE1));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_normal));
    GL_ASSUME_SUCCESS(glActiveTexture(GL_TEXTURE2));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_emission));
    GL_ASSUME_SUCCESS(glActiveTexture(GL_TEXTURE3));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_ambient));
    GL_ASSUME_SUCCESS(glActiveTexture(GL_TEXTURE4));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_diffuse));

    if (lm == ShaderGraph::PHONG) {
        GL_ASSUME_SUCCESS(glActiveTexture(GL_TEXTURE5));
        GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_specular));
    }

    light_volumes_pass(lights, lm);

    configure_face_culling(true);
    quad_pass(lights, lm);

    configure_face_culling(true);
    configure_depth_writing(true);
    configure_blending(false);
}

void Renderer::gather_lights(const com::Folder *collection, std::vector<const com::Folder *> &lights) {
    for (com::ContextItem *const item : collection->items()) {
        auto subfolder = dynamic_cast<com::Folder *>(item);
        if (subfolder == nullptr || subfolder->name() == ObjectSystem::uniforms_folder_name() ||
            subfolder->name() == ObjectSystem::frames_folder_name())
            continue;

        if (subfolder->name() == ObjectSystem::lights_folder_name())
            lights.push_back(subfolder);
        else
            gather_lights(subfolder, lights);
    }
}

void Renderer::configure_blending(bool enable, GLenum sfactor, GLenum dfactor) {
    if (enable) {
        GL_ASSUME_SUCCESS(glEnable(GL_BLEND));
        GL_ASSUME_SUCCESS(glBlendFunc(sfactor, dfactor));
    } else {
        GL_ASSUME_SUCCESS(glDisable(GL_BLEND));
    }
}

void Renderer::configure_scissor_test(bool enable) {
    if (enable) {
        vec2 viewport_size = viewport_system()->active_viewport()->size_absolute();
        glEnable(GL_SCISSOR_TEST);
        glScissor(0, 0, (GLsizei)viewport_size.x, (GLsizei)viewport_size.y);
    }
    else
        glDisable(GL_SCISSOR_TEST);
}

void Renderer::configure_line_width(scalar width) {
    GLfloat lineWidthRange[2] = {1.0f, 1.0f};
    glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, lineWidthRange);
    glLineWidth(std::min(width, lineWidthRange[1]));
}

void Renderer::configure_depth_writing(bool enable) {
    GL_ASSUME_SUCCESS(glDepthMask(enable ? GL_TRUE : GL_FALSE));
}

void Renderer::configure_face_culling(bool enable, GLenum mode) {
    if (enable) {
        GL_ASSUME_SUCCESS(glEnable(GL_CULL_FACE));
        GL_ASSUME_SUCCESS(glCullFace(mode));
    } else {
        GL_ASSUME_SUCCESS(glDisable(GL_CULL_FACE));
    }
}

void Renderer::copy_depth_buffer() const {
    auto [width, height] = detail::window_size();

    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_READ_FRAMEBUFFER, m_g_buffer));
    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0));
    GL_ASSUME_SUCCESS(glBlitFramebuffer(0, 0, width, height, 0, 0, width, height,
                                        GL_DEPTH_BUFFER_BIT, GL_NEAREST));
    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

void Renderer::resize_fullscreen_textures() {
    auto [width, height] = detail::window_size();
    if (width == m_win_width && height == m_win_height)
        return;

    m_win_width = width;
    m_win_height = height;

    // deallocate previous textures
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_position));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_normal));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_ambient));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_diffuse));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_specular));
    GL_ASSUME_SUCCESS(glDeleteTextures(1, &m_g_emission));
    GL_ASSUME_SUCCESS(glDeleteRenderbuffers(1, &m_rbo_depth));

    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, m_g_buffer));

    // position buffer
    GL_ASSUME_SUCCESS(glGenTextures(1, &m_g_position));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_position));
    GL_ASSUME_SUCCESS(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_g_position, 0));

    // normal buffer
    GL_ASSUME_SUCCESS(glGenTextures(1, &m_g_normal));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_normal));
    GL_ASSUME_SUCCESS(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_g_normal, 0));

    // ambient color buffer
    GL_ASSUME_SUCCESS(glGenTextures(1, &m_g_ambient));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_ambient));
    GL_ASSUME_SUCCESS(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_g_ambient, 0));

    // diffuse color buffer
    GL_ASSUME_SUCCESS(glGenTextures(1, &m_g_diffuse));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_diffuse));
    GL_ASSUME_SUCCESS(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_g_diffuse, 0));

    // specular color buffer
    GL_ASSUME_SUCCESS(glGenTextures(1, &m_g_specular));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_specular));
    GL_ASSUME_SUCCESS(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, m_g_specular, 0));

    // emission color buffer
    GL_ASSUME_SUCCESS(glGenTextures(1, &m_g_emission));
    GL_ASSUME_SUCCESS(glBindTexture(GL_TEXTURE_2D, m_g_emission));
    GL_ASSUME_SUCCESS(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL_ASSUME_SUCCESS(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, m_g_emission, 0));

    // tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
    unsigned int attachments[6] = {
        GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2,
        GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5
    };
    GL_ASSUME_SUCCESS(glDrawBuffers(6, attachments));

    // create and attach depth buffer (renderbuffer)
    GL_ASSUME_SUCCESS(glGenRenderbuffers(1, &m_rbo_depth));
    GL_ASSUME_SUCCESS(glBindRenderbuffer(GL_RENDERBUFFER, m_rbo_depth));
    GL_ASSUME_SUCCESS(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height));
    GL_ASSUME_SUCCESS(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_rbo_depth));
    ASSUMPTION(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    GL_ASSUME_SUCCESS(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

void Renderer::present_collection(com::Folder *collection, CommonVisitData const &common) {
    if (auto* scissor_file = object_system()->get_scissor_file(collection)) {
        vec4u scissor = scissor_file->get();
        glScissor(scissor.x, scissor.y, scissor.z, scissor.w);
    }
    for (com::ContextItem *const item : collection->items()) {
        auto subfolder = dynamic_cast<com::Folder *>(item);
        if (subfolder == nullptr || subfolder->name() == ObjectSystem::uniforms_folder_name() ||
            subfolder->name() == ObjectSystem::lights_folder_name())
            continue;

        if (subfolder->name() == ObjectSystem::frames_folder_name())
            present_object(collection, common);
        else
            present_collection(subfolder, common);
    }
}

void Renderer::present_collection_transparent(com::Folder *collection, CommonVisitData const &common) {
    configure_blending(true);
    configure_face_culling(true);
    configure_depth_writing(false);

    objects_buffer_t objects;
    gather_objects(objects, collection);
    std::ranges::sort(objects, {},
        [&](const auto &pair) {
            return -glm::length(pair.second->frame().origin() - common.camera_pos);
        });

    for (auto &[obj, fr] : objects)
        present_object(obj, common, fr);

    configure_blending(false);
    configure_face_culling(false);
    configure_depth_writing(true);
}

void Renderer::gather_objects(objects_buffer_t &objects, com::Folder *collection) {
    for (com::ContextItem *const item : collection->items()) {
        auto subfolder = dynamic_cast<com::Folder *>(item);
        if (subfolder == nullptr || subfolder->name() == ObjectSystem::uniforms_folder_name() ||
            subfolder->name() == ObjectSystem::lights_folder_name())
            continue;

        if (subfolder->name() == ObjectSystem::frames_folder_name()) {
            for (com::ContextItem *const item : subfolder->items()) {
                com::Frame *const frame = dynamic_cast<com::Link *>(item)->target<com::Frame>();
                INVARIANT(frame != nullptr);
                objects.emplace_back(collection, frame);
            }
        } else {
            gather_objects(objects, subfolder);
        }
    }
}

void Renderer::light_volumes_pass(const std::vector<const com::Folder *> &lights,
                                  ShaderGraph::lighting_model lm) const {

    auto deferred_shader = lm == ShaderGraph::PHONG ?
        shader_system()->light_volumes_shader()
      : shader_system()->light_volumes_shader_PBR();
    shader_system()->set_active_shader(deferred_shader);

    CommonVisitData const common{
        camera_system()->active_camera()->matrix(),
        camera_system()->active_frame()->in(),
        camera_system()->active_frame()->frame().origin()
    };

    auto [ambient, directional, point] = deferred_shader->set_lights(lights);
    deferred_shader->set_camera_uniforms(common.view_matrix, common.projection_matrix, common.camera_pos);

    deferred_shader->set_uniform("gPosition", 0);
    deferred_shader->set_uniform("gNormal", 1);

    switch (lm) {
    case ShaderGraph::PHONG:
        deferred_shader->set_uniform("gDiffuse", 4);
        deferred_shader->set_uniform("gSpecular", 5);
        break;
    case ShaderGraph::PBR:
        deferred_shader->set_uniform("gAlbedo", 3);
        deferred_shader->set_uniform("gMetRoughAo", 4);
        break;
    default:
        UNREACHABLE();
    }

    buffer_system()->set_active_buffer(m_light_sphere);
    GL_ASSUME_SUCCESS(glDrawElementsInstanced(
            GL_TRIANGLES, (GLsizei)m_light_sphere->indices()->size(),
            GL_UNSIGNED_INT, nullptr, point));
}

void Renderer::quad_pass(const std::vector<const com::Folder *> &lights, ShaderGraph::lighting_model lm) const {
    auto deferred_shader = lm == ShaderGraph::PHONG ?
        shader_system()->deferred_lighting_shader()
      : shader_system()->deferred_lighting_shader_PBR();
    shader_system()->set_active_shader(deferred_shader);

    deferred_shader->set_lights(lights);
    deferred_shader->set_uniform(Shader::camera_pos_uniform, camera_system()->active_frame()->frame().origin());
    deferred_shader->set_uniform(Shader::background_color_uniform, background_system()->background()->color());

    deferred_shader->set_uniform("gPosition", 0);
    deferred_shader->set_uniform("gNormal", 1);
    deferred_shader->set_uniform("gEmission", 2);

    switch (lm) {
    case ShaderGraph::PHONG:
        deferred_shader->set_uniform("gAmbient", 3);
        deferred_shader->set_uniform("gDiffuse", 4);
        deferred_shader->set_uniform("gSpecular", 5);
        break;
    case ShaderGraph::PBR:
        deferred_shader->set_uniform("gAlbedo", 3);
        deferred_shader->set_uniform("gMetRoughAo", 4);
        break;
    default:
        UNREACHABLE();
    }

    buffer_system()->set_active_buffer(m_empty_vao);
    GL_ASSUME_SUCCESS(glDrawArrays(GL_TRIANGLES, 0, 3));
}

void Renderer::present_object(com::Folder *const object, CommonVisitData const &common,
                              com::Frame *frame) {
    Material *const material = object_system()->get_material(object);
    ASSUMPTION(material != nullptr);
    material_system()->set_active_material(material);

    auto shader = material->shader();
    ASSUMPTION(shader != nullptr);

    shader->set_camera_uniforms(common.view_matrix, common.projection_matrix, common.camera_pos);
    shader->set_time_uniforms(static_cast<int>(SDL_GetTicks()));

    if (shader->lighting_type() == Shader::LIT) {
        auto lights = object_system()->get_lights(object);
        ASSUMPTION(lights != nullptr);
        shader->set_lights(*lights);
    }

    Buffer *const buffer = object_system()->get_buffer(object);
    ASSUMPTION(buffer != nullptr);
    buffer_system()->set_active_buffer(buffer);

    auto present_frame = [&](com::Frame *f){
        shader->set_model_uniforms(f->out(), transposed(inverted(f->out())));

        switch (buffer->primitive_type()) {
        case Buffer::POINT:
            if (buffer->indices() == nullptr)
                glDrawArrays(GL_POINTS, 0U, (GLsizei)buffer->positions()->size());
            else
                glDrawElements(GL_POINTS, (GLsizei)buffer->indices()->size(), GL_UNSIGNED_INT, nullptr);
            break;
        case Buffer::LINE:
            if (buffer->indices() == nullptr)
                glDrawArrays(GL_LINES, 0U, (GLsizei)(buffer->positions()->size()));
            else
                glDrawElements(GL_LINES, (GLsizei)buffer->indices()->size(), GL_UNSIGNED_INT, nullptr);
            break;
        case Buffer::TRIANGLE:
            if (buffer->indices() == nullptr)
                glDrawArrays(GL_TRIANGLES, 0U, (GLsizei)(buffer->positions()->size()));
            else
                glDrawElements(GL_TRIANGLES, (GLsizei)buffer->indices()->size(), GL_UNSIGNED_INT, nullptr);
            break;
        default:
            UNREACHABLE();
            break;
        }
        ASSUMPTION(glGetError() == 0U);
    };

    if (frame != nullptr) {
        present_frame(frame);
        return;
    }

    for (com::ContextItem *const item : object_system()->get_frames(object)->items()) {
        frame = dynamic_cast<com::Link *>(item)->target<com::Frame>();
        INVARIANT(frame != nullptr);
        present_frame(frame);
    }
}

} // namespace gfx
