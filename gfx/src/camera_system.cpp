#include <gfx/camera_system.hpp>
#include <com/frame.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>

namespace gfx {

CameraSystem::CameraSystem()
    : com::Library{ self_name() }
    , m_cameras { nullptr }
    , m_default_camera { nullptr }
    , m_active_camera { nullptr } 
{}

CameraSystem::~CameraSystem()
{}

void CameraSystem::initialize()
{
    m_cameras = folder()->push_back<com::Folder>("cameras");
    m_default_camera = insert_camera({ "default" });
    m_active_camera = m_default_camera; 
}

void CameraSystem::release()
{
    folder()->erase(cameras());
}

Camera* CameraSystem::insert_camera(com::ContextPath const& path)
{
    com::Folder* const camera_folder = cameras()->push_back_folders(path);
    ASSUMPTION(camera_folder->is_under(cameras()));
    Camera* camera = camera_folder->find<Camera>(Camera::self_file_name());
    if (camera != nullptr)
    {
        INVARIANT(camera_folder->find<com::Link>(frame_link_file_name()));
        return camera;
    }
    camera = camera_folder->push_back<Camera>();
    camera_folder->push_back<com::Link>(frame_link_file_name(), nullptr);
    return camera;

}

void CameraSystem::erase_camera(com::Folder* const camera_folder)
{
    ASSUMPTION(camera_folder->is_under(cameras()));
    camera_folder->erase(frame_link_file_name());
    camera_folder->erase(Camera::self_file_name());
    com::Folder::erase_empty(camera_folder, cameras());
}

com::Frame* CameraSystem::get_frame(com::Folder* camera_folder) {
    ASSUMPTION(camera_folder->is_under(cameras()));
    return camera_folder->find<com::Link>(frame_link_file_name())->target<com::Frame>();
}

void CameraSystem::set_frame(com::Folder* const camera_folder, com::Frame* const frame)
{
    ASSUMPTION(camera_folder->is_under(cameras()));
    camera_folder->find<com::Link>(frame_link_file_name())->set_target(frame);
}

com::Frame* CameraSystem::active_frame() const
{
    return m_active_camera->folder()->find<com::Link>(frame_link_file_name())->target<com::Frame>();
}

void CameraSystem::activate_camera(com::Folder* const camera_folder)
{
    ASSUMPTION(camera_folder->is_under(cameras()));
    m_active_camera = camera_folder->find<Camera>(Camera::self_file_name());
}

}
