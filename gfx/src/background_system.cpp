#include <gfx/background_system.hpp>

namespace gfx {

BackgroundSystem::BackgroundSystem()
    : com::Library{ self_name() }
    , m_background{ nullptr }
{}

BackgroundSystem::~BackgroundSystem()
{}

void BackgroundSystem::initialize()
{
    m_background = folder()->push_back<com::Folder>("backgrounds")->push_back<Background>();
}

void BackgroundSystem::release()
{
    folder()->erase(background()->folder());
}

}
