#include <gfx/background.hpp>

namespace gfx {

Background::Background()
    : com::File{ self_file_name() }
    , m_color { 0.25f, 0.25f, 0.25f }
{}

}
