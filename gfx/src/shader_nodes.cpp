#include <lfs/index.hpp>
#include <gfx/image_utils.hpp>
#include <gfx/light.hpp>
#include <gfx/shader_nodes.hpp>
#include <osi/opengl.hpp>
#include <algorithm>

namespace gfx {

void ShaderGraph::UniformNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].decl.push_back("uniform " + m_outputs[0].type_name() + ' ' + stage_name(stage));
    code[stage].code.push_back(m_outputs[0].type_name() + ' ' + variable_name() + " = " + stage_name(stage));
}

ShaderGraph::Stage ShaderGraph::MasterNodeUnlit::output_to_input_stage(Stage out, std::size_t in) const {
    Stage s;
    switch (in) {
    case color:
    case emission:
    case alpha:
    case alpha_threshold:
        s = Stage::FRAGMENT;
        break;
    case position:
        s = Stage::VERTEX;
        break;
    default:
        UNREACHABLE();
    }
    return s;
}

ShaderGraph::Stage ShaderGraph::MasterNodeLit::output_to_input_stage(Stage out, std::size_t in) const {
    Stage s;
    switch (in) {
    case position:
    case normal:
    case tangent:
        s = Stage::VERTEX;
        break;
    case normal_ts:
    case ambient:
    case diffuse:
    case specular:
    case emission:
    case alpha:
    case alpha_threshold:
        s = Stage::FRAGMENT;
        break;
    default:
        UNREACHABLE();
    }
    return s;
}

ShaderGraph::Stage ShaderGraph::MasterNodeDeferred::output_to_input_stage(Stage out, std::size_t in) const {
    Stage s;
    switch (in) {
    case position:
    case normal:
    case tangent:
        s = Stage::VERTEX;
        break;
    case normal_ts:
    case ambient:
    case diffuse:
    case specular:
    case emission:
        s = Stage::FRAGMENT;
        break;
    default:
        UNREACHABLE();
    }
    return s;
}

ShaderGraph::Stage ShaderGraph::MasterNodePBR::output_to_input_stage(Stage out, std::size_t in) const {
    Stage s;
    switch (in) {
    case position:
    case normal:
    case tangent:
        s = Stage::VERTEX;
        break;
    case normal_ts:
    case albedo:
    case emission:
    case metallic:
    case roughness:
    case ao:
    case alpha:
    case alpha_threshold:
        s = Stage::FRAGMENT;
        break;
    default:
        UNREACHABLE();
    }
    return s;
}

ShaderGraph::Stage ShaderGraph::MasterNodeDeferredPBR::output_to_input_stage(Stage out, std::size_t in) const {
    Stage s;
    switch (in) {
    case position:
    case normal:
    case tangent:
        s = Stage::VERTEX;
        break;
    case normal_ts:
    case albedo:
    case emission:
    case metallic:
    case roughness:
    case ao:
        s = Stage::FRAGMENT;
        break;
    default:
        UNREACHABLE();
    }
    return s;
}

std::string ShaderGraph::InputNode::stage_name(Stage stage) const {
    std::string postfix;
    switch (stage) {
    case Stage::VERTEX:
        postfix = "vert";
        break;
    case Stage::FRAGMENT:
        postfix = "frag";
        break;
    case Stage::ANY:
        break;
    }
    return variable_name() + '_' + postfix;
}

ShaderGraph::TextureNode::TextureNode(const com::ContextPath &tex_file, DataType::ElementType out_t,
                                      std::string override_sampler_name)
    : Node("Texture Sampler", "Samples a texture based on provided texture coordinates.",
           { { "Texture Coordinates", "Texture Coordinates", { DataType::ElementType::VEC2 } } },
           { { "Color", "The sampled texture color.", { out_t } } }),
      m_id{ 0 }, m_shader{ nullptr }, m_handle{ nullptr }, m_tex_file{ tex_file },
      m_override_sampler_name{std::move(override_sampler_name)} {
    ASSUMPTION(out_t == DataType::SCALAR || out_t == DataType::VEC2 || out_t == DataType::VEC3 || out_t == DataType::VEC4);
}

void ShaderGraph::TextureNode::activate(int texture_unit) const {
    ASSUMPTION(texture_unit >= 0);

    glActiveTexture(GL_TEXTURE0 + texture_unit);
    INVARIANT(glGetError() == 0U);
    glBindTexture(GL_TEXTURE_2D, m_id);
    INVARIANT(glGetError() == 0U);

    m_shader->set_uniform(sampler_name(), texture_unit);
}

std::string ShaderGraph::TextureNode::sampler_name() const {
    return m_override_sampler_name.empty() ? variable_name() + "_sampler" : m_override_sampler_name;
}

ShaderGraph::TextureNode::~TextureNode() { glDeleteTextures(1, &m_id); }

void ShaderGraph::TextureNode::load_texture() {
    m_handle = lfs::loader()->load(m_tex_file);
    if (m_handle->state() == lfs::ResourceHandle::LOADED)
        generate_texture();
    else
        m_handle->register_on_content_changed(m_shader);
}

void ShaderGraph::TextureNode::generate_texture() {
    ASSUMPTION(m_id == 0);
    ASSUMPTION(m_handle != nullptr && m_handle->state() == lfs::ResourceHandle::LOADED);

    auto data = m_handle->data();
    m_handle->unregister_on_content_changed(m_shader);

    // generate texture
    glGenTextures(1, &m_id);
    INVARIANT(glGetError() == 0U);
    glBindTexture(GL_TEXTURE_2D, m_id);
    INVARIANT(glGetError() == 0U);

    // set texture wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    INVARIANT(glGetError() == 0U);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    INVARIANT(glGetError() == 0U);

    // set texture filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    INVARIANT(glGetError() == 0U);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    INVARIANT(glGetError() == 0U);

    // load texture
    int width, height, nr_channels;
    auto img_data = decode_png_image_from_bytes(data.data(), data.size(), width, height, nr_channels);
    if (img_data == nullptr)
        throw std::runtime_error{ "failed to load texture: " };

    // determine image format
    GLenum format;
    switch (nr_channels) {
    case 1:
        format = GL_RED;
        break;
    case 3:
        format = GL_RGB;
        break;
    case 4:
        format = GL_RGBA;
        break;
    default:
        UNREACHABLE();
    }

    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, img_data);
    INVARIANT(glGetError() == 0U);
    glGenerateMipmap(GL_TEXTURE_2D);
    INVARIANT(glGetError() == 0U);

    free_memory_of_decoded_png_image(img_data);
    glBindTexture(GL_TEXTURE_2D, 0);
    INVARIANT(glGetError() == 0U);
}

void ShaderGraph::TextureNode::add_code(StagesCode &code, Stage stage) const {
    INVARIANT(stage == Stage::FRAGMENT);

    std::string swizzle;
    switch (m_outputs[0].definition.type.element_type) {
    case DataType::SCALAR:
        swizzle = ".r";
        break;
    case DataType::VEC2:
        swizzle = ".rg";
        break;
    case DataType::VEC3:
        swizzle = ".rgb";
        break;
    case DataType::VEC4:
        swizzle = ".rgba";
        break;
    default:
        UNREACHABLE();
    }

    code[stage].decl.push_back("uniform sampler2D " + sampler_name());
    code[stage].code.push_back(make_initializer(
        "texture(" + sampler_name() + ", " + input_name(inputs::tex_coord) + ')' + swizzle));
}

void ShaderGraph::InputVaryingNode::add_code(StagesCode &code, Stage stage) const {
    auto decl =
        "layout(location = " + std::to_string(m_layout) + ") in " + m_outputs[0].type_name() + ' ' + universal_name();
    if (std::ranges::find(code[Stage::VERTEX].decl, decl) == code[Stage::VERTEX].decl.end())
        code[Stage::VERTEX].decl.push_back(decl);

    // add interface to pass between stages
    if (stage != Stage::VERTEX) {
        pass_to_fragment(code, 0, universal_name());
    } else {
        code[stage].code.push_back(m_outputs[0].type_name() + ' ' + variable_name() + " = " + universal_name());
    }
}

void ShaderGraph::ConstantNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(m_declaration);
}

vec3 const ShaderGraph::ErrorColorNode::error_color{ 1, 0, 1 };

void ShaderGraph::MasterNodeUnlit::add_code(StagesCode &code, Stage stage) const {
    code[Stage::VERTEX].decl.emplace_back(
        "uniform mat4 " + model_matrix_uniform + ";\n"
        "uniform mat4 " + view_matrix_uniform + ";\n"
        "uniform mat4 " + projection_matrix_uniform);

    code[Stage::VERTEX].code.emplace_back(
        "gl_Position = " + projection_matrix_uniform + " * " +
        view_matrix_uniform + " * " + model_matrix_uniform + " * " +
        "vec4(" + input_name(inputs::position) + ", 1.)");

    code[Stage::FRAGMENT].decl.emplace_back("out vec4 fragColor");

    std::string frag_code;
    switch (m_transparency) {
    case OPAQUE:
        frag_code = "fragColor = vec4(" + input_name(inputs::color)
                        + " + " + input_name(inputs::emission) + ", 1.)";
        break;
    case PARTIAL:
        frag_code = "fragColor = vec4(" + input_name(inputs::color)
                        + " + " + input_name(inputs::emission) + ", " + input_name(inputs::alpha) + ")";
        break;
    case THRESHOLD:
        frag_code = "if (" + input_name(inputs::alpha) + " < " + input_name(inputs::alpha_threshold) + ")\n"
                "        discard;\n"
                "    fragColor = vec4(" + input_name(inputs::color)
                    + " + " + input_name(inputs::emission) + ", 1.)";
        break;
    default:
        UNREACHABLE();
    }

    code[Stage::FRAGMENT].code.push_back(frag_code);
}

void ShaderGraph::MasterNodeLit::add_code(StagesCode &code, Stage stage) const {
    code[Stage::VERTEX].decl.emplace_back(
        "uniform mat4 " + model_matrix_uniform + ";\n"
        "uniform mat4 " + view_matrix_uniform + ";\n"
        "uniform mat4 " + projection_matrix_uniform + ";\n"
        "uniform mat4 " + normal_matrix_uniform + ";\n"
        "out vec3 position;\n"
        "out mat3 TBN");

    code[Stage::FRAGMENT].decl.emplace_back(
        "in vec3 position;\n"
        "in mat3 TBN");

    code[Stage::VERTEX].code.emplace_back(
        "vec3 N = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::normal) + ");\n"
        "    vec3 T = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::tangent) + ");\n"
        "    vec3 B = cross(N, T);\n"
        "    TBN = mat3(T, B, N);\n"
        "    position = vec3(" + model_matrix_uniform + " * " + "vec4(" + input_name(inputs::position) + ", 1.));\n"
        "    gl_Position = " + projection_matrix_uniform + " * " + view_matrix_uniform + " * " + "vec4(position, 1.)");

    code[Stage::FRAGMENT].code.emplace_back(
        input_name(inputs::ambient) + " = clamp(" + input_name(inputs::ambient) + ", vec3(0.), vec3(1.));\n    " +
        input_name(inputs::diffuse) + " = clamp(" + input_name(inputs::diffuse) + ", vec3(0.), vec3(1.));\n    " +
        input_name(inputs::specular) + " = clamp(" + input_name(inputs::specular) + ", vec3(0.), vec3(1.))");

    add_lighting_code(code[Stage::FRAGMENT]);
}

void ShaderGraph::MasterNodeLit::add_lighting_code(DeclAndCode &fragment) const {
    fragment.decl.emplace_back(AmbientLight::declarations());
    fragment.decl.emplace_back(DirectionalLight::declarations());
    fragment.decl.emplace_back(PointLight::declarations());
    fragment.decl.emplace_back("uniform vec3 " + camera_pos_uniform);

    auto a_name = AmbientLight::glsl::u_name();
    auto a_name_idx = a_name + "[i]";
    auto d_name = DirectionalLight::glsl::u_name();
    auto d_name_idx = d_name + "[i]";
    auto p_name = PointLight::glsl::u_name();
    auto p_name_idx = p_name + "[i]";

    auto calc_a = AmbientLight::function_call(a_name_idx, input_name(inputs::ambient));
    auto calc_d = DirectionalLight::function_call(
        d_name_idx, input_name(inputs::diffuse),
        input_name(inputs::specular), "normal", "viewDir");
    auto calc_p = PointLight::function_call(
        p_name_idx, input_name(inputs::diffuse),
        input_name(inputs::specular), "normal", "position", "viewDir");

    fragment.decl.emplace_back(
        "out vec4 fragColor;\n"
        "uniform int " + AmbientLight::glsl::nr_lights() + ";\n"
        "uniform " + AmbientLight::glsl::type() + ' ' + a_name + '[' + std::to_string(max_ambient_lights) + "];\n"
        "uniform int " + DirectionalLight::glsl::nr_lights() + ";\n"
        "uniform " + DirectionalLight::glsl::type() + ' ' + d_name + '[' + std::to_string(max_directional_lights) + "];\n"
        "uniform int " + PointLight::glsl::nr_lights() + ";\n"
        "uniform " + PointLight::glsl::type() + ' ' + p_name + '[' + std::to_string(max_point_lights) + ']');

    if (m_transparency == THRESHOLD)
        fragment.code.emplace_back(
            "if (" + input_name(inputs::alpha) + " < " + input_name(inputs::alpha_threshold) + ")\n"
            "        discard");

    fragment.code.emplace_back(
        "vec3 normal = normalize(TBN * (" + input_name(inputs::normal_ts) + " * 2. - 1.));\n"
        "    vec3 viewDir = normalize(" + camera_pos_uniform + " - position);\n"
        "    vec3 result = vec3(0, 0, 0);\n"
        "    for (int i = 0; i < " + AmbientLight::glsl::nr_lights() + "; i++)\n"
        "        result += " + calc_a + ";\n"
        "    for (int i = 0; i < " + DirectionalLight::glsl::nr_lights() + "; i++)\n"
        "        result += " + calc_d + ";\n"
        "    for (int i = 0; i < " + PointLight::glsl::nr_lights() + "; i++)\n"
        "        if (length(" + p_name_idx + PointLight::glsl::position() + " - position) <= " + p_name_idx + PointLight::glsl::radius() + ")\n"
        "            result += " + calc_p);

    std::string final_color;
    switch (m_transparency) {
    case OPAQUE:
    case THRESHOLD:
        final_color = "fragColor = vec4(result + " + input_name(inputs::emission) + ", 1.)";
        break;
    case PARTIAL:
        final_color = "fragColor = vec4(result + " + input_name(inputs::emission) + ", " + input_name(inputs::alpha) + ")";
        break;
    default:
        UNREACHABLE();
    }

    fragment.code.push_back(final_color);
}

void ShaderGraph::MasterNodeDeferred::add_code(StagesCode &code, Stage stage) const {
    code[Stage::VERTEX].decl.emplace_back(
        "uniform mat4 " + model_matrix_uniform + ";\n"
        "uniform mat4 " + view_matrix_uniform + ";\n"
        "uniform mat4 " + projection_matrix_uniform + ";\n"
        "uniform mat4 " + normal_matrix_uniform + ";\n"
        "out vec3 position;\n"
        "out mat3 TBN");

    code[Stage::VERTEX].code.emplace_back(
        "vec3 N = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::normal) + ");\n"
        "    vec3 T = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::tangent) + ");\n"
        "    vec3 B = cross(N, T);\n"
        "    TBN = mat3(T, B, N);\n"
        "    position = vec3(" + model_matrix_uniform + " * " + "vec4(" + input_name(inputs::position) + ", 1.));\n"
        "    gl_Position = " + projection_matrix_uniform + " * " + view_matrix_uniform + " * " + "vec4(position, 1.)"
    );

    code[Stage::FRAGMENT].decl.emplace_back(
        "in vec3 position;\n"
        "in mat3 TBN;\n"
        "layout (location = 0) out vec4 g_position;\n"
        "layout (location = 1) out vec4 g_normal;\n"
        "layout (location = 2) out vec4 g_ambient;\n"
        "layout (location = 3) out vec4 g_diffuse;\n"
        "layout (location = 4) out vec4 g_specular;\n"
        "layout (location = 5) out vec4 g_emission"
    );

    code[Stage::FRAGMENT].code.emplace_back(
        "g_position = vec4(position, 1.);\n"
        "    g_normal = vec4(normalize(TBN * (" + input_name(inputs::normal_ts) + " * 2. - 1.)), 0.);\n"
        "    g_ambient = vec4(clamp(" + input_name(inputs::ambient) + ", vec3(0.), vec3(1.)), 1.);\n"
        "    g_diffuse = vec4(clamp(" + input_name(inputs::diffuse) + ", vec3(0.), vec3(1.)), 1.);\n"
        "    g_specular = vec4(clamp(" + input_name(inputs::specular) + ", vec3(0.), vec3(1.)), 1.);\n"
        "    g_emission = vec4(" + input_name(inputs::emission) + ", 1.)"
    );
}

void ShaderGraph::MasterNodePBR::add_code(StagesCode &code, Stage stage) const {
    code[Stage::VERTEX].decl.emplace_back(
        "uniform mat4 " + model_matrix_uniform + ";\n"
        "uniform mat4 " + view_matrix_uniform + ";\n"
        "uniform mat4 " + projection_matrix_uniform + ";\n"
        "uniform mat4 " + normal_matrix_uniform + ";\n"
        "out vec3 position;\n"
        "out mat3 TBN");

    code[Stage::FRAGMENT].decl.emplace_back(
        "in vec3 position;\n"
        "in mat3 TBN");

    code[Stage::VERTEX].code.emplace_back(
        "vec3 N = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::normal) + ");\n"
        "    vec3 T = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::tangent) + ");\n"
        "    vec3 B = cross(N, T);\n"
        "    TBN = mat3(T, B, N);\n"
        "    position = vec3(" + model_matrix_uniform + " * " + "vec4(" + input_name(inputs::position) + ", 1.));\n"
        "    gl_Position = " + projection_matrix_uniform + " * " + view_matrix_uniform + " * " + "vec4(position, 1.)");

    code[Stage::FRAGMENT].code.emplace_back(
        input_name(inputs::albedo) + " = clamp(" + input_name(inputs::albedo) + ", vec3(0.), vec3(1.));\n    " +
        input_name(inputs::metallic) + " = clamp(" + input_name(inputs::metallic) + ", 0., 1.);\n    " +
        input_name(inputs::roughness) + " = clamp(" + input_name(inputs::roughness) + ", 0., 1.);\n    " +
        input_name(inputs::ao) + " = clamp(" + input_name(inputs::ao) + ", 0., 1.)");

    add_lighting_code(code[Stage::FRAGMENT]);
}

void ShaderGraph::MasterNodePBR::add_lighting_code(DeclAndCode &fragment) const {
    fragment.decl.emplace_back(DirectionalLight::struct_decl());
    fragment.decl.emplace_back(PointLight::struct_decl());
    fragment.decl.emplace_back("uniform vec3 " + camera_pos_uniform);

    auto d_name = DirectionalLight::glsl::u_name();
    auto d_name_idx = d_name + "[i]";
    auto p_name = PointLight::glsl::u_name();
    auto p_name_idx = p_name + "[i]";

    fragment.decl.emplace_back(
        "out vec4 fragColor;\n"
        "uniform int " + DirectionalLight::glsl::nr_lights() + ";\n"
        "uniform " + DirectionalLight::glsl::type() + ' ' + d_name + '[' + std::to_string(max_directional_lights) + "];\n"
        "uniform int " + PointLight::glsl::nr_lights() + ";\n"
        "uniform " + PointLight::glsl::type() + ' ' + p_name + '[' + std::to_string(max_point_lights) + ']');

    // Adapted from https://learnopengl.com/PBR/Lighting
    std::string decl = R"(const float PI = 3.14159265359;
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
})";

    fragment.decl.push_back(decl);

    if (m_transparency == THRESHOLD)
        fragment.code.emplace_back(
            "if (" + input_name(inputs::alpha) + " < " + input_name(inputs::alpha_threshold) + ")\n"
            "        discard");

    // Adapted from https://learnopengl.com/PBR/Lighting
    auto code = R"(vec3 albedo     = )" + input_name(inputs::albedo) + R"(;
    float metallic  = )" + input_name(metallic) + R"(;
    float roughness = )" + input_name(roughness) + R"(;
    float ao        = )" + input_name(ao) + R"(;

    vec3 N = normalize(TBN * ()" + input_name(inputs::normal_ts) + R"( * 2. - 1.));
    vec3 V = normalize()" + camera_pos_uniform + R"( - position);

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    for(int i = 0; i < )" + PointLight::glsl::nr_lights() + R"(; ++i) {
        float distance = length()" + p_name_idx + PointLight::glsl::position() + R"( - position);
        if (distance > )" + p_name_idx + PointLight::glsl::radius() + R"()
            continue;

        float attenuation = 1.0 / ()" +
            p_name_idx + PointLight::glsl::atten_const() +
            " + " + p_name_idx + PointLight::glsl::atten_lin() + " * distance + " +
            p_name_idx + PointLight::glsl::atten_quad() + R"( * distance * distance);
        vec3 radiance = )" + p_name_idx + PointLight::glsl::color() + R"( * attenuation;

        vec3 L = normalize()" + p_name_idx + PointLight::glsl::position() + R"( - position);
        vec3 H = normalize(V + L);

        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001; // + 0.0001 to prevent divide by zero
        vec3 specular = numerator / denominator;

        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;

        float NdotL = max(dot(N, L), 0.0);

        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
    }
    for(int i = 0; i < )" + DirectionalLight::glsl::nr_lights() + R"(; ++i) {
        vec3 L = normalize(-)" + d_name_idx + DirectionalLight::glsl::direction() + R"();
        vec3 H = normalize(V + L);
        vec3 radiance = )" + d_name_idx + DirectionalLight::glsl::color() + R"(;
        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001;
        vec3 specular = numerator / denominator;
        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;
        float NdotL = max(dot(N, L), 0.0);
        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
    }

    vec3 ambient = vec3(0.03) * albedo * ao;

    vec3 color = ambient + Lo + )" + input_name(inputs::emission);

    fragment.code.push_back(code);

    std::string final_color;
    switch (m_transparency) {
    case OPAQUE:
    case THRESHOLD:
        final_color = "fragColor = vec4(color, 1.)";
        break;
    case PARTIAL:
        final_color = "fragColor = vec4(color, " + input_name(inputs::alpha) + ")";
        break;
    default:
        UNREACHABLE();
    }

    fragment.code.push_back(final_color);
}

void ShaderGraph::MasterNodeDeferredPBR::add_code(StagesCode &code, Stage stage) const {
    code[Stage::VERTEX].decl.emplace_back(
        "uniform mat4 " + model_matrix_uniform + ";\n"
        "uniform mat4 " + view_matrix_uniform + ";\n"
        "uniform mat4 " + projection_matrix_uniform + ";\n"
        "uniform mat4 " + normal_matrix_uniform + ";\n"
        "out vec3 position;\n"
        "out mat3 TBN");

    code[Stage::VERTEX].code.emplace_back(
        "vec3 N = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::normal) + ");\n"
        "    vec3 T = normalize(mat3(" + normal_matrix_uniform + ") * " + input_name(inputs::tangent) + ");\n"
        "    vec3 B = cross(N, T);\n"
        "    TBN = mat3(T, B, N);\n"
        "    position = vec3(" + model_matrix_uniform + " * " + "vec4(" + input_name(inputs::position) + ", 1.));\n"
        "    gl_Position = " + projection_matrix_uniform + " * " + view_matrix_uniform + " * " + "vec4(position, 1.)"
    );

    code[Stage::FRAGMENT].decl.emplace_back(
        "in vec3 position;\n"
        "in mat3 TBN;\n"
        "layout (location = 0) out vec4 g_position;\n"
        "layout (location = 1) out vec4 g_normal;\n"
        "layout (location = 2) out vec4 g_albedo;\n"
        "layout (location = 3) out vec4 g_met_rough_ao;\n"
        "layout (location = 5) out vec4 g_emission"
    );

    code[Stage::FRAGMENT].code.emplace_back(
        "g_position = vec4(position, 1.);\n"
        "    g_normal = vec4(normalize(TBN * (" + input_name(inputs::normal_ts) + " * 2. - 1.)), 0.);\n"
        "    g_albedo = vec4(clamp(" + input_name(inputs::albedo) + ", vec3(0.), vec3(1.)), 1.);\n"
        "    g_met_rough_ao = clamp(vec4("
        + input_name(inputs::metallic) + ", "
        + input_name(inputs::roughness) + ", "
        + input_name(inputs::ao) + ", 1.), vec4(0.), vec4(1.));\n"
        "    g_emission = vec4(" + input_name(inputs::emission) + ", 1.)"
    );
}

void ShaderGraph::MultiplyNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer(input_name(inputs::left) +
                               " * " + input_name(inputs::right)));
}

void ShaderGraph::DivideNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer(input_name(inputs::left) +
                               " / " + input_name(inputs::right)));
}

void ShaderGraph::AddNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer(input_name(inputs::left) +
                               " + " + input_name(inputs::right)));
}

void ShaderGraph::SubtractNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer(input_name(inputs::left) +
                                   " - " + input_name(inputs::right)));
}

void ShaderGraph::MatrixVectorMultiplyNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer(input_name(inputs::left) +
                               " * " + input_name(inputs::right)));
}

void ShaderGraph::ScalarMultiplyNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer(input_name(inputs::left) +
                               " * " + input_name(inputs::right)));
}

void ShaderGraph::ExpandVectorNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.emplace_back(make_initializer(m_outputs[0].type_name() +
                                  '(' + input_name(0) + ", " + std::to_string(m_value) + ')'));
}

void ShaderGraph::DecomposeVectorNode::add_code(StagesCode &code, Stage stage) const {
    auto &c = code[stage].code;
    if (!m_outputs[0].connections.empty())
        c.emplace_back(make_initializer(input_name(0) + ".x", 0));
    if (!m_outputs[1].connections.empty())
        c.emplace_back(make_initializer(input_name(0) + ".y", 1));
    if (m_type != DataType::VEC2 && !m_outputs[2].connections.empty())
        c.emplace_back(make_initializer(input_name(0) + ".z", 2));
    if (m_type == DataType::VEC4 && !m_outputs[3].connections.empty())
        c.emplace_back(make_initializer(input_name(0) + ".w", 3));
}

void ShaderGraph::ComposeVectorNode::add_code(StagesCode &code, Stage stage) const {
    auto value = input_name(0) + ", " + input_name(1);
    if (m_type != DataType::VEC2)
        value += ", " + input_name(2);
    if (m_type == DataType::VEC4)
        value += ", " + input_name(3);

    code[stage].code.push_back(make_initializer(DataType::to_string(m_type) + '(' + value + ')'));
}

void ShaderGraph::WorldPositionNode::add_code(StagesCode &code, Stage stage) const {
    auto value = "vec3(" + model_matrix_uniform + " * vec4(vertex_position, 1.))";
    if (stage == Stage::VERTEX)
        code[Stage::VERTEX].code.push_back(make_initializer(value));
    else
        pass_to_fragment(code, 0, value);

}

void ShaderGraph::CustomFunctionNode::add_code(StagesCode &code, Stage stage) const {
    auto function_name = variable_name() + "_func";
    auto definition = m_outputs[0].type_name() + ' ' + function_name + '(';
    for (auto it = m_inputs.begin(); it != m_inputs.end(); ++it) {
        definition += it->type_name() + ' ' + it->definition.name;
        if (it + 1 != m_inputs.end())
            definition += ", ";
    }
    definition += ") {\n" + m_code + "\n}";
    code[stage].decl.emplace_back(definition);

    auto function_call = function_name + '(';
    for (std::size_t i = 0; i < m_inputs.size(); ++i) {
        function_call += input_name(i);
        if (i + 1 < m_inputs.size())
            function_call += ", ";
    }
    function_call.push_back(')');
    code[stage].code.emplace_back(make_initializer(function_call));
}

void ShaderGraph::TimeNode::add_code(StagesCode &code, Stage stage) const {
    auto uniform = stage == Stage::VERTEX ? time_uniform_vert : time_uniform_frag;
    auto decl = "uniform int " + uniform;
    if (std::ranges::find(code[stage].decl, decl) == code[stage].decl.end())
        code[stage].decl.push_back(decl);

    code[stage].code.push_back(make_initializer(uniform + " * " + std::to_string(m_multiplier)));
}

void ShaderGraph::PerlinNoise3DNode::add_code(StagesCode &code, Stage stage) const {
    static const auto declarations = R"(vec3 PerlinNoise3DNode_mod289_3(vec3 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}
vec4 PerlinNoise3DNode_mod289_4(vec4 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}
vec4 PerlinNoise3DNode_permute(vec4 x) {
    return PerlinNoise3DNode_mod289_4(((x * 34.0) + 1.0) * x);
}
vec4 PerlinNoise3DNode_taylorInvSqrt(vec4 r) {
    return 1.79284291400159 - 0.85373472095314 * r;
}
vec3 PerlinNoise3DNode_fade(vec3 t) {
    return t * t * t * (t * (t * 6.0 - 15.0) + 10.0);
}
float PerlinNoise3DNode_cnoise(vec3 P) {
    vec3 Pi0 = floor(P); // Integer part for indexing.
    vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1.
    Pi0 = PerlinNoise3DNode_mod289_3(Pi0);
    Pi1 = PerlinNoise3DNode_mod289_3(Pi1);
    vec3 Pf0 = fract(P); // Fractional part for interpolation.
    vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0.
    vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
    vec4 iy = vec4(Pi0.yy, Pi1.yy);
    vec4 iz0 = vec4(Pi0.z);
    vec4 iz1 = vec4(Pi1.z);

    vec4 ixy = PerlinNoise3DNode_permute(PerlinNoise3DNode_permute(ix) + iy);
    vec4 ixy0 = PerlinNoise3DNode_permute(ixy + iz0);
    vec4 ixy1 = PerlinNoise3DNode_permute(ixy + iz1);

    vec4 gx0 = ixy0 * (1.0 / 7.0);
    vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;
    gx0 = fract(gx0);
    vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
    vec4 sz0 = step(gz0, vec4(0.0));
    gx0 -= sz0 * (step(0.0, gx0) - 0.5);
    gy0 -= sz0 * (step(0.0, gy0) - 0.5);

    vec4 gx1 = ixy1 * (1.0 / 7.0);
    vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;
    gx1 = fract(gx1);
    vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
    vec4 sz1 = step(gz1, vec4(0.0));
    gx1 -= sz1 * (step(0.0, gx1) - 0.5);
    gy1 -= sz1 * (step(0.0, gy1) - 0.5);

    vec3 g000 = vec3(gx0.x, gy0.x, gz0.x);
    vec3 g100 = vec3(gx0.y, gy0.y, gz0.y);
    vec3 g010 = vec3(gx0.z, gy0.z, gz0.z);
    vec3 g110 = vec3(gx0.w, gy0.w, gz0.w);
    vec3 g001 = vec3(gx1.x, gy1.x, gz1.x);
    vec3 g101 = vec3(gx1.y, gy1.y, gz1.y);
    vec3 g011 = vec3(gx1.z, gy1.z, gz1.z);
    vec3 g111 = vec3(gx1.w, gy1.w, gz1.w);

    vec4 norm0 = PerlinNoise3DNode_taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
    g000 *= norm0.x;
    g010 *= norm0.y;
    g100 *= norm0.z;
    g110 *= norm0.w;
    vec4 norm1 = PerlinNoise3DNode_taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
    g001 *= norm1.x;
    g011 *= norm1.y;
    g101 *= norm1.z;
    g111 *= norm1.w;

    float n000 = dot(g000, Pf0);
    float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
    float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
    float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
    float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
    float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
    float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
    float n111 = dot(g111, Pf1);

    vec3 fade_xyz = PerlinNoise3DNode_fade(Pf0);
    vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
    vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
    float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x);
    return 2.2 * n_xyz;
})";

    std::string flag = "// PerlinNoise3DNode declarations";
    if (std::ranges::find(code[stage].decl, flag) == code[stage].decl.end()) {
        code[stage].decl.push_back(flag);
        code[stage].decl.emplace_back(declarations);
    }

    code[stage].code.push_back(make_initializer(
        "PerlinNoise3DNode_cnoise(vec3(("
        + input_name(uv) + " + "
        + input_name(offset) + ") * "
        + input_name(scale) + ", "
        + input_name(time) + ")) * 0.5 + 0.5"));
}

void ShaderGraph::StepNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer("0.f"));
    code[stage].code.emplace_back(
        "if (" + input_name(x) + " > " + input_name(edge) + ")\n"
        "        " + variable_name() + " = 1.f");
}

void ShaderGraph::ConditionNode::add_code(StagesCode &code, Stage stage) const {
    std::string op;
    switch (m_condition) {
    case EQ:
        op = "==";
        break;
    case NE:
        op = "!=";
        break;
    case LE:
        op = "<=";
        break;
    case GE:
        op = ">=";
        break;
    case LT:
        op = "<";
        break;
    case GT:
        op = ">";
        break;
    default:
        UNREACHABLE();
    }

    code[stage].code.push_back(
        m_outputs[0].type_name() + ' ' + variable_name() + ";\n"
        "    if (" + input_name(left) + op + input_name(right) + ")\n"
        "        " + variable_name() + " = " + input_name(true_res) + ";\n"
        "    else\n"
        "        " + variable_name() + " = " + input_name(false_res));
}

void ShaderGraph::SineNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer("sin(" + input_name(0) + ')'));
}

void ShaderGraph::CosineNode::add_code(StagesCode &code, Stage stage) const {
    code[stage].code.push_back(make_initializer("cos(" + input_name(0) + ')'));
}

} // namespace gfx