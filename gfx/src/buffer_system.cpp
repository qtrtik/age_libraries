#include <gfx/buffer_system.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>

namespace gfx {

BufferSystem::BufferSystem()
    : com::Library{ self_name() }
    , m_buffers { nullptr }
    , m_active_buffer { nullptr } 
{}

BufferSystem::~BufferSystem()
{}

void BufferSystem::initialize()
{
    m_buffers = folder()->push_back<com::Folder>("buffers");
    m_active_buffer = m_buffers->push_back<com::Link>("active_buffer.link", nullptr); 
}

void BufferSystem::release()
{
    folder()->erase(buffers());
}

void BufferSystem::set_active_buffer(Buffer* const buffer)
{
    if (active_buffer() != buffer)
    {
        buffer->compile();
        buffer->activate();
        m_active_buffer->set_target(buffer);
    }
}

Buffer* BufferSystem::insert_buffer(std::string const& name, Buffer::PrimitiveType const primitive_type, com::ContextPath const& sub_folders)
{
    com::Folder* const folder = buffers()->push_back_folders(sub_folders);
    ASSUMPTION(folder->is_under(buffers()));
    com::ContextItem* const item = folder->find(name);
    if (item == nullptr)
        return folder->push_back<Buffer>(name, primitive_type);
    Buffer* const buffer = dynamic_cast<Buffer*>(item);
    ASSUMPTION(buffer != nullptr && buffer->primitive_type() == primitive_type);
    return buffer;
}

void BufferSystem::erase_buffer(Buffer* const buffer)
{
    ASSUMPTION(buffer->is_under(buffers()));
    buffer->folder()->erase(buffer);
}

}
