#include <osi/index.hpp>
#include <lfs/resource_handle.hpp>
#include <gfx/text_shader.hpp>
#include <utils/timeprof.hpp>
#include <algorithm>

namespace gfx {

TextShader::TextShader(const std::string &name, lighting l, std::string vertex_shader, std::string fragment_shader)
    : Shader{ name, l }, m_vertex_shader{ std::move(vertex_shader) },
      m_fragment_shader{ std::move(fragment_shader) } {}

void TextShader::compile() {
#ifndef MEASURING_COMPILE
    if (gl_shader() != nullptr)
        return;
    Shader::compile(m_vertex_shader, m_fragment_shader);
#else
    if (folder()->name() != "built-in-shaders" && osi::timer()->passed_rounds() > 0) {
        std::chrono::time_point<std::chrono::steady_clock> start = std::chrono::steady_clock::now();
        Shader::compile(m_vertex_shader, m_fragment_shader);
        std::cout << (std::chrono::steady_clock::now() - start).count() << std::endl;
    } else {
        Shader::compile(m_vertex_shader, m_fragment_shader);
    }
#endif
}

void TextShader::activate() const {
    ASSUMPTION(gl_shader() != nullptr);
    gl_shader()->activate();
    bind_textures();
}

void TextShader::bind_textures() const {
    GLint texture_units;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &texture_units);
    if (static_cast<int>(m_textures.size()) >= texture_units)
        throw std::runtime_error{ "Maximum texture count exceeded. Max: " + std::to_string(texture_units) +
                                  ", Current: " + std::to_string(m_textures.size()) };

    int unit = 0;
    for (auto t : m_textures) {
        t->activate(unit);
        ++unit;
    }
}

void TextShader::on_content_changed(File *sender) {
    auto handle = dynamic_cast<lfs::ResourceHandle *>(sender);
    ASSUMPTION(handle != nullptr);

    switch (handle->state()) {
    case lfs::ResourceHandle::LOADING:
    case lfs::ResourceHandle::DISCARDED:
        break;
    case lfs::ResourceHandle::LOADED: {
        auto it = std::ranges::find_if(m_textures, [&](auto t) { return t->m_handle == handle; });
        ASSUMPTION(it != m_textures.end());
        (*it)->generate_texture();

        if (gl_shader() != nullptr) {
            activate();
        }
        break;
    }
    case lfs::ResourceHandle::ERROR:
        throw std::runtime_error{ "error loading file: " + handle->error_message() };
    default:
        UNREACHABLE();
    }
}

void TextShader::add_texture(ShaderGraph::TextureNode *tn) {
    m_textures.push_back(tn);
    tn->m_shader = this;
    tn->load_texture();
}

} // namespace gfx

