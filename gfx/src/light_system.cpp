#include <gfx/light_system.hpp>
#include <gfx/index.hpp>
#include <com/frame.hpp>
#include <utils/assumptions.hpp>

namespace gfx {

LightSystem::LightSystem()
    : com::Library{ self_name() }
    , m_lights { nullptr }
{}

LightSystem::~LightSystem()
{}

void LightSystem::initialize()
{
    m_lights = folder()->push_back<com::Folder>("lights");
}

void LightSystem::release()
{
    folder()->erase(lights());
}

Light* LightSystem::get_light_props(com::Folder* light) const
{
    return light->find<Light>(light_props_name());
}

com::Frame* LightSystem::get_frame(com::Folder* light) const
{
    return light->find<com::Link>(frame_link_name())->target<com::Frame>();
}

com::Folder* LightSystem::get_shadow_casters(com::Folder* light) const
{
    return light->find<com::Folder>(shadow_casters_folder_name());
}

void LightSystem::erase_light(com::Folder* const light)
{
    ASSUMPTION(light->is_under(lights()));
    light->erase(light_props_name());
    light->erase(frame_link_name());
    light->erase(shadow_casters_folder_name());
    com::Folder::erase_empty(light, lights());
}

void LightSystem::insert_shadow_caster(com::Folder* light, com::Folder* object)
{
    ASSUMPTION(light->is_under(lights()));
    ASSUMPTION(object->is_under(object_system()->objects()));
    ASSUMPTION(!has_shadow_caster(light, object));
    com::Folder* const casters = get_shadow_casters(light);
    ASSUMPTION(casters != nullptr);
    casters->push_back<com::Link>(casters->generate_unique_name(object->name(), ".link"), object);
}

void LightSystem::erase_shadow_caster(com::Folder* light, com::Folder* object)
{
    ASSUMPTION(light->is_under(lights()));
    ASSUMPTION(object->is_under(object_system()->objects()));
    com::Folder* const casters = get_shadow_casters(light);
    ASSUMPTION(casters != nullptr);
    if (object->linkage() != nullptr)
        for (com::Link* link : std::vector<com::Link*>{ object->linkage()->begin(), object->linkage()->end() })
            if (link->folder() == casters)
            {
                casters->erase(link);
                return;
            }
    UNREACHABLE();
}

bool LightSystem::has_shadow_caster(com::Folder* light, com::Folder* object) const
{
    ASSUMPTION(light->is_under(lights()));
    ASSUMPTION(object->is_under(object_system()->objects()));
    com::Folder* const casters = get_shadow_casters(light);
    ASSUMPTION(casters != nullptr);
    if (object->linkage() != nullptr)
        for (com::Link* link : std::vector<com::Link*>{ object->linkage()->begin(), object->linkage()->end() })
            if (link->folder() == casters)
                return true;
    return false;

}

}
