#include <osi/index.hpp>
#include <utils/timeprof.hpp>
#include <com/frame.hpp>
#include <gfx/index.hpp>
#include <gfx/shader.hpp>
#include <sstream>
#include <unordered_set>
#include <utils/development.hpp>

namespace gfx {

void Shader::compile(std::string const &vertex_shader, std::string const &fragment_shader) {
// #ifdef DEBUG
//      std::cout << "\n======= Compiling shader '" << name() << "' =========\n"
//                << "--- Vertex shader ---\n"
//                << vertex_shader << '\n'
//                << "--- Fragment shader ---\n"
//                << fragment_shader << std::endl;
// #endif

    m_gl_shader = std::make_unique<detail::ShaderGL>();
    m_gl_shader->compile(vertex_shader, fragment_shader);
}

std::string wrap_shader(ShaderGraph::DeclAndCode const &code, std::string const &header = "#version 330 core\n") {
    std::stringstream sstr;
    sstr << header << '\n';
    for (std::string const &decl : code.decl)
        sstr << decl << ";\n";
    sstr << "\nvoid main()\n{\n";
    for (std::string const &expr : code.code)
        sstr << "    " << expr << ";\n";
    sstr << "}\n";
    return sstr.str();
}

void ShaderGraph::compile() {
#ifndef MEASURING_COMPILE
    if (gl_shader() != nullptr)
        return;
#else
    if (osi::timer()->passed_rounds() > 0)
    {
        std::chrono::time_point<std::chrono::steady_clock> start = std::chrono::steady_clock::now();
        check_for_cycles();

        StagesCode code;
        Node::visited_t visited;
        m_root->add_code_helper(code, Stage::VERTEX, visited);

        Shader::compile(wrap_shader(code[Stage::VERTEX]), wrap_shader(code[Stage::FRAGMENT]));
        std::cout << (std::chrono::steady_clock::now() - start).count() << std::endl;
    }
    else
#endif
    {
        check_for_cycles();

        StagesCode code;
        Node::visited_t visited;
        m_root->add_code_helper(code, Stage::VERTEX, visited);

        Shader::compile(wrap_shader(code[Stage::VERTEX]), wrap_shader(code[Stage::FRAGMENT]));
    }
}

void ShaderGraph::check_for_cycles() const {
    std::unordered_set<const Node *> visited, in_branch;
    check_for_cycles_rec(m_nodes[0].get(), visited, in_branch);
}

void ShaderGraph::check_for_cycles_rec(const Node *node, std::unordered_set<const Node *> &visited,
                                  std::unordered_set<const Node *> &in_branch) {
    visited.insert(node);
    in_branch.insert(node);

    for (const auto &in : node->m_inputs) {
        if (!in.connected())
            continue;

        const auto n = in.connection.node;
        if (in_branch.contains(n))
            throw std::logic_error{ "A cycle in shader graph detected between nodes: " + node->name() + " and " +
                                    n->name() };

        if (!visited.contains(n))
            check_for_cycles_rec(n, visited, in_branch);
    }

    in_branch.erase(node);
}

} // namespace gfx
