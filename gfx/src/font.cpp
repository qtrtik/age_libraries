# include <osi/index.hpp>
# include <lfs/index.hpp>
# include <gfx/font.hpp>
# include <gfx/shader_graph.hpp>
# include <gfx/material.hpp>
# include <gfx/index.hpp>
# include <boost/property_tree/ptree.hpp>
# include <boost/property_tree/json_parser.hpp>

namespace gfx {
    typedef ShaderGraph Graph;
    typedef ShaderGraph::Node::DataType::ElementType ElementType;

    FontLoader::FontLoader() : com::Folder {self_name()} {}
    FontLoader::~FontLoader() {}

    bool FontLoader::is_font_loaded(std::string const &font_name, size_t font_size) {
        return find<FontProps>(this->font_name(font_name, font_size))->state == FontProps::LOADED;
    }

    bool FontLoader::is_font_loaded(FontProps* props) {
        return props->state == FontProps::LOADED;
    }

    Material* FontLoader::create_material_for_font(std::string const& name, size_t font_size) {
        auto shader = gfx::shader_system()->insert_shader<Graph>(
            { "text_shaders" },
            font_name(name, font_size) + "_shader",
            Graph::UNLIT,
            Graph::PHONG,
            Graph::PARTIAL
        );

        // font should be loaded already
        auto* textureNode = shader->insert<Graph::TextureNode>(com::ContextPath{"age", "texture", "font", font_name(name, font_size) + "_font_atlas.png"}, ElementType::VEC4);
        auto* uvCoordNode = shader->insert<Graph::VaryingTexcoordNode>();
        auto* decomposeNode = shader->insert<Graph::DecomposeVectorNode>(ElementType::VEC4);
        auto* composeNode = shader->insert<Graph::ComposeVectorNode>(ElementType::VEC3);
        auto* colorNode = shader->insert<Graph::VaryingColorNode>();
        auto* multiplyNode = shader->insert<Graph::MultiplyNode>(ElementType::VEC3);

        shader->connect(decomposeNode, 0, textureNode, 0);
        shader->connect(composeNode, 0, decomposeNode, 0);
        shader->connect(composeNode, 1, decomposeNode, 1);
        shader->connect(composeNode, 2, decomposeNode, 2);

        shader->connect(multiplyNode, 0, colorNode, 0);
        shader->connect(multiplyNode, 1, composeNode, 0);

        shader->connect(textureNode, Graph::TextureNode::inputs::tex_coord, uvCoordNode, 0);
        shader->connect(shader->root(), Graph::MasterNodeUnlit::inputs::color, multiplyNode, 0);

        auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
        shader->connect(shader->root(), Graph::MasterNodeUnlit::inputs::emission, emission, 0);
        shader->connect(shader->root(), Graph::MasterNodeUnlit::inputs::alpha, decomposeNode, 3);
        return gfx::material_system()->insert_material(font_name(name, font_size) + "_material", shader, {"font_materials"});
    }

    Material * FontLoader::get_material_for_font(std::string const &name, size_t const font_size) {
        auto* material = material_system()->materials()->locate<Material>({"font_materials", font_name(name, font_size) + "_material"});
        if (material)
            return material;
        return create_material_for_font(name, font_size);
    }

    void FontLoader::on_content_changed(com::File* const sender) {
        if (auto* props = dynamic_cast<FontProps*>(sender)) {
            notify_font_requests(props->name());
            props->unregister_on_content_changed(this);
        }
    }

    void FontLoader::notify_font_requests(std::string const &name) const {
        for (UiElement* element : m_font_requests.at(name)) {
            element->on_font_loaded(name);
        }
    }

    FontProps * FontLoader::load_font_for_element(UiElement *element, std::string const &f_name, size_t font_size) {
        std::string const& name = font_name(f_name, font_size);

        auto* font_props = find<FontProps>(name);

        if (!font_props) {
            font_props = push_back<FontProps>(f_name, font_size);
            font_props->register_on_content_changed(this);
        }


        if (!is_font_loaded(f_name, font_size)) {
            m_font_requests[name].emplace_back(element);
        } else {
            element->on_font_loaded(name);
        }

        return font_props;
    }

    FontProps::FontProps(std::string const&name, size_t font_size): com::File(FontLoader::font_name(name, font_size)), m_font_name(name), m_font_size(font_size) {}
    FontProps::~FontProps() {}

    void FontProps::initialize() {
        com::ContextPath data_path = {osi::data()->dir().string(), "age", "font"};
        data_path.push_back(FontLoader::font_properties_file_name(m_font_name, m_font_size));

        lfs::ResourceHandle* handle = lfs::loader()->load(data_path);

        if (handle->state() == lfs::ResourceHandle::LOADED) {
            on_content_changed(handle);
        } else {
            handle->register_on_content_changed(this);
        }
    }

    void FontProps::on_content_changed(File* sender) {
        auto fontResource = dynamic_cast<lfs::ResourceHandle*>(sender);
        ASSUMPTION(fontResource != nullptr);

        const std::vector<uint8_t>& fontData = fontResource->data();

        fontResource->unregister_on_content_changed(this);
        fontResource->set_discardable(true);

        try {
            std::string jsonString(fontData.begin(), fontData.end());
            std::istringstream jsonStream(jsonString);
            boost::property_tree::ptree pt;
            boost::property_tree::read_json(jsonStream, pt);

            atlas_width = pt.get<float>("atlas_width");
            atlas_height = pt.get<float>("atlas_height");
            m_font_size = pt.get<size_t>("font_size");
            max_height = pt.get<size_t>("max_height");
            num_glyphs = pt.get<size_t>("num_glyphs");

            for (const auto& item : pt.get_child("character_props")) {
                auto char_iter = item.second.begin();
                uint32_t char_code = char_iter->second.get_value<uint32_t>();
                ++char_iter;
                const auto& char_data = char_iter->second;

                Character character;
                character.advance = char_data.get<float>("advance");

                auto bearing_iter = char_data.get_child("bearing").begin();
                character.bearing = {
                    bearing_iter->second.get_value<float>(),
                    (++bearing_iter)->second.get_value<float>()
                };

                character.offset_x = char_data.get<float>("offset_x");

                auto size_iter = char_data.get_child("size").begin();
                character.size = {
                    size_iter->second.get_value<float>(),
                    (++size_iter)->second.get_value<float>()
                };

                character_props[char_code] = character;
            }

            state = LOADED;
        } catch (const boost::property_tree::json_parser_error& e) {
            state = ERROR;
        }

        notify_content_changed();
    }

}