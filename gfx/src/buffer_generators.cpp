#include <gfx/buffer_generators.hpp>
#include <gfx/index.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>
#include <utils/config.hpp>
#include <osi/index.hpp>
#if COMPILER() == COMPILER_CLANG() || COMPILER() == COMPILER_GCC()
#   define cosf cos
#   define sinf sin
#endif

namespace gfx {
BufferGenerators::BufferGenerators()
    : com::Library{ self_name() }
{}

BufferGenerators::~BufferGenerators()
{}

void BufferGenerators::initialize()
{}

void BufferGenerators::release()
{}

Buffer *BufferGenerators::insert_empty() {
    Buffer* const buffer = buffer_system()->insert_buffer("empty", Buffer::LINE, {  });
    return buffer;
}

Buffer *BufferGenerators::insert_empty_triangle(std::string const& name, com::ContextPath const& subfolders) {
    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::TRIANGLE, subfolders);
    return buffer;
}

Buffer* BufferGenerators::insert_procedural_basis()
{
    Buffer* const buffer = buffer_system()->insert_buffer("basis", Buffer::LINE, { "procedural" });
    buffer->insert_positions({
        { 0, 0, 0 }, { 1, 0, 0 },
        { 0, 0, 0 }, { 0, 1, 0 },
        { 0, 0, 0 }, { 0, 0, 1 }
        });
    buffer->insert_colors({
        { 1, 0, 0 }, { 1, 0, 0 },
        { 0, 1, 0 }, { 0, 1, 0 },
        { 0, 0, 1 }, { 0, 0, 1 }
        });
    return buffer;
}

Buffer* BufferGenerators::insert_procedural_grid()
{
    Buffer::PositionsVec vertices;
    Buffer::ColorsVec colors;
    {
        scalar const half_size { 50.0f };
        vec3 const dark_gray { 0.4f, 0.4f, 0.4f };
        vec3 const light_gray { 0.5f, 0.5f, 0.5f };
        vec3 const red { 1.0f, 0.0f, 0.0f };
        vec3 const green { 0.0f, 1.0f, 0.0f };
        vec3 const blue { 0.0f, 0.0f, 1.0f };

        int num_steps = 1U;
        for (scalar s = 1.0f; s < half_size + 0.5f; s += 1.0f, ++num_steps)
            if (num_steps % 10 != 0)
                for (scalar coord : { -s, s })
                {
                    vertices.push_back({ -half_size, coord, 0.0f });
                    vertices.push_back({  half_size, coord, 0.0f });
                    vertices.push_back({ coord, -half_size, 0.0f });
                    vertices.push_back({ coord,  half_size, 0.0f });
                }
        while (colors.size() < vertices.size())
            colors.push_back(dark_gray);

        for (scalar s = 10.0f; s < half_size + 0.5f; s += 10.0f)
            for (scalar coord : { -s, s })
            {
                vertices.push_back({ -half_size, coord, 0.0f });
                vertices.push_back({  half_size, coord, 0.0f });
                vertices.push_back({ coord, -half_size, 0.0f });
                vertices.push_back({ coord,  half_size, 0.0f });
            }
        while (colors.size() < vertices.size())
            colors.push_back(light_gray);

        vertices.push_back({ -half_size, 0.0f, 0.0f });
        vertices.push_back({  half_size, 0.0f, 0.0f });
        colors.push_back(red);
        colors.push_back(red);

        vertices.push_back({ 0.0f, -half_size, 0.0f });
        vertices.push_back({ 0.0f,  half_size, 0.0f });
        colors.push_back(green);
        colors.push_back(green);

        vertices.push_back({ 0.0f, 0.0f, -half_size });
        vertices.push_back({ 0.0f, 0.0f,  half_size });
        colors.push_back(blue);
        colors.push_back(blue);

        vertices.push_back({ 1.0f, 0.0f, 0.0f });
        vertices.push_back({ 0.0f, 1.0f, 0.0f });
        colors.push_back(red);
        colors.push_back(green);

        vertices.push_back({ 0.0f, 1.0f, 0.0f });
        vertices.push_back({ 0.0f, 0.0f, 1.0f });
        colors.push_back(green);
        colors.push_back(blue);

        vertices.push_back({ 0.0f, 0.0f, 1.0f });
        vertices.push_back({ 1.0f, 0.0f, 0.0f });
        colors.push_back(blue);
        colors.push_back(red);
    }

    Buffer* const buffer = buffer_system()->insert_buffer("grid", Buffer::LINE, { "procedural" });
    buffer->insert_positions(vertices);
    buffer->insert_colors(colors);
    return buffer;
}

Buffer* BufferGenerators::insert_procedural_rectangle_variable(
    vec2 const& half_sizes_along_axes,
    vec2 const& half_sizes_along_inverted_axes,
    std::string const& name,
    com::ContextPath const& sub_folders
)
{
    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::TRIANGLE, sub_folders);
    buffer->insert_indices({
        0, 1, 2,
        0, 2, 3
        });
    buffer->insert_positions({
        { -get<0>(half_sizes_along_inverted_axes), -get<1>(half_sizes_along_inverted_axes), 0 },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_inverted_axes), 0 },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), 0 },
        { -get<0>(half_sizes_along_inverted_axes),  get<1>(half_sizes_along_axes), 0 },
        });
    buffer->insert_normals({
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        });
    buffer->insert_texcoords({
        { 0, 0 },
        { 1, 0 },
        { 1, 1 },
        { 0, 1 },
        });
    return buffer;

}

Buffer* BufferGenerators::insert_procedural_rectangle_variable_wireframe(
    vec2 const& half_sizes_along_axes,
    vec2 const& half_sizes_along_inverted_axes,
    std::string const& name,
    com::ContextPath const& sub_folders
)
{
    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::LINE, sub_folders);
    buffer->insert_indices({
        0, 1,
        1, 2,
        2, 3,
        3, 0
        });
    buffer->insert_positions({
        { -get<0>(half_sizes_along_inverted_axes), -get<1>(half_sizes_along_inverted_axes), 0 },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_inverted_axes), 0 },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), 0 },
        { -get<0>(half_sizes_along_inverted_axes),  get<1>(half_sizes_along_axes), 0 },
        });
    buffer->insert_normals({
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        });
    buffer->insert_texcoords({
        { 0, 0 },
        { 1, 0 },
        { 1, 1 },
        { 0, 1 },
        });
    return buffer;
}

Buffer* BufferGenerators::insert_procedural_rectangle(
    vec2 const& half_sizes_along_axes,
    std::string const& name,
    com::ContextPath const& sub_folders
)
{
    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::TRIANGLE, sub_folders);
    buffer->insert_indices({
        0, 1, 2,
        0, 2, 3
        });
    buffer->insert_positions({
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), 0 },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), 0 },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), 0 },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), 0 },
        });
    buffer->insert_normals({
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        });
    buffer->insert_texcoords({
        { 0, 0 },
        { 1, 0 },
        { 1, 1 },
        { 0, 1 },
        });
    return buffer;
}


Buffer* BufferGenerators::insert_procedural_rectangle_wireframe(
    vec2 const& half_sizes_along_axes,
    std::string const& name,
    com::ContextPath const& sub_folders
) 
{
    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::LINE, sub_folders);
    buffer->insert_indices({
        0, 1,
        1, 2,
        2, 3,
        3, 0
        });
    buffer->insert_positions({
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), 0 },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), 0 },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), 0 },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), 0 },
        });
    buffer->insert_normals({
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        { 0, 0, 1 },
        });
    buffer->insert_texcoords({
        { 0, 0 },
        { 1, 0 },
        { 1, 1 },
        { 0, 1 },
        });
    return buffer;
}

Buffer* BufferGenerators::insert_procedural_box_solid(
    vec3 const& half_sizes_along_axes,
    std::string const& name,
    com::ContextPath const& sub_folders
    )
{
    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::TRIANGLE, sub_folders);
    buffer->insert_positions({
        // face xy, z-min
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },    // 0
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },

        // face xy, z-max
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },    // 4
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },

        // face xz, y-min
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },    // 8
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },

        // face xz, y-max
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },    // 12
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },

        // face yz, x-min
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },    // 16
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },

        // face yz, x-max
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },    // 20
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
    });
    buffer->insert_normals({
        // face xy, z-min
        {  0.0f,  0.0f, -1.0f, },   // 0
        {  0.0f,  0.0f, -1.0f, },
        {  0.0f,  0.0f, -1.0f, },
        {  0.0f,  0.0f, -1.0f, },

        // face xy, z-max
        {  0.0f,  0.0f,  1.0f, },   // 4
        {  0.0f,  0.0f,  1.0f, },
        {  0.0f,  0.0f,  1.0f, },
        {  0.0f,  0.0f,  1.0f, },

        // face xz, y-min
        {  0.0f, -1.0f,  0.0f, },   // 8
        {  0.0f, -1.0f,  0.0f, },
        {  0.0f, -1.0f,  0.0f, },
        {  0.0f, -1.0f,  0.0f, },

        // face xz, y-max
        {  0.0f,  1.0f,  0.0f, },   // 12
        {  0.0f,  1.0f,  0.0f, },
        {  0.0f,  1.0f,  0.0f, },
        {  0.0f,  1.0f,  0.0f, },

        // face yz, x-min
        { -1.0f,  0.0f,  0.0f, },   // 16
        { -1.0f,  0.0f,  0.0f, },
        { -1.0f,  0.0f,  0.0f, },
        { -1.0f,  0.0f,  0.0f, },

        // face yz, x-max
        {  1.0f,  0.0f,  0.0f, },   // 20
        {  1.0f,  0.0f,  0.0f, },
        {  1.0f,  0.0f,  0.0f, },
        {  1.0f,  0.0f,  0.0f, },
    });
    buffer->insert_tangents({
        // face xy, z-min
        {  0.0f,  1.0f, 0.0f, },   // 0
        {  0.0f,  1.0f, 0.0f, },
        {  0.0f,  1.0f, 0.0f, },
        {  0.0f,  1.0f, 0.0f, },

        // face xy, z-max
        {  0.0f,  1.0f,  0.0f, },   // 4
        {  0.0f,  1.0f,  0.0f, },
        {  0.0f,  1.0f,  0.0f, },
        {  0.0f,  1.0f,  0.0f, },

        // face xz, y-min
        {  1.0f, 0.0f,  0.0f, },   // 8
        {  1.0f, 0.0f,  0.0f, },
        {  1.0f, 0.0f,  0.0f, },
        {  1.0f, 0.0f,  0.0f, },

        // face xz, y-max
        {  -1.0f,  0.0f,  0.0f, },   // 12
        {  -1.0f,  0.0f,  0.0f, },
        {  -1.0f,  0.0f,  0.0f, },
        {  -1.0f,  0.0f,  0.0f, },

        // face yz, x-min
        { 0.0f,  -1.0f,  0.0f, },   // 16
        { 0.0f,  -1.0f,  0.0f, },
        { 0.0f,  -1.0f,  0.0f, },
        { 0.0f,  -1.0f,  0.0f, },

        // face yz, x-max
        { 0.0f,  1.0f,  0.0f, },   // 20
        { 0.0f,  1.0f,  0.0f, },
        { 0.0f,  1.0f,  0.0f, },
        { 0.0f,  1.0f,  0.0f, },
    });
    buffer->insert_texcoords({
        // face xy, z-min
        { 0.0f,  1.0f },
        { 0.0f,  0.0f },
        { 1.0f,  0.0f },
        { 1.0f,  1.0f },

        // face xy, z-max
        { 0.0f,  0.0f },
        { 0.0f,  1.0f },
        { 1.0f,  1.0f },
        { 1.0f,  0.0f },

        // face xz, y-min
        { 0.0f,  1.0f },
        { 1.0f,  1.0f },
        { 1.0f,  0.0f },
        { 0.0f,  0.0f },

        // face xz, y-max
        { 1.0f,  1.0f },
        { 0.0f,  1.0f },
        { 0.0f,  0.0f },
        { 1.0f,  0.0f },

        // face yz, x-min
        { 1.0f,  1.0f },
        { 0.0f,  1.0f },
        { 0.0f,  0.0f },
        { 1.0f,  0.0f },

        // face yz, x-max
        { 0.0f,  1.0f },
        { 1.0f,  1.0f },
        { 1.0f,  0.0f },
        { 0.0f,  0.0f },
    });
    buffer->insert_indices({
        // face xy, z-min
        0U, 3U, 2U,  0U, 2U, 1U,
        // face xy, z-max
        4U, 5U, 6U,  4U, 6U, 7U,
        // face xz, y-min
        8U, 9U, 10U,  8U, 10U, 11U,
        // face xz, y-max
        12U, 15U, 14U,  12U, 14U, 13U,
        // face yz, x-min
        16U, 19U, 18U,  16U, 18U, 17U,
        // face yz, x-max
        20U, 21U, 22U,  20U, 22U, 23U,
    });

    return buffer;
}

Buffer* BufferGenerators::insert_procedural_box_wireframe(
    vec3 const& half_sizes_along_axes,
    std::string const& name,
    com::ContextPath const& sub_folders
    )
{
    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::LINE, sub_folders);
    buffer->insert_positions({
        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) },

        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) }, {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) }, {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) }, { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) }, { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },

        { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, { -get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, {  get<0>(half_sizes_along_axes), -get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, {  get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
        { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes), -get<2>(half_sizes_along_axes) }, { -get<0>(half_sizes_along_axes),  get<1>(half_sizes_along_axes),  get<2>(half_sizes_along_axes) },
    });
    return buffer;
}

Buffer* BufferGenerators::insert_procedural_sphere_solid(
    scalar const radius,
    std::uint8_t const num_lines_per_quarter_of_circle,
    std::string const& name,
    com::ContextPath const& sub_folders
    )
{
    ASSUMPTION(radius > 1e-4f);
    ASSUMPTION(num_lines_per_quarter_of_circle != 0U);

    std::uint16_t const num_vertices_in_horisontal_slice = num_lines_per_quarter_of_circle * 4U;
    std::uint16_t const num_horisontal_slices = 2U * (num_lines_per_quarter_of_circle - 1U) + 1U;

    std::uint16_t const num_vertices = num_horisontal_slices * num_vertices_in_horisontal_slice + 2U;

    std::uint16_t const num_triangles = 2U * (num_horisontal_slices - 1U) * num_vertices_in_horisontal_slice +
                                        2U * num_vertices_in_horisontal_slice;

    std::uint16_t const num_indices = 3U * num_triangles;

    Buffer::PositionsVec vertices;
    Buffer::NormalsVec normals;
    {
        vertices.reserve(num_vertices);
        normals.reserve(num_vertices);
        scalar const  delta_phi = (Num::pi / 2.0f) / static_cast<scalar>(num_lines_per_quarter_of_circle);
        for (std::int32_t  i = -(std::int32_t)(num_lines_per_quarter_of_circle - 1U); i < num_lines_per_quarter_of_circle; ++i)
        {
            scalar const  psi = static_cast<scalar>(i)* delta_phi;
            scalar const  cos_psi = std::cosf(psi);
            scalar const  sin_psi = std::sinf(psi);
            for (std::int32_t  j = 0U; j < num_vertices_in_horisontal_slice; ++j)
            {
                scalar const  phi = static_cast<scalar>(j)* delta_phi;
                scalar const  cos_phi = std::cosf(phi);
                scalar const  sin_phi = std::sinf(phi);
                vec3 const  v( cos_phi * cos_psi, sin_phi * cos_psi, sin_psi);
                vertices.push_back(radius * v);
                normals.push_back(v);
            }
        }
        vertices.push_back({ 0.0f, 0.0f, -radius });
        normals.push_back({ 0.0f, 0.0f, -1.0f });
        vertices.push_back({ 0.0f, 0.0f, radius });
        normals.push_back({ 0.0f, 0.0f, 1.0f });
        INVARIANT(vertices.size() == num_vertices && normals.size() == num_vertices);
    }

    Buffer::IndicesVec indices;
    {
        indices.reserve(num_indices);
        for (std::uint16_t i = num_vertices_in_horisontal_slice - 1U, j = 0U; j < num_vertices_in_horisontal_slice; i = j, ++j)
        {
            indices.push_back(j);
            indices.push_back(i);
            indices.push_back(num_vertices - 2U );
        }
        for (std::uint16_t k = 0U; k + 1U < num_horisontal_slices; ++k)
            for (std::uint16_t  i = num_vertices_in_horisontal_slice - 1U, j = 0U; j < num_vertices_in_horisontal_slice; i = j, ++j)
            {
                std::uint32_t const  lo_shift = k * num_vertices_in_horisontal_slice;
                std::uint32_t const  hi_shift = lo_shift + num_vertices_in_horisontal_slice;
                indices.push_back(lo_shift + i);
                indices.push_back(lo_shift + j);
                indices.push_back(hi_shift + j);

                indices.push_back(lo_shift + i);
                indices.push_back(hi_shift + j);
                indices.push_back(hi_shift + i);
            }
        std::uint32_t const  shift = num_vertices - 2U - num_vertices_in_horisontal_slice;
        for (std::uint16_t i = num_vertices_in_horisontal_slice - 1U, j = 0U; j < num_vertices_in_horisontal_slice; i = j, ++j)
        {
            indices.push_back(shift + i);
            indices.push_back(shift + j);
            indices.push_back(num_vertices - 1U);
        }
        INVARIANT(indices.size() == num_indices);
    }

    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::TRIANGLE, sub_folders);
    buffer->insert_positions(vertices);
    buffer->insert_normals(normals);
    buffer->insert_indices(indices);

    return buffer;
}

Buffer* BufferGenerators::insert_procedural_sphere_wireframe(
    scalar const radius,
    std::uint8_t const num_lines_per_quarter_of_circle,
    std::string const& name,
    com::ContextPath const& sub_folders
    )
{
    ASSUMPTION(radius > 1e-4f);
    ASSUMPTION(num_lines_per_quarter_of_circle != 0U);

    Buffer::PositionsVec vertices;
    vertices.reserve(2U * 3U * 4U * static_cast<std::uint16_t>(num_lines_per_quarter_of_circle));

    scalar const delta_phi = (Num::pi / 2.0f) / static_cast<scalar>(num_lines_per_quarter_of_circle);
    vertices.push_back({ radius, 0.0f, 0.0f });
    for (std::uint16_t i = 1U; i != num_lines_per_quarter_of_circle; ++i)
    {
        scalar const phi = static_cast<scalar>(i) * delta_phi;
        vertices.push_back({ radius * std::cosf(phi), radius * std::sinf(phi), 0.0f });
        vertices.push_back(vertices.back());
    }
    vertices.push_back({ 0.0f, radius, 0.0f });

    for (std::size_t i = 0U, n = vertices.size(); i != n; ++i)
    {
        vec3 const& v = vertices.at(i);
        vertices.push_back({ -get<0>(v), get<1>(v), 0.0f });
    }
    for (std::size_t i = 0U, n = vertices.size(); i != n; ++i)
    {
        vec3 const& v = vertices.at(i);
        vertices.push_back({ get<0>(v), -get<1>(v), 0.0f });
    }

    std::size_t const  num_vertices_per_ring = vertices.size();
    INVARIANT(num_vertices_per_ring == 2UL * 4UL * num_lines_per_quarter_of_circle);

    for (std::size_t i = 0U; i != num_vertices_per_ring; ++i)
    {
        vec3 const& v = vertices.at(i);
        vertices.push_back({ get<0>(v), 0.0f, get<1>(v) });
    }

    for (std::size_t i = 0U; i != num_vertices_per_ring; ++i)
    {
        vec3 const& v = vertices.at(i);
        vertices.push_back({ 0.0f, get<0>(v), get<1>(v) });
    }

    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::LINE, sub_folders);
    buffer->insert_positions(vertices);

    return buffer;
}

Buffer* BufferGenerators::insert_procedural_capsule_solid(
    scalar const half_distance_between_end_points,
    scalar const thickness_from_central_line,
    std::uint8_t const num_lines_per_quarter_of_circle,
    std::string const& name,
    com::ContextPath const& sub_folders
    )
{
    ASSUMPTION(half_distance_between_end_points > 1e-4f);
    ASSUMPTION(thickness_from_central_line > 1e-4f);
    ASSUMPTION(num_lines_per_quarter_of_circle != 0U);

    Buffer::PositionsVec vertices;
    Buffer::NormalsVec normals;

    auto const push_back_triangle = [&vertices, &normals](
        vec3 const& A, vec3 const& B, vec3 const& C, vec3 const& n) -> void {
        vertices.push_back(A); normals.push_back(n);
        vertices.push_back(B); normals.push_back(n);
        vertices.push_back(C); normals.push_back(n);
    };

    scalar const delta_phi = (Num::pi / 2.0f) / static_cast<scalar>(num_lines_per_quarter_of_circle);

    std::uint32_t const num_horisontal_steps = 4U * num_lines_per_quarter_of_circle;
    std::uint32_t const num_vertical_steps = 2U * num_lines_per_quarter_of_circle;
    for (std::uint32_t i = 0U; i < num_horisontal_steps; ++i)
    {
        scalar const cFI1 = std::cosf(i * delta_phi);
        scalar const sFI1 = std::sinf(i * delta_phi);

        scalar const cFI2 = std::cosf((i + 1U) * delta_phi);
        scalar const sFI2 = std::sinf((i + 1U) * delta_phi);

        for (std::uint8_t j = 0U; j < num_vertical_steps; ++j)
        {
            scalar const cPSI1 = std::cosf(j * delta_phi - Num::pi / 2.0f);
            scalar const sPSI1 = std::sinf(j * delta_phi - Num::pi / 2.0f);

            scalar const cPSI2 = std::cosf((j + 1U) * delta_phi - Num::pi / 2.0f);
            scalar const sPSI2 = std::sinf((j + 1U) * delta_phi - Num::pi / 2.0f);

            vec3 const w[4] = {
                { cFI1 * cPSI1, sFI1 * cPSI1, sPSI1 },
                { cFI1 * cPSI2, sFI1 * cPSI2, sPSI2 },
                { cFI2 * cPSI1, sFI2 * cPSI1, sPSI1 },
                { cFI2 * cPSI2, sFI2 * cPSI2, sPSI2 },
            };
            vec3 const n = normalized(w[0] + w[1] + w[2] + w[3]);
            vec3 const shift_z =
                (j < num_lines_per_quarter_of_circle ? -1.0f : 1.0f) * half_distance_between_end_points * axis<vec3, 2>();
            vec3 const v[4] = {
                thickness_from_central_line * w[0] + shift_z,
                thickness_from_central_line * w[1] + shift_z,
                thickness_from_central_line * w[2] + shift_z,
                thickness_from_central_line * w[3] + shift_z
            };

            if (j == 0U)
                push_back_triangle(v[2], v[3], v[1], n);
            else if (j == num_vertical_steps - 1U)
                push_back_triangle(v[0], v[2], v[1], n);
            else
            {
                push_back_triangle(v[0], v[2], v[1], n);
                push_back_triangle(v[2], v[3], v[1], n);
            }
        }
    }
    for (std::uint8_t i = 0U; i < num_horisontal_steps; ++i)
    {
        scalar const cFI1 = std::cosf(i * delta_phi);
        scalar const sFI1 = std::sinf(i * delta_phi);

        scalar const cFI2 = std::cosf((i + 1U) * delta_phi);
        scalar const sFI2 = std::sinf((i + 1U) * delta_phi);

        vec3 const w[2] = { { cFI1, sFI1, 0.0f }, { cFI2, sFI2, 0.0f } };
        vec3 const n = normalized(w[0] + w[1]);
        vec3 const v[4] = {
            thickness_from_central_line * w[0] - half_distance_between_end_points * axis<vec3, 2>(),
            thickness_from_central_line * w[0] + half_distance_between_end_points * axis<vec3, 2>(),
            thickness_from_central_line * w[1] - half_distance_between_end_points * axis<vec3, 2>(),
            thickness_from_central_line * w[1] + half_distance_between_end_points * axis<vec3, 2>(),
        };

        push_back_triangle(v[0], v[2], v[1], n);
        push_back_triangle(v[2], v[3], v[1], n);
    }

    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::TRIANGLE, sub_folders);
    buffer->insert_positions(vertices);
    buffer->insert_normals(normals);

    return buffer;
}

Buffer* BufferGenerators::insert_procedural_capsule_wireframe(
    scalar const half_distance_between_end_points,
    scalar const thickness_from_central_line,
    std::uint8_t const num_lines_per_quarter_of_circle,
    std::string const& name,
    com::ContextPath const& sub_folders
    )
{
    ASSUMPTION(half_distance_between_end_points > 1e-4f);
    ASSUMPTION(thickness_from_central_line > 1e-4f);
    ASSUMPTION(num_lines_per_quarter_of_circle != 0U);

    Buffer::PositionsVec vertices;
    vertices.reserve(2U * ((3U + 2U) * 4U * static_cast<std::uint16_t>(num_lines_per_quarter_of_circle) + 4U));

    scalar const  delta_phi = (Num::pi / 2.0f) / static_cast<scalar>(num_lines_per_quarter_of_circle);
    vertices.push_back({ thickness_from_central_line, 0.0f, 0.0f });
    for (std::uint16_t  i = 1U; i != num_lines_per_quarter_of_circle; ++i)
    {
        scalar const  phi = static_cast<scalar>(i) * delta_phi;
        vertices.push_back({ thickness_from_central_line * std::cosf(phi), thickness_from_central_line * std::sinf(phi), 0.0f });
        vertices.push_back(vertices.back());
    }
    vertices.push_back({ 0.0f, thickness_from_central_line, 0.0f });

    std::size_t const  num_vertices_per_quarter_ring = vertices.size();

    for (std::size_t  i = 0U, n = vertices.size(); i != n; ++i)
    {
        vec3 const&  v = vertices.at(i);
        vertices.push_back({ -get<0>(v), get<1>(v), 0.0f });
    }

    std::size_t const  num_vertices_per_half_ring = vertices.size();

    for (std::size_t  i = 0U, n = vertices.size(); i != n; ++i)
    {
        vec3 const&  v =  vertices.at(i);
        vertices.push_back({ get<0>(v), -get<1>(v), 0.0f });
    }

    std::size_t const  num_vertices_per_ring = vertices.size();
    INVARIANT(num_vertices_per_ring == 2UL * 4UL * num_lines_per_quarter_of_circle);

    for (std::size_t i = 0U; i != num_vertices_per_ring; ++i)
    {
        vec3 const&  v = vertices.at(i);
        vertices.push_back({ get<0>(v), get<1>(v), half_distance_between_end_points });
    }

    for (std::size_t i = 0U; i != num_vertices_per_ring; ++i)
    {
        vec3 const&  v = vertices.at(i);
        vertices.push_back({ get<0>(v), get<1>(v), -half_distance_between_end_points });
    }

    {
        vec3 const&  v = vertices.at(0U);
        vertices.push_back({  get<0>(v), get<1>(v),  half_distance_between_end_points });
        vertices.push_back({  get<0>(v), get<1>(v), -half_distance_between_end_points });
        vertices.push_back({ -get<0>(v), get<1>(v),  half_distance_between_end_points });
        vertices.push_back({ -get<0>(v), get<1>(v), -half_distance_between_end_points });
    }

    {
        vec3 const&  v = vertices.at(num_vertices_per_quarter_ring - 1UL);
        vertices.push_back({ get<0>(v),  get<1>(v),  half_distance_between_end_points });
        vertices.push_back({ get<0>(v),  get<1>(v), -half_distance_between_end_points });
        vertices.push_back({ get<0>(v), -get<1>(v),  half_distance_between_end_points });
        vertices.push_back({ get<0>(v), -get<1>(v), -half_distance_between_end_points });
    }

    for (std::size_t  i = 0U; i != num_vertices_per_half_ring; ++i)
    {
        vec3 const&  v = vertices.at(i);
        vertices.push_back({ get<0>(v), 0.0f, get<1>(v) + half_distance_between_end_points });
    }

    for (std::size_t  i = 0U; i != num_vertices_per_half_ring; ++i)
    {
        vec3 const&  v = vertices.at(i);
        vertices.push_back({ get<0>(v), 0.0f, -get<1>(v) - half_distance_between_end_points });
    }

    for (std::size_t  i = 0U; i != num_vertices_per_half_ring; ++i)
    {
        vec3 const&  v = vertices.at(i);
        vertices.push_back({ 0.0f, get<0>(v), get<1>(v) + half_distance_between_end_points });
    }

    for (std::size_t  i = 0U; i != num_vertices_per_half_ring; ++i)
    {
        vec3 const&  v = vertices.at(i);
        vertices.push_back({ 0.0f, get<0>(v), -get<1>(v) - half_distance_between_end_points });
    }

    Buffer* const buffer = buffer_system()->insert_buffer(name, Buffer::LINE, sub_folders);
    buffer->insert_positions(vertices);

    return buffer;
}

Buffer* BufferGenerators::create_text_buffer(
    const std::string& text,
    FontProps& atlas_props,
    CursorProps& cursor_props,
    size_t max_width,
    const std::string& id,
    vec3 color,
    bool text_wrap
)
{
    Buffer* buffer = buffer_system()->insert_buffer((id.empty() ? text : id), Buffer::TRIANGLE, {"text_buffers"});

    Buffer::PositionsVec positions;
    Buffer::TexcoordsVec texcoords;
    Buffer::ColorsVec colors;
    positions.reserve(text.size() * 6U);
    texcoords.reserve(text.size() * 6U);
    colors.reserve(text.size() * 6U);

    this->populate_buffer_vectors(text, atlas_props, cursor_props, positions, texcoords, colors, max_width, color, text_wrap);

    buffer->insert_positions(positions);
    buffer->insert_texcoords(texcoords);
    buffer->insert_colors(colors);

    return buffer;
}

void BufferGenerators::populate_buffer_vectors(
    const std::string& text,
    FontProps& atlas_props,
    CursorProps& cursor_props,
    Buffer::PositionsVec& positions,
    Buffer::TexcoordsVec& texcoords,
    Buffer::ColorsVec& colors,
    size_t max_width,
    vec3 color,
    bool text_wrap
)
{
    vec2 pixel_size = osi::window()->pixel_size_in_meters();
    scalar pixel_width = pixel_size.x;
    scalar pixel_height = pixel_size.y;
    scalar minimal_advance = (atlas_props.m_font_size / 4.8) * pixel_width;
    scalar max_width_normalized = max_width * pixel_width;
    scalar red_color = color.r / 255.f;
    scalar green_color = color.g / 255.f;
    scalar blue_color = color.b / 255.f;

    for (uint32_t c : text) {
        if ((c < 0x0020 || c > 0x017F) && c != 0x0020) continue;

        Character character;
        if (atlas_props.character_props.empty())
            character = Character();
        else if (c == 0x0020)
            character = Character();
        else
            character = atlas_props.character_props.at(c);

        scalar advance = std::max(minimal_advance, character.advance * pixel_width);
        scalar new_cursorX = cursor_props.cursorX + advance;

        // Advance to new line if the character doesn't fit in the line
        if (text_wrap && new_cursorX > max_width_normalized) {
            cursor_props.cursorY -= atlas_props.m_font_size * pixel_height;
            cursor_props.cursorX = 0.0f;
        }

        scalar char_size_u = character.size.x / atlas_props.atlas_width;
        scalar char_offset_u = character.offset_x / atlas_props.atlas_width;
        scalar char_size_v = character.size.y / atlas_props.atlas_height;

        scalar char_bearing_x = character.bearing.x * pixel_width;
        scalar char_bearing_y = character.bearing.y * pixel_height;

        scalar char_size_x = character.size.x * pixel_width;
        scalar char_size_y = character.size.y * pixel_height;

        vec2 lo_uv = {char_offset_u, 0.0f};
        vec2 hi_uv = {char_offset_u + char_size_u, char_size_v};

        vec2 lo_xy = {cursor_props.cursorX + char_bearing_x, cursor_props.cursorY - (char_size_y - char_bearing_y)};
        vec2 hi_xy = {lo_xy.x + char_size_x, lo_xy.y + char_size_y};
        lo_xy.y -= atlas_props.max_height * pixel_height;
        hi_xy.y -= atlas_props.max_height * pixel_height;

        positions.push_back({lo_xy.x, lo_xy.y, 0.0f});
        texcoords.push_back({lo_uv.x, lo_uv.y});

        positions.push_back({hi_xy.x, lo_xy.y, 0.0f});
        texcoords.push_back({hi_uv.x, lo_uv.y});

        positions.push_back({hi_xy.x, hi_xy.y, 0.0f});
        texcoords.push_back({hi_uv.x, hi_uv.y});

        positions.push_back({lo_xy.x, lo_xy.y, 0.0f});
        texcoords.push_back({lo_uv.x, lo_uv.y});

        positions.push_back({hi_xy.x, hi_xy.y, 0.0f});
        texcoords.push_back({hi_uv.x, hi_uv.y});

        positions.push_back({lo_xy.x, hi_xy.y, 0.0f});
        texcoords.push_back({lo_uv.x, hi_uv.y});

        // temporary red
        colors.push_back({ red_color, green_color, blue_color });
        colors.push_back({ red_color, green_color, blue_color });
        colors.push_back({ red_color, green_color, blue_color });
        colors.push_back({ red_color, green_color, blue_color });
        colors.push_back({ red_color, green_color, blue_color });
        colors.push_back({ red_color, green_color, blue_color });

        // Advance cursor
        cursor_props.prevCursorX = cursor_props.cursorX;
        cursor_props.cursorX += advance;
    }
    atlas_props.set_state(FontProps::FONT_STATE::LOADED);
}


}
