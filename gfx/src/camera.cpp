#include <gfx/camera.hpp>
#include <gfx/index.hpp>
#include <osi/index.hpp>

namespace gfx {

Camera::Camera()
    : com::File(self_file_name())
    , m_bottom_left{ 0, 0 }
    , m_top_right{ 1, 1 }
    , m_near{ 0.25f }
    , m_far{ 500.0f }
    , m_relative{ true }
    , m_perspective{ true }
{}

vec2 Camera::bottom_left_absolute() const
{
    return relative_to_window() ?
        osi::window()->to_absolute_meters(viewport_system()->active_viewport()->point_to_window(bottom_left() - vec2{ 0.5f, 0.5f })) :
        bottom_left();
}

vec2 Camera::top_right_absolute() const
{
    return relative_to_window() ?
        osi::window()->to_absolute_meters(viewport_system()->active_viewport()->point_to_window(top_right() - vec2{ 0.5f, 0.5f })) :
        top_right();
}

vec2 Camera::middle_absolute() const
{
    return relative_to_window() ?
        osi::window()->to_absolute_meters(viewport_system()->active_viewport()->point_to_window((top_right() + bottom_left()) - vec2{ 0.5f, 0.5f })) :
        (top_right() + bottom_left()) / 2.0f;
}


void Camera::set_bottom_left(vec2 const& bottom_left)
{
    m_bottom_left = bottom_left;
}

void Camera::set_top_right(vec2 const& top_right)
{
    m_top_right = top_right;
}

void Camera::set_near(scalar const value)
{
    ASSUMPTION(value >= 0.01f);
    m_near = value;
}

void Camera::set_far(scalar const value)
{
    ASSUMPTION(value >= 0.02f);
    m_far = value;
}

mat4x4 Camera::matrix_perspective() const
{
    ASSUMPTION(get<0>(bottom_left()) < get<0>(top_right()) && get<1>(bottom_left()) < get<1>(top_right()));
    ASSUMPTION(near() >= 0.01f && near() <= far() - 0.01f);

    vec2 const bl = bottom_left_absolute();
    vec2 const tr = top_right_absolute();
    vec2 const add = bl + tr;
    vec2 const sub = tr - bl;

    return {
        {
            (2.0f * m_near) / get<0>(sub),
            0.0f,
            0.0f,
            0.0f
        },
        {
            0.0f,
            2.0f * m_near / get<1>(sub),
            0.0f,
            0.0f
        },
        {
             get<0>(add) / get<0>(sub),
             get<1>(add) / get<1>(sub),
            -(m_far + m_near) / (m_far - m_near),
            -1.0f
        },
        {
            0.0f,
            0.0f,
            -(2.0f * m_far * m_near) / (m_far - m_near),
            0.0f
        }
    };
}

mat4x4 Camera::matrix_orthograpics() const
{
    ASSUMPTION(get<0>(bottom_left()) < get<0>(top_right()) && get<1>(bottom_left()) < get<1>(top_right()));
    ASSUMPTION(near() >= 0.01f && near() <= far() - 0.01f);

    vec2 const bl = bottom_left_absolute();
    vec2 const tr = top_right_absolute();
    vec2 const add = bl + tr;
    vec2 const sub = tr - bl;

    return {
        {
            2.0f / get<0>(sub),
            0.0f,
            0.0f,
            0.0f
        },
        {
            0.0f,
            2.0f / get<1>(sub),
            0.0f,
            0.0f
        },
        {
            0.0f,
            0.0f,
            -2.0f / (m_far - m_near),
            0.0f
        },
        {
             -get<0>(add) / get<0>(sub),
             -get<1>(add) / get<1>(sub),
            -(m_far + m_near) / (m_far - m_near),
            1.0f
        }
    };
}

}
