#include <gfx/light.hpp>
#include <algorithm>

namespace gfx {

Light::Light(vec3 const &color) : com::File{ self_name() }, m_color{ color } {}

Light::~Light() {}

AmbientLight::AmbientLight(vec3 const &color) : Light{ color } {}

AmbientLight::~AmbientLight() {}

std::string AmbientLight::declarations() {
    return struct_decl() + ';' + function_decl();
}

std::string AmbientLight::struct_decl() {
    return R"(
struct AmbientLight {
    vec3 color;
})";
}

std::string AmbientLight::function_decl() {
    return R"(
vec3 calc_ambient_light(AmbientLight _light, vec3 _m_ambient)
{
    return _light.color * _m_ambient * 0.2;
})";
}

std::string AmbientLight::function_call(const std::string &_light, const std::string &_m_ambient) {
    return "calc_ambient_light(" + _light + ", " + _m_ambient + ')';
}

DirectionalLight::DirectionalLight(vec3 const &color) : Light{ color } {}

DirectionalLight::~DirectionalLight() {}

std::string DirectionalLight::declarations() {
    return struct_decl() + ';' + function_decl();
}

std::string DirectionalLight::struct_decl() {
    return R"(
struct DirectionalLight {
    vec3 color, direction;
})";
}

std::string DirectionalLight::function_decl() {
    return R"(
vec3 calc_directional_light(DirectionalLight _light, vec3 _m_diffuse,
                            vec3 _m_specular, vec3 _normal, vec3 _view_dir)
{
    vec3 lightDir = normalize(-_light.direction);
    float diff = max(dot(_normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, _normal);
    float spec = pow(max(dot(_view_dir, reflectDir), 0.0), 32);
    vec3 diffuse = diff * _m_diffuse;
    vec3 specular = spec * _m_specular;
    return (diffuse + specular) * _light.color;
})";
}

std::string DirectionalLight::function_call(const std::string &_light,
                                            const std::string &_m_diffuse, const std::string &_m_specular,
                                            const std::string &_normal, const std::string &_view_dir) {
    return "calc_directional_light(" + _light + ", " + _m_diffuse + ", " +
           _m_specular + ", " + _normal + ", " + _view_dir + ')';
}

PointLight::PointLight(vec3 const &color, float atten_const, float atten_lin, float atten_quad)
    : Light{ color }, m_atten_const{ atten_const }, m_atten_lin{ atten_lin }, m_atten_quad{ atten_quad } {
    // (Imax / attenuation) == (10 / 256)
    auto light_max = std::max({ color.r, color.g, color.b });
    m_radius = (-m_atten_lin + sqrtf(m_atten_lin * m_atten_lin - 4 * m_atten_quad * (m_atten_const - (256.f / 10.f) * light_max))) /
               (2.f * m_atten_quad);
}

PointLight::~PointLight() {}

std::string PointLight::declarations() {
    return struct_decl() + ';' + function_decl();
}

std::string PointLight::struct_decl() {
    return R"(
struct PointLight {
    vec3 color, position;
    float atten_const, atten_lin, atten_quad, radius;
})";
}

std::string PointLight::function_decl() {
    return R"(
vec3 calc_point_light(PointLight _light, vec3 _m_diffuse, vec3 _m_specular,
                      vec3 _normal, vec3 _frag_pos, vec3 _view_dir)
{
    vec3 lightDir = normalize(_light.position - _frag_pos);
    float diff = max(dot(_normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, _normal);
    float spec = pow(max(dot(_view_dir, reflectDir), 0.0), 32);
    float distance = length(_light.position - _frag_pos);
    float attenuation = 1.0 / (_light.atten_const + _light.atten_lin * distance + _light.atten_quad * distance * distance);
    vec3 diffuse = diff * _m_diffuse;
    vec3 specular = spec * _m_specular;
    return (diffuse + specular) * attenuation * _light.color;
})";
}

std::string PointLight::function_call(const std::string &_light,
                                      const std::string &_m_diffuse, const std::string &_m_specular,
                                      const std::string &_normal, const std::string &_frag_pos,
                                      const std::string &_view_dir) {
    return "calc_point_light(" + _light + ", " + _m_diffuse +
           ", " + _m_specular + ", " + _normal + ", " + _frag_pos + ", " + _view_dir + ')';
}

} // namespace gfx
