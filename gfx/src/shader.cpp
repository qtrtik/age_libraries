#include <algorithm>
#include <com/frame.hpp>
#include <gfx/index.hpp>
#include <gfx/light.hpp>
#include <gfx/shader.hpp>
#include <iostream>
#include <lfs/index.hpp>
#include <lfs/loader.hpp>
#include <sstream>
#include <utils/development.hpp>
#include <utils/log.hpp>

namespace gfx::detail {

static GLuint create_opengl_shader(GLenum shader_type, std::string const &source_code) {
    ASSUMPTION(shader_type == GL_VERTEX_SHADER || shader_type == GL_FRAGMENT_SHADER);
    ASSUMPTION(!source_code.empty());
    ASSUMPTION(glGetError() == 0U);

    std::string const shader_type_name{ shader_type == GL_VERTEX_SHADER ? "GL_VERTEX_SHADER" : "GL_FRAGMENT_SHADER" };

    GLuint const shader = glCreateShader(shader_type);
    ASSUMPTION(glGetError() == 0U);

    if (shader == 0U)
        throw std::logic_error{ "Call to 'glCreateShader' with '" + shader_type_name + "' has FAILED." };

    auto const source_code_ptr = source_code.data();
    GL_ASSUME_SUCCESS(glShaderSource(shader, (GLsizei)1, (GLchar const **)&source_code_ptr, nullptr));

    GL_ASSUME_SUCCESS(glCompileShader(shader));

    int is_build_successfull;

    GL_ASSUME_SUCCESS(glGetShaderiv(shader, GL_COMPILE_STATUS, &is_build_successfull));

    if (is_build_successfull != GL_TRUE) {
        int log_length;

        GL_ASSUME_SUCCESS(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length));

        std::string error_message;

        if (log_length <= 0)
            error_message = "Compiling shader of type " + shader_type_name +
                            " has FAILED. "
                            "Cannot receive the error message, because the build log has negative length.";
        else {
            error_message.resize(log_length + 1U, 0U);

            GL_ASSUME_SUCCESS(glGetShaderInfoLog(shader, log_length, &log_length, &error_message.front()));
        }

        GL_ASSUME_SUCCESS(glDeleteShader(shader));

        std::stringstream sstr;
        sstr << "Compiled shader:" << std::endl
             << source_code << std::endl
             << "Error message:" << std::endl
             << error_message << std::endl;
        std::string error_report = sstr.str();

        LOG(LSL_ERROR, error_report);

        std::cout << error_report;
        std::cout.flush();

        throw std::logic_error{ std::move(error_report) };
    }

    return shader;
}

GLuint create_opengl_program(GLuint const vertex_shader, GLuint const fragment_shader) {
    ASSUMPTION(vertex_shader != 0U && fragment_shader != 0U);
    ASSUMPTION(glGetError() == 0U);

    GLuint const shader_program = glCreateProgram();
    ASSUMPTION(glGetError() == 0U);

    if (shader_program == 0U)
        throw std::logic_error{ "Cannot create opengl program." };

    GL_ASSUME_SUCCESS(glAttachShader(shader_program, vertex_shader));

    GL_ASSUME_SUCCESS(glAttachShader(shader_program, fragment_shader));

    GL_ASSUME_SUCCESS(glLinkProgram(shader_program));

    int is_build_successfull;

    GL_ASSUME_SUCCESS(glGetProgramiv(shader_program, GL_LINK_STATUS, &is_build_successfull));

    if (is_build_successfull != GL_TRUE) {
        int log_length;
        GL_ASSUME_SUCCESS(glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &log_length));

        std::string error_message;

        if (log_length <= 0)
            error_message = "Linking of program has failed. Cannot receive the error message, "
                            "because the build log has negative length.";
        else {
            error_message.resize(log_length + 1U, 0U);
            GL_ASSUME_SUCCESS(glGetProgramInfoLog(shader_program, log_length, &log_length, &error_message.at(0U)));
        }

        GL_ASSUME_SUCCESS(glDetachShader(shader_program, vertex_shader));

        GL_ASSUME_SUCCESS(glDetachShader(shader_program, fragment_shader));

        GL_ASSUME_SUCCESS(glDeleteProgram(shader_program));

        throw std::logic_error{ std::move(error_message) };
    }

    return shader_program;
}

ShaderGL::ShaderGL() : m_vertex_shader{ 0 }, m_fragment_shader{ 0 }, m_program{ 0 } {}

ShaderGL::~ShaderGL() { release(); }

void ShaderGL::compile(std::string const &vertex_shader, std::string const &fragment_shader) {
    try {
        m_vertex_shader = detail::create_opengl_shader(GL_VERTEX_SHADER, vertex_shader);
        m_fragment_shader = detail::create_opengl_shader(GL_FRAGMENT_SHADER, fragment_shader);
        m_program = create_opengl_program(m_vertex_shader, m_fragment_shader);
        release_shaders();
    } catch (...) {
        release();
        throw;
    }
}

void ShaderGL::release() {
    if (m_program != 0) {
        GL_ASSUME_SUCCESS(glDeleteProgram(m_program));

        m_program = 0;
    }

    release_shaders();
}

void ShaderGL::release_shaders() {
    if (m_fragment_shader != 0) {
        GL_ASSUME_SUCCESS(glDeleteShader(m_fragment_shader));

        m_fragment_shader = 0;
    }

    if (m_vertex_shader != 0) {
        GL_ASSUME_SUCCESS(glDeleteShader(m_vertex_shader));

        m_vertex_shader = 0;
    }
}

void ShaderGL::activate() {
    ASSUMPTION(m_program != 0);

    GL_ASSUME_SUCCESS(glUseProgram(m_program));
}

bool ShaderGL::has_uniform(std::string const &var_name) const {
    GLint const location = glGetUniformLocation(m_program, var_name.c_str());
    INVARIANT(glGetError() == 0U);
    return location != -1;
}

GLuint shader_program_id(ShaderGL *const shader_gl) { return shader_gl->program_id(); }

static GLint layout_location(GLuint const program_id, std::string const &var_name) {
    GLint const location = glGetUniformLocation(program_id, var_name.c_str());
    INVARIANT(location != -1);
    INVARIANT(glGetError() == 0U);
    return location;
}

static void set_uniform(GLint const location, std::uint32_t const &value) {
    glUniform1ui(location, value);
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, std::int32_t const &value) {
    glUniform1i(location, value);
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, scalar const &value) {
    glUniform1f(location, value);
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, vec2 const &value) {
    glUniform2fv(location, 1U, element_array_ptr(value));
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, vec3 const &value) {
    glUniform3fv(location, 1U, element_array_ptr(value));
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, vec4 const &value) {
    glUniform4fv(location, 1U, element_array_ptr(value));
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, mat2x2 const &value) {
    glUniformMatrix2fv(location, 1U, GL_FALSE, element_array_ptr(value));
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, mat3x3 const &value) {
    glUniformMatrix3fv(location, 1U, GL_FALSE, element_array_ptr(value));
    INVARIANT(glGetError() == 0U);
}

static void set_uniform(GLint const location, mat4x4 const &value) {
    glUniformMatrix4fv(location, 1U, GL_FALSE, element_array_ptr(value));
    INVARIANT(glGetError() == 0U);
}

template <typename T>
static void set_uniform(ShaderGL *const shader, std::string const &var_name, T const &value) {
    set_uniform(layout_location(shader_program_id(shader), var_name), value);
}

} // namespace gfx::detail

namespace gfx {

Shader::Shader(const std::string &name, lighting l) : com::File{ name }, m_lighting_type{ l } {}

void Shader::set_camera_uniforms(glm::mat4 view, glm::mat4 projection, glm::vec3 camera_pos) const {
    maybe_set_uniform(view_matrix_uniform, view);
    maybe_set_uniform(projection_matrix_uniform, projection);
    maybe_set_uniform(camera_pos_uniform, camera_pos);
}

void Shader::set_model_uniforms(glm::mat4 model, glm::mat4 normal) const {
    maybe_set_uniform(model_matrix_uniform, model);
    maybe_set_uniform(normal_matrix_uniform, normal);
}


void Shader::set_time_uniforms(int time) const {
    maybe_set_uniform(time_uniform_frag, time);
    maybe_set_uniform(time_uniform_vert, time);
}

void Shader::on_change() {
    m_gl_shader = nullptr;
    notify_content_changed();
}

void Shader::set_lights(const com::Folder &lights) const {
    int point_lights = 0, dir_lights = 0, amb_lights = 0;
    for (auto i : lights.items()) {
        ASSUMPTION(i->is_link());
        auto link = dynamic_cast<com::Link *>(i);

        auto light_folder = link->target<com::Folder>();
        ASSUMPTION(light_folder != nullptr);

        auto light = light_system()->get_light_props(light_folder);
        ASSUMPTION(light != nullptr);

        set_light(*light, point_lights, dir_lights, amb_lights);
    }

    maybe_set_uniform(AmbientLight::glsl::nr_lights(), amb_lights);
    maybe_set_uniform(DirectionalLight::glsl::nr_lights(), dir_lights);
    maybe_set_uniform(PointLight::glsl::nr_lights(), point_lights);
}

std::tuple<int,int,int> Shader::set_lights(const std::vector<const com::Folder *> &lights) const {
    std::set<Light *> visited;
    int point_lights = 0, dir_lights = 0, amb_lights = 0;
    for (auto l_folder : lights) {
        for (auto i : l_folder->items()) {
            ASSUMPTION(i->is_link());
            auto link = dynamic_cast<com::Link *>(i);

            auto light_folder = link->target<com::Folder>();
            ASSUMPTION(light_folder != nullptr);

            auto light = light_system()->get_light_props(light_folder);
            ASSUMPTION(light != nullptr);

            auto [_, not_visited] = visited.insert(light);
            if (not_visited)
                set_light(*light, point_lights, dir_lights, amb_lights);
        }
    }

    maybe_set_uniform(AmbientLight::glsl::nr_lights(), amb_lights);
    maybe_set_uniform(DirectionalLight::glsl::nr_lights(), dir_lights);
    maybe_set_uniform(PointLight::glsl::nr_lights(), point_lights);

    return {amb_lights, dir_lights, point_lights};
}

void Shader::set_light(const Light &light, int &point_lights, int &dir_lights, int &amb_lights) const {
    switch (light.type()) {
    case Light::type_t::ambient: {
        if (amb_lights >= max_ambient_lights)
            break;

        auto u_name = AmbientLight::glsl::u_name() + '[' + std::to_string(amb_lights) + ']';
        ++amb_lights;

        maybe_set_uniform(u_name + AmbientLight::glsl::color(), light.color());
        break;
    }
    case Light::type_t::directional: {
        if (dir_lights >= max_directional_lights)
            break;

        auto u_name = DirectionalLight::glsl::u_name() + '[' + std::to_string(dir_lights) + ']';
        ++dir_lights;

        maybe_set_uniform(u_name + DirectionalLight::glsl::color(), light.color());

        auto frame = light_system()->get_frame(light.folder());
        ASSUMPTION(frame != nullptr);

        vec3 direction = vec4{ 0, 0, -1, 0 } * frame->out();
        maybe_set_uniform(u_name + DirectionalLight::glsl::direction(), direction);
        break;
    }
    case Light::type_t::point: {
        if (point_lights >= max_point_lights)
            break;

        auto index = '[' + std::to_string(point_lights) + ']';
        ++point_lights;

        auto u_name = PointLight::glsl::u_name() + index;

        maybe_set_uniform(u_name + PointLight::glsl::color(), light.color());

        auto frame = light_system()->get_frame(light.folder());
        ASSUMPTION(frame != nullptr);
        auto position = frame->frame().origin();
        maybe_set_uniform(u_name + PointLight::glsl::position(), position);

        auto &point = dynamic_cast<const PointLight &>(light);
        maybe_set_uniform(u_name + PointLight::glsl::radius(), point.radius());
        maybe_set_uniform(u_name + PointLight::glsl::atten_const(), point.atten_const());
        maybe_set_uniform(u_name + PointLight::glsl::atten_lin(), point.atten_lin());
        maybe_set_uniform(u_name + PointLight::glsl::atten_quad(), point.atten_quad());

        if (m_lighting_type == DEFERRED) {
            maybe_set_uniform("positions" + index, position);
            maybe_set_uniform("radii" + index, point.radius());
        }

        break;
    }
    }
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, int const value) {
    detail::set_uniform(shader, var_name, value);
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, scalar const value) {
    detail::set_uniform(shader, var_name, value);
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, vec2 const &value) {
    detail::set_uniform(shader, var_name, value);
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, vec3 const &value) {
    detail::set_uniform(shader, var_name, value);
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, vec4 const &value) {
    detail::set_uniform(shader, var_name, value);
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, mat2x2 const &value) {
    detail::set_uniform(shader, var_name, value);
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, mat3x3 const &value) {
    detail::set_uniform(shader, var_name, value);
}

void Shader::set_uniform(detail::ShaderGL *const shader, std::string const &var_name, mat4x4 const &value) {
    detail::set_uniform(shader, var_name, value);
}

} // namespace gfx
