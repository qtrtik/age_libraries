# include "utils/timeprof.hpp"

# include <gfx/index.hpp>
# include <com/math_files.hpp>
# include <osi/index.hpp>
# include <gfx/ui/state.hpp>
# include <utils/assumptions.hpp>
# include <ranges>

using namespace com;

namespace gfx {
    typedef ShaderGraph Graph;
    typedef ShaderGraph::Node::DataType::ElementType ElementType;

    UiSystem::UiSystem() : Library { self_name() }, m_active_scene(nullptr), m_ui {nullptr}, m_captured_element(nullptr), m_focused_element(nullptr), m_hovered_element(nullptr) {}

    UiSystem::~UiSystem() {}

    void UiSystem::initialize() {
        m_ui = folder()->push_back<Folder>("ui");
        m_default_camera_frame = m_ui->push_back<Frame>();
    }

    void UiSystem::release() {
        // for (Frame* frame : m_scene_frames) {
        //     frame->unregister_on_content_changed(this);
        // }
        // m_scene_frames.clear();
        remove_handlers(uis());
        folder()->erase(uis());
    }
    
    scalar UiSystem::get_z_coord(Camera* camera) {
        return -camera->near() - 0.01f;
    }

    Transform* UiSystem::get_transform(Folder* const element) const {
        return element->find<Transform>(transform_folder_name());
    }

    Folder* UiSystem::get_object(Folder* const element) const {
        ASSUMPTION(element->is_under(uis()));
        auto* object_link = get_object_link(element);
        return object_link ? object_link->target<Folder>() : nullptr;
    }

    Link* UiSystem::get_object_link(Folder* const element) const {
        ASSUMPTION(element->is_under(uis()));
        return element->find<Link>(UiElement::object_folder_link_name());
    }

    Frame * UiSystem::get_frame(Folder *ui_folder) const {
        ASSUMPTION(ui_folder->is_under(uis()));
        return ui_folder->find<Folder>(UiElement::frame_folder_name())->find<Frame>(UiElement::frame_file_name());
    }

    Buffer* UiSystem::get_buffer(UiElement* const element) const {
        if (element->get_buffer() != nullptr) {
            return element->get_buffer();
        }
        return get_object(element)->find<Link>(object_system()->buffer_link_name())->target<Buffer>();
    }

    Link* UiSystem::get_buffer_link(UiElement* const element) const {
        // todo: switch link to ui element buffer link
        return get_object(element)->find<Link>(object_system()->buffer_link_name());
    }

    State* UiSystem::get_element_state(Folder* const element) const {
        return element->find<State>(State::self_name());
    }

    Folder * UiSystem::get_materials_folder(Folder* ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        auto* materials_folder = ui_object->find<Folder>(UiElement::materials_folder_name());
        return materials_folder == nullptr ? ui_object->push_back<Folder>(UiElement::materials_folder_name()) : materials_folder;
    }

    Folder * UiSystem::get_buffers_folder(Folder* ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        auto* buffers_folder = ui_object->find<Folder>(UiElement::buffers_folder_name());
        return buffers_folder == nullptr ? ui_object->push_back<Folder>(UiElement::buffers_folder_name()) : buffers_folder;
    }

    Material* UiSystem::get_enabled_material(Folder* ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        return get_materials_folder(ui_object)->find<Link>(UiElement::enabled_link_name())->target<Material>();
    }

    Material * UiSystem::get_pressed_material(Folder* ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        return get_materials_folder(ui_object)->find<Link>(UiElement::pressed_link_name())->target<Material>();
    }

    Material * UiSystem::get_hovered_material(Folder* ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        return get_materials_folder(ui_object)->find<Link>(UiElement::hovered_link_name())->target<Material>();
    }

    Material * UiSystem::get_texture_material(std::string const &name, ContextPath path, bool flip) {
        ContextPath path_name = path;
        path_name.push_back(name + "_material");
        auto* material = material_system()->materials()->locate<Material>(path_name);
        if (material)
            return material;
        return create_texture_material(name, path, flip);
    }

    Material* UiSystem::create_texture_material(std::string const& name, ContextPath path, bool flip) {
        auto shader = shader_system()->insert_shader<Graph>(
            path,
            name,
            Graph::UNLIT,
            Graph::PHONG,
            Graph::PARTIAL
        );

        ContextPath path_name = path;
        path_name.push_back(name);
        auto* textureNode = shader->insert<Graph::TextureNode>(path_name, ElementType::VEC4);
        auto* uvCoordNode = shader->insert<Graph::VaryingTexcoordNode>();

        auto* constantOneNode = shader->insert<Graph::ConstantNode>(scalar{1.0});
        auto* decomposeUvNode = shader->insert<Graph::DecomposeVectorNode>(ElementType::VEC2);
        // Used for inverting the y-axis of the texture
        auto* subtractNode = shader->insert<Graph::SubtractNode>(ElementType::SCALAR);
        auto* composeUvNode = shader->insert<Graph::ComposeVectorNode>(ElementType::VEC2);

        shader->connect(decomposeUvNode, 0, uvCoordNode, 0);
        shader->connect(subtractNode, 0, constantOneNode, 0);
        shader->connect(subtractNode, 1, decomposeUvNode, 1);
        shader->connect(composeUvNode, 0, decomposeUvNode, 0);
        if (flip)
            shader->connect(composeUvNode, 1, subtractNode, 0);
        else
            shader->connect(composeUvNode, 1, decomposeUvNode, 1);

        shader->connect(textureNode, Graph::TextureNode::inputs::tex_coord, composeUvNode, 0);

        auto* decomposeNode = shader->insert<Graph::DecomposeVectorNode>(ElementType::VEC4);
        auto* composeNode = shader->insert<Graph::ComposeVectorNode>(ElementType::VEC3);
        shader->connect(decomposeNode, 0, textureNode, 0);
        shader->connect(composeNode, 0, decomposeNode, 0);
        shader->connect(composeNode, 1, decomposeNode, 1);
        shader->connect(composeNode, 2, decomposeNode, 2);

        shader->connect(shader->root(), Graph::MasterNodeUnlit::inputs::color, composeNode, 0);

        auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
        shader->connect(shader->root(), Graph::MasterNodeUnlit::inputs::emission, emission, 0);
        shader->connect(shader->root(), Graph::MasterNodeUnlit::inputs::alpha, decomposeNode, 3);
        return material_system()->insert_material(name + "_material", shader, path);
    }

    Buffer * UiSystem::get_hovered_buffer(com::Folder *ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        return get_buffers_folder(ui_object)->find<Link>(UiElement::hovered_link_name())->target<Buffer>();
    }

    Buffer * UiSystem::get_enabled_buffer(com::Folder *ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        return get_buffers_folder(ui_object)->find<Link>(UiElement::enabled_link_name())->target<Buffer>();
    }

    Buffer * UiSystem::get_pressed_buffer(com::Folder *ui_object) const {
        ASSUMPTION(ui_object->is_under(uis()));
        return get_buffers_folder(ui_object)->find<Link>(UiElement::pressed_link_name())->target<Buffer>();
    }

    Folder* UiSystem::create_scene(std::string const& name) {
        auto* scene = m_ui->locate<Folder>({"ui", name});
        ASSUMPTION(scene == nullptr);

        // Create scene folder as well as corresponding objects folder
        auto* scene_folder = m_ui->push_back<Folder>(name);
        auto* scene_object_folder = object_system()->objects()->push_back_folders({"ui", name});
        scene_folder->push_back<Link>(object_folder_link_file_name(), scene_object_folder);
        scene_folder->push_back<Link>(viewport_link_file_name(), viewport_system()->viewport_default());
        scene_folder->push_back<Folder>(frame_folder_name())->push_back<Frame>();
        scene_folder->push_back<Folder>(default_layer_name());
        scene_object_folder->push_back<Folder>(default_layer_name());

        return scene_folder;
    }

    Folder* UiSystem::get_scene(std::string const& name) {
        return m_ui->find<Folder>(name);
    }

    void UiSystem::set_active_scene(std::string const &name) {
        m_active_scene = get_scene(name);
        m_hovered_element = nullptr;
        m_focused_element = nullptr;
        m_captured_element = nullptr;
        handle_mouse_move(osi::mouse()->pos());
    }

    /**
     * Assigns camera to scene and parents scene frame to camera frame (for 2D UI)
     * @param scene scene to modify
     * @param camera camera to assign to scene
     * @param is_3d whether the scene is 3D
     */
    void UiSystem::assign_camera_to_scene(Folder* scene, Camera* camera, bool is_3d) {
        ASSUMPTION(scene->is_under(uis()));
        auto* camera_link = scene->find<Link>(camera_link_file_name());;
        if (camera_link == nullptr) {
            camera_link = scene->push_back<Link>(camera_link_file_name(), nullptr);
        }
        camera_link->set_target(camera);
        auto* camera_frame = camera_system()->get_frame(camera->folder());

        // bool alr_registered = false;
        // for (ContextItem* registered_item : camera_frame->registered_on_content_changed()) {
        //     if (registered_item == this) {
        //         alr_registered = true;
        //     }
        // }
        // if (!alr_registered) {
        //     camera_frame->register_on_content_changed(this);
        //     m_scene_frames.emplace(camera_frame);
        // }

        if (!is_3d) {
            get_scene_frame_folder(scene)->push_back<Link>(Frame::parent_link_file_name(), camera_frame);
            get_scene_frame(scene)->set_origin({-camera->middle_absolute(), ui_system()->get_z_coord(camera)});
        }
    }

    /**
     * Assigns object to UI scene (for 3D UI)
     * @param scene
     * @param object_frame
     */
    void UiSystem::assign_object_to_scene(Folder* scene, Frame* object_frame) {
        ASSUMPTION(scene->is_under(uis()));
        get_scene_frame_folder(scene)->push_back<Link>(Frame::parent_link_file_name(), object_frame);
    }

    Folder* UiSystem::get_scene_frame_folder(Folder *scene) {
        return scene->find<Folder>(frame_folder_name());
    }

    Frame* UiSystem::get_scene_frame(Folder *scene) {
        return scene->locate<Frame>({frame_folder_name(), Frame::self_file_name()});
    }

    Viewport * UiSystem::get_scene_viewport(Folder *scene) {
        return scene->find<Link>(viewport_link_file_name())->target<Viewport>();
    }

    void UiSystem::set_scene_viewport(com::Folder *scene, Viewport *viewport) {
        auto* viewport_link = scene->find<Link>(viewport_link_file_name());
        viewport_link->set_target(viewport);
    }

    Frame * UiSystem::get_scene_parent_frame(Folder *scene) {
        return get_scene_frame_folder(scene)->find<Link>(Frame::parent_link_file_name())->target<Frame>();
    }

    ContextPath UiSystem::get_scene_object_path(Folder *scene_folder, Folder *ui_object) {
        ASSUMPTION(ui_object->is_under(scene_folder));
        ContextPath path = {};
        for (Folder* parent = ui_object; parent != scene_folder->folder(); parent = parent->folder()) {
            path.emplace_back(parent->name());
        }
        path.emplace_back("ui");
        reverse(path.begin(), path.end());
        return path;
    }

    Folder * UiSystem::get_element_scene_layer(Folder *scene_folder, UiElement *element) {
        Folder* parent = element;
        while (parent->folder() != nullptr && parent->folder() != scene_folder) {
            parent = parent->folder();
        }
        return parent;
    }

    Folder * UiSystem::insert_scene_layer_top(Folder *scene_folder, std::string const &name) {
        ASSUMPTION(scene_folder->is_under(uis()));
        object_system()->objects()->push_back_folders({"ui", scene_folder->name(), name});
        return scene_folder->push_back<Folder>(name);
    }

    Folder * UiSystem::insert_scene_layer_bottom(Folder *scene_folder, std::string const &name) {
        ASSUMPTION(scene_folder->is_under(uis()));
        object_system()->objects()->push_front_folders({"ui", scene_folder->name(), name});
        return scene_folder->push_front<Folder>(name);
    }

    Folder * UiSystem::insert_scene_layer_above(Folder *scene_folder, Folder *after_layer_folder, std::string const &name) {
        ASSUMPTION(scene_folder->is_under(uis()));
        ASSUMPTION(after_layer_folder->is_under(uis()));
        object_system()->objects()->locate<Folder>({"ui", scene_folder->name()})->insert_after<Folder>(after_layer_folder->name(), name);
        return scene_folder->insert_after<Folder>(after_layer_folder->name(), name);
    }

    Folder * UiSystem::insert_scene_layer_under(Folder *scene_folder, Folder *before_layer_folder, std::string const &name) {
        ASSUMPTION(scene_folder->is_under(uis()));
        ASSUMPTION(before_layer_folder->is_under(uis()));
        object_system()->objects()->locate<Folder>({"ui", scene_folder->name()})->insert_before<Folder>(before_layer_folder->name(), name);
        return scene_folder->insert_before<Folder>(before_layer_folder->name(), name);
    }

    Camera* UiSystem::get_scene_camera(Folder* scene) {
        ASSUMPTION(scene == uis() || scene->is_under(uis()));
        auto* camera_link = scene->find<Link>(camera_link_file_name());
        if (camera_link == nullptr) return nullptr;
        return camera_link->target<gfx::Camera>();
    }

    void UiSystem::present_scene(Folder *scene_folder, bool scissor, scalar line_width) {
        if (scene_folder == nullptr)
            return;
        if (get_scene_object_folder(scene_folder) == nullptr)
            return;
        ASSUMPTION(scene_folder->is_under(uis()));
        on_change(scene_folder);
        // Set up camera
        Camera* camera = get_scene_camera(scene_folder);
        viewport_system()->set_active_viewport(get_scene_viewport(scene_folder));
        camera_system()->activate_camera(camera ? camera->folder() : camera_system()->default_camera_folder());

        Renderer::configure_depth_writing(false);
        Renderer::configure_scissor_test(scissor);
        Renderer::configure_blending(true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        Renderer::configure_line_width(line_width);

        renderer()->present_collection(get_scene_object_folder(scene_folder), Renderer::pipeline::FORWARD);

        Renderer::configure_line_width(line_width);
        Renderer::configure_depth_writing(true);
        Renderer::configure_blending(false, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        Renderer::configure_scissor_test(false);
    }

    void UiSystem::present_active_scene(bool scissor, scalar line_width) {
        present_scene(m_active_scene, scissor, line_width);
    }

    vec2 UiSystem::get_size_in_meters(vec2i size_in_pixels) {
        vec2 size_normalized = (vec2)size_in_pixels / (vec2)osi::window()->size();
        return osi::window()->to_absolute_meters(size_normalized);
    }

    vec2i UiSystem::get_size_in_pixels(vec2 size_in_meters) {
        vec2 pixel_size = osi::window()->pixel_size_in_meters();
        return round(size_in_meters / pixel_size);
    }

    void UiSystem::set_element_state(Folder* element_folder, UI_STATE state) {
        get_element_state(element_folder)->set_state(state);
    }

    void UiSystem::calculate_screen_space_aabb(Folder* element_folder, Folder* scene_folder) {
        auto* element = dynamic_cast<UiElement*>(element_folder);
        AABB aabb = calculate_screen_space_aabb(element, scene_folder, element->get_normalized_size());
        element->get_transform()->set_aabb(aabb);
    }

AABB UiSystem::calculate_screen_space_aabb(Folder *element_folder, Folder *scene_folder, AABB ws_aabb) {
        Camera* camera = get_scene_camera(scene_folder);
        if (camera == nullptr)
            camera = camera_system()->active_camera();
        auto* camera_frame = camera_system()->get_frame(camera->folder());
        auto* viewport = ui_system()->get_scene_viewport(scene_folder);
        auto* element = dynamic_cast<UiElement*>(element_folder);

        Frame* frame = element != nullptr ? element->get_frame() : ui_system()->get_frame(element_folder);

        mat4x4 projection_matrix = camera->matrix();
        mat4x4 view_matrix = camera_frame->in();
        mat4x4 model_matrix = frame->out();

        vec4 transformed_lo = projection_matrix * view_matrix * model_matrix * vec4(ws_aabb.lo, 1.0f);
        vec4 transformed_hi = projection_matrix * view_matrix * model_matrix * vec4(ws_aabb.hi, 1.0f);

        transformed_lo /= transformed_lo.w;
        transformed_hi /= transformed_hi.w;

        transformed_lo = (transformed_lo + vec4(1.0f, 1.0f, 0.0f, 0.0f)) * 0.5f;
        transformed_hi = (transformed_hi + vec4(1.0f, 1.0f, 0.0f, 0.0f)) * 0.5f;

        vec2 bl = viewport->bottom_left();
        vec2 tr = viewport->top_right();
        vec2 size = viewport->size();

        // Scale and offset by viewport
        transformed_lo.x = transformed_lo.x * size.x + bl.x;
        transformed_lo.y = transformed_lo.y * size.y + bl.y;
        transformed_hi.x = transformed_hi.x * size.x + bl.x;
        transformed_hi.y = transformed_hi.y * size.y + bl.y;

        transformed_lo.x = std::max(transformed_lo.x, bl.x);
        transformed_lo.y = std::max(transformed_lo.y, bl.y);
        transformed_hi.x = std::min(transformed_hi.x, tr.x);
        transformed_hi.y = std::min(transformed_hi.y, tr.y);

        transformed_lo.x = transformed_lo.x * 2.0f - 1.0f;
        transformed_lo.y = transformed_lo.y * 2.0f - 1.0f;
        transformed_hi.x = transformed_hi.x * 2.0f - 1.0f;
        transformed_hi.y = transformed_hi.y * 2.0f - 1.0f;

        return AABB {contract(transformed_lo), contract(transformed_hi)};
    }

    vec2 UiSystem::calculate_world_space_coords(Folder* element_folder, vec2 screen_coords_ndc, Folder* scene_folder) {
        Camera* camera = get_scene_camera(scene_folder);
        mat4x4 projection_matrix = camera->matrix();
        mat4x4 view_matrix = camera_system()->get_frame(camera->folder())->in();
        mat4x4 model_matrix = get_frame(element_folder)->out();
        auto* viewport = ui_system()->get_scene_viewport(scene_folder);

        vec2 bl = viewport->bottom_left();
        vec2 size = viewport->size();

        screen_coords_ndc = (screen_coords_ndc + vec2(1.0f, 1.0f)) * 0.5f;

        // Scale and offset inversely by viewport
        screen_coords_ndc.x = screen_coords_ndc.x / size.x - bl.x;
        screen_coords_ndc.y = screen_coords_ndc.y / size.y - bl.y;

        screen_coords_ndc.x = screen_coords_ndc.x * 2.0f - 1.0f;
        screen_coords_ndc.y = screen_coords_ndc.y * 2.0f - 1.0f;

        vec4 clip_space_pos = vec4(screen_coords_ndc.x, screen_coords_ndc.y, 0.0f, 1.0f);

        vec4 world_space_pos = inverse(projection_matrix * view_matrix * model_matrix) * clip_space_pos;
        world_space_pos /= world_space_pos.w;

        return world_space_pos;
    }

    void UiSystem::on_content_changed(File *sender) {
        Library::on_content_changed(sender);
        // if (auto* frame = dynamic_cast<Frame*>(sender)) {
        //     for (Folder* scene_folder : uis()->subfolders()) {
        //         if (camera_system()->get_frame(get_scene_camera(scene_folder)->folder()) == frame) {
        //             // Camera Frame of scene moved
        //             on_change(scene_folder);
        //         }
        //     }
        // }
    }

    void UiSystem::remove_handlers(Folder* sender_element) {
        if (auto* element_folder = dynamic_cast<UiElement*>(sender_element)) {
            element_folder->get_frame_parent_link()->unregister_on_target_changed(element_folder);
            element_folder->locate<Frame>({UiElement::frame_folder_name(), UiElement::frame_file_name()})->unregister_on_content_changed(element_folder);
            File* state_file = get_element_state(element_folder);
            get_transform(element_folder)->get_anchor()->unregister_on_content_changed(element_folder);
            for (ContextItem* registered_elem : state_file->registered_on_content_changed())
                state_file->unregister_on_content_changed(registered_elem);
        }
        for (Folder* folder : sender_element->subfolders()) {
            remove_handlers(folder);
        }
    }

    void UiSystem::handle_mouse_move(vec2i position) {
        if (m_active_scene == nullptr)
            return;
        vec2 mouse_pos_transformed = (vec2(position) / vec2(osi::window()->size())) * 2.0f - 1.0f;
        mouse_pos_transformed.y *= -1.0f;
        handle_mouse_move_rec(m_active_scene, mouse_pos_transformed);
        if (m_captured_element != nullptr)
            m_captured_element->on_mouse_moved(mouse_pos_transformed);
    }

    void UiSystem::handle_mouse_move_rec(Folder* folder, vec2 position) {
        for (Folder* subfolder : std::ranges::views::reverse(folder->subfolders())) {
            if (auto* element_folder = dynamic_cast<UiElement*>(subfolder)) {
                if (!element_folder->can_receive_events()) {
                    handle_mouse_move_rec(element_folder->get_foreground_folder(), position);
                    continue;
                }
                AABB coords = element_folder->get_transform()->get_aabb();
                if (AABB::projection_intersects_xy(coords, position)) {
                    if (m_captured_element == nullptr) {
                        set_element_state(element_folder, HOVERED);
                        if (m_hovered_element != nullptr && m_hovered_element != element_folder)
                            set_element_state(m_hovered_element, ENABLED);
                        m_hovered_element = element_folder;
                        return;
                    }
                } else {
                    if (m_captured_element == element_folder) continue;
                    set_element_state(element_folder, ENABLED);
                    if (m_hovered_element == element_folder) m_hovered_element = nullptr;
                }
            } else {
                handle_mouse_move_rec(subfolder, position);
            }
        }
    }

    void UiSystem::handle_mouse_press(vec2i position, MouseEvent mouse_event) {
        if (m_active_scene == nullptr)
            return;
        if (m_hovered_element != nullptr && m_hovered_element->get_mouse_mask() == mouse_event) {
            vec2 mouse_pos_transformed = vec2(position) / vec2(osi::window()->size()) * 2.0f - 1.0f;
            mouse_pos_transformed.y *= -1.0f;
            m_captured_element = m_hovered_element;
            m_focused_element = m_hovered_element;
            set_element_state(m_hovered_element, PRESSED);
            m_hovered_element->on_mouse_pressed(mouse_pos_transformed);
        }
    }

    void UiSystem::handle_mouse_release(vec2i position, MouseEvent mouse_event) {
        if (m_active_scene == nullptr)
            return;
        vec2 mouse_pos_transformed = vec2(position) / vec2(osi::window()->size()) * 2.0f - 1.0f;
        mouse_pos_transformed.y *= -1.0f;

        if (m_captured_element != nullptr && m_captured_element->get_mouse_mask() == mouse_event) {
            if (AABB::projection_intersects_xy(m_captured_element->get_transform()->get_aabb(), mouse_pos_transformed)) {
                set_element_state(m_captured_element, HOVERED);
                m_captured_element->on_click(mouse_pos_transformed);
            } else {
                set_element_state(m_captured_element, ENABLED);
                m_captured_element->on_mouse_released(mouse_pos_transformed);
            }
            m_captured_element = nullptr;
        }
        handle_mouse_move(position);
    }

    void UiSystem::handle_mouse_input() {
        vec2 mouse_pos_transformed = (vec2(osi::mouse()->pos()) / vec2(osi::window()->size())) * 2.0f - 1.0f;
        mouse_pos_transformed.y *= -1.0f;
        handle_mouse_input_rec(m_active_scene, mouse_pos_transformed);
    }

    void UiSystem::handle_mouse_input_rec(Folder *folder, vec2 mouse_position) {
        bool mouse_button_pressed = osi::mouse()->down().contains("MouseLeft");

        for (Folder* subfolder : folder->subfolders()) {
             if (auto* element_folder = dynamic_cast<UiElement*>(subfolder)) {
                 if (!has_object(element_folder) || !element_folder->can_receive_events()) {
                     return;
                 }
                 AABB coords = element_folder->get_transform()->get_aabb();
                 if (!mouse_button_pressed) {
                    m_captured_element = nullptr;
                 }
                 if (AABB::projection_intersects_xy(coords, mouse_position)) {
                     if (m_captured_element != nullptr) continue;
                     if (mouse_button_pressed) {
                         m_captured_element = element_folder;
                         m_focused_element = element_folder;
                         set_element_state(element_folder, PRESSED);
                     }
                     else
                         set_element_state(element_folder, HOVERED);
                 } else {
                     if (m_captured_element != element_folder)
                     set_element_state(element_folder, ENABLED);
                 }
            } else {
                handle_mouse_input_rec(subfolder, mouse_position);
            }
            // Improve so that this function is only called when some attributes are changed, like pivot, width, height. Store the AABB instead of calculating it each frame.
        }
    }

    void UiSystem::set_parent_frame(UiElement* ui_element, Frame* parent) {
        auto* parent_link = ui_element->get_frame_parent_link();
        if (parent_link == nullptr)
            parent_link = ui_element->get_frame_folder()->push_back<Link>(Frame::parent_link_file_name(), nullptr);
        parent_link->set_target(parent);
    }

    /**
     * \brief Inserts UI element into object system folder of scene.
     * \param ui_element Folder of element in UI system
     * \param frame Frame of the UI element
     * \param material Material to be used for rendering
     * \param buffer Buffer to be used for rendering
     * \return Created folder in object system
     */
    Folder* UiSystem::insert_object(UiElement* ui_element, Folder* ui_object, Folder* scene_folder, Frame* frame, Material* material, Buffer* buffer, bool front) {
        ContextPath object_path = get_scene_object_path(scene_folder, ui_object ? ui_object : ui_element);;

        Folder* object;
        if (front) {
            object = object_system()->insert_object_front(
                 object_path,
                 material,
                 buffer
            );
        } else {
            object = object_system()->insert_object(
                    object_path,
                    material,
                    buffer
            );
        }

        object_system()->push_frame_back(
            object,
            frame 
        );

        calculate_screen_space_aabb(ui_element, scene_folder);

        if (ui_object) {
            auto* object_link = ui_object->find<Link>(UiElement::object_folder_link_name());
            if (object_link == nullptr)
                object_link = ui_object->push_back<Link>(UiElement::object_folder_link_name(), nullptr);
            object_link->set_target(object);
        }

        return object;
    }

    void UiSystem::erase_object(Folder* scene_folder, Folder* ui_object) {
        auto* object = object_system()->objects()->locate<Folder>(ui_system()->get_scene_object_path(scene_folder, ui_object));
        if (!object) return;
        if (auto* ui_element = dynamic_cast<UiElement*>(ui_object)) {
            for (Folder* subfolder : ui_element->get_foreground_folder()->subfolders()) {
                erase_object(scene_folder, subfolder);
            }
            for (Folder* subfolder : ui_element->get_background_folder()->subfolders()) {
                erase_object(scene_folder, subfolder);
            }
        }
        object->clear();
        Folder::erase_empty(object);
        ui_object->erase(UiElement::object_folder_link_name());
    }

    void UiSystem::erase_element(UiElement *ui_element) {
        ui_system()->remove_handlers(ui_element);
        ui_system()->erase_object(ui_element->get_scene_folder(), ui_element) ;
        ui_element->clear();
        ui_element->folder()->erase(ui_element);
        if (m_hovered_element == ui_element)
            m_hovered_element = nullptr;
        if (m_captured_element == ui_element)
            m_captured_element = nullptr;
        if (m_focused_element == ui_element)
            m_focused_element = nullptr;
    }

    void UiSystem::insert_objects(UiElement *ui_element, bool front) {
        Folder* scene = ui_element->get_scene_folder();
        ASSUMPTION(ui_element->is_under(scene));
        Folder* ui_element_object_folder;
        if (front) {
             ui_element_object_folder = object_system()->objects()->push_front_folders(get_scene_object_path(scene, ui_element));
        } else {
             ui_element_object_folder = object_system()->objects()->push_back_folders(get_scene_object_path(scene, ui_element));
        }
        ui_element_object_folder->clear();
        auto* object_folder_link = ui_element->find<Link>(object_folder_link_file_name());
        if (object_folder_link == nullptr) {
            object_folder_link = ui_element->push_back<Link>(object_folder_link_file_name(), nullptr);
        }
        object_folder_link->set_target(ui_element_object_folder);
        auto* background_folder = ui_element_object_folder->push_back<Folder>(UiElement::background_folder_name());
        auto* foreground_folder = ui_element_object_folder->push_back<Folder>(UiElement::foreground_folder_name());
        object_system()->insert_scissor_file(background_folder, ui_element->get_background_scissor_file()->get());
        object_system()->insert_scissor_file(foreground_folder, ui_element->get_foreground_scissor_file()->get());

        insert_background_objects(ui_element, scene);
        insert_foreground_objects(ui_element, scene);

        recalculate_element(ui_element);
    }

    void UiSystem::insert_foreground_objects(UiElement *ui_element, Folder* scene, bool front) {
        insert_objects_in_folder(ui_element, ui_element->get_foreground_folder(), scene, front);
    }

    void UiSystem::insert_background_objects(UiElement *ui_element, Folder* scene, bool front) {
        insert_objects_in_folder(ui_element, ui_element->get_background_folder(), scene, front);
    }

    void UiSystem::insert_objects_in_folder(UiElement *ui_element, Folder* folder, Folder* scene, bool front) {
        for (Folder* ui_object : folder->subfolders()) {
            if (auto* sub_element = dynamic_cast<UiElement*>(ui_object)) {
                insert_objects(sub_element);
                continue;
            }
            auto* material_folder = ui_object->find<Folder>(UiElement::materials_folder_name());
            auto* enabled_material_link = material_folder->find<Link>(UiElement::enabled_link_name());
            ASSUMPTION(enabled_material_link->target() != nullptr);
            auto* buffer_folder = ui_object->find<Folder>(UiElement::buffers_folder_name());
            auto* enabled_buffer_link = buffer_folder->find<Link>(UiElement::enabled_link_name());
            ASSUMPTION(enabled_buffer_link->target() != nullptr);
            auto* object_frame = ui_system()->get_frame(ui_object);
            auto* material = enabled_material_link->target() == nullptr ?
                material_system()->insert_default_material("error_material", {"error_materials"}, {0.9, 0.063, 0.875}) :
                enabled_material_link->target<Material>();

            insert_object(ui_element, ui_object, scene, object_frame, material, enabled_buffer_link->target<Buffer>(), front);
        }
    }

    void UiSystem::on_change(Folder* scene_folder) {
        TMPROF_BLOCK();
        if (scene_folder == nullptr)
            return;
        auto* scene = scene_folder;
        auto* camera = get_scene_camera(scene);
        auto* camera_frame = camera_system()->get_frame(camera->folder());
        auto* scene_frame = ui_system()->get_scene_parent_frame(scene);
        if (camera_frame == scene_frame) {
            ui_system()->get_scene_frame(scene)->set_origin({-camera->middle_absolute(), ui_system()->get_z_coord(camera)});
        }
        for (Folder* sub_folder : scene->subfolders()) {
            on_change_rec(sub_folder);
        }
    }

    void UiSystem::on_change_rec(Folder* folder) {
        if (folder == nullptr)
            return;
        UiElement* element = dynamic_cast<UiElement*>(folder);
        if (element != nullptr) {
            recalculate_element(element);
        }
        for (Folder* sub_folder : folder->subfolders()) {
            on_change_rec(sub_folder);
        }
    }

    void UiSystem::on_mouse_pressed(vec2 position) {
        if (m_hovered_element != nullptr) m_hovered_element->on_mouse_pressed(position);
    }

    void UiSystem::on_key_pressed(const std::string& key) {
        if (m_focused_element != nullptr) m_focused_element->on_key_pressed(key);
    }

    void UiSystem::on_text_entered(const std::string &text) {
        if (m_focused_element != nullptr) m_focused_element->on_text_entered(text);
    }

    void UiSystem::recalculate_element(UiElement* element) {
        element->on_anchor_changed(element->get_transform()->get_anchor());
        calculate_screen_space_aabb(element, element->get_scene_folder());
        element->recalculate_objects();
        element->recalculate_scissor();
    }

    void UiSystem::insert_default_materials() {
        m_default_rect_material = material_system()->insert_default_material(
        "rect_material", { "rect_materials" }, vec3(1.0f, 1.0f, 1.0f));
        m_default_enabled_material = material_system()->insert_default_material(
            "enabled_material", { "state_materials" }, vec3{ 0.3f, 0.3f, 0.3f });
        m_default_hovered_material = material_system()->insert_default_material(
            "hovered_material", { "state_materials" }, vec3{ 0.5f, 0.5f, 0.5f });
        m_default_pressed_material = material_system()->insert_default_material(
            "pressed_material", { "state_materials" }, vec3{ 0.7f, 0.7f, 0.7f });
    }

}