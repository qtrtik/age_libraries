#include <gfx/viewport_system.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>

namespace gfx {

ViewportSystem::ViewportSystem()
    : com::Library{ self_name() }
    , m_viewports { nullptr }
    , m_viewport_default { nullptr }
    , m_active_viewport { nullptr } 
{}

ViewportSystem::~ViewportSystem()
{}

void ViewportSystem::initialize()
{
    m_viewports = folder()->push_back<com::Folder>("viewports");
    m_viewport_default = viewports()->push_back<Viewport>("default");
    m_active_viewport = viewports()->push_back<com::Link>("active_viewport.link", m_viewport_default);
}

void ViewportSystem::release()
{
    folder()->erase(viewports());
}

void ViewportSystem::set_active_viewport(Viewport* const viewport)
{
    ASSUMPTION(viewport->is_under(viewports()));
    m_active_viewport->set_target(viewport);
}

Viewport* ViewportSystem::insert_viewport(std::string const& name, com::ContextPath const& sub_folders)
{
    com::Folder* const f = viewports()->push_back_folders(sub_folders);
    ASSUMPTION(f == viewports() || f->is_under(viewports()));
    com::ContextItem* const item = f->find<Viewport>(name);
    if (item == nullptr)
        return f->push_back<Viewport>(name);
    Viewport* const viewport = dynamic_cast<Viewport*>(item);
    ASSUMPTION(viewport != nullptr);
    return viewport;
}

void ViewportSystem::erase_viewport(Viewport* const viewport)
{
    ASSUMPTION(viewport->is_under(viewports()));
    viewport->folder()->erase(viewport);
}

}
