#include <gfx/viewport.hpp>
#include <osi/index.hpp>
#include <osi/opengl.hpp>
#include <utils/assumptions.hpp>

namespace gfx {

Viewport::Viewport(std::string const& name)
    : com::File(name)
    , m_bottom_left{ 0, 0 }
    , m_top_right{ 1, 1 }
    , m_relative{ true }
{}

vec2 Viewport::bottom_left_absolute() const
{
    return relative_to_window() ? osi::window()->to_absolute_pixels(bottom_left()) : bottom_left();
}

vec2 Viewport::top_right_absolute() const
{
    return relative_to_window() ? osi::window()->to_absolute_pixels(top_right()) : top_right();
}

vec2 Viewport::size_absolute() const
{
    return relative_to_window() ? osi::window()->to_absolute_pixels(size()) : size();
}

vec2 Viewport::point_to_window(vec2 const& p) const
{
    return bottom_left() + hadamard_product(p, top_right() - bottom_left());
}

void Viewport::activate() const
{
    ASSUMPTION(get<0>(bottom_left()) < get<0>(top_right()) && get<1>(bottom_left()) < get<1>(top_right()));

    vec2 const bl = bottom_left_absolute();
    vec2 const tr = top_right_absolute();
    vec2 const size = tr - bl;

    glViewport((GLint)get<0>(bl), (GLint)get<1>(bl), (GLsizei)get<0>(size), (GLsizei)get<1>(size));
    ASSUMPTION(glGetError() == 0U);
}

}
