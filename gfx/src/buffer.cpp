#include <gfx/buffer.hpp>
#include <osi/opengl.hpp>
#include <utils/assumptions.hpp>
#include <utils/invariants.hpp>
#include <utils/log.hpp>
#include <memory>
#include <sstream>
#include <iostream>

namespace gfx::detail {

struct BufferGL
{
    BufferGL();
    ~BufferGL();

    void compile(Buffer const* buffer);
    void release();

    void activate();

private:
    GLuint m_vao;
    GLuint m_ebo;
    std::vector<GLuint> m_vbos;
};

BufferGL::BufferGL()
    : m_vao{ 0 }
    , m_ebo{ 0 }
    , m_vbos{}
{}

BufferGL::~BufferGL()
{
    release();
}

void BufferGL::compile(Buffer const* const buffer)
{
    try
    {
        glGenVertexArrays(1, &m_vao);
        ASSUMPTION(glGetError() == 0U);

        glBindVertexArray(m_vao);
        ASSUMPTION(glGetError() == 0U);

        if (buffer->indices() != nullptr)
        {
            ASSUMPTION(
                buffer->primitive_type() == Buffer::POINT ||
                buffer->indices()->size() % (buffer->primitive_type() == Buffer::LINE ? 2 : 3) == 0
                );

            glGenBuffers(1, &m_ebo);
            ASSUMPTION(glGetError() == 0U);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
            ASSUMPTION(glGetError() == 0U);

            glBufferData(
                GL_ELEMENT_ARRAY_BUFFER,
                buffer->indices()->size() * sizeof(decltype(buffer->indices()->front())),
                buffer->indices()->data(),
                GL_STATIC_DRAW
                );
            ASSUMPTION(glGetError() == 0U);
        }
        else
        {
            ASSUMPTION(
                buffer->primitive_type() == Buffer::POINT ||
                buffer->positions()->size() % (buffer->primitive_type() == Buffer::LINE ? 2 : 3) == 0
                );
        }

        if (buffer->positions() != nullptr) {
            m_vbos.push_back(0);
            glGenBuffers(1, &m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            glBindBuffer(GL_ARRAY_BUFFER, m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            auto const& data = *buffer->positions();
            ASSUMPTION(!data.empty());

            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(decltype(data.front())), data.data(), GL_STATIC_DRAW);
            ASSUMPTION(glGetError() == 0U);

            GLuint const layout = ShaderGraph::VaryingPositionNode::default_layout;
            int constexpr num_elements = dim<std::remove_cvref<decltype(data)>::type::value_type>();
            int constexpr sizeof_element = sizeof(std::remove_cvref<decltype(data.front())>::type::value_type);

            glEnableVertexAttribArray(layout);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribPointer(layout, num_elements, GL_FLOAT, GL_FALSE, num_elements * sizeof_element, (void*)0);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribDivisor(layout, 0);
            INVARIANT(glGetError() == 0U);
        }

        if (buffer->normals() != nullptr)
        {
            m_vbos.push_back(0);
            glGenBuffers(1, &m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            glBindBuffer(GL_ARRAY_BUFFER, m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            auto const& data = *buffer->normals();
            ASSUMPTION(data.size() == buffer->positions()->size());

            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(decltype(data.front())), data.data(), GL_STATIC_DRAW);
            ASSUMPTION(glGetError() == 0U);

            GLuint const layout = ShaderGraph::VaryingNormalNode::default_layout;
            int constexpr num_elements = dim<std::remove_cvref<decltype(data)>::type::value_type>();
            int constexpr sizeof_element = sizeof(std::remove_cvref<decltype(data.front())>::type::value_type);

            glEnableVertexAttribArray(layout);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribPointer(layout, num_elements, GL_FLOAT, GL_FALSE, num_elements * sizeof_element, (void*)0);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribDivisor(layout, 0);
            INVARIANT(glGetError() == 0U);
        }

        if (buffer->tangents() != nullptr)
        {
            m_vbos.push_back(0);
            glGenBuffers(1, &m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            glBindBuffer(GL_ARRAY_BUFFER, m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            auto const& data = *buffer->tangents();
            ASSUMPTION(data.size() == buffer->positions()->size());

            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(decltype(data.front())), data.data(), GL_STATIC_DRAW);
            ASSUMPTION(glGetError() == 0U);

            GLuint const layout = ShaderGraph::VaryingTangentNode::default_layout;
            int constexpr num_elements = dim<std::remove_cvref<decltype(data)>::type::value_type>();
            int constexpr sizeof_element = sizeof(std::remove_cvref<decltype(data.front())>::type::value_type);

            glEnableVertexAttribArray(layout);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribPointer(layout, num_elements, GL_FLOAT, GL_FALSE, num_elements * sizeof_element, (void*)0);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribDivisor(layout, 0);
            INVARIANT(glGetError() == 0U);
        }

        if (buffer->texcoords() != nullptr)
        {
            m_vbos.push_back(0);
            glGenBuffers(1, &m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            glBindBuffer(GL_ARRAY_BUFFER, m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            auto const& data = *buffer->texcoords();
            ASSUMPTION(data.size() == buffer->positions()->size());

            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(decltype(data.front())), data.data(), GL_STATIC_DRAW);
            ASSUMPTION(glGetError() == 0U);

            GLuint const layout = ShaderGraph::VaryingTexcoordNode::default_layout;
            int constexpr num_elements = dim<std::remove_cvref<decltype(data)>::type::value_type>();
            int constexpr sizeof_element = sizeof(std::remove_cvref<decltype(data.front())>::type::value_type);

            glEnableVertexAttribArray(layout);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribPointer(layout, num_elements, GL_FLOAT, GL_FALSE, num_elements * sizeof_element, (void*)0);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribDivisor(layout, 0);
            INVARIANT(glGetError() == 0U);
        }

        if (buffer->weights() != nullptr)
        {
            m_vbos.push_back(0);
            glGenBuffers(1, &m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            glBindBuffer(GL_ARRAY_BUFFER, m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            auto const& data = *buffer->weights();
            ASSUMPTION(data.size() == buffer->positions()->size());

            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(decltype(data.front())), data.data(), GL_STATIC_DRAW);
            ASSUMPTION(glGetError() == 0U);

            GLuint const layout = ShaderGraph::VaryingWeightNode::default_layout;
            int constexpr num_elements = dim<std::remove_cvref<decltype(data)>::type::value_type>();
            int constexpr sizeof_element = sizeof(std::remove_cvref<decltype(data.front())>::type::value_type);

            glEnableVertexAttribArray(layout);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribPointer(layout, num_elements, GL_FLOAT, GL_FALSE, num_elements * sizeof_element, (void*)0);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribDivisor(layout, 0);
            INVARIANT(glGetError() == 0U);
        }

        if (buffer->colors() != nullptr)
        {
            m_vbos.push_back(0);
            glGenBuffers(1, &m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            glBindBuffer(GL_ARRAY_BUFFER, m_vbos.back());
            ASSUMPTION(glGetError() == 0U);

            auto const& data = *buffer->colors();
            ASSUMPTION(data.size() == buffer->positions()->size());

            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(decltype(data.front())), data.data(), GL_STATIC_DRAW);
            ASSUMPTION(glGetError() == 0U);

            GLuint const layout = ShaderGraph::VaryingColorNode::default_layout;
            int constexpr num_elements = dim<std::remove_cvref<decltype(data)>::type::value_type>();
            int constexpr sizeof_element = sizeof(std::remove_cvref<decltype(data.front())>::type::value_type);

            glEnableVertexAttribArray(layout);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribPointer(layout, num_elements, GL_FLOAT, GL_FALSE, num_elements * sizeof_element, (void*)0);
            ASSUMPTION(glGetError() == 0U);

            glVertexAttribDivisor(layout, 0);
            INVARIANT(glGetError() == 0U);
        }

        glBindVertexArray(0);
        ASSUMPTION(glGetError() == 0U);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        ASSUMPTION(glGetError() == 0U);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        ASSUMPTION(glGetError() == 0U);
    }
    catch(...)
    {
        release();
        throw;
    }
}

void BufferGL::release()
{
    while (!m_vbos.empty())
    {
        if (m_vbos.back() != 0)
        {
            glDeleteBuffers(1, &m_vbos.back());
            ASSUMPTION(glGetError() == 0U);
        }
        m_vbos.pop_back();
    }
    if (m_ebo != 0)
    {
        glDeleteBuffers(1, &m_ebo);
        ASSUMPTION(glGetError() == 0U);

        m_ebo = 0;
    }
    if (m_vao != 0)
    {
        glDeleteVertexArrays(1, &m_vao);
        ASSUMPTION(glGetError() == 0U);

        m_vao = 0;
    }
}

void BufferGL::activate()
{
    ASSUMPTION(m_vao != 0);
 
    glBindVertexArray(m_vao);
    ASSUMPTION(glGetError() == 0U);
}

}

namespace gfx {

Buffer::Buffer(std::string const& name, PrimitiveType primitive_type)
    : com::File(name)
    , m_primitive_type(primitive_type)
    , m_indices{ nullptr }
    , m_positions{ nullptr }
    , m_normals{ nullptr }
    , m_tangents{ nullptr }
    , m_texcoords{ nullptr }
    , m_weights{ nullptr }
    , m_colors{ nullptr }
    , m_aabb{ zero<vec3>(), zero<vec3>() }
    , m_gl_buffer{ nullptr }
{}

Buffer::~Buffer()
{
    delete m_gl_buffer;
    delete m_colors;
    delete m_weights;
    delete m_texcoords;
    delete m_tangents;
    delete m_normals;
    delete m_positions;
    delete m_indices;
}

void Buffer::insert_indices(IndicesVec const& data)
{
    ASSUMPTION(m_indices == nullptr && !data.empty());
    m_indices = new IndicesVec{ data.begin(), data.end() };
    on_change();
}

void Buffer::insert_positions(PositionsVec const& data)
{
    ASSUMPTION(m_positions == nullptr && !data.empty());
    m_positions = new PositionsVec{ data.begin(), data.end() };
    m_aabb = AABB::from_points(&m_positions->front(), &m_positions->front() + m_positions->size());
    on_change();
}

void Buffer::insert_normals(NormalsVec const& data)
{
    ASSUMPTION(m_normals == nullptr && !data.empty());
    m_normals = new NormalsVec{ data.begin(), data.end() };
    on_change();
}

void Buffer::insert_tangents(TangentsVec const& data)
{
    ASSUMPTION(m_tangents == nullptr && !data.empty());
    m_tangents = new TangentsVec{ data.begin(), data.end() };
    on_change();
}

void Buffer::insert_texcoords(TexcoordsVec const& data)
{
    ASSUMPTION(m_texcoords == nullptr && !data.empty());
    m_texcoords = new TexcoordsVec{ data.begin(), data.end() };
    on_change();
}

void Buffer::insert_weights(WeightsVec const& data)
{
    ASSUMPTION(m_weights == nullptr && !data.empty());
    m_weights = new WeightsVec{ data.begin(), data.end() };
    on_change();
}

void Buffer::insert_colors(ColorsVec const& data)
{
    ASSUMPTION(m_colors == nullptr && !data.empty());
    m_colors = new ColorsVec{ data.begin(), data.end() };
    on_change();
}

void Buffer::erase_indices()
{
    if (m_indices != nullptr)
    {
        delete m_indices;
        m_indices = nullptr;
        on_change();
    }
}

void Buffer::erase_positions()
{
    if (m_positions != nullptr)
    {
        delete m_positions;
        m_positions = nullptr;
        on_change();
    }
}

void Buffer::erase_normals()
{
    if (m_normals != nullptr)
    {
        delete m_normals;
        m_normals = nullptr;
        on_change();
    }
}

void Buffer::erase_tangents()
{
    if (m_tangents != nullptr)
    {
        delete m_tangents;
        m_tangents = nullptr;
        on_change();
    }
}

void Buffer::erase_texcoords()
{
    if (m_texcoords != nullptr)
    {
        delete m_texcoords;
        m_texcoords = nullptr;
        on_change();
    }
}

void Buffer::erase_weights()
{
    if (m_weights != nullptr)
    {
        delete m_weights;
        m_weights = nullptr;
        on_change();
    }
}

void Buffer::erase_colors()
{
    if (m_colors != nullptr)
    {
        delete m_colors;
        m_colors = nullptr;
        on_change();
    }
}

void Buffer::clear()
{
    erase_indices();
    erase_positions();
    erase_normals();
    erase_tangents();
    erase_texcoords();
    erase_weights();
    erase_colors();
}

void Buffer::activate() const
{
    ASSUMPTION(m_gl_buffer != nullptr);
    m_gl_buffer->activate();
}

void Buffer::on_change()
{
    delete m_gl_buffer;
    m_gl_buffer = nullptr;
    notify_content_changed();
}

void Buffer::compile()
{
    if (m_gl_buffer != nullptr)
        return;

    std::unique_ptr<detail::BufferGL> gl_buffer { std::make_unique<detail::BufferGL>() };
    gl_buffer->compile(this);
    m_gl_buffer = gl_buffer.release();
}

}
