# include <gfx/ui_generators.hpp>
# include <gfx/index.hpp>
# include <gfx/ui/ui_element.hpp>
# include <utility>
# include <utils/assumptions.hpp>

using namespace com;

namespace gfx {

    UiGenerators::UiGenerators() : com::Library { self_name() } {};
    UiGenerators::~UiGenerators() {};

    Button* UiGenerators::create_ui_button(std::string const& name, vec2i size, UiElement* parent, Folder* scene_folder, Folder* layer_folder, std::string const& text) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        Button* button;
        if (parent == nullptr)
            button = ui_system()->insert_element<Button>(name, scene_folder, layer_folder, size, text);
        else
            button = ui_system()->insert_child_element<Button>(name, parent, scene_folder, layer_folder, size, text);
        return button;
    }

    CheckBox * UiGenerators::create_ui_check_box(std::string const &name, vec2i size, UiElement* parent, Folder *scene_folder, Folder *layer_folder) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        CheckBox* check_box;
        if (parent == nullptr)
            check_box = ui_system()->insert_element<CheckBox>(name, scene_folder, layer_folder, size);
        else
            check_box = ui_system()->insert_child_element<CheckBox>(name, parent, scene_folder, layer_folder, size);
        return check_box;
    }

    Slider* UiGenerators::create_ui_slider(std::string const &name, vec2i size, Alignment alignment,
        UiElement* parent, Folder *scene_folder, Folder *layer_folder, scalar value
    ) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        Slider* slider;
        if (parent == nullptr)
            slider = ui_system()->insert_element<Slider>(name, scene_folder, layer_folder, size, value, alignment);
        else
            slider = ui_system()->insert_child_element<Slider>(name, parent, scene_folder, layer_folder, size, value, alignment);
        return slider;
    }

    ProgressBar * UiGenerators::create_ui_progress_bar(std::string const &name, vec2i size, UiElement *parent,
        Folder *scene_folder, Folder *layer_folder, scalar value
    ) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        ProgressBar* progress_bar;
        if (parent == nullptr)
            progress_bar = ui_system()->insert_element<ProgressBar>(name, scene_folder, layer_folder, size, value);
        else
            progress_bar = ui_system()->insert_child_element<ProgressBar>(name, parent, scene_folder, layer_folder, size, value);
        return progress_bar;
    }

    LineTextEdit* UiGenerators::create_ui_line_text_edit(std::string const &name, vec2i size, UiElement *parent,
        Folder *scene_folder, Folder *layer_folder, std::string text
    ) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        LineTextEdit* line_text_edit;
        if (parent == nullptr)
            line_text_edit = ui_system()->insert_element<LineTextEdit>(name, scene_folder, layer_folder, size, text);
        else
            line_text_edit = ui_system()->insert_child_element<LineTextEdit>(name, parent, scene_folder, layer_folder, size, text);
        return line_text_edit;
    }

    StaticText * UiGenerators::create_ui_static_text(std::string const &name, vec2i size, UiElement *parent,
        Folder *scene_folder, Folder *layer_folder, std::string text, bool text_wrap
    ) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        StaticText* static_text;
        if (parent == nullptr)
            static_text = ui_system()->insert_element<StaticText>(name, scene_folder, layer_folder, size, text, text_wrap);
        else
            static_text = ui_system()->insert_child_element<StaticText>(name, parent, scene_folder, layer_folder, size, text, text_wrap);
        return static_text;
    }

    TabBar * UiGenerators::create_ui_tab_bar(std::string const &name, vec2i size, UiElement *parent,
        Folder *scene_folder, Folder *layer_folder, vec2i tab_size
    ) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        TabBar* tab_bar;
        if (parent == nullptr)
            tab_bar = ui_system()->insert_element<TabBar>(name, scene_folder, layer_folder, size, tab_size);
        else
            tab_bar = ui_system()->insert_child_element<TabBar>(name, parent, scene_folder, layer_folder, size, tab_size);
        return tab_bar;
    }

    ListBox * UiGenerators::create_ui_list_box(std::string const &name, vec2i size, UiElement *parent,
        Folder *scene_folder, Folder *layer_folder, vec2i item_size
    ) {
        ASSUMPTION(scene_folder->is_under(ui_system()->uis()));
        ASSUMPTION(layer_folder->is_under(scene_folder));
        ListBox* list_box;
        if (parent == nullptr)
            list_box = ui_system()->insert_element<ListBox>(name, scene_folder, layer_folder, size, item_size);
        else
            list_box = ui_system()->insert_child_element<ListBox>(name, parent, scene_folder, layer_folder, size, item_size);
        return list_box;
    }

}