#ifndef GFX_VIEWPORT_HPP_INCLUDED
#   define GFX_VIEWPORT_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>

namespace gfx {

struct Viewport final : public com::File
{
    Viewport(std::string const& name);
    ~Viewport() override {}

    vec2 const& bottom_left() const { return m_bottom_left; }
    vec2 const& top_right() const { return m_top_right; }
    bool relative_to_window() const { return m_relative; }
    vec2 size() const { return m_top_right - m_bottom_left; }
    vec2 bottom_left_absolute() const;
    vec2 top_right_absolute() const;
    vec2 size_absolute() const;

    vec2 point_to_window(vec2 const& p) const;

    void set_bottom_left(vec2 const& bottom_left) { m_bottom_left = bottom_left; }
    void set_top_right(vec2 const& top_right) { m_top_right = top_right; }
    void set_relative_to_window(bool const state) { m_relative = state; }

    void activate() const;

private:
    vec2 m_bottom_left;
    vec2 m_top_right;
    bool m_relative;
};

}

#endif
