#ifndef GFX_LIGHT_SYSTEM_HPP_INCLUDED
#   define GFX_LIGHT_SYSTEM_HPP_INCLUDED

#   include <gfx/light.hpp>
#   include <com/library.hpp>
#   include <utils/assumptions.hpp>

namespace com { struct Frame; }

namespace gfx {

struct LightSystem final : public com::Library
{
    static inline std::string self_name() { return "light_system.lib"; }

    LightSystem();
    ~LightSystem() override;

    static inline std::string light_props_name() { return Light::self_name(); }
    static inline std::string frame_link_name() { return "frame.link"; }
    static inline std::string shadow_casters_folder_name() { return "shadow_casters"; }

    com::Folder* lights() const { return m_lights; }

    Light* get_light_props(com::Folder* light) const;
    com::Frame* get_frame(com::Folder* light) const;
    com::Folder* get_shadow_casters(com::Folder* light) const;

    template<typename LightType, class... ParameterTypes>
    com::Folder* insert_light(com::ContextPath const& path, com::Frame* frame, ParameterTypes... args);
    void erase_light(com::Folder* light);

    void insert_shadow_caster(com::Folder* light, com::Folder* object);
    void erase_shadow_caster(com::Folder* light, com::Folder* object);
    bool has_shadow_caster(com::Folder* light, com::Folder* object) const;

protected:

    void initialize() override;
    void release() override;

private:
    com::Folder* m_lights;
};

template<typename LightType, class... ParameterTypes>
com::Folder* LightSystem::insert_light(com::ContextPath const& path, com::Frame* frame, ParameterTypes... args)
{
    com::Folder* const light = lights()->push_back_folders(path);
    ASSUMPTION(light->is_under(lights()));
    light->push_back<com::Folder>(shadow_casters_folder_name());
    light->push_back<com::Link>(frame_link_name(), frame);
    light->push_back<LightType>(args...);
    return light;
}

}

#endif
