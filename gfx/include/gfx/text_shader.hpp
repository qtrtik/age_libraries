#ifndef GFX_TEXT_SHADER_HPP_INCLUDED
#define GFX_TEXT_SHADER_HPP_INCLUDED

#include <gfx/shader_graph.hpp>
#include <gfx/shader.hpp>

namespace gfx {

struct TextShader final : public Shader {
    TextShader(const std::string &name, lighting l, std::string vertex_shader, std::string fragment_shader);

    void compile() override;

    void activate() const override;

    void on_content_changed(File *sender) override;

    void add_texture(ShaderGraph::TextureNode *tn);

  private:
    void bind_textures() const;

    std::string m_vertex_shader, m_fragment_shader;
    std::vector<ShaderGraph::TextureNode*> m_textures;
};

} // namespace gfx

#endif