#ifndef GFX_LIGHT_HPP_INCLUDED
#define GFX_LIGHT_HPP_INCLUDED

#include <com/context.hpp>
#include <math/math.hpp>

namespace gfx {

struct Light : public com::File {
    enum class type_t { ambient, directional, point };

    static inline std::string self_name() { return "light"; }

    Light(vec3 const &color);
    ~Light() override;

    vec3 const &color() const { return m_color; }
    void set_color(vec3 const &color) { m_color = color; }

    virtual type_t type() const = 0;

  private:
    vec3 m_color;
};

struct AmbientLight final : public Light {
    struct glsl {
        glsl() = delete;

        static std::string type() { return "AmbientLight"; }
        static std::string u_name() { return "ambientLights"; }
        static std::string color() { return ".color"; }
        static std::string nr_lights() { return "nrAmbientLights"; }
    };

    AmbientLight(vec3 const &color);
    ~AmbientLight() override;

    type_t type() const override { return type_t::ambient; }

    /**
     * Used to add lighting logic utilities to the fragment shader.
     * @return helper function shader code.
     */
    static std::string declarations();
    static std::string struct_decl();
    static std::string function_decl();

    /**
     * Returns a string representing a shader function call computing light contribution
     * with the arguments passed to it. See `declarations`.
     * Arguments are shader code expressions passed to the function as arguments.
     * @return function call.
     */
    static std::string function_call(const std::string &_light, const std::string &_m_ambient);
};

/**
 * The direction is the -Z axis of the associated com::Frame.
 */
struct DirectionalLight final : public Light {
    struct glsl {
        glsl() = delete;

        static std::string type() { return "DirectionalLight"; }
        static std::string u_name() { return "directionalLights"; }
        static std::string color() { return ".color"; }
        static std::string direction() { return ".direction"; }
        static std::string nr_lights() { return "nrDirectionalLights"; }
    };

    DirectionalLight(vec3 const &color);
    ~DirectionalLight() override;

    type_t type() const override { return type_t::directional; }

    /**
     * Used to add lighting logic utilities to the fragment shader.
     * @return helper function shader code.
     */
    static std::string declarations();
    static std::string struct_decl();
    static std::string function_decl();

    /**
     * Returns a string representing a shader function call computing light contribution
     * with the arguments passed to it. See `declarations`.
     * Arguments are shader code expressions passed to the function as arguments.
     * @return function call.
     */
    static std::string function_call(const std::string &_light,
                                     const std::string &_m_diffuse, const std::string &_m_specular,
                                     const std::string &_normal, const std::string &_view_dir);
};

/**
 * The position is the origin of the associated com::Frame.
 * The intensity decays quadraticaly.
 */
struct PointLight final : public Light {
    struct glsl {
        glsl() = delete;

        static std::string type() { return "PointLight"; }
        static std::string u_name() { return "pointLights"; }
        static std::string color() { return ".color"; }
        static std::string position() { return ".position"; }
        static std::string atten_const() { return ".atten_const"; }
        static std::string atten_lin() { return ".atten_lin"; }
        static std::string atten_quad() { return ".atten_quad"; }
        static std::string radius() { return ".radius"; }
        static std::string nr_lights() { return "nrPointLights"; }
    };

    PointLight(vec3 const &color, float atten_const, float atten_lin, float atten_quad);
    ~PointLight() override;

    float radius() const { return m_radius; }
    float atten_const() const { return m_atten_const; }
    float atten_lin() const { return m_atten_lin; }
    float atten_quad() const { return m_atten_quad; }

    type_t type() const override { return type_t::point; }

    /**
     * Used to add lighting logic utilities to the fragment shader.
     * @return helper function shader code.
     */
    static std::string declarations();
    static std::string struct_decl();
    static std::string function_decl();

    /**
     * Returns a string representing a shader function call computing light contribution
     * with the arguments passed to it. See `declarations`.
     * Arguments are shader code expressions passed to the function as arguments.
     * @return function call.
     */
    static std::string function_call(const std::string &_light,
                                     const std::string &_m_diffuse, const std::string &_m_specular,
                                     const std::string &_normal, const std::string &_frag_pos,
                                     const std::string &_view_dir);

  private:
    float m_radius, m_atten_const, m_atten_lin, m_atten_quad;
};

} // namespace gfx

#endif
