#ifndef GFX_FONT_HPP_INCLUDED
# define GFX_FONT_HPP_INCLUDED
# include <math/math_scalar_type.hpp>
# include <filesystem>
# include <map>
#include <stack>

namespace gfx {
struct UiElement;

struct Material;

    struct Character {
        vec2 size;
        vec2 bearing;
        int offset_x;
        signed long advance;
    };

    struct FontProps : com::File {
        enum FONT_STATE {
            LOADED,
            LOADING,
            ERROR
        };

        FontProps(std::string const &name, size_t font_size);
        ~FontProps() override;

        void initialize() override;

        void set_state(FONT_STATE new_state) { state = new_state; }
        scalar get_minimal_advance() { return minimal_advance; }
        void on_content_changed(com::File *sender) override;

        FONT_STATE state{LOADING};
        std::string const& m_font_name;
        size_t m_font_size{};
        size_t num_glyphs{};
        size_t max_height{};
        scalar atlas_width{};
        scalar atlas_height{};
        scalar minimal_advance{10.0f};
        std::map<uint32_t, Character> character_props;
    };

    struct CursorProps {
        scalar cursorX{0.0f};
        scalar prevCursorX{0.0f};
        scalar cursorY{0.0f};
    };

    struct FontLoader : com::Folder {
        static inline std::string self_name() { return "font_loader"; }
        static inline std::string font_name(std::string const &name, size_t font_size) { return name + "_" + std::to_string(font_size); }
        static inline std::string font_regular_name(std::string const& name, size_t font_size) { return font_name(name + "-Regular", font_size); }
        static inline std::string font_bold_name(std::string const& name, size_t font_size) { return font_name(name + "-Bold", font_size); }
        static inline std::string font_italic_name(std::string const& name, size_t font_size) { return font_name(name + "-Italic", font_size); }
        static inline std::string font_regular_suffix() { return "-Regular"; }
        static inline std::string font_bold_suffix() { return "-Bold"; }
        static inline std::string font_italic_suffix() { return "-Italic"; }
        static inline std::string font_properties_file_name(const std::string& f_name, size_t font_size) { return font_name(f_name, font_size) + "_properties.json"; }

        FontLoader();
        ~FontLoader();

        bool is_font_loaded(std::string const& font_name, size_t font_size);
        bool is_font_loaded(FontProps *props);
        FontProps* get_font(std::string const &name, size_t font_size) { return find<FontProps>(font_name(name, font_size)); }

        FontProps* load_font(com::ContextPath const &path, std::string const &name, size_t font_size);

        FontProps* load_font_for_element(UiElement* element, std::string const& f_name, size_t font_size);

        Material* create_material_for_font(std::string const& name, size_t font_size);
        Material* get_material_for_font(std::string const &name, size_t font_size);

        std::map<std::string, std::list<UiElement*>> m_font_requests;

    protected:
        void on_content_changed(com::File *sender) override;
        void notify_font_requests(std::string const &name) const;

    };

}

#endif
