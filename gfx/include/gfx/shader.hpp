#ifndef GFX_SHADER_HPP_INCLUDED
#define GFX_SHADER_HPP_INCLUDED

#include <com/context.hpp>
#include <gfx/light.hpp>
#include <math/math.hpp>
#include <memory>
#include <osi/opengl.hpp>
#include <string>
#include <utils/invariants.hpp>

namespace gfx::detail {
struct ShaderGL {
    ShaderGL();
    ~ShaderGL();

    void compile(std::string const &vertex_shader, std::string const &fragment_shader);
    void release();
    void release_shaders();

    void activate();

    GLuint program_id() const { return m_program; }

    bool has_uniform(std::string const &var_name) const;

  private:
    GLuint m_vertex_shader;
    GLuint m_fragment_shader;
    GLuint m_program;
};
} // namespace gfx::detail

namespace gfx {
struct ShaderSystem;

struct Shader : public com::File {
#ifdef MEASURING_FPS
    static constexpr unsigned max_point_lights = 1000;
#else
    static constexpr unsigned max_point_lights = 20;
#endif
    static constexpr unsigned max_directional_lights = 1;
    static constexpr unsigned max_ambient_lights = 1;

    enum lighting { UNLIT, LIT, DEFERRED };

    Shader(const std::string &name, lighting l);

    virtual void compile() = 0;

    void set_uniform(const std::string& name, auto value) const {
        set_uniform(m_gl_shader.get(), name, value);
    }

    bool maybe_set_uniform(const std::string& name, auto value) const {
        if (!m_gl_shader->has_uniform(name))
            return false;
        set_uniform(name, value);
        return true;
    }

    /**
     * Sets the view matrix, projection matrix, and camera position uniforms, if present.
     * @param view view matrix
     * @param projection projection matrix
     * @param camera_pos camera position
     */
    void set_camera_uniforms(glm::mat4 view, glm::mat4 projection, glm::vec3 camera_pos) const;

    /**
     * Sets model and normal matrix uniforms, if present.
     * @param model model matrix
     * @param normal normal matrix
     */
    void set_model_uniforms(glm::mat4 model, glm::mat4 normal) const;

    /**
     * Sets time uniforms, if present.
     * @param time
     */
    void set_time_uniforms(int time) const;

    inline static const std::string model_matrix_uniform = "model";
    inline static const std::string view_matrix_uniform = "view";
    inline static const std::string projection_matrix_uniform = "projection";
    inline static const std::string normal_matrix_uniform = "normal_matrix";
    inline static const std::string camera_pos_uniform = "camera_pos";
    inline static const std::string background_color_uniform = "background_color";
    inline static const std::string time_uniform_frag = "time_frag";
    inline static const std::string time_uniform_vert = "time_vert";

    lighting lighting_type() const { return m_lighting_type; }
    detail::ShaderGL *gl_shader() const { return m_gl_shader.get(); }

  protected:
    void compile(std::string const &vertex_shader, std::string const &fragment_shader);

    void on_change();

  private:
    friend struct ShaderSystem;
    friend struct Material;
    friend struct Renderer;

    virtual void activate() const = 0;

    /**
     * Sets lighting uniforms in the shader.
     * @param lights lights folder of the currently rendered object.
     */
    void set_lights(const com::Folder &lights) const;

    /**
     * Sets lighting uniforms in the shader. Used in deferred rendering.
     * @param lights lights folders of the currently rendered objects.
     * @returns number of ligths per type: [ambient, directional, point]
     */
    std::tuple<int,int,int> set_lights(const std::vector<const com::Folder *> &lights) const;

    void set_light(const Light &light, int &point_lights, int &dir_lights, int &amb_lights) const;

    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, int value);
    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, scalar value);
    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, vec2 const &value);
    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, vec3 const &value);
    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, vec4 const &value);
    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, mat2x2 const &value);
    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, mat3x3 const &value);
    static void set_uniform(detail::ShaderGL *shader, std::string const &var_name, mat4x4 const &value);

    lighting m_lighting_type;
    std::unique_ptr<detail::ShaderGL> m_gl_shader;
};

} // namespace gfx

#endif
