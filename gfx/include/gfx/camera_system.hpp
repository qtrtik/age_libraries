#ifndef GFX_CAMERA_SYSTEM_HPP_INCLUDED
#   define GFX_CAMERA_SYSTEM_HPP_INCLUDED

#   include <gfx/camera.hpp>
#   include <com/library.hpp>
#   include <math/math.hpp>

namespace com { struct Frame; }

namespace gfx {

struct CameraSystem final : public com::Library
{
    static inline std::string self_name() { return "camera_system.lib"; }
    static inline std::string frame_link_file_name() { return "frame.link"; }

    CameraSystem();
    ~CameraSystem() override;

    com::Folder* cameras() { return m_cameras; }

    Camera* insert_camera(com::ContextPath const& path);
    void erase_camera(com::Folder* camera_folder);
    com::Frame *get_frame(com::Folder* camera_folder);

    void set_frame(com::Folder* camera_folder, com::Frame* frame);
    void set_frame(com::Frame* const frame) { set_frame(default_camera_folder(), frame); }

    Camera* default_camera() { return m_default_camera; }
    com::Folder* default_camera_folder() { return m_default_camera->folder(); }

    Camera* active_camera() const { return m_active_camera; }
    com::Folder* active_camera_folder() const { return m_active_camera->folder(); }
    com::Frame* active_frame() const;

    void activate_camera(com::Folder* camera_folder);
    void activate_default_camera() { activate_camera(default_camera_folder()); }

protected:

    void initialize() override;
    void release() override;

private:
    com::Folder* m_cameras;
    Camera* m_default_camera;
    Camera* m_active_camera;
};

}

#endif
