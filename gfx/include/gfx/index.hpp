#ifndef GFX_INDEX_HPP_INCLUDED
#define GFX_INDEX_HPP_INCLUDED

#   include <gfx/background_system.hpp>
#   include <gfx/viewport_system.hpp>
#   include <gfx/camera_system.hpp>
#   include <gfx/shader_system.hpp>
#   include <gfx/buffer_system.hpp>
#   include <gfx/buffer_generators.hpp>
#   include <gfx/material_system.hpp>
#   include <gfx/object_system.hpp>
#   include <gfx/light_system.hpp>
#   include <gfx/renderer.hpp>
#   include <com/context.hpp>
#   include <gfx/ui_system.hpp>
#   include <gfx/ui_generators.hpp>

namespace gfx {

struct Index final {
    static Index const &instance();

    com::Folder* root() const { return m_root; }
    BackgroundSystem* background_system() const { return m_background_system; }
    ViewportSystem* viewport_system() const { return m_viewport_system; }
    CameraSystem* camera_system() const { return m_camera_system; }
    ShaderSystem* shader_system() const { return m_shader_system; }
    MaterialSystem *material_system() const { return m_material_system; }
    BufferSystem* buffer_system() const { return m_buffer_system; }
    BufferGenerators* buffer_generators() const { return m_buffer_generators; }
    UiSystem* ui_system() const { return m_ui_system; }
    UiGenerators* ui_generators() const { return m_ui_generators; }
    ObjectSystem* object_system() const { return m_object_system; }
    LightSystem* light_system() const { return m_light_system; }
    Renderer* renderer() const { return m_renderer; }
    FontLoader* font_loader() const { return m_font_loader; }

  private:
    Index();
    Index(Index const &) = delete;
    Index(Index &&) = delete;
    Index &operator=(Index const &) const = delete;
    Index &operator=(Index &&) const = delete;

    com::Folder* m_root;
    BackgroundSystem* m_background_system;
    ViewportSystem* m_viewport_system;
    CameraSystem* m_camera_system;
    ShaderSystem* m_shader_system;
    MaterialSystem *m_material_system;
    BufferSystem* m_buffer_system;
    BufferGenerators* m_buffer_generators;
    UiSystem* m_ui_system;
    UiGenerators* m_ui_generators;
    ObjectSystem* m_object_system;
    LightSystem* m_light_system;
    Renderer* m_renderer;
    FontLoader* m_font_loader;
};

inline Index const &index() { return Index::instance(); }

inline com::Folder* root() { return index().root(); }
inline BackgroundSystem* background_system() { return index().background_system(); }
inline ViewportSystem* viewport_system() { return index().viewport_system(); }
inline CameraSystem* camera_system() { return index().camera_system(); }
inline ShaderSystem* shader_system() { return index().shader_system(); }
inline MaterialSystem *material_system() { return index().material_system(); }
inline BufferSystem* buffer_system() { return index().buffer_system(); }
inline BufferGenerators* buffer_generators() { return index().buffer_generators(); }
inline UiSystem* ui_system() { return index().ui_system(); }
inline UiGenerators* ui_generators() { return index().ui_generators(); }
inline ObjectSystem* object_system() { return index().object_system(); }
inline LightSystem* light_system() { return index().light_system(); }
inline Renderer* renderer() { return index().renderer(); }
inline FontLoader* font_loader() { return index().font_loader(); }

} // namespace gfx

#endif
