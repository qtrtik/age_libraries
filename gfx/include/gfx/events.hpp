#ifndef GFX_EVENTS_INCLUDED_HPP
#define GFX_EVENTS_INCLUDED_HPP

namespace gfx {
    enum MouseEvent {
        LEFT,
        RIGHT,
        MIDDLE
    };
}

#endif