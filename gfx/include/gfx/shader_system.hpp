#ifndef GFX_SHADER_SYSTEM_HPP_INCLUDED
#define GFX_SHADER_SYSTEM_HPP_INCLUDED

#include <com/library.hpp>
#include <gfx/shader.hpp>
#include <math/math.hpp>

namespace gfx {

struct ShaderSystem final : public com::Library {
    static inline std::string self_name() { return "shader_system.lib"; }

    ShaderSystem();
    ~ShaderSystem() override;

    com::Folder *shaders() { return m_shaders; }

    Shader *active_shader() const { return dynamic_cast<Shader *>(m_active_shader->target()); }
    com::Link *active_shader_link() const { return m_active_shader; }
    void set_active_shader(Shader *shader);

    template <class ShaderType, class... Args>
    ShaderType *insert_shader(com::ContextPath const &sub_folders, const std::string &name, Args... args);

    void erase_shader(Shader *shader);

    /**
     * Generates a default unlit shader with a single color uniform node named 'color'.
     * @return a default unlit shader
     */
    Shader *default_shader();

    /**
     * Generates an unlit shader with vertex colors.
     * @return an unlit shader with vertex colors
     */
    Shader *forward_unlit_vertex_color_shader();

    /**
     * Generates a shader used in deferred rendering as a lighting pass.
     * @return lighting pass shader.
     */
    Shader *deferred_lighting_shader();

    /**
     * Generates a shader used in deferred rendering as a lighting pass
     * with light volumes.
     * @return lighting pass shader.
     */
    Shader *light_volumes_shader();

    /**
     * Generates a shader used in deferred rendering as a lighting pass.
     * @return lighting pass shader.
     */
    Shader *deferred_lighting_shader_PBR();

    /**
     * Generates a shader used in deferred rendering as a lighting pass
     * with light volumes.
     * @return lighting pass shader.
     */
    Shader *light_volumes_shader_PBR();

  protected:
    void initialize() override;
    void release() override;

  private:
    com::Folder *m_shaders;
    Shader *m_shader_default, *m_forward_unlit_vertex_color_shader, *m_deferred_lighting_shader, *m_deferred_lighting_shader_PBR,
           *m_light_volumes_shader, *m_light_volumes_shader_PBR;
    com::Link *m_active_shader;
};

template <class ShaderType, class... Args>
ShaderType *ShaderSystem::insert_shader(com::ContextPath const &sub_folders, const std::string &name, Args... args) {
    com::Folder *const folder = shaders()->push_back_folders(sub_folders);
    ASSUMPTION(folder->is_under(shaders()));
    com::ContextItem *const item = folder->find(name);
    if (item == nullptr)
        return folder->push_back<ShaderType>(name, args...);

    auto shader = dynamic_cast<ShaderType *>(item);
    ASSUMPTION(shader != nullptr);
    return shader;
}

} // namespace gfx

#endif
