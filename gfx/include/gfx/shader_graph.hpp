#ifndef GFX_SHADER_GRAPH_HPP_INCLUDED
#define GFX_SHADER_GRAPH_HPP_INCLUDED

#include <lfs/resource_handle.hpp>
#include <gfx/shader.hpp>
#include <map>
#include <set>
#include <variant>

namespace gfx {

struct ShaderGraph final : public Shader {
    enum class Stage { VERTEX, FRAGMENT, ANY };

    enum transparency { OPAQUE, PARTIAL, THRESHOLD };

    enum lighting_model { PHONG, PBR };

    // ABSTRACT NODES
    // ===================================
    struct MasterNode;
    struct InputNode;
    struct InputVaryingNode;
    struct BinaryOperatorNode;

    // MASTER NODES
    // ===================================
    struct MasterNodeUnlit;
    struct MasterNodeLit;
    struct MasterNodeDeferred;
    struct MasterNodePBR;
    struct MasterNodeDeferredPBR;

    // INPUT NODES
    // ===================================
    struct UniformNode;
    struct TimeNode;

    struct TextureNode;

    struct ConstantNode;
    struct ErrorColorNode;

    struct VaryingPositionNode;
    struct VaryingNormalNode;
    struct VaryingTangentNode;
    struct VaryingTexcoordNode;
    struct VaryingWeightNode;
    struct VaryingColorNode;

    struct WorldPositionNode;

    // OPERATOR NODES
    // ===================================
    struct AddNode;
    struct SubtractNode;
    struct MultiplyNode;
    struct DivideNode;
    struct MatrixVectorMultiplyNode;
    struct ScalarMultiplyNode;

    struct CustomFunctionNode;

    // VECTOR UTILITY NODES
    // ===================================
    struct ExpandVectorNode;
    struct ComposeVectorNode;
    struct DecomposeVectorNode;

    // NOISE GENERATION NODES
    // ===================================
    struct PerlinNoise3DNode;

    // LOGIC NODES
    // ===================================
    struct StepNode;
    struct ConditionNode;

    // MATH NODES
    // ===================================
    struct SineNode;
    struct CosineNode;

    struct DeclAndCode {
        std::vector<std::string> decl;
        std::vector<std::string> code;
    };

    using StagesCode = std::unordered_map<Stage, DeclAndCode>;

    struct Node {
        struct DataType {
            enum ElementType { BOOL, INT, SCALAR, VEC2, VEC3, VEC4, MAT2X2, MAT3X3, MAT4X4 };
            using elem_variant_t = std::variant<bool, int, scalar, vec2, vec3, vec4, mat2x2, mat3x3, mat4x4>;

            static std::string to_string(ElementType type) {
                switch (type) {
                case BOOL:
                    return "bool";
                case INT:
                    return "int";
                case SCALAR:
                    return "float";
                case VEC2:
                    return "vec2";
                case VEC3:
                    return "vec3";
                case VEC4:
                    return "vec4";
                case MAT2X2:
                    return "mat2";
                case MAT3X3:
                    return "mat3";
                case MAT4X4:
                    return "mat4";
                default: {
                    UNREACHABLE();
                    return "";
                }
                }
            }

            // clang-format off
            template<enum ElementType, typename Unused = void> struct ToValueType;
            template<typename Unused> struct ToValueType<BOOL, Unused> { using value_type = bool; };
            template<typename Unused> struct ToValueType<INT, Unused> { using value_type = int; };
            template<typename Unused> struct ToValueType<SCALAR, Unused> { using value_type = scalar; };
            template<typename Unused> struct ToValueType<VEC2, Unused> { using value_type = vec2; };
            template<typename Unused> struct ToValueType<VEC3, Unused> { using value_type = vec3; };
            template<typename Unused> struct ToValueType<VEC4, Unused> { using value_type = vec4; };
            template<typename Unused> struct ToValueType<MAT2X2, Unused> { using value_type = mat2x2; };
            template<typename Unused> struct ToValueType<MAT3X3, Unused> { using value_type = mat2x2; };
            template<typename Unused> struct ToValueType<MAT4X4, Unused> { using value_type = mat4x4; };

            template<typename ValueType, typename Unused = void> struct ToElementType;
            template<typename Unused> struct ToElementType<bool, Unused> { static constexpr ElementType value = BOOL; };
            template<typename Unused> struct ToElementType<int, Unused> { static constexpr ElementType value = INT; };
            template<typename Unused> struct ToElementType<scalar, Unused> { static constexpr ElementType value = SCALAR; };
            template<typename Unused> struct ToElementType<vec2, Unused> { static constexpr ElementType value = VEC2; };
            template<typename Unused> struct ToElementType<vec3, Unused> { static constexpr ElementType value = VEC3; };
            template<typename Unused> struct ToElementType<vec4, Unused> { static constexpr ElementType value = VEC4; };
            template<typename Unused> struct ToElementType<mat2x2, Unused> { static constexpr ElementType value = MAT2X2; };
            template<typename Unused> struct ToElementType<mat3x3, Unused> { static constexpr ElementType value = MAT2X2; };
            template<typename Unused> struct ToElementType<mat4x4, Unused> { static constexpr ElementType value = MAT4X4; };
            // clang-format on

            friend bool operator==(DataType const &, DataType const &) = default;

            ElementType element_type;
        };

        struct SlotDefinition {
            std::string name;
            std::string description;
            DataType type;

            std::string type_name() const;
        };

        struct SlotConnection {
            Node *node;
            std::size_t index;

            bool connected() const { return node != nullptr; }

            friend bool operator==(const SlotConnection &, const SlotConnection &) = default;
        };

        struct InputSlot {
            explicit InputSlot(SlotDefinition definition)
                : definition{ std::move(definition) }, connection{ nullptr, 0 } {}

            bool connected() const { return connection.node != nullptr; }

            std::string type_name() const { return definition.type_name(); }

            SlotDefinition definition;
            SlotConnection connection;
        };

        struct OutputSlot {
            explicit OutputSlot(SlotDefinition definition) : definition{ std::move(definition) } {}

            std::string type_name() const { return definition.type_name(); }

            SlotDefinition definition;
            std::vector<SlotConnection> connections;
        };

        Node(std::string const &name, std::string const &description, std::vector<SlotDefinition> const &inputs,
             std::vector<SlotDefinition> const &outputs);
        virtual ~Node() = default;

        std::string const &name() const { return m_name; }
        std::string const &description() const { return m_description; }
        std::vector<InputSlot> const &inputs() const { return m_inputs; }
        std::vector<OutputSlot> const &outputs() const { return m_outputs; }

        /**
         * Nodes for which this evaluates to true should not be touched by the end user.
         * @return boolean indicating whether the node is built in.
         */
        virtual bool is_built_in() const { return false; }

      private:
        /**
         * Adds code of that implements the functionality oh this node.
         * @param code stores the code here.
         * @param stage the shader stage in which to generate the code.
         */
        virtual void add_code(StagesCode &code, Stage stage) const = 0;

        using visited_t = std::map<Stage, std::set<const Node *>>;
        /**
         * Traverses the shader graph tree with the DFS strategy calling add_code in post-order.
         * @param code the resulting shader code.
         * @param stage the shader stage in which to generate the code.
         * @param visited keeps track of visited nodes to avoid code duplication.
         */
        void add_code_helper(StagesCode &code, Stage stage, visited_t &visited) const;

        /**
         * \brief Determines the stage in which the input code should be generated
         * \param out the stage in which the output of this node is needed
         * \param in the index of the input slot
         * \return input code shader stage
         */
        virtual Stage output_to_input_stage(Stage out, std::size_t in) const;

        std::string variable_name(std::size_t out = 0) const;

        /**
         * \brief Shorthand for getting variable_name of an input slot
         * \param in input slot index
         * \return variable_name of the input slot
         */
        std::string input_name(std::size_t in) const;

        /**
         * Creates an initializer for the variable of an output of a node.
         * @param value initial value
         * @param out output slot
         * @return initializer (`type name = value`)
         */
        std::string make_initializer(const std::string &value, std::size_t out = 0) const;

        /**
         * Passes a value from the vertex shader to the fragment shader.
         * @param code shader code
         * @param out node output slot to be passed
         * @param value value to pass
         */
        void pass_to_fragment(StagesCode &code, std::size_t out, const std::string &value) const;

        std::string interface_name() const;

        friend struct ShaderGraph;

        std::string m_name;
        std::string m_description;
        std::vector<InputSlot> m_inputs;
        std::vector<OutputSlot> m_outputs;
    };

    struct MasterNode : public Node {
        using Node::Node;
    };

    /**
     * \brief Represents a node with a single output and no inputs.
     * Used to expose shader variables/uniforms to the shader graph.
     */
    struct InputNode : public Node {
        explicit InputNode(const SlotDefinition &output) : Node(output.name, output.description, {}, { output }) {}

        std::string stage_name(Stage stage) const;
    };

    struct UniformNode : public InputNode {
        UniformNode(const std::string &name, DataType::ElementType e)
          : InputNode({ name, name, { e } }) {}

        void add_code(StagesCode &code, Stage stage) const override;

        template <typename T>
        void set_uniform(T const &value) const {
            ASSUMPTION(m_outputs[0].definition.type.element_type == DataType::ToElementType<T>::value);
            ASSUMPTION(m_shader->gl_shader() != nullptr);

            bool in_vert = m_shader->maybe_set_uniform(stage_name(Stage::VERTEX), value);
            bool in_frag = m_shader->maybe_set_uniform(stage_name(Stage::FRAGMENT), value);
            ASSUMPTION(in_vert || in_frag);
        }

        DataType output_type() const { return m_outputs[0].definition.type; }

      private:
        friend struct ShaderGraph;
        ShaderGraph *m_shader = nullptr;
    };

    struct TextureNode : public Node {
        TextureNode(const com::ContextPath &tex_file, DataType::ElementType out_t,
            std::string override_sampler_name = "");

        void add_code(StagesCode &code, Stage stage) const override;

        void activate(int texture_unit) const;

        unsigned id() const { return m_id; }

        ~TextureNode() override;

        enum inputs { tex_coord };

      private:
        std::string sampler_name() const;

        void load_texture();

        void generate_texture();

        friend struct ShaderGraph;
        friend struct TextShader;

        unsigned m_id;
        Shader *m_shader;
        lfs::ResourceHandle *m_handle;
        com::ContextPath m_tex_file;
        std::string m_override_sampler_name;
    };

    ShaderGraph(std::string const &name, lighting l, lighting_model lm = PHONG, transparency t = OPAQUE);

    Node *root() const { return m_root; }
    std::vector<UniformNode *> const &inputs_uniform() const { return m_uniforms; }

    template <class NodeType, typename... Args>
    NodeType *insert(Args... args);
    void erase(Node *node);

    Node *connect(Node *dst_node, std::size_t input_slot, Node *src_node, std::size_t output_slot);
    Node *disconnect(Node *node, std::size_t const &input_slot);
    Node *disconnect(Node *node);

  protected:
    void on_content_changed(com::File *sender) override;

  private:
    friend struct ShaderSystem;
    friend struct Material;
    friend struct Renderer;

    void compile() override;

    void activate() const override;

    void check_for_cycles() const;
    static void check_for_cycles_rec(const Node *node, std::unordered_set<const Node *> &visited,
                                     std::unordered_set<const Node *> &in_branch);

    /**
     * \brief Binds textures in 'm_textures' to texture units and sets corresponding sampler uniforms
     */
    void bind_textures() const;

    Node *m_root;
    std::vector<std::unique_ptr<Node>> m_nodes;
    std::vector<UniformNode *> m_uniforms;
    std::vector<TextureNode *> m_textures;
};

template <typename NodeType, class... Args>
NodeType *ShaderGraph::insert(Args... args) {
    static_assert(std::is_base_of_v<Node, NodeType>,
                  "The template param NodeType must be derived from ::gfx::ShaderGraph::Node.");

    m_nodes.push_back(std::make_unique<NodeType>(args...));
    auto node = m_nodes.back().get();

    if (auto ptr = dynamic_cast<MasterNode *>(node)) {
        ASSUMPTION(m_nodes.size() == 1);
        ASSUMPTION(m_root == nullptr);
        m_root = ptr;
    } else if (auto ptr = dynamic_cast<UniformNode *>(node)) {
        m_uniforms.push_back(ptr);
        ptr->m_shader = this;
    } else if (auto ptr = dynamic_cast<TextureNode *>(node)) {
        m_textures.push_back(ptr);
        ptr->m_shader = this;
        ptr->load_texture();
    }

    on_change();

    return dynamic_cast<NodeType *>(node);
}

}

#endif
