#ifndef GFX_RENDERER_HPP_INCLUDED
#define GFX_RENDERER_HPP_INCLUDED

#include <com/library.hpp>
#include <math/math.hpp>

namespace gfx {

/**
 * Provides rendering functionality. There are three rendering pipelines available:
 * \code FORWARD\endcode, \code DEFERRED\endcode, and \code TRANSPARENT\endcode.
 * Example usage:
 * \code{.cpp}
 * gfx::renderer()->clear_render_buffers();
 * gfx::renderer()->present_collection(objects_df, gfx::Renderer::DEFERRED);
 * std::vector<com::Folder*> lights;
 * gfx::Renderer::gather_lights(objects_df, lights);
 * gfx::renderer()->lighting_pass(lights);
 * gfx::renderer()->copy_depth_buffer();
 * gfx::renderer()->present_collection(objects_fw, gfx::Renderer::FORWARD);
 * \endcode
 */
struct Renderer : public com::Library {
    static inline std::string self_name() { return "renderer.lib"; }

    Renderer();
    ~Renderer() override;

    enum pipeline { FORWARD, DEFERRED, TRANSPARENT };

    /**
     * Renders all objects in a folder. The FS tree is searched recursively using DFS.
     * Allows selection between rendering pipelines. When rendering with \code FORWARD\endcode
     * or \code TRNASPARENT\endcode, the objects are rendered into the default framebuffer.
     * When rendering with \code DEFERRED\endcode, the objects are rendered into
     * the g-buffer. When rendering with \code TRNASPARENT\endcode, the objects' frames
     * are first sorted with respect to their distance to the active camera.
     * @param collection folder containing objects to render.
     * @param p rendering pipeline switch.
     */
    void present_collection(com::Folder *collection, pipeline p);

    /**
     * Clear the color and depth of the default framebuffer.
     * @param using_deferred if true, the g-buffer and its
     * textures are cleared as well.
     */
    void clear_render_buffers(bool using_deferred = false);

    /**
     * Copy the content of the geometry's depth buffer to the default framebuffer's depth buffer.
     */
    void copy_depth_buffer() const;

    /**
     * Calculate lighting by iterating over a screen filled quad
     * pixel-by-pixel using the gBuffer's content.
     * @param lights lights that will influence the rendered scene.
     * @param lm ligthing model used for lighting computations. For this function to work
     * properly, this should match the shader lighting model of the
     */
    void lighting_pass(const std::vector<const com::Folder*> &lights,
                       ShaderGraph::lighting_model lm = ShaderGraph::PHONG) const;

    /**
     * Utility function used for gathering all lights that light a collection of objects
     * when using deferred rendering.
     * @param collection folder containing rendered objects.
     * @param lights a vector which will be populated with gathered lights.
     */
    static void gather_lights(const com::Folder *collection, std::vector<const com::Folder*> &lights);

    /**
     * Configures blending options in OpenGL.
     * Can be used to render (partially) transparent objects.
     * For more info, see https://docs.gl/gl3/glBlendFunc.
     * @param enable determines whether to enable or disable blending.
     * @param sfactor is passed to glBlendFunc.
     * @param dfactor is passed to glBlendFunc.
     */
    static void configure_blending(bool enable, GLenum sfactor = GL_SRC_ALPHA, GLenum dfactor = GL_ONE_MINUS_SRC_ALPHA);

    /**
     * Configures scissor test option in OpenGL
     * @param enable whether or not to enable the scissor test
     */
    static void configure_scissor_test(bool enable);

    /**
     * Configures OpenGL line width
     * NOTE: may not work on newer OpenGL versions
     * @param width
     */
    static void configure_line_width(scalar width);

    /**
     * Enables or disables writing to the depth buffer.
     * Useful when rendering semi-transparent objects.
     * @param enable chooses enable or disable.
     */
    static void configure_depth_writing(bool enable);

    /**
     * Enables or disables face culling.
     * @param enable chooses enable or disable.
     * @param mode passed to glCullFace (https://docs.gl/gl3/glCullFace).
     */
    static void configure_face_culling(bool enable, GLenum mode = GL_BACK);

  protected:
    void initialize() override;
    void release() override;

  private:
    struct CommonVisitData {
        mat4x4 projection_matrix;
        mat4x4 const &view_matrix;
        vec3 camera_pos;
    };

    virtual void present_object(com::Folder *object, CommonVisitData const &common,
                                com::Frame *frame = nullptr);

    void resize_fullscreen_textures();

    void present_collection(com::Folder *collection, CommonVisitData const &common);

    void present_collection_transparent(com::Folder *collection, CommonVisitData const &common);

    using objects_buffer_t = std::vector<std::pair<com::Folder*, com::Frame*>>;
    static void gather_objects(objects_buffer_t &objects, com::Folder *collection);

    void light_volumes_pass(const std::vector<const com::Folder *> &lights,
                            ShaderGraph::lighting_model lm) const;

    void quad_pass(const std::vector<const com::Folder *> &lights,
                   ShaderGraph::lighting_model lm) const;

    int m_win_width, m_win_height;
    unsigned m_g_buffer, m_g_position, m_g_normal,
             m_g_ambient, m_g_diffuse, m_g_specular,
             m_g_emission, m_rbo_depth;
    Buffer *m_empty_vao, *m_light_sphere;
};

} // namespace gfx

#endif
