#ifndef GFX_VIEWPORT_SYSTEM_HPP_INCLUDED
#   define GFX_VIEWPORT_SYSTEM_HPP_INCLUDED

#   include <gfx/viewport.hpp>
#   include <com/library.hpp>
#   include <math/math.hpp>

namespace gfx {

struct ViewportSystem final : public com::Library
{
    static inline std::string self_name() { return "viewport_system.lib"; }

    ViewportSystem();
    ~ViewportSystem() override;

    com::Folder* viewports() { return m_viewports; }
    Viewport* viewport_default() { return m_viewport_default; }

    Viewport* active_viewport() const { return dynamic_cast<Viewport*>(m_active_viewport->target()); }
    com::Link* active_viewport_link() const { return m_active_viewport; }
    void set_active_viewport(Viewport* viewport);

    Viewport* insert_viewport(std::string const& name, com::ContextPath const& sub_folders);
    void erase_viewport(Viewport* viewport);

protected:

    void initialize() override;
    void release() override;

private:
    com::Folder* m_viewports;
    Viewport* m_viewport_default;
    com::Link* m_active_viewport;
};

}

#endif
