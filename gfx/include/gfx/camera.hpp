#ifndef GFX_CAMERA_HPP_INCLUDED
#   define GFX_CAMERA_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>

namespace gfx {

struct Camera final : public com::File
{
    Camera();
    ~Camera() override {}

    static inline std::string self_file_name() { return "camera"; }

    vec2 const& bottom_left() const { return m_bottom_left; }
    vec2 const& top_right() const { return m_top_right; }
    bool relative_to_window() const { return m_relative; }
    vec2 bottom_left_absolute() const;
    vec2 top_right_absolute() const;
    vec2 middle_absolute() const;

    scalar near() const { return m_near; }
    scalar far() const { return m_far; }

    bool perspective() const { return m_perspective; }

    void set_bottom_left(vec2 const& bottom_left);
    void set_top_right(vec2 const& top_right);
    void set_relative_to_window(bool const state) { m_relative = state; }

    void set_near(scalar const value);
    void set_far(scalar const value);

    void set_perspective(bool const state) { m_perspective = state; }

    mat4x4 matrix() const { return perspective() ? matrix_perspective() : matrix_orthograpics(); }
    mat4x4 matrix_perspective() const;
    mat4x4 matrix_orthograpics() const;

private:
    vec2 m_bottom_left;
    vec2 m_top_right;
    scalar m_near;
    scalar m_far;
    bool m_relative;
    bool m_perspective;
};

}

#endif
