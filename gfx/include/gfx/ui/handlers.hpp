# ifndef GFX_ELEMENT_HANDLERS_HPP_INCLUDED
# define GFX_ELEMENT_HANDLERS_HPP_INCLUDED
# include <com/context.hpp>

namespace gfx {
    struct UiElement;

    struct UiEventHandler : com::File {
        UiEventHandler(std::string const& name);
        ~UiEventHandler();

        virtual void on_hover(UiElement* element) {}
        virtual void on_click(UiElement* element) {}
        virtual void on_press(UiElement* element) {}
        virtual void on_release(UiElement* element) {}
        virtual void on_key_pressed(UiElement* element, std::string const& key) {}
        virtual void on_toggle(UiElement* element) {}
        virtual void on_value_changed(UiElement* element) {}
        virtual void on_size_changed(UiElement* element) {}
    };

    struct ButtonHandler : UiEventHandler {
        ButtonHandler(std::string const& name);
        ~ButtonHandler();

        void on_click(UiElement* element) override;
    };

    struct CheckBoxHandler : UiEventHandler {
        CheckBoxHandler(std::string const& name);
        ~CheckBoxHandler();

        void on_toggle(UiElement* element) override;
    };

    struct IntervalHandler : UiEventHandler {
        IntervalHandler(std::string const& name);
        ~IntervalHandler();

        void on_value_changed(UiElement* element) override;

    };

    // NOTE: This is an example Handler that resizes border and background buffers when size changes
    struct BufferHandler : UiEventHandler {
        static inline std::string self_name() { return "buffer_handler"; };

        BufferHandler();
        ~BufferHandler();

        void on_size_changed(UiElement* element) override;

    };

}

#endif