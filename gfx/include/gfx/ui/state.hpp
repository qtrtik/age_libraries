# ifndef GFX_STATE_HPP_INCLUDED
# define GFX_STATE_HPP_INCLUDED
# include <com/context.hpp>

namespace gfx {
    enum UI_STATE {
        DISABLED,
        ENABLED,
        HOVERED,
        PRESSED,
    };

    struct State : public com::File {
        static inline std::string self_name() { return "state"; }

        State();
        ~State();

        UI_STATE get_state() { return state; }
        void set_state(UI_STATE new_state);
        void set_state_always(UI_STATE new_state);

        protected:
            void initialize() override;
            void release() override;
        private:
            UI_STATE state;
    };
}

#endif