# ifndef UI_UPDATER_HPP_INCLUDED
# define UI_UPDATER_HPP
# include <com/runner.hpp>

namespace gfx {

    struct UiUpdater : com::Runner {

        static inline std::string self_name() { return "ui_updater.run" ; }

        UiUpdater();
        ~UiUpdater() override;

        void next_round() override;

        protected:
            void initialize() override;
            void release() override;

    private:
        double seconds;
        uint64_t ticks;
    };

}
#endif