# ifndef GFX_TRANSFORM_HPP_INCLUDED
# define GFX_TRANSFORM_HPP_INCLUDED

# include <com/math_files.hpp>
# include <com/context.hpp>
# include <gfx/ui/anchor.hpp>

namespace gfx {

    struct Transform : public com::Folder {
        static inline std::string self_name() { return "transform"; }
        static inline std::string anchor_file_name() { return Anchor::self_name(); }
        static inline std::string aabb_name() { return "aabb"; }

        Transform();
        ~Transform();

        Anchor *get_anchor();
        AABB get_aabb();

        void set_pivot(vec2 new_pivot);
        void set_anchor(scalar bottom, scalar left, scalar top, scalar right);
        void set_aabb(AABB new_aabb);

        template <typename T>
        void register_on_parameter_changed(std::string param_name, com::ContextItem* receiver) {
            auto* param_file = find<T>(param_name);
            if (param_file != nullptr) {
                param_file->register_on_content_changed(receiver);
            }
        }

        template <typename T>
        void unregister_on_parameter_changed(std::string param_name, com::ContextItem* receiver) {
            T* param_file = find<T>(param_name);
            if (param_file != nullptr) {
                param_file->unregister_on_content_changed(receiver);
            }
        }

        protected:
            void initialize() override;
            void release() override;

    };
}

#endif
