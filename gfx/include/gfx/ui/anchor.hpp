# ifndef GFX_ANCHOR_INCLUDED
# define GFX_ANCHOR_INCLUDED
# include <com/context.hpp>
# include <math/math_scalar_type.hpp>

namespace gfx {
    struct Anchor : public com::File {
        static inline std::string self_name() { return "Anchor"; }

        Anchor(scalar bottom, scalar left, scalar top, scalar right);
        ~Anchor();

        scalar get_bottom() const { return m_bottom; }
        scalar get_left() const { return m_left; }
        scalar get_top() const { return m_top; }
        scalar get_right() const { return m_right; }

        void set_anchors(scalar new_bottom, scalar new_left, scalar new_top, scalar new_right);
        void disable_anchors();
        bool is_zero() const;
        bool is_init() const { return m_init; }

    private:
        scalar m_bottom;
        scalar m_left;
        scalar m_top;
        scalar m_right;

        bool m_init = false;
    };
};

#endif
