# ifndef UI_ELEMENT_INCLUDED
# define UI_ELEMENT_INCLUDED

# include "gfx/buffer.hpp"
# include "gfx/font.hpp"

# include <com/frame.hpp>
# include <com/context.hpp>
# include <gfx/ui/transform.hpp>
# include <gfx/ui/state.hpp>
# include <gfx/ui/handlers.hpp>
# include <gfx/events.hpp>
# include <utility>

namespace gfx {
struct Interval;
struct BaseButton;

enum Alignment {
    HORIZONTAL,
    VERTICAL
};

struct UiElement : public com::Folder {
    static inline std::string self_name() { return "ui_element"; }
    static inline std::string transform_folder_name() { return Transform::self_name(); }
    static inline std::string state_file_name() { return State::self_name(); }
    static inline std::string buffer_link_file_name() { return "buffer.link"; }
    static inline std::string material_link_file_name() { return "material.link"; }
    static inline std::string object_folder_link_name() { return "object_folder.link"; }

    static inline std::string materials_folder_name() { return "materials"; }
    static inline std::string buffers_folder_name() { return "buffers"; }
    static inline std::string enabled_link_name() { return "enabled.link"; }
    static inline std::string hovered_link_name() { return "hovered.link"; }
    static inline std::string pressed_link_name() { return "pressed.link"; }
    static inline std::string lights_folder_name() { return "lights"; }
    static inline std::string frame_folder_name() { return "frame"; }
    static inline std::string frame_file_name() { return "frame"; }
    static inline std::string frame_parent_link_name() { return "frame_parent.link"; }
    static inline std::string foreground_folder_name() { return "foreground"; }
    static inline std::string background_folder_name() { return "background"; }
    static inline std::string rectangle_file_name() { return "rect"; }
    static inline std::string rectangle_pixels_file_name() { return "rect_pixels"; }
    static inline std::string scissor_file_name() { return "scissor"; }
    static inline std::string border_ui_object_name() { return "border"; }
    static inline std::string background_ui_object_name() { return "background"; }

    UiElement(std::string const &name, Folder* scene_folder);
    UiElement(std::string const &name, Folder* scene_folder, vec2 size);
    UiElement();
    ~UiElement();

    // Context functions
    Transform *get_transform() const;
    State *get_state_file() const { return find<State>(state_file_name()); }
    Buffer *get_buffer() const { return find<com::Link>(buffer_link_file_name())->target<Buffer>(); }
    com::Link *get_buffer_link() const { return find<com::Link>(buffer_link_file_name()); }
    void set_scene_folder(Folder* scene_folder) { m_scene_folder = scene_folder; }
    Folder* get_scene_folder() { return m_scene_folder; }

    // Context functions
    com::FileAABB* get_norm_size_file() const { return find<com::FileAABB>(rectangle_file_name()); }
    AABB get_normalized_size() const { return find<com::FileAABB>(rectangle_file_name())->get(); }
    void set_normalized_size(vec3 lo, vec3 hi);
    Folder *get_frame_folder() const { return find<Folder>(frame_folder_name()); }
    com::Frame *get_frame() const { return get_frame_folder()->find<com::Frame>(frame_file_name()); }
    com::Link *get_frame_parent_link() const { return get_frame_folder()->find<com::Link>(frame_parent_link_name()); }
    Folder *get_background_folder() const { return find<Folder>(background_folder_name()); }
    Folder *get_foreground_folder() const { return find<Folder>(foreground_folder_name()); }
    com::Link *get_object_folder_link(Folder *ui_object);
    Folder *get_object_folder(Folder *ui_object);
    com::Frame* get_ui_object_frame(Folder* ui_object);
    com::Frame *insert_frame(Folder *ui_object, com::Frame* parent = nullptr);
    com::FileVec4u* get_scissor_file() const;
    com::FileVec4u* get_foreground_scissor_file() const;
    com::FileVec4u* get_background_scissor_file() const;

    Folder* set_background_object_buffer(Material* enabled_material, Material* hovered_material, Material* pressed_material);
    Folder* set_background_object_buffer(Material* material);
    Folder* set_background_object_buffer(Buffer* buffer);
    Folder* set_border_object_buffer(Buffer* buffer);
    com::Folder *set_border_object_buffer(Material *material);
    Folder* get_background_object() { return m_background_object; }
    Folder* get_border_object() { return m_border_object; }

    Folder *set_object_all(Folder* ui_object, Material* material, Buffer* buffer);
    Folder *set_object_enabled(Folder* ui_object, Material* material, Buffer* buffer);
    Folder *set_object_hovered(Folder* ui_object, Material* material, Buffer* buffer);
    Folder *set_object_pressed(Folder* ui_object, Material* material, Buffer* buffer);
    Folder *set_object_materials_all(Folder* ui_object, Material* material);
    Folder *set_object_materials_enabled(Folder* ui_object, Material* material);
    Folder *set_object_materials_hovered(Folder* ui_object, Material* material);
    Folder *set_object_materials_pressed(Folder* ui_object, Material* material);
    Folder *set_object_buffers_all(Folder* ui_object, Buffer* buffer);
    Folder *set_object_buffers_enabled(Folder* ui_object, Buffer* buffer);
    Folder *set_object_buffers_hovered(Folder* ui_object, Buffer* buffer);
    Folder *set_object_buffers_pressed(Folder* ui_object, Buffer* buffer);
    void recalculate_size_in_meters();
    void recalculate_scissor();

    void update_objects();

    void request_fonts_to_load(const std::string& font_name, size_t font_size, bool bold, bool italic);
    void on_font_loaded(const std::string& name);;
    virtual void on_all_fonts_loaded();

    FontProps* get_regular_font_props() const;
    Material* get_regular_font_material() const;
    FontProps* get_bold_font_props() const;
    Material* get_bold_font_material() const;
    FontProps* get_italic_font_props() const;
    Material* get_italic_font_material() const;

    vec3 get_origin_from_scene_frame_link();;

    // Handler functions
    void register_handler(UiEventHandler *handler) { m_element_handlers.insert(handler); }
    void unregister_handler(UiEventHandler *handler) { m_element_handlers.erase(handler); };

    MouseEvent get_mouse_mask() const { return mouse_mask; }
    void set_mouse_mask(MouseEvent mask) { mouse_mask = mask; }
    vec2u get_size() { return m_size; }
    vec2 get_size_in_meters() { return m_size_meters; }
    UiElement* get_parent() { return m_parent; }

    virtual void on_element_toggle_changed(BaseButton* child_element) {
        if (m_parent != nullptr) m_parent->on_element_toggle_changed(child_element);
    }
    virtual void on_element_moved(UiElement* child_element, vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_moved(child_element, position);
    }
    virtual void on_element_pressed(UiElement *child_element, vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_pressed(child_element, position);
    }
    virtual void on_element_key_pressed(UiElement *child_element, std::string const &key) {
        if (m_parent != nullptr) m_parent->on_element_key_pressed(child_element, key);
    }
    virtual void on_element_text_entered(UiElement *child_element, std::string const &text) {
        if (m_parent != nullptr) m_parent->on_element_text_entered(child_element, text);
    }
    virtual void on_element_hovered(UiElement *child_element, vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_hovered(child_element, position);
    }
    virtual void on_element_released(UiElement *child_element, vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_released(child_element, position);
    }
    virtual void on_element_value_changed(Interval* child_interval, scalar value) {
        if (m_parent != nullptr) m_parent->on_element_value_changed(child_interval, value);
    }
    virtual void on_element_clicked(UiElement* child, vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_clicked(child, position);
    }

protected:
    void initialize() override;
    void release() override;

    virtual void recalculate_objects() {}

    void on_target_changed(com::Link *sender) override;
    virtual void on_state_changed(State *state);
    void on_anchor_changed(Anchor* anchor);
    virtual bool can_receive_events() { return false; }

    void update_objects(Folder *ui_objects, UI_STATE state);

    void on_content_changed(com::File *sender) override;

    std::unordered_set<UiEventHandler *> get_handlers() { return m_element_handlers; }

    template <typename... Args>
    void notify_handlers(void (UiEventHandler::*event)(UiElement*, Args...), Args&&... args) {
        for (UiEventHandler* event_handler : get_handlers()) {
            (event_handler->*event)(this, std::forward<Args>(args)...);
        }
    }

    virtual void on_key_pressed(std::string const &key) {
        if (m_parent != nullptr) m_parent->on_element_key_pressed(this, key);
        notify_handlers(&UiEventHandler::on_key_pressed, key);
    }
    virtual void on_text_entered(std::string const &text) {
        if (m_parent != nullptr) m_parent->on_element_text_entered(this, text);
        notify_handlers(&UiEventHandler::on_press);
    }
    virtual void on_mouse_moved(vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_moved(this, position);
        notify_handlers(&UiEventHandler::on_hover);
    }
    virtual void on_mouse_pressed(vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_pressed(this, position);
        notify_handlers(&UiEventHandler::on_press);
    }
    virtual void on_mouse_released(vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_released(this, position);
        notify_handlers(&UiEventHandler::on_release);
    }
    virtual void on_click(vec2 position) {
        if (m_parent != nullptr) m_parent->on_element_clicked(this, position);
        notify_handlers(&UiEventHandler::on_click);
    }
    virtual void on_size_changed() {
        notify_handlers(&UiEventHandler::on_size_changed);
    }

    Folder* m_background_object = nullptr;
    Folder* m_border_object = nullptr;

    std::string m_font_name;
    size_t m_font_size;

    std::unordered_set<std::string> m_required_fonts;
private:
    friend struct UiSystem;

    void _set_object(Folder *materials_folder, Folder *buffers_folder, std::string const &state_name,
                     Material *material, Buffer *buffer);

    FontProps* get_font_props(std::string const& font_name, size_t font_size) const;
    Material* get_font_material(std::string const& name, size_t font_size) const;

    UiElement* m_parent;
    std::unordered_set<UiEventHandler *> m_element_handlers;
    vec2u m_size;
    vec2 m_size_meters;
    MouseEvent mouse_mask;
    Folder * m_scene_folder;

};

struct StaticText : public UiElement {
    static inline std::string props_file_name() { return "text_props"; }
    static inline std::string aabb_file_name() { return "aabb"; }
    static inline std::string rect_file_name() { return "rect"; }
    static inline std::string aabb_union_file_name() { return "aabb_union"; }

    enum HorizontalAlignment {
        LEFT,
        H_CENTER,
        RIGHT
    };

    enum VerticalAlignment {
        TOP,
        V_CENTER,
        BOTTOM
    };


    StaticText(std::string const &name, Folder* scene_folder, std::string text);
    StaticText(std::string const &name, Folder *scene_folder, std::string text, bool text_wrap);
    StaticText(std::string const &name, Folder* scene_folder, vec2i size, std::string text);
    StaticText(std::string const &name, Folder* scene_folder, vec2i size, std::string text, bool text_wrap);
    ~StaticText();

    void set_text(std::string text) {
        m_text = std::move(text);
        on_text_changed();
    }

    void set_font_name(std::string new_name) {
        m_font_name = std::move(new_name);
        on_text_changed();
    }

    void set_font_size(size_t new_size) {
        m_font_size = new_size;
        on_text_changed();
    }

    void set_vertical_alignment(VerticalAlignment vertical_alignment) {
        m_v_alignment = vertical_alignment;
        on_text_changed();
    }

    void set_horizontal_alignment(HorizontalAlignment horizontal_alignment) {
        m_h_alignment = horizontal_alignment;
        on_text_changed();
    }
    void set_max_buffer_size(size_t max_buffer_size) {
        ASSUMPTION(max_buffer_size > 0);
        m_max_text_size = max_buffer_size;
        on_text_changed();
    }

    void initialize_text_objects();

    std::string get_text() { return m_text; }
    std::string get_font_name() { return m_font_name; }
    Folder *get_text_ui_object() { return get_foreground_folder()->find<Folder>("text"); }
    CursorProps *get_cursor_props() { return &m_cursor_props; }

protected:
    void initialize() override;
    void release() override;

    void on_element_value_changed(Interval *child_interval, scalar value) override;
    void recalculate_objects() override;

    void on_all_fonts_loaded() override;


private:
    friend struct UiSystem;
    void on_text_changed();
    void recalculate_based_on_value(scalar value);

    enum FontStyle {
        BOLD,
        ITALIC,
        REGULAR
    };

    struct TextStyle {
        vec3 color;
        FontStyle font_style;
    };

    struct TextSegment {
        std::string text;
        TextStyle style;
    };

    FontProps * get_font_props_based_on_segment(const TextSegment & segment) const;
    Material *get_font_material_based_on_segment(const TextSegment &segment) const;

    std::vector<TextSegment> batch_formatted_text(const std::string& text);

    std::list<Folder*> m_text_objects;
    std::list<std::string> m_texts;
    size_t m_current_row{};
    Folder* m_current_object{};
    size_t m_max_text_size{1000};

    std::string m_text;
    bool m_text_wrap;
    CursorProps m_cursor_props;
    vec2i m_size{};
    HorizontalAlignment m_h_alignment = H_CENTER;
    VerticalAlignment m_v_alignment = V_CENTER;
};

struct LineTextEdit : public UiElement {
    static inline std::string props_file_name() { return "text_props"; }
    static inline std::string ws_aabb_file_name() { return "ws_aabb"; }
    static inline std::string ss_aabb_file_name() { return "ss_aabb"; }
    static inline std::string aabb_union_file_name() { return "aabb_union"; }

    LineTextEdit(std::string const &name, Folder* scene_folder, vec2i size, std::string text);
    ~LineTextEdit();

    void set_text(std::string text) {
        m_text = std::move(text);
    }

    void set_font_name(std::string new_name) {
        m_font_name = std::move(new_name);
        on_font_changed();
    }

    void set_font_size(size_t new_size) {
        m_font_size = new_size;
        on_font_changed();
    }

    void set_cursor_index(int new_idx, scalar new_cursorX) {
        m_cursor_idx = std::clamp(new_idx, -1, (int)m_text.size() - 1);
        m_cursor_props.prevCursorX = m_cursor_props.cursorX;
        m_cursor_props.cursorX = new_cursorX;
        on_cursor_changed(new_idx, new_cursorX);
    }

    com::FileAABB* get_ss_aabb() { return m_text_object->find<com::FileAABB>(ss_aabb_file_name()); }
    com::FileAABB* get_ws_aabb() { return m_text_object->find<com::FileAABB>(ws_aabb_file_name()); }
    std::string get_text() { return m_text; }
    std::string get_font_name() { return m_font_name; }
    Folder *get_text_ui_object() { return get_foreground_folder()->find<Folder>("text"); }

protected:
    void initialize() override;
    void release() override;

    void recalculate_objects() override;

    bool can_receive_events() override { return true; }

    void on_all_fonts_loaded() override;

private:
    friend struct UiSystem;
    void on_text_entered(std::string const &text) override;
    void on_key_pressed(std::string const &key) override;
    void on_mouse_pressed(vec2 position) override;
    void on_font_changed();
    void on_cursor_changed(int new_idx, scalar new_cursorX);

    Buffer *insert_new_text_buffer();
    void erase_text_object();

    void calculate_cursor_position(AABB text_ws_aabb, vec2 ss_mouse_pos);

    Folder* m_text_object{nullptr};
    Folder* m_cursor_object{nullptr};
    CursorProps m_cursor_props;
    vec2i m_size{};
    std::string m_text{""};
    vec3 m_text_color{1.f};
    int m_cursor_idx{};
};

struct BaseButton : public UiElement {

    BaseButton(std::string const &name, Folder* scene_folder);
    BaseButton(std::string const &name, Folder* scene_folder, vec2 size);
    BaseButton(std::string const &name, Folder* scene_folder, bool toggleable);
    BaseButton(std::string const &name, Folder* scene_folder, vec2 size, bool toggleable);
    ~BaseButton();

    bool is_toggleable() const { return m_toggleable; }
    bool is_toggled() const { return m_toggled; }

protected:
    void set_toggleable(bool toggleable) { m_toggleable = toggleable; }

    void set_toggled(bool new_toggled) {
        m_toggled = new_toggled;
        on_toggle_changed();
    }

    virtual void on_toggle_changed() {
        if (get_parent()!= nullptr) get_parent()->on_element_toggle_changed(this);
        notify_handlers(&UiEventHandler::on_toggle);
    }

    bool can_receive_events() override { return true; }

private:
    bool m_toggleable;
    bool m_toggled;
};

struct Button : public BaseButton {

    Button(std::string const &name, Folder* scene_folder);
    Button(std::string const &name, Folder* scene_folder, vec2 size, std::string const& text);
    ~Button();


protected:
    void on_all_fonts_loaded() override;
    void initialize() override;
    void release() override;
    void on_state_changed(State *state) override;
    bool can_receive_events() override { return true; }

private:
    std::string m_text;
};

struct CheckBox : public BaseButton {

    CheckBox(std::string const &name, Folder* scene_folder, vec2i size);
    ~CheckBox();

protected:
    void initialize() override;
    void release() override;
    void on_state_changed(State *state) override;
    void on_toggle_changed() override;
    bool can_receive_events() override { return true; }

};

struct Interval : public UiElement {

    Interval(std::string const &name, Folder* scene_folder, vec2i size, scalar value);
    ~Interval();

    void set_value(scalar new_value) {
        m_value = new_value;
        on_value_changed(new_value);
    }

    scalar get_value() const { return m_value; }

    virtual void on_value_changed(scalar value) {
        if (get_parent() != nullptr) get_parent()->on_element_value_changed(this, get_value());
        notify_handlers(&UiEventHandler::on_value_changed);
    }

protected:
    bool can_receive_events() override { return true; }

private:
    scalar m_value;
};

struct ProgressBar : public Interval {
    ProgressBar(std::string const &name, Folder* scene_folder, vec2i size, scalar value);
    ProgressBar(std::string const &name, Folder* scene_folder, vec2i size);
    ~ProgressBar();

    void on_value_changed(scalar value) override;
    void set_foreground_material(Material* material);

    protected:
        bool can_receive_events() override { return false; }
        void initialize() override;
        void release() override;
        void on_size_changed() override;

    private:
        Folder* m_foreground_folder;
};

struct Slider : public Interval {

    Slider(std::string const &name, Folder* scene_folder, vec2i size);
    Slider(std::string const &name, Folder* scene_folder, vec2i size, scalar value);
    Slider(std::string const &name, Folder* scene_folder, vec2i size, Alignment alignment);
    Slider(std::string const &name, Folder* scene_folder, vec2i size, scalar value, Alignment alignment);
    ~Slider();

protected:
    void initialize() override;
    void release() override;
    void on_mouse_moved(vec2 position) override;
    void on_mouse_pressed(vec2 position) override;
    void on_size_changed() override;
    void recalculate_objects() override;

private:
    Alignment m_alignment;
};

struct ListBox : public UiElement {
    ListBox(std::string const &name, Folder* scene_folder, vec2i size, vec2i item_size);
    ~ListBox();

    UiElement *insert_item_generic();
    Slider* insert_slider(vec2i size);

    void on_element_value_changed(Interval *child_interval, scalar value) override;

    std::set<UiElement*> get_elements() const { return m_elements; }

protected:
    void initialize() override;
    void release() override;
    bool can_receive_events() override { return false; }

    void on_element_pressed(UiElement *subelement, vec2 position) override;
    void on_size_changed() override;

private:
    size_t m_counter;
    size_t m_idx;
    vec2i m_item_size;
    vec2 m_new_item_frame_pos;
    std::set<UiElement*> m_elements;
    Slider* m_slider = nullptr;
    size_t m_items_height;
};

struct TabBar : public UiElement {
    TabBar(std::string const &name, Folder* scene_folder, vec2i size, vec2i tab_size);
    ~TabBar();

    UiElement *add_tab(std::string const& name, Folder* scene_folder, Folder* layer_folder, std::string const& font_name, size_t font_size);
    UiElement* get_active_tab() const { return m_active_tab; }
    UiElement* get_tab_content(UiElement* tab) { return m_tab_contents[tab]; }
    void set_active_tab(UiElement* tab) ;
    UiElement* get_active_content() const { return m_active_content; }
    void set_active_content(UiElement* content_element);
    void assign_content_to_tab(UiElement *content, UiElement *tab);

protected:
    void initialize() override;
    void release() override;
    bool can_receive_events() override { return false; }

    void on_tab_changed(UiElement* tab);
    void on_element_pressed(UiElement *subelement, vec2 position) override;
private:
    std::map<UiElement*, UiElement* > m_tab_contents;
    std::set<UiElement*> m_tabs;
    vec2i m_tab_size;
    size_t m_idx;
    vec2 m_new_tab_frame_pos;
    UiElement* m_active_content;
    UiElement* m_active_tab;
};

}

#endif