#ifndef GFX_BACKGROUND_HPP_INCLUDED
#   define GFX_BACKGROUND_HPP_INCLUDED

#   include <com/context.hpp>
#   include <math/math.hpp>

namespace gfx {

struct Background final : public com::File
{
    Background();
    ~Background() override {}

    static inline std::string self_file_name() { return "background"; }

    vec3 const& color() const { return m_color; }
    vec3& color() { return m_color; }

private:
    vec3 m_color;
};

}

#endif
