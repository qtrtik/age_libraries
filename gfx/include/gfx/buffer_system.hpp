#ifndef GFX_BUFFER_SYSTEM_HPP_INCLUDED
#   define GFX_BUFFER_SYSTEM_HPP_INCLUDED

#   include <gfx/buffer.hpp>
#   include <com/library.hpp>
#   include <math/math.hpp>

namespace gfx {

struct BufferSystem final : public com::Library
{
    static inline std::string self_name() { return "buffer_system.lib"; }

    BufferSystem();
    ~BufferSystem() override;

    com::Folder* buffers() { return m_buffers; }
    Buffer* active_buffer() const { return dynamic_cast<Buffer*>(m_active_buffer->target()); }
    com::Link* active_buffer_link() const { return m_active_buffer; }
    void set_active_buffer(Buffer* buffer);

    Buffer* insert_buffer(std::string const& name, Buffer::PrimitiveType primitive_type, com::ContextPath const& sub_folders);
    void erase_buffer(Buffer* buffer);

protected:

    void initialize() override;
    void release() override;

private:
    com::Folder* m_buffers;
    com::Link* m_active_buffer;
};

}

#endif
