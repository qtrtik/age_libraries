#ifndef GFX_BUFFER_HPP_INCLUDED
#   define GFX_BUFFER_HPP_INCLUDED

#   include <gfx/shader_nodes.hpp>
#   include <com/context.hpp>
#   include <math/bounding_volume.hpp>
#   include <math/math.hpp>
#   include <vector>
#   include <string>

namespace gfx::detail { struct BufferGL; }

namespace gfx {

struct BufferSystem;

struct Buffer final : public com::File
{
    enum PrimitiveType
    {
        POINT,
        LINE,
        TRIANGLE
    };

    Buffer(std::string const& name, PrimitiveType primitive_type);
    ~Buffer() override;

    template<typename VaryingNode>
    struct ToVector { using type = std::vector<typename ShaderGraph::Node::DataType::ToValueType<VaryingNode::element_type>::value_type>; };

    using IndicesVec = std::vector<std::uint32_t>;
    using PositionsVec = typename ToVector<ShaderGraph::VaryingPositionNode>::type;
    using NormalsVec = typename ToVector<ShaderGraph::VaryingNormalNode>::type;
    using TangentsVec = typename ToVector<ShaderGraph::VaryingTangentNode>::type;
    using TexcoordsVec = typename ToVector<ShaderGraph::VaryingTexcoordNode>::type;
    using WeightsVec = typename ToVector<ShaderGraph::VaryingWeightNode>::type;
    using ColorsVec = typename ToVector<ShaderGraph::VaryingColorNode>::type;

    PrimitiveType primitive_type() const { return m_primitive_type; }

    IndicesVec const* indices() const { return m_indices; }

    PositionsVec const* positions() const { return m_positions; }
    NormalsVec const* normals() const { return m_normals; }
    TangentsVec const* tangents() const { return m_tangents; }
    TexcoordsVec const* texcoords() const { return m_texcoords; }
    WeightsVec const* weights() const { return m_weights; }
    ColorsVec const* colors() const { return m_colors; }

    AABB const& aabb() const { return m_aabb; }

    void insert_indices(IndicesVec const& data);
    void insert_positions(PositionsVec const& data);
    void insert_normals(NormalsVec const& data);
    void insert_tangents(TangentsVec const& data);
    void insert_texcoords(TexcoordsVec const& data);
    void insert_weights(WeightsVec const& data);
    void insert_colors(ColorsVec const& data);

    void erase_indices();
    void erase_positions();
    void erase_normals();
    void erase_tangents();
    void erase_texcoords();
    void erase_weights();
    void erase_colors();

    void clear();

    void compile();

private:
    friend struct ::gfx::BufferSystem;

    void activate() const;

    void on_change();

    PrimitiveType m_primitive_type;

    IndicesVec* m_indices;

    PositionsVec* m_positions;
    NormalsVec* m_normals;
    TangentsVec* m_tangents;
    TexcoordsVec* m_texcoords;
    WeightsVec* m_weights;
    ColorsVec* m_colors;

    AABB m_aabb;

    detail::BufferGL* m_gl_buffer;
};

}

#endif
