#ifndef GFX_MATERIAL_HPP_INCLUDED
#define GFX_MATERIAL_HPP_INCLUDED

#include <com/context.hpp>
#include <com/math_files.hpp>
#include <gfx/shader.hpp>
#include <string>

namespace gfx {
struct MaterialSystem;

struct Material final : public com::Folder {
    Material(std::string const &name, Shader *shader);

    const Shader *shader() const { return m_shader; }
    const Folder *uniforms() const { return m_uniforms; }

    template <typename T>
    void set_uniform(const std::string &name, T value) {
        auto u_file = m_uniforms->find(name);
        ASSUMPTION(u_file != nullptr);

        auto m_file = dynamic_cast<com::MathFile<T> *>(u_file);
        ASSUMPTION(m_file != nullptr);
        m_file->set(value);
    }

  protected:
    void initialize() override;
    void release() override;

    void on_content_changed(com::File *sender) override;

  private:
    friend struct MaterialSystem;

    void activate() const;

    void set_shader_uniforms() const;

    void insert_uniform(const std::string &name, ShaderGraph::Node::DataType::ElementType e,
                        std::unordered_set<ContextItem *> &uniforms_in_graph);

    void update_uniforms_folder();

    Shader *m_shader;
    Folder *m_uniforms;
};
} // namespace gfx

#endif
