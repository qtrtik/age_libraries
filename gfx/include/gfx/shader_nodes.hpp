#ifndef GFX_SHADER_NODES_HPP_INCLUDED
#define GFX_SHADER_NODES_HPP_INCLUDED

#include <gfx/shader_graph.hpp>

namespace gfx {

struct ShaderGraph::MasterNodeUnlit : public MasterNode {
    explicit MasterNodeUnlit(transparency t)
        : MasterNode("master node", "Master output node (root) in the shader graph.",
               {
                   { "position", "Vertex position in object space.", { DataType::ElementType::VEC3 } },
                   { "color", "Material ambient color.", { DataType::ElementType::VEC3 } },
                   { "emission", "Material emissive color.", { DataType::ElementType::VEC3 } },
               },
               {}),
          m_transparency{ t } {
        SlotDefinition alpha{ "alpha", "Material alpha value.", { DataType::ElementType::SCALAR } };

        switch (t) {
        case OPAQUE:
            break;
        case PARTIAL:
            m_inputs.emplace_back(alpha);
            break;
        case THRESHOLD:
            m_inputs.emplace_back(alpha);
            m_inputs.emplace_back(
                SlotDefinition{ "alpha threshold", "Determines alpha cutoff.", { DataType::ElementType::SCALAR } });
            break;
        default:
            UNREACHABLE();
        }
    }

    enum inputs { position, color, emission, alpha, alpha_threshold };

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    Stage output_to_input_stage(Stage out, std::size_t in) const override;

    transparency m_transparency;
};

struct ShaderGraph::MasterNodeLit : public MasterNode {
    explicit MasterNodeLit(transparency t)
        : MasterNode("master node", "Master output node (root) in the shader graph.",
               {
                   // vertex shader
                   { "position", "Vertex position in object space.", { DataType::ElementType::VEC3 } },
                   { "normal", "Vertex normal in object space.", { DataType::ElementType::VEC3 } },
                   { "tangent", "Vertex tangent in object space.", { DataType::ElementType::VEC3 } },

                   // fragment shader
                   { "normal", "Fragment normal in tangent space in the range [0,1].", { DataType::ElementType::VEC3 } },
                   { "ambient color", "Material ambient color.", { DataType::ElementType::VEC3 } },
                   { "diffuse color", "Material diffuse color.", { DataType::ElementType::VEC3 } },
                   { "specular color", "Material specular color.", { DataType::ElementType::VEC3 } },
                   { "emission", "Material emissive color.", { DataType::ElementType::VEC3 } },
               },
               {}),
          m_transparency{ t } {
        SlotDefinition alpha{ "alpha", "Material alpha value.", { DataType::ElementType::SCALAR } };

        switch (t) {
        case OPAQUE:
            break;
        case PARTIAL:
            m_inputs.emplace_back(alpha);
            break;
        case THRESHOLD:
            m_inputs.emplace_back(alpha);
            m_inputs.emplace_back(
                SlotDefinition{ "alpha threshold", "Determines alpha cutoff.", { DataType::ElementType::SCALAR } });
            break;
        default:
            UNREACHABLE();
        }
    }

    enum inputs { position, normal, tangent, normal_ts, ambient, diffuse, specular, emission, alpha, alpha_threshold };

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    /**
     * Adds phong lighting logic to the fragment shader.
     * @param fragment shader code.
     */
    void add_lighting_code(DeclAndCode &fragment) const;

    Stage output_to_input_stage(Stage out, std::size_t in) const override;

    transparency m_transparency;
};

struct ShaderGraph::MasterNodeDeferred : public MasterNode {
    MasterNodeDeferred()
        : MasterNode("master node", "Master output node (root) in the shader graph.",
               {
                   // vertex shader
                   { "position", "Vertex position in object space.", { DataType::ElementType::VEC3 } },
                   { "normal", "Vertex normal in object space.", { DataType::ElementType::VEC3 } },
                   { "tangent", "Vertex tangent in object space.", { DataType::ElementType::VEC3 } },

                   // fragment shader
                   { "normal", "Fragment normal in tangent space in the range [0,1].", { DataType::ElementType::VEC3 } },
                   { "ambient color", "Material ambient color.", { DataType::ElementType::VEC3 } },
                   { "diffuse color", "Material diffuse color.", { DataType::ElementType::VEC3 } },
                   { "specular color", "Material specular color.", { DataType::ElementType::VEC3 } },
                   { "emission", "Material emissive color.", { DataType::ElementType::VEC3 } },
               },
               {}) {}

    enum inputs { position, normal, tangent, normal_ts, ambient, diffuse, specular, emission };

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    Stage output_to_input_stage(Stage out, std::size_t in) const override;
};

struct ShaderGraph::MasterNodePBR : public MasterNode {
    explicit MasterNodePBR(transparency t)
        : MasterNode("master node", "Master output node (root) in the shader graph.",
               {
                   // vertex shader
                   { "position", "Vertex position in object space.", { DataType::ElementType::VEC3 } },
                   { "normal", "Vertex normal in object space.", { DataType::ElementType::VEC3 } },
                   { "tangent", "Vertex tangent in object space.", { DataType::ElementType::VEC3 } },

                   // fragment shader
                   { "normal", "Fragment normal in tangent space in the range [0,1].", { DataType::ElementType::VEC3 } },
                   { "albedo", "Material albedo color.", { DataType::ElementType::VEC3 } },
                   { "emission", "Material emissive color.", { DataType::ElementType::VEC3 } },
                   { "metallic", "Material metallic value.", { DataType::ElementType::SCALAR } },
                   { "roughness", "Material roughness value.", { DataType::ElementType::SCALAR } },
                   { "ambient occlusion", "Material ambient occlusion value.", { DataType::ElementType::SCALAR } },
               },
               {}),
          m_transparency{ t } {
        SlotDefinition alpha{ "alpha", "Material alpha value.", { DataType::ElementType::SCALAR } };

        switch (t) {
        case OPAQUE:
            break;
        case PARTIAL:
            m_inputs.emplace_back(alpha);
            break;
        case THRESHOLD:
            m_inputs.emplace_back(alpha);
            m_inputs.emplace_back(
                SlotDefinition{ "alpha threshold", "Determines alpha cutoff.", { DataType::ElementType::SCALAR } });
            break;
        default:
            UNREACHABLE();
        }
    }

    enum inputs { position, normal, tangent, normal_ts, albedo, emission, metallic, roughness, ao, alpha, alpha_threshold };

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    /**
     * Adds PBR lighting logic to the fragment shader.
     * @param fragment shader code.
     */
    void add_lighting_code(DeclAndCode &fragment) const;

    Stage output_to_input_stage(Stage out, std::size_t in) const override;

    transparency m_transparency;
};

struct ShaderGraph::MasterNodeDeferredPBR : public MasterNode {
    explicit MasterNodeDeferredPBR()
        : MasterNode("master node", "Master output node (root) in the shader graph.",
               {
                   // vertex shader
                   { "position", "Vertex position in object space.", { DataType::ElementType::VEC3 } },
                   { "normal", "Vertex normal in object space.", { DataType::ElementType::VEC3 } },
                   { "tangent", "Vertex tangent in object space.", { DataType::ElementType::VEC3 } },

                   // fragment shader
                   { "normal", "Fragment normal in tangent space in the range [0,1].", { DataType::ElementType::VEC3 } },
                   { "albedo", "Material albedo color.", { DataType::ElementType::VEC3 } },
                   { "emission", "Material emissive color.", { DataType::ElementType::VEC3 } },
                   { "metallic", "Material metallic value.", { DataType::ElementType::SCALAR } },
                   { "roughness", "Material roughness value.", { DataType::ElementType::SCALAR } },
                   { "ambient occlusion", "Material ambient occlusion value.", { DataType::ElementType::SCALAR } },
               },
               {}) {}

    enum inputs { position, normal, tangent, normal_ts, albedo, emission, metallic, roughness, ao };

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    Stage output_to_input_stage(Stage out, std::size_t in) const override;
};

struct ShaderGraph::InputVaryingNode : public InputNode {
    InputVaryingNode(SlotDefinition const &output, std::uint8_t const layout)
        : InputNode(output), m_layout{ layout } {}

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    /**
     * \brief Varying variables of the same type (layout) cannot be duplicated.
     * Therefore, a universal name is used in shader declarations.
     * \return The universal name of the input varying variable.
     */
    virtual std::string universal_name() const = 0;

    std::uint8_t m_layout;
};

struct ShaderGraph::ConstantNode : public Node {
    template <typename ValueType>
    explicit ConstantNode(ValueType const &value)
        : Node("Constant", "A constant value.", {},
               { { "Constant", "A constant value.", { DataType::ToElementType<ValueType>::value } } }),
          m_declaration{ make_declaration(value, variable_name()) } {}

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    static std::string make_initializer(bool const &value) { return (value ? "true" : "false"); }
    static std::string make_initializer(scalar const &value) { return std::to_string(value); }
    static std::string make_initializer(vec2 const &value) {
        return std::string("vec2(") + std::to_string(get<0>(value)) + ", " + std::to_string(get<1>(value)) + ')';
    }

    static std::string make_initializer(vec3 const &value) {
        return std::string("vec3(") + std::to_string(get<0>(value)) + ", " + std::to_string(get<1>(value)) + ", " +
               std::to_string(get<2>(value)) + ')';
    }

    static std::string make_initializer(vec4 const &value) {
        return std::string("vec4(") + std::to_string(get<0>(value)) + ", " + std::to_string(get<1>(value)) + ", " +
               std::to_string(get<2>(value)) + ", " + std::to_string(get<3>(value)) + ')';
    }

    // TODO: Add make_initializer() for matXxX types.

    template <typename ValueType>
    static std::string make_declaration(ValueType const &value, std::string const &var_name) {
        return DataType::to_string(DataType::ToElementType<ValueType>::value) + ' ' + var_name + " = " +
               make_initializer(value);
    }

    std::string m_declaration;
};

struct ShaderGraph::BinaryOperatorNode : public Node {
    BinaryOperatorNode(std::string const &name, std::string const &description, SlotDefinition const &input_left,
                       SlotDefinition const &input_right, SlotDefinition const &output)
        : Node(name, description, { input_left, input_right }, { output }) {}

    enum inputs { left, right };
};

struct ShaderGraph::VaryingPositionNode : public InputVaryingNode {
    VaryingPositionNode()
        : InputVaryingNode(
              { "position", "The model-space position of the currently processed vertex.", { element_type } },
              default_layout) {}

    std::string universal_name() const override { return "vertex_position"; }

    static constexpr Node::DataType::ElementType element_type = Node::DataType::VEC3;
    static constexpr std::uint8_t default_layout = 0;
};

struct ShaderGraph::VaryingNormalNode : public InputVaryingNode {
    VaryingNormalNode()
        : InputVaryingNode({ "normal", "The model-space normal of the currently processed vertex.", { element_type } },
                           default_layout) {}

    std::string universal_name() const override { return "vertex_normal"; }

    static constexpr Node::DataType::ElementType element_type = Node::DataType::VEC3;
    static constexpr std::uint8_t default_layout = 1;
};

struct ShaderGraph::VaryingTangentNode : public InputVaryingNode {
    VaryingTangentNode()
        : InputVaryingNode(
              { "tangent", "The model-space tangent of the currently processed vertex.", { element_type } },
              default_layout) {}

    std::string universal_name() const override { return "vertex_tangent"; }

    static constexpr Node::DataType::ElementType element_type = Node::DataType::VEC3;
    static constexpr std::uint8_t default_layout = 2;
};

struct ShaderGraph::VaryingTexcoordNode : public InputVaryingNode {
    VaryingTexcoordNode()
        : InputVaryingNode(
              { "texcoord", "The texture coordinates of the currently processed vertex.", { element_type } },
              default_layout) {}

    std::string universal_name() const override { return "texture_coord"; }

    static constexpr Node::DataType::ElementType element_type = Node::DataType::VEC2;
    static constexpr std::uint8_t default_layout = 3;
};

struct ShaderGraph::VaryingWeightNode : public InputVaryingNode {
    VaryingWeightNode()
        : InputVaryingNode(
              { "weight",
                "The weight vector (for bones of an associated skeleton) of the currently processed vertex.",
                { element_type } },
              default_layout) {}
    static constexpr Node::DataType::ElementType element_type = Node::DataType::VEC4;
    static constexpr std::uint8_t default_layout = 4;

    std::string universal_name() const override { return "vertex_weight"; }
};

struct ShaderGraph::VaryingColorNode : public InputVaryingNode {
    VaryingColorNode()
        : InputVaryingNode({ "color", "The color of the currently processed vertex.", { element_type } },
                           default_layout) {}

    std::string universal_name() const override { return "vertex_color"; }

    static constexpr Node::DataType::ElementType element_type = Node::DataType::VEC3;
    static constexpr std::uint8_t default_layout = 5;
};

struct ShaderGraph::ErrorColorNode : public ConstantNode {
    static vec3 const error_color;
    ErrorColorNode() : ConstantNode{ error_color } {}
};

struct ShaderGraph::MultiplyNode : public BinaryOperatorNode {
    explicit MultiplyNode(DataType::ElementType type) : BinaryOperatorNode(
            "multiply", "multiply",
            {"left", "left", {type}},
            {"right", "right", {type}},
            {"result", "result", {type}}
        ) {}

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::DivideNode : public BinaryOperatorNode {
    explicit DivideNode(DataType::ElementType type) : BinaryOperatorNode(
            "divide", "divide",
            {"left", "left", {type}},
            {"right", "right", {type}},
            {"result", "result", {type}}
        ) {
        ASSUMPTION(type == DataType::SCALAR || type == DataType::INT);
    }

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::AddNode : public BinaryOperatorNode {
    explicit AddNode(DataType::ElementType type) : BinaryOperatorNode(
            "add", "add",
            {"left", "left", {type}},
            {"right", "right", {type}},
            {"result", "result", {type}}
        ) {}

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::SubtractNode : public BinaryOperatorNode {
    explicit SubtractNode(DataType::ElementType type) : BinaryOperatorNode(
            "subtract", "subtract",
            {"left", "left", {type}},
            {"right", "right", {type}},
            {"result", "result", {type}}
        ) {}

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::MatrixVectorMultiplyNode : public BinaryOperatorNode {
    explicit MatrixVectorMultiplyNode(DataType::ElementType const matrix_type)
        : BinaryOperatorNode("Matrix multiplication", "Multiplies two matrices of the same dimensions.",
                             { "matrix", "Matrix (left) operand.", { matrix_type } },
                             { "vector", "Vector (right) operand.", { vector_type(matrix_type) } },
                             { "result", "Result from the operator.", { vector_type(matrix_type) } }) {
        ASSUMPTION(matrix_type == DataType::MAT2X2 || matrix_type == DataType::MAT3X3 ||
                   matrix_type == DataType::MAT4X4);
    }

    static inline DataType::ElementType vector_type(DataType::ElementType const matrix_type) {
        return matrix_type == DataType::MAT2X2 ? DataType::VEC2
                                               : (matrix_type == DataType::MAT3X3 ? DataType::VEC3 : DataType::VEC4);
    }

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::ScalarMultiplyNode : public BinaryOperatorNode {
    explicit ScalarMultiplyNode(DataType::ElementType const other_type)
        : BinaryOperatorNode("Scalar multiplicaiton", "Multiplies a scalar and some other value.",
                             { "scalar", "Scalar (left) operand.", { DataType::SCALAR } },
                             { "other", "Other (right) operand.", { other_type } },
                             { "result", "Result from the multiplication.", { other_type } }) {
        ASSUMPTION(other_type != DataType::BOOL);
    }

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::ExpandVectorNode : public Node {
    explicit ExpandVectorNode(DataType::ElementType const type, scalar const value = 1.0f)
        : Node("Expand", "Adds one coordinate (initialized to " + std::to_string(value) + ") to the input vector.",
               { { "vector", "Vector to expand.", { type } } },
               { { "vector", "Expanded vector.", { type == DataType::VEC2 ? DataType::VEC3 : DataType::VEC4 } } }),
          m_value{ value } {
        ASSUMPTION(type == DataType::VEC2 || type == DataType::VEC3);
    }

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    scalar m_value;
};

struct ShaderGraph::DecomposeVectorNode : public Node {
    explicit DecomposeVectorNode(DataType::ElementType const type)
        : Node("Decompose", "Decomposes vector components.",
               { { "vector", "Vector to decompose.", { type } } },
               {
                   { "x", "First component.", { DataType::SCALAR } },
                   { "y", "Second component.", { DataType::SCALAR } },
               }), m_type{ type }
    {
        ASSUMPTION(type == DataType::VEC2 || type == DataType::VEC3 || type == DataType::VEC4);

        if (type != DataType::VEC2)
            m_outputs.emplace_back(SlotDefinition{ "z", "Third component.", { DataType::SCALAR } });
        if (type == DataType::VEC4)
            m_outputs.emplace_back(SlotDefinition{ "w", "Fourth component.", { DataType::SCALAR } });
    }

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    DataType::ElementType m_type;
};

struct ShaderGraph::ComposeVectorNode : public Node {
    explicit ComposeVectorNode(DataType::ElementType const type)
        : Node("Compose", "Constructs a vector out of components.",
               {
                   { "x", "First component.", { DataType::SCALAR } },
                   { "y", "Second component.", { DataType::SCALAR } },
               },
               { { "vector", "Composed vector.", { type } } }), m_type{ type }
    {
        ASSUMPTION(type == DataType::VEC2 || type == DataType::VEC3 || type == DataType::VEC4);

        if (type != DataType::VEC2)
            m_inputs.emplace_back(SlotDefinition{ "z", "Third component.", { DataType::SCALAR } });
        if (type == DataType::VEC4)
            m_inputs.emplace_back(SlotDefinition{ "w", "Fourth component.", { DataType::SCALAR } });
    }

    void add_code(StagesCode &code, Stage stage) const override;

private:
    DataType::ElementType m_type;
};

struct ShaderGraph::WorldPositionNode : public InputNode {
    WorldPositionNode()
        : InputNode({ "world position",
                      "The world-space position of the currently processed vertex/fragment.",
                      { DataType::ElementType::VEC3 } }) {}

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::CustomFunctionNode : public Node {
    CustomFunctionNode(const std::vector<SlotDefinition> &inputs, const SlotDefinition &output, std::string code)
        : Node(output.name, output.description, inputs, { output }), m_code{ std::move(code) } {}

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    std::string m_code;
};

struct ShaderGraph::TimeNode : public InputNode {
    explicit TimeNode(float multiplier = 1.f) : InputNode({
        "time",
        "Time in milliseconds since the SDL library initialization.",
        { DataType::SCALAR },
    }), m_multiplier{multiplier} {}

    void add_code(StagesCode &code, Stage stage) const override;

  private:
    float m_multiplier;
};

struct ShaderGraph::PerlinNoise3DNode : public Node {
    PerlinNoise3DNode() : Node(
        "perlin noise 3D",
        "Generates 3D Perlin noise",
        {
            {"uv", "Texture coordinates.", { DataType::VEC2 }},
            {"offset", "Offset applied to texture coordinates.", { DataType::VEC2 }},
            {"scale", "Used to change noise size.", { DataType::SCALAR }},
            {"time", "Adds animation.", { DataType::SCALAR }},
        },
        {
            {"result", "Perlin noise 3D.", { DataType::SCALAR }}
        }
    ) {}

    enum inputs { uv, offset, scale, time };

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::StepNode : public Node {
    StepNode() : Node(
        "step",
        "If x is bigger than edge, output 1, otherwise 0.",
        {
            {"edge", "edge", { DataType::SCALAR }},
            {"x", "x", { DataType::SCALAR }},
        },
        {
            {"step", "result of the step function", { DataType::SCALAR }}
        }
    ) {}

    enum inputs { edge, x };

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::ConditionNode : public Node {
    enum condition { EQ, NE, LE, GE, LT, GT };

    ConditionNode(DataType::ElementType in, DataType::ElementType out, condition c) : Node(
        "condition",
        "Outputs a value based on a condition",
        {
            {"left-compare", "Left operand of the condition", { in }},
            {"right-compare", "Right operand of the condition", { in }},
            {"true", "Value outputted, if condition is true", { out }},
            {"false", "Value outputted, if condition is false", { out }},
        },
        {
            {"output", "Output based on condition.", { out }}
        }), m_condition(c) {}

    enum inputs { left, right, true_res, false_res };

    void add_code(StagesCode &code, Stage stage) const override;

private:
    condition m_condition;
};

struct ShaderGraph::SineNode : public Node {
    SineNode() : Node(
        "sine",
        "Computes the sine of an angle",
        {
            {"angle", "angle", {DataType::SCALAR}}
        },
        {
            {"sine", "Sine of angle", {DataType::SCALAR}}
        }
    ) {}

    void add_code(StagesCode &code, Stage stage) const override;
};

struct ShaderGraph::CosineNode : public Node {
    CosineNode() : Node(
        "cosine",
        "Computes the cosine of an angle",
        {
            {"angle", "angle", {DataType::SCALAR}}
        },
        {
            {"cosine", "Cosine of angle", {DataType::SCALAR}}
        }
    ) {}

    void add_code(StagesCode &code, Stage stage) const override;
};

} // namespace gfx

#endif

