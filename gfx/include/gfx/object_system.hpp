#ifndef GFX_OBJECT_SYSTEM_HPP_INCLUDED
#   define GFX_OBJECT_SYSTEM_HPP_INCLUDED

#   include <com/library.hpp>
#   include <math/math.hpp>
#   include <vector>

namespace com { struct Frame; }

namespace gfx {

struct Material;
struct Buffer;

struct ObjectSystem final : public com::Library
{
    static inline std::string self_name() { return "object_system.lib"; }

    ObjectSystem();
    ~ObjectSystem() override;

    static inline std::string frames_folder_name() { return "frames"; }
    static inline std::string uniforms_folder_name() { return "uniforms"; }
    static inline std::string lights_folder_name() { return "lights"; }
    static inline std::string material_link_name() { return "material.link"; }
    static inline std::string buffer_link_name() { return "buffer.link"; }
    static inline std::string scissor_file_name() { return "scissor"; }

    com::Folder* objects() const { return m_objects; }

    com::Folder* insert_object(com::ContextPath const& path, Material* material, Buffer* buffer);
    com::Folder *insert_object_front(com::ContextPath const &path, Material *material, Buffer *buffer);
    void erase_object(com::Folder* object);
    void erase_object_only(com::Folder *object);

    void push_frame_front(com::Folder* object, com::Frame* frame, std::string const& name = "");
    void push_frame_back(com::Folder* object, com::Frame* frame, std::string const& name = "");
    void insert_frame_before(com::Folder* object, com::Frame* frame, std::string const& pivot_frame_name, std::string const& name = "");
    void insert_frame_after(com::Folder* object, com::Frame* frame, std::string const& pivot_frame_name, std::string const& name = "");
    void erase_frame(com::Folder* object, com::Frame* frame);
    bool has_frame(com::Folder* object, com::Frame* frame) const;

    void insert_light(com::Folder* object, com::Folder* light);
    void erase_light(com::Folder* object, com::Folder* light);
    bool has_light(com::Folder* object, com::Folder* light) const;

    void insert_scissor_file(::com::Folder* object, vec4u scissor) const;

    Material* get_material(com::Folder* const object) const;
    Buffer* get_buffer(com::Folder* const object) const;
    com::Folder* get_frames(com::Folder* const object) const;
    com::Folder* get_uniforms(com::Folder* const object) const;
    com::Folder* get_lights(com::Folder* const object) const;
    com::FileVec4u* get_scissor_file (com::Folder* const object) const;

    void set_material(com::Folder* const object, Material* material) const;

protected:

    void initialize() override;
    void release() override;

private:
    com::Folder* m_objects;
};

}

#endif
