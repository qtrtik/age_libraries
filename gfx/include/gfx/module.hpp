#ifndef GFX_MODULE_HPP_INCLUDED
#   define GFX_MODULE_HPP_INCLUDED

namespace com { struct Folder; }

namespace gfx {

void boot(com::Folder* ctx_root);
void shutdown(com::Folder* ctx_root);

}

#endif
