#ifndef GFX_BUFFER_GENERATORS_HPP_INCLUDED
#   define GFX_BUFFER_GENERATORS_HPP_INCLUDED

#   include <gfx/buffer.hpp>
#   include <com/library.hpp>
#   include <math/math.hpp>
#   include <gfx/font.hpp>

namespace gfx {

struct BufferGenerators final : public com::Library
{
    static inline std::string self_name() { return "buffer_generators.lib"; }

    BufferGenerators();
    ~BufferGenerators() override;

    Buffer* insert_empty();
    Buffer *insert_empty_triangle(std::string const& name, com::ContextPath const& subfolders);
    Buffer* insert_procedural_basis();
    Buffer* insert_procedural_grid();
    Buffer* insert_procedural_rectangle(vec2 const& half_sizes_along_axes, std::string const& name, com::ContextPath const& sub_folders);
    Buffer* insert_procedural_rectangle_variable(vec2 const& half_sizes_along_axes, vec2 const& half_size_along_inverted_axes, std::string const& name, com::ContextPath const& sub_folders);
    Buffer* insert_procedural_rectangle_variable_wireframe(vec2 const& half_sizes_along_axes, vec2 const& half_sizes_along_inverted_axes, std::string const& name, com::ContextPath const& sub_folders);
    Buffer* insert_procedural_rectangle_wireframe(vec2 const& half_sizes_along_axes, std::string const& name, com::ContextPath const& sub_folders);

    Buffer* insert_procedural_box_solid(vec3 const& half_sizes_along_axes, std::string const& name, com::ContextPath const& sub_folders);
    Buffer* insert_procedural_box_wireframe(vec3 const& half_sizes_along_axes, std::string const& name, com::ContextPath const& sub_folders);

    Buffer* insert_procedural_sphere_solid(
        scalar const radius,
        std::uint8_t const num_lines_per_quarter_of_circle,
        std::string const& name,
        com::ContextPath const& sub_folders
        );
    Buffer* insert_procedural_sphere_wireframe(
        scalar const radius,
        std::uint8_t const num_lines_per_quarter_of_circle,
        std::string const& name,
        com::ContextPath const& sub_folders
        );

    Buffer* insert_procedural_diamond_solid(scalar const radius, std::string const& name, com::ContextPath const& sub_folders) {
        return insert_procedural_sphere_solid(radius, 1U, name, sub_folders); }
    Buffer* insert_procedural_diamond_wireframe(scalar const radius, std::string const& name, com::ContextPath const& sub_folders) {
        return insert_procedural_sphere_wireframe(radius, 1U, name, sub_folders); }

    Buffer* insert_procedural_capsule_solid(
        scalar const half_distance_between_end_points,
        scalar const thickness_from_central_line,
        std::uint8_t const num_lines_per_quarter_of_circle,
        std::string const& name,
        com::ContextPath const& sub_folders
        );

    Buffer* insert_procedural_capsule_wireframe(
        scalar const half_distance_between_end_points,
        scalar const thickness_from_central_line,
        std::uint8_t const num_lines_per_quarter_of_circle,
        std::string const& name,
        com::ContextPath const& sub_folders
    );

    Buffer* create_text_buffer(
        const std::string& text,
        FontProps& atlas_props,
        CursorProps& cursor_props,
        size_t max_width,
        const std::string& id,
        vec3 color = {255, 255, 255},
        bool text_wrap = false
    );

    void populate_buffer_vectors(
        const std::string& text,
        FontProps& atlas_props,
        CursorProps& cursor_props,
        Buffer::PositionsVec& positions,
        Buffer::TexcoordsVec& texcoords,
        Buffer::ColorsVec& colors,
        size_t max_width,
        vec3 color = {255, 255, 255},
        bool text_wrap = false
    );

protected:
    void initialize() override;
    void release() override;
};

}

#endif
