#ifndef GFX_MATERIAL_SYSTEM_HPP_INCLUDED
#define GFX_MATERIAL_SYSTEM_HPP_INCLUDED

#include <com/library.hpp>
#include <gfx/material.hpp>
#include <math/math.hpp>

namespace gfx {

struct MaterialSystem final : public com::Library {
    static std::string self_name() { return "material_system.lib"; }

    MaterialSystem();

    com::Folder *materials() { return m_materials; }

    Material *active_material() const { return dynamic_cast<Material *>(m_active_material->target()); }
    com::Link *active_material_link() const { return m_active_material; }
    void set_active_material(Material *material);

    Material *insert_material(std::string const &name, Shader *shader, com::ContextPath const &sub_folders);
    void erase_material(Material *material);

    /**
     * Creates a default material using the default unlit shader with the specified color.
     * @param name the newly created material folder name
     * @param sub_folders path of the folder to which the material will be added
     * @param color determines the ambient color of the material
     * @return default material
     */
    Material *insert_default_material(std::string const &name, com::ContextPath const &sub_folders, vec3 color);

  protected:
    void initialize() override;
    void release() override;

  private:
    com::Folder *m_materials;
    com::Link *m_active_material;
};

} // namespace gfx

#endif
