#ifndef GFX_IMAGE_UTILS_HPP_INCLUDED
#   define GFX_IMAGE_UTILS_HPP_INCLUDED

#   include <cstdint>
#   include <utility>

namespace gfx {


std::uint8_t*  decode_png_image_from_bytes(
        std::uint8_t const* const  bytes,
        std::size_t const  num_bytes,
        int&  out_width,
        int&  out_height,
        int&  out_num_channels
        );


void  free_memory_of_decoded_png_image(std::uint8_t* const  png_bytes);


}

#endif
