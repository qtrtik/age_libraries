# ifndef UI_GENERATORS_INCLUDED
# define UI_GENERATORS_INCLUDED
# include <gfx/buffer.hpp>
# include <com/library.hpp>
# include <math/math.hpp>
# include <gfx/ui/ui_element.hpp>

namespace gfx {

    struct Button;
    struct StaticText;

    struct UiGenerators final : public com::Library {
        static inline std::string self_name() { return "ui_generators.lib"; }

        UiGenerators();
        ~UiGenerators() override;

        Button* create_ui_button(::std::string const& name, vec2i size, UiElement* parent, com::Folder* scene_folder, com::Folder* layer_folder, std::string const& text = "");
        CheckBox* create_ui_check_box(std::string const& name, vec2i size, UiElement* parent, com::Folder* scene_folder, com::Folder* layer_folder);
        Slider* create_ui_slider(std::string const& name, vec2i size, Alignment alignment, UiElement* parent, com::Folder* scene_folder, com::Folder* layer_folder, scalar value = 1.0f);
        ProgressBar* create_ui_progress_bar(std::string const& name, vec2i size, UiElement* parent, com::Folder* scene_folder, com::Folder* layer_folder, scalar value = 1.0f);
        LineTextEdit* create_ui_line_text_edit(std::string const &name, vec2i size, UiElement *parent, com::Folder *scene_folder, com::Folder *layer_folder, std::string text);
        StaticText* create_ui_static_text(std::string const &name, vec2i size, UiElement *parent, com::Folder *scene_folder, com::Folder *layer_folder, std::string text = "", bool text_wrap = false);
        TabBar* create_ui_tab_bar(std::string const &name, vec2i size, UiElement *parent, com::Folder *scene_folder, com::Folder *layer_folder, vec2i tab_size);
        ListBox* create_ui_list_box(std::string const &name, vec2i size, UiElement *parent, com::Folder *scene_folder, com::Folder *layer_folder, vec2i item_size);

    };
};

#endif
