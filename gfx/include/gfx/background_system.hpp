#ifndef GFX_BACKGROUND_SYSTEM_HPP_INCLUDED
#   define GFX_BACKGROUND_SYSTEM_HPP_INCLUDED

#   include <gfx/background.hpp>
#   include <com/library.hpp>
#   include <math/math.hpp>

namespace gfx {

struct BackgroundSystem final : public com::Library
{
    static inline std::string self_name() { return "background_system.lib"; }

    BackgroundSystem();
    ~BackgroundSystem() override;

    Background* background() { return m_background; }

protected:

    void initialize() override;
    void release() override;

private:
    Background* m_background;
};

}

#endif
