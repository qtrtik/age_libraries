# ifndef UI_SYSTEM_INCLUDED
# define UI_SYSTEM_INCLUDED
# include "camera.hpp"
# include "viewport.hpp"
# include "osi/window.hpp"

# include <com/library.hpp>
# include <gfx/shader.hpp>
# include <gfx/buffer.hpp>
# include <com/frame.hpp>
# include <gfx/ui/state.hpp>
# include <gfx/ui/transform.hpp>
# include <gfx/ui/ui_element.hpp>
# include <gfx/events.hpp>

namespace gfx {

    struct UiSystem final : public com::Library {
        static inline std::string self_name() { return "ui_system.lib"; }
        static inline std::string transform_folder_name() { return Transform::self_name(); }
        static inline std::string object_link_file_name() { return "object.link"; }
        static inline std::string object_folder_link_file_name() { return "object_folder.link"; }
        static inline std::string frame_folder_name() { return "frame"; }
        static inline std::string parent_link_file_name() { return "parent.link"; }
        static inline std::string camera_link_file_name() { return "camera.link"; }
        static inline std::string viewport_link_file_name() { return "viewport.link"; }
        static inline std::string state_file_name() { return State::self_name(); }
        static inline std::string default_layer_name() { return "default_layer"; }

        UiSystem(); 
        ~UiSystem();

        com::Folder* uis() const { return m_ui; }

        void set_parent_frame(UiElement* ui_element, com::Frame* parent);
        com::Folder* insert_object(UiElement* ui_element, com::Folder* ui_object, com::Folder* scene_folder,
                                    com::Frame* frame, Material * material, Buffer* buffer, bool front = false);
        void erase_object(com::Folder* scene_folder, com::Folder* ui_object);
        void erase_element(UiElement* ui_element);
        void insert_objects(UiElement* ui_element, bool front = false);
        void insert_foreground_objects(UiElement *ui_element, com::Folder *scene, bool front = false);
        void insert_background_objects(UiElement *ui_element, com::Folder *scene, bool front = false);
        void insert_objects_in_folder(UiElement* ui_element, com::Folder* folder, com::Folder* scene, bool front = false);

        scalar get_z_coord(Camera* camera);
        Transform* get_transform(com::Folder* element) const;
        com::Folder* get_object(com::Folder* element) const;
        com::Link* get_object_link(com::Folder* element) const;
        com::Frame* get_frame(com::Folder* ui_folder) const;
        Buffer* get_buffer(UiElement* element) const;
        com::Link* get_buffer_link(UiElement* element) const;
        State* get_element_state(com::Folder* element) const;

        com::Folder* get_materials_folder(com::Folder* ui_object) const;
        com::Folder* get_buffers_folder(com::Folder* ui_object) const;
        Material* get_enabled_material(com::Folder* ui_object) const;
        Material* get_pressed_material(com::Folder* ui_object) const;
        Material* get_hovered_material(com::Folder* ui_object) const;
        Material* get_texture_material(std::string const& name, com::ContextPath path, bool flip = true);
        Material* create_texture_material(std::string const& name, com::ContextPath path, bool flip = true);
        Buffer* get_hovered_buffer(com::Folder* ui_object) const;
        Buffer* get_enabled_buffer(com::Folder* ui_object) const;
        Buffer* get_pressed_buffer(com::Folder* ui_object) const;

        bool has_object(com::Folder* element) const { return get_object_link(element) != nullptr; }

        void set_element_state(com::Folder* element_folder, UI_STATE state);

        // Scene management functions
        com::Folder* create_scene(std::string const& name);
        com::Folder* get_scene(std::string const& name);
        com::Folder* get_scene_object_folder(com::Folder* scene) { return scene->find<com::Link>(object_folder_link_file_name())->target<com::Folder>(); }
        void set_active_scene(std::string const& name);
        com::Folder* get_active_scene() { return m_active_scene; }
        Camera* get_scene_camera(com::Folder* scene);
        void assign_camera_to_scene(com::Folder* scene, Camera* camera, bool is_3d);
        void assign_object_to_scene(com::Folder *scene, com::Frame *object_frame);
        com::Folder* get_scene_frame_folder(com::Folder* scene);
        com::Frame* get_scene_frame(com::Folder* scene);
        Viewport* get_scene_viewport(com::Folder* scene);
        void set_scene_viewport(com::Folder* scene, Viewport* viewport);
        com::Frame* get_scene_parent_frame(com::Folder* scene);
        com::ContextPath get_scene_object_path(com::Folder *scene_folder, com::Folder *ui_object);

        com::Folder* get_default_scene_layer(com::Folder* scene_folder) { return get_scene_layer(scene_folder, default_layer_name()); }
        com::Folder* get_scene_layer(com::Folder* scene_folder, std::string const& name) { return scene_folder->find<com::Folder>(name); }
        com::Folder* get_element_scene_layer(com::Folder* scene_folder, UiElement* element);
        com::Folder* insert_scene_layer_top(com::Folder* scene_folder, std::string const& name);
        com::Folder* insert_scene_layer_bottom(com::Folder* scene_folder, std::string const& name);
        com::Folder* insert_scene_layer_above(com::Folder* scene_folder, com::Folder* after_layer_folder, std::string const& name);
        com::Folder* insert_scene_layer_under(com::Folder* scene_folder, com::Folder* before_layer_folder, std::string const& name);

        com::Frame* get_default_camera_frame() { return m_default_camera_frame; }
        void present_scene(com::Folder* scene_folder, bool scissor, scalar line_width = 1.0f);
        void present_active_scene(bool scissor, scalar line_width = 1.0f);

        //Helper functions
        vec2 get_size_in_meters(vec2i size_in_pixels);
        vec2i get_size_in_pixels(vec2 size_in_meters);

        void remove_handlers(com::Folder* sender_element);

        void handle_mouse_move(vec2i position);
        void handle_mouse_press(vec2i position, MouseEvent mouse_event);
        void handle_mouse_release(vec2i position, MouseEvent mouse_event);
        void handle_mouse_input();

        void on_change(com::Folder* scene_folder);
        void on_change_rec(com::Folder* folder);
        void on_mouse_pressed(vec2 position);
        void on_mouse_moved(vec2 position);
        void on_mouse_released(vec2 position);
        void on_mouse_wheel_scrolled();
        void on_key_pressed(const std::string& key);
        void on_text_entered(const std::string &text);
        void on_scene_changed();
        void recalculate_element(UiElement* element);
        template <class UiType, class ... ParameterTypes>
        UiType *insert_element(std::string const &name, com::Folder *scene_folder, com::Folder* layer_folder, ParameterTypes... args);
        template <class UiType, class ... ParameterTypes>
        UiType *insert_element(std::string const &name, com::Folder *insert_folder, com::Folder *scene_folder, com::Folder* layer_folder,
                               ParameterTypes... args);
        template <class UiType, class ... ParameterTypes>
        UiType *insert_child_element(std::string const &name, UiElement *element, com::Folder *scene_folder, com::Folder* layer_folder,
                                   ParameterTypes... args);

        void insert_default_materials();

        Material* get_default_enabled_material() { return m_default_enabled_material; }
        Material* get_default_hovered_material() { return m_default_hovered_material; }
        Material* get_default_pressed_material() { return m_default_pressed_material; }
        Material* get_default_rect_material() { return m_default_rect_material; }

        AABB calculate_screen_space_aabb(com::Folder* element, com::Folder* scene_folder, AABB ws_aabb);
        vec2 calculate_world_space_coords(com::Folder* element_folder, vec2 screen_coords_ndc, com::Folder* scene_folder);

    protected:
        void on_content_changed(File *sender) override;

    private:
        void calculate_screen_space_aabb(com::Folder* element, com::Folder* scene_folder);
        void handle_mouse_input_rec(com::Folder *folder, vec2 mouse_position);
        void handle_mouse_move_rec(com::Folder* folder, vec2 position);

        protected:
            void initialize() override;
            void release() override;
        private:
            com::Folder* m_ui;
            UiElement* m_hovered_element;
            UiElement* m_captured_element;
            UiElement*  m_focused_element;
            com::Folder* m_active_scene;
            com::Folder* m_ui_object_folder;

            // Materials
            Material* m_default_enabled_material;
            Material* m_default_hovered_material;
            Material* m_default_pressed_material;
            Material* m_default_rect_material;

            // Default camera frame
            com::Frame* m_default_camera_frame;

            std::unordered_set<com::Frame*>  m_scene_frames;
    };
    template<typename UiType, class... ParameterTypes>
    UiType* UiSystem::insert_element(std::string const& name, com::Folder* scene_folder, com::Folder* layer_folder, ParameterTypes... args) {
        return insert_element<UiType>(name, layer_folder ? layer_folder : scene_folder, scene_folder, layer_folder, args...);
    }

    template<typename UiType, class... ParameterTypes>
    UiType* UiSystem::insert_element(std::string const& name, com::Folder* insert_folder, com::Folder* scene_folder, com::Folder* layer_folder, ParameterTypes... args) {
        static_assert(std::is_base_of<UiElement, UiType>::value, "UiType must be derived from UiElement");
        ASSUMPTION(scene_folder->is_under(uis()));
        if (layer_folder == nullptr) {
            ASSUMPTION(insert_folder == scene_folder || insert_folder->is_under(scene_folder));
        } else {
            ASSUMPTION(layer_folder->is_under(scene_folder));
            ASSUMPTION(insert_folder == layer_folder || insert_folder->is_under(layer_folder));
        }
        UiType * item = (insert_folder->find<UiType>(name));
        auto* scene_frame = get_scene_frame_folder(scene_folder)->find<com::Frame>(UiElement::frame_file_name());
        ASSUMPTION(scene_frame != nullptr);
        if (item == nullptr) {
            item = insert_folder->push_back<UiType>(name, scene_folder, args...);
        }
        item->get_frame_parent_link()->set_target(scene_frame);
        return item;
    }

    template<typename UiType, class... ParameterTypes>
    UiType* UiSystem::insert_child_element(std::string const& name, UiElement* element, com::Folder* scene_folder, com::Folder* layer_folder, ParameterTypes... args) {
        static_assert(std::is_base_of<UiElement, UiType>::value, "UiT-ype must be derived from UiElement");
        ASSUMPTION(layer_folder->is_under(scene_folder));
        UiType* sub_element = insert_element<UiType>(name, element->get_foreground_folder(), scene_folder, layer_folder, args...);
        sub_element->get_frame_parent_link()->set_target(element->get_frame());
        return sub_element;
    }

}

#endif
